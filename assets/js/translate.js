var sourceId='';
var targetId='';
var g_token = '';
onLoad();
function onLoad() {
  // Get an access token now.  Good for 10 minutes.
     getToken();
  // Get a new one every 9 minutes.
  setInterval(getToken, 9 * 60 * 1000);
}
function translateSourceTarget(source,target) {
  sourceId="#"+source;
  targetId="#"+target;
  var src = $(sourceId).val();
  translate(src, "en", "zh-CHS");
}

function getToken() {
  // url for get token
  var requestStr = "../Order/GetTransilation";
  $.ajax({
    url: requestStr,
    type: "GET",
    cache: true,
    dataType: 'json',
    async:true,
    success: function (data) {
      token=data.token;
      g_token = token.access_token;
    }
  });
}
function translate(text, from, to) {
  var transdata = new Object;
  transdata.text = text;
  transdata.from = from;
  transdata.to = to;
  // A major puzzle solved.  Who would have guessed you specify the jsonp callback in oncomplete?
  transdata.oncomplete = 'ajaxTranslateCallback';
 // Another major puzzle.  The authentication is supplied in the deprecated appId param.
  transdata.appId = "Bearer " + g_token;
  var requestStr = "http://api.microsofttranslator.com/V2/Ajax.svc/Translate";
  $.ajax({
    url: requestStr,
    type: "GET",
    data: transdata,
    dataType: 'jsonp',
    cache: true
  });
}
function ajaxTranslateCallback(response) {
  // Display translated text in the right textarea.
  $(targetId).val(response);
}
