-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 182.50.133.90
-- Generation Time: Nov 11, 2016 at 01:40 PM
-- Server version: 5.5.43-37.2-log
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mixmycontainer`
--

-- --------------------------------------------------------

--
-- Table structure for table `tble_account`
--

CREATE TABLE `tble_account` (
  `EmailId` varchar(250) NOT NULL,
  `EmailSettings` int(11) NOT NULL,
  `Language` varchar(150) NOT NULL,
  `Locale` varchar(250) NOT NULL,
  `Image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_account`
--

INSERT INTO `tble_account` (`EmailId`, `EmailSettings`, `Language`, `Locale`, `Image`) VALUES
('anoopmannazhi36@gmail.com', 0, 'Chinese', 'AE', '1478776182.jpg'),
('anooppm@minusbugs.com', 1, 'English', 'AI', '1478592599.jpg'),
('rk@zainmax.net', 0, 'English', 'AM', '1478782799.png');

-- --------------------------------------------------------

--
-- Table structure for table `tble_booth`
--

CREATE TABLE `tble_booth` (
  `BoothId` int(11) NOT NULL,
  `OrderId` int(30) NOT NULL,
  `CompanyId` int(30) NOT NULL,
  `BoothNumber` int(30) NOT NULL,
  `BoothName` varchar(500) NOT NULL,
  `Phone` int(30) NOT NULL,
  `BusinessScope` varchar(500) NOT NULL,
  `ValuePurchase` int(30) NOT NULL,
  `Image` varchar(100) NOT NULL,
  `BoothStatus` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tble_companysetup`
--

CREATE TABLE `tble_companysetup` (
  `CompanyID` int(11) NOT NULL,
  `CompanyName` varchar(100) NOT NULL,
  `Country` varchar(100) NOT NULL,
  `AddressType` varchar(50) NOT NULL,
  `Address` varchar(500) NOT NULL,
  `ZipCode` int(25) NOT NULL,
  `Phone` varchar(225) NOT NULL,
  `Mobile` varchar(225) NOT NULL,
  `Fax` varchar(50) NOT NULL,
  `Skype` varchar(50) NOT NULL,
  `MsnID` varchar(50) NOT NULL,
  `QqID` varchar(50) NOT NULL,
  `OrderEmail` varchar(50) NOT NULL,
  `AgentEmail` varchar(50) NOT NULL,
  `ExchangeRate` int(30) NOT NULL,
  `Active3rdCurrency` varchar(30) NOT NULL,
  `ThirdCurrency` varchar(30) NOT NULL,
  `ExchangeRate3rdCurrency` varchar(50) DEFAULT NULL,
  `TransilateFrom` varchar(50) NOT NULL,
  `TransilateTo` varchar(50) NOT NULL,
  `GrossweightCarton` int(30) NOT NULL,
  `Width` int(100) NOT NULL,
  `Height` int(100) NOT NULL,
  `Length` int(100) NOT NULL,
  `Volume` int(100) NOT NULL,
  `AgentContact` int(225) NOT NULL,
  `userToken` varchar(100) NOT NULL,
  `UserMail` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_companysetup`
--

INSERT INTO `tble_companysetup` (`CompanyID`, `CompanyName`, `Country`, `AddressType`, `Address`, `ZipCode`, `Phone`, `Mobile`, `Fax`, `Skype`, `MsnID`, `QqID`, `OrderEmail`, `AgentEmail`, `ExchangeRate`, `Active3rdCurrency`, `ThirdCurrency`, `ExchangeRate3rdCurrency`, `TransilateFrom`, `TransilateTo`, `GrossweightCarton`, `Width`, `Height`, `Length`, `Volume`, `AgentContact`, `userToken`, `UserMail`) VALUES
(1, 'aaaaa', 'sss', 'ddd', 'ffff', 111, '111', '22222', '565656', '556cgvgf', 'nbgnf454', 'gmb4522', 'manjumathew458@gmail.com', 'manjumathew458@gmail.com', 26, '266', '326', '426', 'english', 'malayalam', 1000, 50, 60, 70, 3000, 2147483647, 'k8gX3Wo211dnPcEQ44aagO8', ''),
(3, 'ghfgh', 'Indonesia', 'postal address', 'sdfsdf', 54564, '56465465465', '7789562312', '555', 'kjasgd', '534', 'asd', 'anooppm@minusbugs.com', '', 55, '1', 'United States dollar(USD)', '55', 'HI', 'AK', 555, 55, 41, 22, 0, 0, 'PkngcO2483ao8gWQE114Xda', ''),
(4, 'Mycompany1', 'Afghanistan', 'postal address', 'fghfgh', 464655, '546546546', '7736572251', '654', 'asf', 'asd', 'anoop', 'anoopmannazhi36gmail.com', '', 24, '0', '? string: ?', '0', 'English', 'Chineese', 119, 114, 120, 112, 2, 0, '23oPW8QX1O41Enkg4aadg8c', 'anoopmannazhi36@gmail.com'),
(5, 'rk', 'India', 'postal address', 'test', 682214, '65464546466', '7736572251', '64654', 'sldkfj', 'LSKD', 'RK@RK.COM', 'Rk@rk.com', '', 60, '0', '? string: ?', '', 'Chineese', 'English', 119, 114, 112, 113, 1, 0, '', 'rk@zainmax.net');

-- --------------------------------------------------------

--
-- Table structure for table `tble_container`
--

CREATE TABLE `tble_container` (
  `ContainerId` int(11) NOT NULL,
  `ContainerType` varchar(30) NOT NULL,
  `OrderId` int(30) NOT NULL,
  `CompanyId` int(30) NOT NULL,
  `FilledWeight` float NOT NULL,
  `FilledVolume` float NOT NULL,
  `Status` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tble_containertypes`
--

CREATE TABLE `tble_containertypes` (
  `ID` int(11) NOT NULL,
  `Container` varchar(200) NOT NULL,
  `MaximumVolume` bigint(100) NOT NULL,
  `MaximumWeight` bigint(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_containertypes`
--

INSERT INTO `tble_containertypes` (`ID`, `Container`, `MaximumVolume`, `MaximumWeight`) VALUES
(1, '20'' FCL (32 CBM)', 32, 24850),
(2, '40'' FCL (66 CBM)', 66, 28800),
(3, '40'' HQ FCL (75 CBM)', 75, 29600);

-- --------------------------------------------------------

--
-- Table structure for table `tble_countrynames`
--

CREATE TABLE `tble_countrynames` (
  `ID` int(11) NOT NULL,
  `CountryName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_countrynames`
--

INSERT INTO `tble_countrynames` (`ID`, `CountryName`) VALUES
(1, 'Afghanistan'),
(2, 'Albania'),
(3, 'Algeria'),
(4, 'American Samoa'),
(5, 'Angola'),
(6, 'Anguilla'),
(7, 'Antartica'),
(8, 'Antigua and Barbuda'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Aruba'),
(12, 'Ashmore and Cartier Island'),
(13, 'Australia'),
(14, 'Austria'),
(15, 'Azerbaijan'),
(16, 'Bahamas'),
(17, 'Bahrain'),
(18, 'Bangladesh'),
(19, 'Barbados'),
(20, 'Belarus'),
(21, 'Belgium'),
(22, 'Belize'),
(23, 'Benin'),
(24, 'Bermuda'),
(25, 'Bhutan'),
(26, 'Bolivia'),
(27, 'Bosnia and Herzegovina'),
(28, 'Botswana'),
(29, 'Brazil'),
(30, 'British Virgin Islands'),
(31, 'Brunei'),
(32, 'Bulgaria'),
(33, 'Burkina Faso'),
(34, 'Burma'),
(35, 'Burundi'),
(36, 'Cambodia'),
(37, 'Cameroon'),
(38, 'Canada'),
(39, 'Cape Verde'),
(40, 'Cayman Islands'),
(41, 'Central African Republic'),
(42, 'Chad'),
(43, 'Chile'),
(44, 'China'),
(45, 'Christmas Island'),
(46, 'Clipperton Island'),
(47, 'Cocos (Keeling) Islands'),
(48, 'Colombia'),
(49, 'Comoros'),
(50, 'Congo, Democratic Republic of the'),
(51, 'Congo, Republic of the'),
(52, 'Cook Islands'),
(53, 'Costa Rica'),
(54, 'Cote d''Ivoire'),
(55, 'Croatia'),
(56, 'Cuba'),
(57, 'Cyprus'),
(58, 'Czeck Republic'),
(59, 'Denmark'),
(60, 'Djibouti'),
(61, 'Dominica'),
(62, 'Dominican Republic'),
(63, 'Ecuador'),
(64, 'Egypt'),
(65, 'El Salvador'),
(66, 'Equatorial Guinea'),
(67, 'Eritrea'),
(68, 'Estonia'),
(69, 'Ethiopia'),
(70, 'Europa Island'),
(71, 'Falkland Islands (Islas Malvinas)'),
(72, 'Faroe Islands'),
(73, 'Fiji'),
(74, 'Finland'),
(75, 'France'),
(76, 'French Guiana'),
(77, 'French Polynesia'),
(78, 'French Southern and Antarctic Lands'),
(79, 'Gabon'),
(80, 'Gambia, The'),
(81, 'Gaza Strip'),
(82, 'Georgia'),
(83, 'Germany'),
(84, 'Ghana'),
(85, 'Gibraltar'),
(86, 'Glorioso Islands'),
(87, 'Greece'),
(88, 'Greenland'),
(89, 'Grenada'),
(90, 'Guadeloupe'),
(91, 'Guam'),
(92, 'Guatemala'),
(93, 'Guernsey'),
(94, 'Guinea'),
(95, 'Guinea-Bissau'),
(96, 'Guyana'),
(97, 'Haiti'),
(98, 'Heard Island and McDonald Islands'),
(99, 'Holy See (Vatican City)'),
(100, 'Honduras'),
(101, 'Hong Kong'),
(102, 'Howland Island'),
(103, 'Hungary'),
(104, 'Iceland'),
(105, 'India'),
(106, 'Indonesia'),
(107, 'Iran'),
(108, 'Iraq'),
(109, 'Ireland'),
(110, 'Ireland, Northern'),
(111, 'Israel'),
(112, 'Italy'),
(113, 'Jamaica'),
(114, 'Jan Mayen'),
(115, 'Japan'),
(116, 'Jarvis Island'),
(117, 'Jersey'),
(118, 'Johnston Atoll'),
(119, 'Jordan'),
(120, 'Juan de Nova Island'),
(121, 'Kazakhstan'),
(122, 'Kenya'),
(123, 'Kiribati'),
(124, 'Korea, North'),
(125, 'Korea, South'),
(126, 'Kuwait'),
(127, 'Kyrgyzstan'),
(128, 'Laos'),
(129, 'Latvia'),
(130, 'Lebanon'),
(131, 'Lesotho'),
(132, 'Liberia'),
(133, 'Libya'),
(134, 'Liechtenstein'),
(135, 'Lithuania'),
(136, 'Luxembourg'),
(137, 'Macau'),
(138, 'Macedonia, Former Yugoslav Republic of'),
(139, 'Madagascar'),
(140, 'Malawi'),
(141, 'Malaysia'),
(142, 'Maldives'),
(143, 'Mali'),
(144, 'Malta'),
(145, 'Man, Isle of'),
(146, 'Marshall Islands'),
(147, 'Martinique'),
(148, 'Mauritania'),
(149, 'Mauritius'),
(150, 'Mayotte'),
(151, 'Mexico'),
(152, 'Micronesia, Federated States of'),
(153, 'Midway Islands'),
(154, 'Moldova'),
(155, 'Monaco'),
(156, 'Mongolia'),
(157, 'Montserrat'),
(158, 'Morocco'),
(159, 'Mozambique'),
(160, 'Namibia'),
(161, 'Nauru'),
(162, 'Nepal'),
(163, 'Netherlands'),
(164, 'Netherlands Antilles'),
(165, 'New Caledonia'),
(166, 'New Zealand'),
(167, 'Nicaragua'),
(168, 'Niger'),
(169, 'Nigeria'),
(170, 'Niue'),
(171, 'Norfolk Island'),
(172, 'Northern Mariana Islands'),
(173, 'Norway'),
(174, 'Oman'),
(175, 'Pakistan'),
(176, 'Palau'),
(177, 'Panama'),
(178, 'Papua New Guinea'),
(179, 'Paraguay'),
(180, 'Peru'),
(181, 'Philippines'),
(182, 'Pitcaim Islands'),
(183, 'Poland'),
(184, 'Portugal'),
(185, 'Puerto Rico'),
(186, 'Qatar'),
(187, 'Reunion'),
(188, 'Romainia'),
(189, 'Russia'),
(190, 'Rwanda'),
(191, 'Saint Helena'),
(192, 'Saint Kitts and Nevis'),
(193, 'Saint Lucia'),
(194, 'Saint Pierre and Miquelon'),
(195, 'Saint Vincent and the Grenadines'),
(196, 'Samoa'),
(197, 'San Marino'),
(198, 'Sao Tome and Principe'),
(199, 'Saudi Arabia'),
(200, 'Scotland'),
(201, 'Senegal');

-- --------------------------------------------------------

--
-- Table structure for table `tble_currencyname`
--

CREATE TABLE `tble_currencyname` (
  `ID` int(11) NOT NULL,
  `CurrencyName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_currencyname`
--

INSERT INTO `tble_currencyname` (`ID`, `CurrencyName`) VALUES
(1, 'Afghan afghani(AFN)'),
(2, 'European euro(EUR)'),
(3, 'Albanian lek(ALL)'),
(4, 'Algerian dinar(DZD)'),
(5, 'United States dollar(USD)'),
(6, 'Angolan kwanza(AOA)'),
(7, 'East Caribbean dollar(XCD)'),
(8, 'Argentine peso(ARS)'),
(9, 'Armenian dram(AMD)'),
(10, 'Aruban florin(AWG)'),
(11, 'Saint Helena pound(SHP)'),
(12, 'Australian dollar(AUD)'),
(13, 'Azerbaijani manat(AZN)'),
(14, 'Bahamian dollar(BSD)'),
(15, 'Bahraini dinar(BHD)'),
(16, 'Bangladeshi taka(BDT)'),
(17, 'Barbadian dollar(BBD)'),
(18, 'Belarusian ruble(BYN)'),
(19, 'Belize dollar(BZD)'),
(20, 'West African CFA franc(XOF)'),
(21, 'Bermudian dollar(BMD)'),
(22, 'Bhutanese ngultrum(BTN)'),
(23, 'Bolivian boliviano(BOB)'),
(24, 'Bosnia and Herzegovina convertible mark(BAM)'),
(25, 'Botswana pula(BWP)'),
(26, 'Brazilian real(BRL)'),
(27, 'Brunei dollar(BND)'),
(28, 'Bulgarian lev(BGN)'),
(29, 'Burundi franc(BIF)'),
(30, 'Cape Verdean escudo(CVE)'),
(31, 'Cambodian riel(KHR)'),
(32, 'Central African CFA franc(XAF)'),
(33, 'Canadian dollar(CAD)'),
(34, 'Cayman Islands dollar(KYD)'),
(35, 'Chilean peso(CLP)'),
(36, 'Chinese Yuan Renminbi(CNY)'),
(37, 'Colombian peso(COP)'),
(38, 'Comorian franc(KMF)'),
(39, 'Congolese franc(CDF)'),
(40, 'Cook Islands dollar(none)'),
(41, 'Costa Rican colon(CRC)'),
(42, 'Croatian kuna(HRK)'),
(43, 'Cuban peso(CUP)'),
(44, 'Netherlands Antillean guilder(ANG)'),
(45, 'Czech koruna(CZK)'),
(46, 'Danish krone(DKK)'),
(47, 'Djiboutian franc(DJF)'),
(48, 'Dominican peso(DOP)'),
(49, 'Egyptian pound(EGP)'),
(50, 'Eritrean nakfa(ERN)'),
(51, 'Ethiopian birr(ETB)'),
(52, 'Falkland Islands pound(FKP)'),
(53, 'Faroese krona(none)'),
(54, 'Fijian dollar(FJD)'),
(55, 'CFP franc(XPF)'),
(56, 'Gambian dalasi(GMD)'),
(57, 'Georgian lari(GEL)'),
(58, 'Ghanaian cedi(GHS)'),
(59, 'Gibraltar pound(GIP)'),
(60, 'Guatemalan quetzal(GTQ)'),
(61, 'Guernsey Pound(GGP)'),
(62, 'Guinean franc(GNF)'),
(63, 'Guyanese dollar(GYD)'),
(64, 'Haitian gourde(HTG)'),
(65, 'Honduran lempira(HNL)'),
(66, 'Hong Kong dollar(HKD)'),
(67, 'Hungarian forint(HUF)'),
(68, 'Icelandic krona(ISK)'),
(69, 'Indian rupee(INR)'),
(70, 'Indonesian rupiah(IDR)'),
(71, 'Israeli new shekel(ILS)'),
(72, 'SDR (Special Drawing Right)(XDR)'),
(73, 'Iranian rial(IRR)'),
(74, 'Iraqi dinar(IQD)'),
(75, 'Manx pound(IMP)'),
(76, 'Israeli new shekel(ILS)'),
(77, 'Jamaican dollar(JMD)'),
(78, 'Japanese yen(JPY)'),
(79, 'Jersey pound(JEP)'),
(80, 'Jordanian dinar(JOD)'),
(81, 'Kazakhstani tenge(KZT)'),
(82, 'Kenyan shilling(KES)'),
(83, 'Kuwaiti dinar(KWD)'),
(84, 'Kyrgyzstani som(KGS)'),
(85, 'Lao kip(LAK)'),
(86, 'Lebanese pound(LBP)'),
(87, 'Lesotho loti(LSL)'),
(88, 'Liberian dollar(LRD)'),
(89, 'Libyan dinar(LYD)'),
(90, 'Swiss franc(CHF)'),
(91, 'Macanese pataca(MOP)'),
(92, 'Macedonian denar(MKD)'),
(93, 'Malagasy ariary(MGA)'),
(94, 'Malawian kwacha(MWK)'),
(95, 'Malaysian ringgit(MYR)'),
(96, 'Maldivian rufiyaa(MVR)'),
(97, 'Mauritanian ouguiya(MRO)'),
(98, 'Mauritian rupee(MUR)'),
(99, 'Manx pound(IMP)'),
(100, 'Mexican peso(MXN)'),
(101, 'Moldovan leu(MDL)'),
(102, 'Mongolian tugrik(MNT)'),
(103, 'Moroccan dirham(MAD)'),
(104, 'Mozambican metical(MZN)'),
(105, 'Myanmar kyat(MMK)'),
(106, 'Namibian dollar(NAD)'),
(107, 'Nepalese rupee(NPR)'),
(108, 'New Zealand dollar(NZD)'),
(109, 'Nicaraguan cordoba(NIO)'),
(110, 'Nigerian naira(NGN)'),
(111, 'North Korean won(KPW)'),
(112, 'Norwegian krone(NOK)'),
(113, 'Omani rial(OMR)'),
(114, 'Pakistani rupee(PKR)'),
(115, 'Papua New Guinean kina(PGK)'),
(116, 'Paraguayan guarani(PYG)'),
(117, 'Peruvian sol(PEN)'),
(118, 'Philippine peso(PHP)'),
(119, 'Polish zloty(PLN)'),
(120, 'Qatari riyal(QAR)'),
(121, 'Romanian leu(RON)'),
(122, 'Russian ruble(RUB)'),
(123, 'Rwandan franc(RWF)'),
(124, 'Samoan tala(WST)'),
(125, 'Sao Tome and Principe dobra(STD)'),
(126, 'Saudi Arabian riyal(SAR)'),
(127, 'Serbian dinar(RSD)'),
(128, 'Seychellois rupee(SCR)'),
(129, 'Sierra Leonean leone(SLL)'),
(130, 'Singapore dollar(SGD)'),
(131, 'Solomon Islands dollar(SBD)'),
(132, 'Somali shilling(SOS)'),
(133, 'South African rand(ZAR)'),
(134, 'Pound sterling(GBP)'),
(135, 'South Korean won(KRW)'),
(136, 'South Sudanese pound(SSP)'),
(137, 'Sri Lankan rupee(LKR)'),
(138, 'Surinamese dollar(SRD)'),
(139, 'Swazi lilangeni(SZL)'),
(140, 'Swedish krona(SEK)'),
(141, 'Syrian pound(SYP)'),
(142, 'New Taiwan dollar(TWD)'),
(143, 'Tajikistani somoni(TJS)'),
(144, 'Tanzanian shilling(TZS)'),
(145, 'Thai baht(THB)'),
(146, 'Tongan pa’anga(TOP)'),
(147, 'Trinidad and Tobago dollar(TTD)'),
(148, 'Tunisian dinar(TND)'),
(149, 'Turkish lira(TRY)'),
(150, '\r\nTurkmen manat(TMT)'),
(151, 'Ugandan shilling(UGX)'),
(152, 'Ukrainian hryvnia(UAH)'),
(153, 'UAE dirham(AED)'),
(154, 'Uruguayan peso(UYU)'),
(155, 'Uzbekistani som(UZS)'),
(156, 'Vanuatu vatu(VUV)'),
(157, 'Venezuelan bolivar(VEF)'),
(158, 'Vietnamese dong(VND)'),
(159, 'Yemeni rial(YER)'),
(160, 'Zambian kwacha(ZMW)');

-- --------------------------------------------------------

--
-- Table structure for table `tble_login`
--

CREATE TABLE `tble_login` (
  `UserID` int(11) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `UserType` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_login`
--

INSERT INTO `tble_login` (`UserID`, `UserName`, `Password`, `Email`, `UserType`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 'manjumathew458@gmail.com', 'admin'),
(4, 'anoop', '9d9361be5815b09805f4d212c855bf8f', 'anooppm@minusbugs.com', 'normal'),
(5, 'rinshad', 'd717eb833c99a9f94b4e229cc049d911', 'green@stromox.com', 'normal'),
(6, 'rk', '4b7e615b13ffb8056d3ec36b3b98981d', 'rk@stromox.com', 'normal'),
(7, 'rin', '5f16e21ab6033f591d12188c76e52c27', 'rk@maileme101.com', 'normal'),
(9, 'anoop', 'd6615aeac1752bfc4ae2a5e07287f9f1', 'anoopmannazhi36@gmail.com', 'normal'),
(10, 'rk', 'ad6fb0338e112e31edcf8c7c437cb524', 'rk@zainmax.net', 'normal'),
(11, 'manju', '1e36275ed2beca944cbfd5470be46c48', 'manju@9me.site', 'normal'),
(12, 'neethu', '727a3f8048200a115ff449afec69d2c8', 'neethu@9me.site', 'normal'),
(13, 'anoop', '0afa9562b5880b51de15783ce03f64cb', 'anu@maileme101.com', 'normal'),
(14, 'anooppm', 'afa43d3e93c6efff400d1377d8a3ac70', 'anu@9me.site', 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `tble_orderitems`
--

CREATE TABLE `tble_orderitems` (
  `ItemId` int(11) NOT NULL,
  `CompanyId` int(11) NOT NULL,
  `OrderId` int(30) NOT NULL,
  `BoothId` int(30) NOT NULL,
  `ContainerId` int(30) NOT NULL,
  `ItemNo` varchar(50) NOT NULL,
  `DescriptionFrom` varchar(5000) NOT NULL,
  `DescriptionTo` varchar(5000) NOT NULL,
  `UnitperCarton` int(30) NOT NULL,
  `WeightofCarton` float NOT NULL,
  `Length` float NOT NULL,
  `Width` float NOT NULL,
  `Height` float NOT NULL,
  `CBM` float NOT NULL,
  `Picture` varchar(50) NOT NULL,
  `PriceInUs` varchar(30) NOT NULL,
  `PriceInRmb` double NOT NULL,
  `3rdCurrencyType` double NOT NULL,
  `PriceIn3rdCurrency` double NOT NULL,
  `UnitQuantity` int(30) NOT NULL,
  `CartonQuantity` int(11) NOT NULL,
  `TotalPriceInUS` double NOT NULL,
  `TotalPriceInRMB` double NOT NULL,
  `TotalPriceIn3rdCurrency` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_orderitems`
--

INSERT INTO `tble_orderitems` (`ItemId`, `CompanyId`, `OrderId`, `BoothId`, `ContainerId`, `ItemNo`, `DescriptionFrom`, `DescriptionTo`, `UnitperCarton`, `WeightofCarton`, `Length`, `Width`, `Height`, `CBM`, `Picture`, `PriceInUs`, `PriceInRmb`, `3rdCurrencyType`, `PriceIn3rdCurrency`, `UnitQuantity`, `CartonQuantity`, `TotalPriceInUS`, `TotalPriceInRMB`, `TotalPriceIn3rdCurrency`) VALUES
(18, 1, 32, 14, 32, 'doll', 'description english', 'descriotion spanish', 10, 100, 10, 20, 30, 0.06, 'http/qikdots.com/MMC/uploads/doll image', '100', 200, 0, 300, 100, 10, 0, 0, 0),
(20, 1, 32, 14, 32, 'grinder', 'description english', 'descriotion spanish', 12, 120, 20, 40, 60, 2, 'http/qikdots.com/MMC/uploads/grinder image', '200', 677, 0, 8770, 500, 42, 0, 0, 0),
(21, 1, 32, 14, 32, 'kettle', 'description english', 'descriotion spanish', 6, 500, 30, 50, 70, 1.75, 'http/qikdots.com/MMC/uploads/kettle image', '', 0, 0, 0, 100, 17, 0, 0, 0),
(22, 1, 33, 2, 48, 'doll', 'description english', 'descriotion spanish', 0, 0, 0, 0, 0, 0, 'http/qikdots.com/MMC/uploads/doll image', '100', 200, 3, 300, 0, 0, 400, 800, 1200),
(23, 1, 35, 15, 52, '44', 'description english', 'descriotion spanish', 0, 0, 0, 0, 0, 0, 'http/qikdots.com/MMC/uploads/shoe image', '', 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tble_orders`
--

CREATE TABLE `tble_orders` (
  `OrderId` int(11) NOT NULL,
  `OrderNumber` int(30) NOT NULL,
  `CompanyId` int(30) NOT NULL,
  `Trade` varchar(500) NOT NULL,
  `ProductType` varchar(500) NOT NULL,
  `TotalOrderUsValue` int(30) NOT NULL,
  `TotalOrderRmbValue` float NOT NULL,
  `Total3rdCurrencyValue` float NOT NULL,
  `TotalCartons` int(25) NOT NULL,
  `TotalBooth` int(20) NOT NULL,
  `TotalContainer` int(30) NOT NULL,
  `TotalOrderWeight` int(30) NOT NULL,
  `TotalOrderCBM` int(30) NOT NULL,
  `OrderStatus` int(30) NOT NULL,
  `LastUpdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tble_orders`
--

INSERT INTO `tble_orders` (`OrderId`, `OrderNumber`, `CompanyId`, `Trade`, `ProductType`, `TotalOrderUsValue`, `TotalOrderRmbValue`, `Total3rdCurrencyValue`, `TotalCartons`, `TotalBooth`, `TotalContainer`, `TotalOrderWeight`, `TotalOrderCBM`, `OrderStatus`, `LastUpdate`) VALUES
(36, 784, 0, '1', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tble_account`
--
ALTER TABLE `tble_account`
  ADD PRIMARY KEY (`EmailId`);

--
-- Indexes for table `tble_booth`
--
ALTER TABLE `tble_booth`
  ADD PRIMARY KEY (`BoothId`);

--
-- Indexes for table `tble_companysetup`
--
ALTER TABLE `tble_companysetup`
  ADD PRIMARY KEY (`CompanyID`);

--
-- Indexes for table `tble_container`
--
ALTER TABLE `tble_container`
  ADD PRIMARY KEY (`ContainerId`);

--
-- Indexes for table `tble_containertypes`
--
ALTER TABLE `tble_containertypes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tble_countrynames`
--
ALTER TABLE `tble_countrynames`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tble_currencyname`
--
ALTER TABLE `tble_currencyname`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tble_login`
--
ALTER TABLE `tble_login`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `tble_orderitems`
--
ALTER TABLE `tble_orderitems`
  ADD PRIMARY KEY (`ItemId`);

--
-- Indexes for table `tble_orders`
--
ALTER TABLE `tble_orders`
  ADD PRIMARY KEY (`OrderId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tble_booth`
--
ALTER TABLE `tble_booth`
  MODIFY `BoothId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tble_companysetup`
--
ALTER TABLE `tble_companysetup`
  MODIFY `CompanyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tble_container`
--
ALTER TABLE `tble_container`
  MODIFY `ContainerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tble_containertypes`
--
ALTER TABLE `tble_containertypes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tble_countrynames`
--
ALTER TABLE `tble_countrynames`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;
--
-- AUTO_INCREMENT for table `tble_currencyname`
--
ALTER TABLE `tble_currencyname`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `tble_login`
--
ALTER TABLE `tble_login`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tble_orderitems`
--
ALTER TABLE `tble_orderitems`
  MODIFY `ItemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tble_orders`
--
ALTER TABLE `tble_orders`
  MODIFY `OrderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
