
var mixMyContainer = angular.module('mixMyContainer',['ngRoute','ngStorage','datatables','webcam', 'ngIntlTelInput'])
.config(function (ngIntlTelInputProvider) {
        ngIntlTelInputProvider.set({
          initialCountry: 'us',
          utilsScript: '../assets/js/utils.js'
        });
      });

