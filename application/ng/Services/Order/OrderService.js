angular.module('mixMyContainer').factory('OrderService', ['$http', '$q', '$timeout', '$filter', '$rootScope', '$window', function($http, $q, $timeout, $filter, $rootScope, $window) {
    var ErrorHandler = function(data, status, headers, config) {
        $window.location = '../Login/errorPage';
    };
    var OrderService = {
        /**function new order**/
        NewOrder: function(orderno, hall, ordertype, trade, boothname, boothno, telephone, businessscope, Businesscard) {

            return $http.post('../Order/SaveOrder', { orderno: orderno, hall: hall, ordertype: ordertype, trade: trade, boothname: boothname, boothno: boothno, telephone: telephone, businessscope: businessscope, Businesscard: Businesscard }); //.error(ErrorHandler);
        },
        /**fetch carton units**/
        Cartonunits: function() {

            return $http.get('../Order/Getcartonunites'); //.error(ErrorHandler);
        },
        /**fetch price units**/

        /**function for new item**/
        NewItem: function(orderid, boothid, containerid, itemno, description, altdescription, cartonGrossWeight, Length, Width, Height, cbm, usprice, unitpercarton, unitqty, priceRMB, ExchangeRate3dCurrency, cartonqty, itemimage, containertype) {

            return $http.post('../Order/SaveItem', { orderid: orderid, boothid: boothid, containerid: containerid, itemno: itemno, description: description, altdescription: altdescription, cartonGrossWeight: cartonGrossWeight, Length: Length, Width: Width, Height: Height, cbm: cbm, usprice: usprice, unitpercarton: unitpercarton, unitqty: unitqty, priceRMB: priceRMB, ExchangeRate3dCurrency: ExchangeRate3dCurrency, cartonqty: cartonqty, itemimage: itemimage, containertype: containertype }); //.error(ErrorHandler);
        },
        /**function subfunction for save item**/
        SubNewItem: function(orderid, boothid, containerid, itemno, description, altdescription, cartonGrossWeight, Length, Width, Height, cbm, usprice, unitpercarton, unitqty, priceRMB, ExchangeRate3dCurrency, cartonqty, itemimage, containertype, balanceweight, balancevolume) {

            return $http.post('../Order/SubSaveItem', { orderid: orderid, boothid: boothid, containerid: containerid, itemno: itemno, description: description, altdescription: altdescription, cartonGrossWeight: cartonGrossWeight, Length: Length, Width: Width, Height: Height, cbm: cbm, usprice: usprice, unitpercarton: unitpercarton, unitqty: unitqty, priceRMB: priceRMB, ExchangeRate3dCurrency: ExchangeRate3dCurrency, cartonqty: cartonqty, itemimage: itemimage, containertype: containertype, balanceweight: balanceweight, balancevolume: balancevolume }); //.error(ErrorHandler);
        },
        /**function for new booth**/
        NewBooth: function(orderid, newBoothName, newBoothNo, newTelephone, newBusinessScope, businesscard,tradeid,hallid) {

            return $http.post('../Order/Savebooth', { orderid: orderid, newBoothName: newBoothName, newBoothNo: newBoothNo, newTelephone: newTelephone, newBusinessScope: newBusinessScope, businesscard: businesscard, tradeId:tradeid,hallId:hallid }); //.error(ErrorHandler);
        },
        /**function for add new container**/
        NewContainer: function(newcontianertype, orderid, containerid, usedweight, usedvolume, currentcontainertype) {

            return $http.post('../Order/Savecontainer', { newcontianertype: newcontianertype, orderid: orderid, containerid: containerid, usedweight: usedweight, usedvolume: usedvolume, currentcontainertype: currentcontainertype }); //.error(ErrorHandler);
        },
        /**function for upgrade container**/
        UpgradeContainer: function(upgradecontainertype, orderid, containerid, totalcontainervolume, totalcontainerweight) {

            return $http.post('../Order/Upgradecontainer', { upgradecontainertype: upgradecontainertype, orderid: orderid, containerid: containerid, totalcontainervolume: totalcontainervolume, totalcontainerweight: totalcontainerweight }); //.error(ErrorHandler);
        },
        /**function for end order**/
        EndOrder: function(orderid) {

            return $http.post('../Order/Endorder', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**function for get order values**/
        GetOrderValues: function(orderid) {

            return $http.post('../Order/Ordervalues', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**function for get booth values**/
        GetBoothValue: function(orderid, boothid) {

            return $http.post('../Order/Getboothvalues', { orderid: orderid, boothid: boothid }); //.error(ErrorHandler);
        },
        /**function for get container percentage value**/
        GetContainerPercentageValue: function(orderid) {

            return $http.post('../Order/GetContainerValues', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**function for view all order details**/
        Viewallorder: function() {

            return $http.post('../Order/Viewallorder'); //.error(ErrorHandler);
        },
        /**function for view all order items**/
        Viewallorderitems: function(orderid) {

            return $http.post('../Order/Viewallorderitems', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**function for view order details**/
        Vieworderdetails: function(orderid) {

            return $http.post('../Order/Vieworderdetails', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**function for get container**/
        Getcontainer: function(orderid) {

            return $http.post('../Order/Getcontainerdetails', { orderid: orderid }); //.error(ErrorHandler);
        },
        /**get all details of the order for continoue the order**/
        Getallorderdetails: function(orderid) {

            return $http.post('../Order/Getalltheorderdetails', { orderid: orderid }); //.error(ErrorHandler);
        },
        Orderinvoice: function(orderid) {

            return $http.post('../Order/Invoceorder', { orderid: orderid }); //.error(ErrorHandler);
        },
        emailinvoice: function(orderid) {

            return $http.get('../Mail/email?orderid=' + orderid, {}); //.error(ErrorHandler);
        },
        containerlist: function() {
            return $http.get('../Order/listcontainer');
        },
        userdcontainers: function(orderid) {
            return $http.post('../Order/Usedcontainers', { orderid: orderid });
        },
        showactivecontainer: function(orderid, activecontianerid) {
            return $http.post('../Order/showactivecontainer', { orderid: orderid, activecontianerid: activecontianerid });
        },
        SaveImage: function(imagename) {
            return $http.post('../Order/saveimage', { imagename: imagename });
        },
        SelectUserCompanyDetails: function() {
            return $http.post('../Order/selectusercompanydetails', {});
        },
        languagetrasilate: function(source, target, text) {
            return $http.get('https://translation.googleapis.com/language/translate/v2?key=AIzaSyDBHOi4NHsi3TK3pIyb2V63dyQoXWtnfpQ&source=' + source + '&target=' + target + '&q=' + text, {});
        },
        /*Abhijith A Nair's code */
        /*get booth info :: parameter: boothno- User Enterd Booth Number */
        getBoothInfo: function(boothno) {
            return $http.get('../Order/GetBoothInfo?boothno=' + boothno, {}); //.error(ErrorHandler);
        },
        /*get trade list */
        getTradelist: function() {
            return $http.get('../Order/GetTradeList', {}); //.error(ErrorHandler);
        },
        /*Get halls on trade :: Parameter tradeid-- User Selected trade */
        getHalllist: function(tradeid) {
            return $http.get('../Order/GetHallist?parent=' + tradeid, {}); //.error(ErrorHandler);
        },
        /*To check the order no already exists or not :: parameter orderid-- Automated or User Enterd orderno */
        checkOrderno: function(orderid) {
            return $http.get('../Order/CheckOrderNo?orderno=' + orderid, {}); //.error(ErrorHandler);
        },
        getContainerList: function() {
            return $http.get('../Order/GetContainerList', {}); //.error(ErrorHandler);
        },
        checkCompanydetails: function() {
            return $http.get('../Order/CheckCompanyDetails', {}); //.error(ErrorHandler);
        },
        saveCustomTradeOrHall: function(name, parent) {

            return $http.post('../Order/SaveCustomTradeOrHall', { name: name, parent: parent }); //.error(ErrorHandler);
        },

        getContainertypes: function(contianertype, volume, weight) {
            return $http.post('../Order/GetContainertypes', { contianertype: contianertype, volume: volume, weight: weight });
        },

        Filterbydate: function(fromdate, todate) {

            return $http.get('../Order/FilterByDate?fromdate=' + fromdate + '&todate=' + todate, {}); //.error(ErrorHandler);
        },
        checkboothIsset: function(orderid) {
            return $http.post('../Order/Checkboothisset', { orderid: orderid });
        },
        Ignorcontianer: function(ignoreorderid,firstcontainerid){
            return $http.post('../Order/Ignoreorderitemsave',{ignoreorderid:ignoreorderid,firstcontainerid:firstcontainerid});
        },
        getCartonvalues : function(){
          return $http.get('../Profile/getCartonvalues', {});
        },
        getadminexhangerate : function(){
          return $http.get('../Order/getadminexhangerate', {});
        },
        checkemailitems : function(orderid){
          return $http.get('../Mail/checkemailitems?orderid=' + orderid, {});
        },
        Checkfornotfilledcontainers : function(orderid,balancevolume,balanceweight,usedweight,usedvolume,cartonqty,cbm,cartonGrossWeight,balancecarton,containertype,containerid){
            return $http.post('../Order/Checkfornotfilledcontainers',{orderid : orderid, balancevolume : balancevolume, balanceweight : balanceweight,usedweight:usedweight,usedvolume:usedvolume, cartonqty:cartonqty, cbm:cbm, cartonGrossWeight:cartonGrossWeight,balancecarton:balancecarton,containertype:containertype,containerid:containerid});
        },
        UpdateContainer : function(orderid,boothid,containerid,unfilledcontainerid,usedweight,usedvolume,currentcontainertype,balancevolume,balanceweight,itemno,description,altdescription,unitpercarton,cartonGrossWeight,Length,Width,Height,cbm,itemimage,usprice,priceRMB,ExchangeRate3dCurrency,unitqty,cartonqty,balancecartons){
            return $http.post('../Order/UpdateContainer',{orderid:orderid,boothid:boothid,containerid:containerid,unfilledcontainerid:unfilledcontainerid,usedvolume:usedvolume,usedweight:usedweight,currentcontainertype:currentcontainertype,balancevolume:balancevolume,balanceweight:balanceweight,itemno:itemno,description:description,altdescription:altdescription,unitpercarton:unitpercarton,cartonGrossWeight:cartonGrossWeight,Length:Length,Width:Width,Height:Height,cbm:cbm,itemimage:itemimage,usprice:usprice,priceRMB:priceRMB,ExchangeRate3dCurrency:ExchangeRate3dCurrency,unitqty:unitqty,cartonqty:cartonqty,balancecartons:balancecartons});
        },
        /*End Of Abhijith A Nair's code */


    }
    return OrderService;
}]);
