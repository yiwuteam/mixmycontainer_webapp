angular.module('adminmixMyContainer').factory('AdminService', ['$http', '$q', '$timeout', '$filter', '$rootScope', '$window', function ($http, $q, $timeout, $filter, $rootScope, $window) {
  var AdminService = {
        getLogin: function (uname,password) {
            var url='login?uname='+uname+'&password='+password;
            return $http.get(url, {});
        },
        saveGoldAgent: function (agentname,agentoffice,companyphone,contact,faxnum,businessyear,goldyear) {
            var url='SaveAgent';
            return $http.post(url, {agentname:agentname,agentoffice:agentoffice,companyphone:companyphone,contact:contact,faxnum:faxnum,businessyear:businessyear,goldyear:goldyear});
        },
        getTradelist: function() {
            return $http.get('../Admin/GetTradeList', {}); //.error(ErrorHandler);
        },
        saveTradeOrHall: function(name, parent) {

            return $http.post('../Admin/SaveTradeOrHall', { name: name, parent: parent }); //.error(ErrorHandler);
        },
        saveContainer: function(container, maxvol,maxweight) {

            return $http.post('../Admin/SaveContainer', { container: container, maxvolume: maxvol,maxweight:maxweight }); //.error(ErrorHandler);
        },
        getGoldAgents: function(pageno) {
            return $http.get('../Admin/GetGoldAgent?page='+pageno, {}); //.error(ErrorHandler);
        },
        getHalllist: function() {
            return $http.get('../Admin/GetHallList', {}); //.error(ErrorHandler);
        },
        getContainerist: function() {
            return $http.get('../Admin/GetContainerList', {}); //.error(ErrorHandler);
        },
        ChangePassword: function(newpassword,oldpassword) {
            return $http.post('../Admin/ChangePassword', {password:newpassword,oldpassword:oldpassword}); //.error(ErrorHandler);
        },
        deletContainer: function(id) {
            return $http.post('../Admin/DeleteContainer', {containerid:id}); //.error(ErrorHandler);
        },
        updateContainer: function(containerid,container, maxvol,maxweight) {
            return $http.post('../Admin/UpdateContainer', { containerid:containerid,container: container, maxvolume: maxvol,maxweight:maxweight }); //.error(ErrorHandler);
        },
        deletTrade: function(id) {
            return $http.post('../Admin/DeleteTradeOrHall', {id:id}); //.error(ErrorHandler);
        },
        updateTrade: function(id,name,parent) {
            return $http.post('../Admin/UpdateTradeOrHall', { id:id,parent:parent,name:name}); //.error(ErrorHandler);
        },
        getUserList: function(pageno) {
            return $http.get('../Admin/GetUsedetails?page='+pageno, {}); //.error(ErrorHandler);
        },
        getDefaultValues: function(){
            return $http.get('../Admin/GetDefault', {});
        },
        updateDefault: function(deafult){
            return $http.post('../Admin/UpdateDefault', {DefaultValues:deafult});
        },
        deleteGoldAgent: function(id){
            return $http.post('../Admin/DeleteAgent', {GoldAgentId:id});
        },
        deleteUser: function(email){
            return $http.post('../Admin/DeleteUser', {UserEmail:email});
        },
        updateGoldAgent: function (agentid,agentname,agentoffice,companyphone,contact,faxnum,businessyear,goldyear) {
            var url='../Admin/updateGoldAgent';
            return $http.post(url, {agentid:agentid,agentname:agentname,agentoffice:agentoffice,companyphone:companyphone,contact:contact,faxnum:faxnum,businessyear:businessyear,goldyear:goldyear});
        },
        deactivateGoldAgent: function(id){
            return $http.post('../Admin/DeactivateAgent', {GoldAgentId:id});
        },
      }
      return AdminService;
}]);
