angular.module('mixMyContainer').factory('AccountService', ['$http', '$q', '$timeout', '$filter', '$rootScope', '$window', function ($http, $q, $timeout, $filter, $rootScope, $window) {
  var AccountService = {
        chengepassword: function (email,password,oldPassword) {
            
            return $http.post('../Profile/chengepassword', {email:email,password:password,oldPassword:oldPassword});
        },
        accountdetails:function (language,emailsettings,locale){
          
          return $http.post('../Profile/profiledetails', {language:language,emailsettings:emailsettings,locale:locale});
        },
        getcontries :function(){
          
          return $http.post('../Profile/getContries', {});
        },
        getCorrency :function(){
          
          return $http.post('../Profile/getCorrency', {});
        },
        imageupload :function(formdata){
          
          return $http.post('../Profile/imageUpload', {formdata:formdata});
        },
        getlocale :function(){
          
          return $http.get('../assets/National.json', {});
        },
        getAccountDetails: function(){
          
          return $http.post('../Profile/getAccountDetails', {});
        },
        companyDetails :function(companysetup,cartonsetup,exchangeratesetup){
          
          return $http.post('../Profile/companyDetails', {companysetup:companysetup,cartonsetup:cartonsetup,exchangeratesetup:exchangeratesetup });
        },
        getaccount : function(){
          
          return $http.post('../Profile/getAccount', {});
        },
        getCompanyDetails : function(){
          
          return $http.post('../Profile/getCompanyDetails', {});

        },
        getCartonvalues : function(){
          
          return $http.post('../Profile/getCartonvalues', {});
        },
        getallgoldagents : function(){
          
          return $http.post('../Profile/getallgoldagents', {});
        }
      }
      return AccountService;
}]);
