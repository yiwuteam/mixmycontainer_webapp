mixMyContainer.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'views/index.php',
              //  templateUrl:'templates/home.html';
                controller: 'LoginController'
            }).
            when('/testing', {
                templateUrl: 'views/dashboard.php',
                controller: 'LoginController'
            })
            .otherwise({
                redirectTo: '/testing'
       });

}]);
