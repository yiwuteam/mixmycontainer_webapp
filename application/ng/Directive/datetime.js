mixMyContainer.directive('dateTime', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.datepicker({
           format:'yyyy-mm-dd',
           pickTime: false,
           todayBtn:'linked',
           todayHighlight:true,
           toggleActive:true,
           autoclose:true,
           startDate:'2016-01-01',
           endDate: '2100-12-31'
          }).on('changeDate', function(e) {
            ngModelCtrl.$setViewValue(e.date);
            scope.$apply();
          });
          
        }
    };
});