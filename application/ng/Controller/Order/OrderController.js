mixMyContainer.controller('OrderController', ['$scope', '$rootScope', 'OrderService', '$window', '$location', '$http', '$localStorage', '$filter', 'DTOptionsBuilder', 'DTColumnBuilder', '$q', '$compile', '$timeout', function($scope, $rootScope, OrderService, $window, $location, $http, $localStorage, $filter, DTOptionsBuilder, DTColumnBuilder, $q, $compile, $timeout) {

    /*declare scopes*/
    $scope.Order = {};
    $scope.Item = {};
    $scope.Orderid = {};
    $scope.Boothid = {};
    $scope.Containerid = {};
    $scope.myImage = {};
    $scope.files = [];

    (function(Order) {
        /* Abhijith A Nair's Code */
        /* Variable used in this scope */
        $('#orderno').addClass('loadinggif');
        Order.IsProcessing = true;
        Order.Trades = [{
            "Id": -1,
            "Name": "Loading..."
        }];
        Order.Halls = [{
            "Id": -1,
            "Name": "Loading..."
        }];
        Order.Conatinertypes = [{
            "Id": -1,
            "Name": "Loading..."
        }];
        Order.BoothImage = "image_icon2.png";
        // Order.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10);
        // Order.dtInstance = {};
        Order.Hall = Order.Halls[0];
        Order.ContainerType = Order.Conatinertypes[0];
        Order.Trade = Order.Trades[0];
        Order.boothImage = "";
        Order.FromDate = $filter("date")(Date.now(), 'yyyy-MM-dd');
        Order.ToDate = $filter("date")(Date.now(), 'yyyy-MM-dd');
        // Order.IsEnable = true;
        /* End of Abhijith A Nair's code */
        /*function save order*/
        Order.saveOrder = function() {
                $('.ajax_loader').show();
                Order.BoothName = "";
                Order.Telephone = "";
                Order.Businessscope = "";
                Order.error = false;
                Order.hallerror = false;
                Order.containererror = false;
                Order.tradeerror = false;
                Order.TempContainer = Order.ContainerType;
                Order.TempHall = Order.Hall;
                Order.TempTrade = Order.Trade;
                Order.OrderNo = Order.OrderNumber;
                Order.Hall = Order.Hall.Id;
                Order.ContainerType = Order.ContainerType.ID;
                Order.Trade = Order.Trade.Id;
                Order.BoothNo = Order.BoothNo;
                if (Order.BoothInfo != undefined) {
                    Order.BoothName = Order.BoothInfo.BoothName;
                    if (Order.BoothName == undefined) {
                        Order.BoothName = "";
                    }
                    Order.Telephone = Order.BoothInfo.Phone;
                    if (Order.Telephone == undefined) {
                        Order.Telephone = "";
                    }
                    Order.Businessscope = Order.BoothInfo.BusinessScope;
                    if (Order.Businessscope == undefined) {
                        Order.Businessscope = "";
                    }
                }
                if(Order.boothImage!=""){
                    Order.Businesscard = Order.boothImage;
                }
                else if(Order.BoothImage!="image_icon2.png" && Order.BoothImage!="")
                {
                     Order.Businesscard=Order.BoothImage;
                }
                else{
                    Order.Businesscard="";
                }
                // base64Image
                $localStorage.orderno = Order.OrderNumber
                if (Order.OrderNo != "" && Order.Hall != "-1" && Order.ContainerType != "-1" && Order.Trade != "-1" && Order.BoothNo != "" && Order.BoothNo != undefined) {
                    OrderService.NewOrder(Order.OrderNo, Order.Hall, Order.ContainerType, Order.Trade, Order.BoothName, Order.BoothNo, Order.Telephone, Order.Businessscope, Order.Businesscard).success(function(response) {
                        $('.ajax_loader').hide();
                        if (response.message == 'true') {
                            $localStorage.orderid = response.orderid; //store order id to localstorage from response
                            $localStorage.boothid = response.boothid; //store booth id to localstorage from response
                            $localStorage.containerid = response.containerid; //store container id to localstorage from reponse
                            $localStorage.firstcontainerid = response.containerid;
                            $localStorage.containetype = Order.TempContainer.ID; // store container type to localstorage from response
                            window.location.href = "../Login/item";
                            toastr["success"]("New order added");
                        } else if (response.comment == 'OrderId Exists') {
                            $('.ajax_loader').hide();
                            toastr.error('Order No is already exists');
                            Order.ContainerType = Order.TempContainer;
                            Order.Hall = Order.TempHall;
                            Order.Trade = Order.TempTrade;
                        } else {
                            $('.ajax_loader').hide();
                            toastr.error('Something bad happend!');
                            Order.ContainerType = Order.TempContainer;
                            Order.Hall = Order.TempHall;
                            Order.Trade = Order.TempTrade;
                        }
                    })
                } else {
                    Order.error = true;
                    if (Order.Hall == "-1") {
                        Order.hallerror = true;
                    }
                    if (Order.ContainerType == "-1") {
                        Order.containererror = true;
                    }
                    if (Order.Trade == "-1") {
                        Order.tradeerror = true;
                    }
                    $('.ajax_loader').hide();
                    //toastr.error('You missed something, please check');
                    Order.ContainerType = Order.TempContainer;
                    Order.Hall = Order.TempHall;
                    Order.Trade = Order.TempTrade;
                }
            }
            /**Abhijith A Nair's code **/
            /* function to get Booth info */
        Order.getBoothInfo = function() {
            if (Order.BoothNo != "" && Order.BoothNo != undefined && Order.BoothNo != null) {
                $('.ajax_loader').show();
                OrderService.getBoothInfo(Order.BoothNo).success(function(response) {
                    $('.ajax_loader').hide();
                    if (response.message == "true") {
                        // swal({
                        //   title: 'Booth Details',
                        //   text: "Do you want manualy enter the booth details?",
                        //   type: 'question',
                        //   showCancelButton: true,
                        //   confirmButtonColor: '#3085d6',
                        //   cancelButtonColor: '#d33',
                        //   confirmButtonText: 'Yes',
                        //   cancelButtonText: 'No',
                        //   confirmButtonClass: 'btn btn-success',
                        //   cancelButtonClass: 'btn btn-danger',

                        // }).then(function () {
                        //   //Nothing to do;
                        // }, function (dismiss) {
                        //   // dismiss can be 'cancel', 'overlay',
                        //   // 'close', and 'timer'
                        //   if (dismiss === 'cancel') {
                        //     //Setting Details
                        //    Order.BoothInfo = response.boothinfo;
                        //   }
                        // })
                        Order.BoothInfo = response.boothinfo;
                        //Order.BoothInfo.BusinessCard = str_replace('_thumb.jpg', '.jpg', Order.BoothInfo.BusinessCard);
                        Order.BoothImage = Order.BoothInfo.BusinessCard;
                    } else {
                        Order.BoothInfo = undefined;
                        Order.BoothImage = "image_icon2.png";
                        console.log(response.comment);
                    }

                });
            }
        }

        /* function Get Trade List */
        Order.getTrades = function() {
            OrderService.getTradelist().success(function(response) {
                if (response.message == "true") {
                    Order.Trades = response.tradelist;
                    if (response.tradelist.length > 0) {
                        Order.Trade = Order.Trades[0];
                        Order.getHalls();
                    }
                } else {
                    console.log(response.comment);
                    Order.Trades = [{
                        "Id": -1,
                        "Name": "No Result"
                    }];

                    Order.Trade = Order.Trades[0];

                }

            });
        }

        /* function get halls on trade */
        Order.getHalls = function() {
            Order.Halls = [{
                "Id": -1,
                "Name": "Loading..."
            }];
            if (Order.Trade != undefined) {
                if (Order.Trade.Id > 0) {
                    OrderService.getHalllist(Order.Trade.Id).success(function(response) {
                        if (response.message == "true") {
                            Order.Halls = response.halllist;
                            if (response.halllist.length > 0) {
                                Order.Hall = response.halllist[0];
                            }
                        } else {
                            console.log(response.comment);
                            Order.Halls = [{
                                "Id": -1,
                                "Name": "No Result"
                            }];
                            Order.Hall = Order.Halls[0];

                        }

                    });
                } else {
                    Order.Halls = [{
                        "Id": -1,
                        "Name": "No Result"
                    }];
                    Order.Hall = Order.Halls[0];
                }
            }
        }

        /*function check orderno exists */
        Order.checkOrderno = function() {
            OrderService.checkOrderno(Order.OrderNumber).success(function(response) {
                if (response.message == "true") {
                    //Nothing Happends
                } else {
                    // Bind new Order no and Notify User
                    Order.OrderNumber = response.OrderNo;
                    toastr.error('OrderNo Exists, So new OrderNo ' + Order.OrderNumber);
                    console.log(response.comment);
                }
            });
        }
        Order.checkCompanyDetails = function() {
                OrderService.checkCompanydetails().success(function(response) {
                    if (response.message == "true") {
                        $('#orderno').removeClass('loadinggif');
                        Order.OrderNumber = response.OrderNo;
                        // Order.IsEnable = false;
                    } else {
                        // Goto Company Setup Page
                        // console.log(response.comment);
                        // swal({
                        //     title: 'Something Missing!',
                        //     text: "Please complete the company setup wizard before adds an order!",
                        //     type: 'warning',
                        //     confirmButtonColor: '#3085d6',
                        //     confirmButtonText: 'Ok',
                        //     allowOutsideClick: false
                        // }).then(function() {
                        //     window.location.href = "../Login/setup";
                        // })
                    }
                });
            }
            /* function get all conatinertypes */
        Order.getContainers = function() {
                OrderService.getContainerList().success(function(response) {
                    if (response.message == "true") {
                        Order.Conatinertypes = response.containerlist;
                        if (response.containerlist.length > 0) {
                            Order.ContainerType = Order.Conatinertypes[0];
                        }
                    } else {
                        Order.Conatinertypes = [{
                            "Id": -1,
                            "Name": "No Result"
                        }];
                        Order.ContainerType = Order.Conatinertypes[0];
                        console.log(response.comment);

                    }

                });
            }
            /*open popup for custom trade */
        Order.addTrade = function() {
            swal({
                title: 'Enter your own Trade District/Area',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: function(trade) {
                    return new Promise(function(resolve, reject) {
                        if (trade === '') {
                            reject('Please Enter Trade District/Area')
                        } else {
                            var parent = 0;
                            OrderService.saveCustomTradeOrHall(trade, parent).success(function(response) {
                                if (response.message == "true") {


                                    var tempVal = {
                                        Id: response.Id,
                                        Name: trade,
                                    }
                                    Order.Trades.push(tempVal);
                                    Order.Trade = tempVal;
                                    resolve()
                                } else {
                                    resolve()
                                    console.log(response.comment);

                                }
                            });
                        }
                    })
                },
                allowOutsideClick: false
            }).then(function(trade) {
                swal({
                    type: 'success',
                    title: 'Success!',
                    html: 'New Trade Added'
                })
            })
        }

        /*Open popup for enter custom hall */
        Order.addHall = function() {
                swal({
                    title: 'Enter your own Hall',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: function(hall) {
                        return new Promise(function(resolve, reject) {
                            if (hall === '') {
                                reject('Please Enter Hall')
                            } else {
                                if (Order.Trade != undefined) {
                                    if (Order.Trade.Id > 0) {
                                        var parent = Order.Trade.Id
                                        OrderService.saveCustomTradeOrHall(hall, parent).success(function(response) {
                                            if (response.message == "true") {

                                                var tempVal = {
                                                    Id: response.Id,
                                                    Name: hall,
                                                }
                                                Order.Hall = tempVal;
                                                resolve()
                                                Order.Halls.push(tempVal);
                                            } else {
                                                console.log(response.comment);
                                                resolve()

                                            }
                                        });
                                    }
                                } else {
                                    resolve()
                                }

                            }
                        })
                    },
                    allowOutsideClick: false
                }).then(function(hall) {
                    swal({
                        type: 'success',
                        title: 'Success!',
                        html: 'New hall added'
                    })
                })
            }
            /*Web Cam Code */
        Order.channel = {};
        Order.webcamError = false;
        Order.Webcamerrormessage = true;
        Order.HideCaptureButton = false;
        //Order.Webcamerrormessage = false;
        Order.onError = function(err) {
            $scope.$apply(
                function() {
                    Order.webcamError = err;
                    Order.Webcamerrormessage = true;
                    Order.HideCaptureButton = false;
                    //Order.HideCaptureButton = true; 
                    // toastr["warning"]("Failed to allocate videosource");
                }
            );
        };
        Order.onStream = function(stream) {};
        Order.onSuccess = function() {
            _video = $scope.channel.video;
            $scope.$apply(function() {
                Order.Webcamerrormessage = false;
                Order.HideCaptureButton = true;
                $scope.patOpts.w = _video.width;
                $scope.patOpts.h = _video.height;
                //$scope.showDemos = true;
            });
        };
        // Order.channel = {
        // the fields below are all optional
        //  videoHeight: 300,
        //  videoWidth: 300,
        //  video: null
        // Will reference the video element on success
        // };
        Order.makeSnapshot = function() {

            if (_video) {

                var patCanvas = document.querySelector("#snapshot");
                if (!patCanvas) return;

                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                sendSnapshotToServer(patCanvas.toDataURL());

                patData = idata;
            }
        };


        var getVideoData = function getVideoData(x, y, w, h) {
            var hiddenCanvas = document.createElement('canvas');
            hiddenCanvas.width = _video.width;
            hiddenCanvas.height = _video.height;
            var ctx = hiddenCanvas.getContext('2d');
            ctx.drawImage(_video, 0, 0, _video.width, _video.height);
            return ctx.getImageData(x, y, w, h);
        };
        var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
            $scope.snapshotData = imgBase64;
            var imagestring = imgBase64.replace(/^data:image\/(png|jpg);base64,/, "");
            var imgData = JSON.stringify(imagestring);
            Order.boothImage = imgData;
        };
        // Order.saveimage = function(imgData) {
        //         Item.imagename = imgData;
        //         OrderService.SaveImage(Item.imagename).success(function(response) {
        //             alert(response);

        //         })

        //     }
        /*End of WebCam Code */
        Order.OrderlistUrl = '../Order/Viewallorder';
        Order.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
                var defer = $q.defer();
                $http.get(Order.OrderlistUrl).then(function(result) {
                    defer.resolve(result.data);
                    Order.IsProcessing = false;
                    // $('.ajax_loader').hide();
                });
                return defer.promise;
            }).withOption('createdRow', function(row) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withOption('processing', true) //for show progress bar
            .withPaginationType('full_numbers')
            .withOption('order', [0, 'desc']);
        Order.dtColumns = [
            DTColumnBuilder.newColumn('OrderId').withTitle('OrderId').notVisible(),
            DTColumnBuilder.newColumn('LastUpdate').withTitle('Date'),
            DTColumnBuilder.newColumn('OrderNumber').withTitle('Order Number'),
            DTColumnBuilder.newColumn('TotalBooth').withTitle('Number of booths'),
            DTColumnBuilder.newColumn('TotalOrderCBM').withTitle('CBM'),
            DTColumnBuilder.newColumn('TotalOrderUsValue').withTitle('Order Value'),
            DTColumnBuilder.newColumn('TotalCartons').withTitle('No of Cartons'),
            DTColumnBuilder.newColumn('', 'Actions').renderWith(function(data, type, full, meta) {
                var buttons = '';
                if (full.OrderStatus == 0) {
                    buttons = buttons + '<button class="btn btn-danger btn-circle" ng-click="Item.Contioueorder(' + full.OrderId + ');" type="button"><i class="fa fa-plus"></i></button>';
                }
                buttons = buttons + '<a href="../Login/vieworder?id=' + full.OrderId + '&status=' + full.OrderStatus + '"><button class="btn btn-info btn-circle pull-right" type="button"><i class="fa fa-eye"></i></button></a>';
                return buttons
            }).notSortable()
        ];
        Order.dtInstance = {};
        Order.rowCallback = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
            $('td', nRow).unbind('click');
            $('td', nRow).bind('click', function() {
                $scope.$apply(function() {
                    //  alert("You've clicked row," + iDisplayIndex);
                });
            });
            return nRow;
        }
        Order.Viewallorder = function() {
            // OrderService.Viewallorder().success(function(response) {
            //     Order.Orders = response;

            // })

        }

        Order.Filterbydate = function() {
                if (!Order.IsProcessing) {
                    Order.IsProcessing = true;
                    Order.FromDate = $filter("date")(Order.FromDate, 'yyyy-MM-dd');
                    Order.ToDate = $filter("date")(Order.ToDate, 'yyyy-MM-dd');
                    if (Order.FromDate <= Order.ToDate) {
                        //   $('.ajax_loader').show();
                        Order.OrderlistUrl = '../Order/FilterByDate?fromdate=' + Order.FromDate + '&todate=' + Order.ToDate;
                        var resetPaging = true;
                        //  Order.dtInstance.reloadData(callback, resetPaging);
                        Order.dtInstance.rerender();
                    } else {
                        toastr.error('Please select valid dates');
                    }
                }
                $timeout(function() {
                    Order.IsProcessing=false;
                }, 5000);
            }
            // OrderService.Filterbydate(Order.FromDate, Order.ToDate).success(function(response) {
            //     if (response.message = "true") {
            //         Order.Orders = response.Orders;
            //         // var resetPaging = false;
            //         // Order.dtInstance.reloadData(callback, resetPaging);

        //     } else {
        //         console.log(respons.comment);
        //         Order.Orders = [];
        //     }

        // })

        Order.ResetList = function() {
            // $('.ajax_loader').show();
            if (!Order.IsProcessing) {
                Order.IsProcessing = true;
                Order.OrderlistUrl = '../Order/Viewallorder';
                var resetPaging = true;
                // Order.dtInstance.reloadData(callback, resetPaging);
                Order.dtInstance.rerender();
               
            }
             $timeout(function() {
                    Order.IsProcessing=false;
                }, 5000);
        }
        Order.newPromise = function() {
            Order.FromDate = $filter("date")(Order.FromDate, 'yyyy-MM-dd');
            Order.ToDate = $filter("date")(Order.ToDate, 'yyyy-MM-dd');
            var defer = $q.defer();
            $http.get(Order.OrderlistUrl).then(function(result) {
                defer.resolve(result.data);
            });
            return defer.promise;
        }


        function callback(json) {
            console.log(json);
        }
        $scope.$watch('Order.Trade.Id', function() {
            Order.getHalls();
        });
        Order.MenuReset = function() {
            if (window.location.href.indexOf("orders") > -1) {
                $rootScope.$broadcast('ResetTable', {});
            }
        }
        $scope.$on('ResetTable', function() {
            Order.ResetList();
        });
        /*End of Abhijith A Nair's Code */

    })($scope.Order);

    /**start scope item**/
    (function(Item) {
        var _video = null,
            patData = null;
        $scope.patOpts = {
            x: 0,
            y: 0,
            w: 25,
            h: 25
        };
        Item.IsCamera = false;
        Item.BoothImage = "image_icon2.png";
        Item.businesscard="";
        Item.CameraOn = function() {
                Item.IsCamera = true;
            }
            /**get carton units**/
        Item.getcartonunits = function() {
                // $('.ajax_loader').show();
                OrderService.Cartonunits().success(function(response) {
                    // $('.ajax_loader').hide();
                    if (response) {
                        if (response[0].GrossweightCarton != 0 && response[0].Length != 0 && response[0].Height != 0 && response[0].Width != 0) {
                            Item.cartonGrossWeight = Number(response[0].GrossweightCarton); //store carton gross weight from response
                            Item.Length = Number(response[0].Length); // store carton length from response
                            Item.Height = Number(response[0].Height); // store carton height from response
                            Item.Width = Number(response[0].Width); //  store carton width from response
                            Item.cbm = Item.Length * Item.Height * Item.Width * 0.000001; // calculate cbm by multiply length, height and width
                        } else {
                            Item.getdefaultCartonValues();
                        }
                        Item.Orderno = $localStorage.orderno;
                        $localStorage.rmbexchangerate = Number(response[0].ExchangeRate);
                        $localStorage.thirdcurrencyexchangerate = Number(response[0].ExchangeRate3dCurrency);
                        $localStorage.activethirdcurrencystatus = response[0].Active3dCurrency;
                        Item.thirdcurrencystatus = response[0].Active3dCurrency;
                        $localStorage.thirdcurrencytype = response[0].ThirdCurrency;
                        Item.thirdcurrencyname = response[0].ThirdCurrency;
                        if ($localStorage.rmbexchangerate == 0) {
                            Item.getadminexhangerate();
                        }

                        // store order no
                    } else {
                        toastr.error('There is no default carton settings');
                    }
                })
            }
            /**end carton units**/

        /**function get totalorder value**/
        Item.gettotalordervalue = function() {
                // $('.ajax_loader').show();
                Item.orderid = $localStorage.orderid;
                OrderService.GetOrderValues(Item.orderid).success(function(response) {
                    // $('.ajax_loader').hide();
                    if (response) {
                        debugger
                        Item.Usprice = Number(response[0].TotalOrderUsValue); // store total order value from response
                        Item.Rmbprice = Number(response[0].TotalOrderRmbValue); // store totalrmb value from response
                        Item.totalcbm = response[0].TotalOrderCBM; // store total order cbm from response
                        Item.totalcbm = parseFloat(Item.totalcbm).toFixed(2)
                        Item.totalweight = Number(response[0].TotalOrderWeight);
                        // store order weight
                    } else {
                        toastr.error('There is no default carton settings');
                    }
                })
            }
            /**end get total order value**/


        /**function purchage current booth**/
        Item.getpurchasecurrentbooth = function() {
                // $('.ajax_loader').show();
                Item.boothid = $localStorage.boothid;
                Item.orderid = $localStorage.orderid;
                OrderService.GetBoothValue(Item.orderid, Item.boothid).success(function(response) {
                    // $('.ajax_loader').hide();

                    if (response) {
                        Item.Usboothprice = response[0].ValuePurchase; // store usbooth price from response
                        Item.Rmbboothprice = response[0].ValuePurchaseRMB; // store value rbm pursechase response
                        //Item.getcontainerfilledpercentage();
                    } else {
                        toastr.error('There is no default carton settings');
                    }
                })
            }
            /***get purchase current booth*/

        /**get container filled percentage**/
        Item.getcontainerfilledpercentage = function() {
                // $('.ajax_loader').show();
                Item.orderid = $localStorage.orderid;
                OrderService.GetContainerPercentageValue(Item.orderid).success(function(response) {
                    // $('.ajax_loader').hide();
                    if (response) {
                        Item.unfilledcontainer = response[0].Container;
                        $volumepercentage = JSON.parse(response[0].Volumepercentage);
                        $weightpercentage = JSON.parse(response[0].Weightpercentage);
                        $localStorage.containerid = response[0].ContainerId;
                        $localStorage.containetype = response[0].ContainerType;
                        //JSON.parse(response);
                        if ($volumepercentage > $weightpercentage) {
                            Item.unfilledvolumepercentage = $volumepercentage;
                        } else {
                            Item.unfilledvolumepercentage = $weightpercentage;
                        }


                    } else {
                        toastr.error('There is no default carton settings');
                    }
                })
            }
            /**end function container filled percentage **/

        /*function calculate carton quantity*/
        Item.calculatecartonqty = function() {
                // $('.ajax_loader').show();
                var unitpercarton = $('#unitpercarton').val();
                var unitqty = $('#unitqty').val();
                var mod = Number(unitqty) % Number(unitpercarton);
                var division = Number(unitqty) / Number(unitpercarton);
                // $('.ajax_loader').hide();
                if (mod == 0) {
                    // alert(division);
                    Item.cartonqty = Number(division);
                }
                if (mod != 0) {
                    if (Number(unitpercarton) > Number(unitqty)) {
                        Item.cartonqty = Number('1');
                    }
                    if (Number(unitpercarton) < Number(unitqty)) {
                        var division1 = parseInt(division) + 1;
                        Item.cartonqty = Number(division1);
                    }
                }

            }
            /**calculate carton size**/
        Item.calculatecartonsize = function() {
            var cbm = $('#Length').val() * $('#Width').val() * $('#Height').val() * 0.000001;
            Item.cbm = Number(cbm);
        }

        /*call functions when item page onload*/
        Item.itempagefunctions = function() {
                Item.BoothAdded = false;
                Item.notSaved = false;
                Item.getcartonunits();
                Item.getcontainerfilledpercentage();
                Item.getpurchasecurrentbooth();
                Item.gettotalordervalue();
                Item.selectusedcontainers();
                Item.selectusercompanydetails();
                Item.checkboothIsselect();

            }
            /*end*/
            /**function for get price conversion**/
        Item.Getuspriceconversion = function() {

            var usprice = $('#priceinus').val();
            var exchangerate = $localStorage.rmbexchangerate;
            if (exchangerate != 0) {
                Item.ExchangeRate = exchangerate * usprice;
            }
            var exchangethirdcurrency = $localStorage.thirdcurrencyexchangerate;
            if ($localStorage.activethirdcurrencystatus == 1) {
                Item.ExchangeRate3dCurrency = exchangethirdcurrency * usprice;
            } else {
                Item.ExchangeRate3dCurrency = 0;
            }
            Item.priceinus = usprice;
        }

        Item.getadminexhangerate = function() {
            OrderService.getadminexhangerate().success(function(response) {
                $localStorage.rmbexchangerate = response.ExchangeRate;

            })
        }

        Item.Getrmbpriceconversion = function() {


                var rmbprice = $('#ExchangeRate').val();
                var exchangerate = $localStorage.rmbexchangerate;
                if (exchangerate != 0) {
                    // Item.priceinus = rmbprice / exchangerate;
                    var priceinus = (rmbprice / exchangerate).toFixed(2);
                    Item.priceinus = Number(priceinus);
                }
                var exchangethirdcurrency = $localStorage.thirdcurrencyexchangerate;
                if ($localStorage.activethirdcurrencystatus == 1) {
                    Item.ExchangeRate3dCurrency = exchangethirdcurrency * Item.priceinus;
                } else {
                    Item.ExchangeRate3dCurrency = 0;
                }
                Item.ExchangeRate = rmbprice;
            }
            /**end get rmbprice conversion**/

        /**function image upload**/
        Item.ImageUpload = function() {
                $('.ajax_loader').show();

                var file = document.getElementById("fileInput").files[0];
                Item.Image = file.name;
                //  $scope.form.image = $scope.files[0];

                $http({
                    method: 'POST',
                    url: '../Order/imageUpload',
                    processData: false,
                    transformRequest: function(data) {
                        var formData = new FormData();
                        formData.append("image", file);
                        formData.append("name", Item.Image);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function(data) {
                    $('.ajax_loader').hide();
                    $('#MyModel2').modal('hide');
                    var str1 = data;
                    str1.replace(/\"/g, "");
                    var str2 = str1.replace(/\"/g, "");
                    $localStorage.imagename = str2;
                });

            }
            /**end function image upload**/

        /**function new container**/
        Item.newContianer = function() {
                $('.ajax_loader').show();
                Item.newcontianertype = $('#newcontainer').val();
                // alert(Item.newcontianertype);
                Item.orderid = $localStorage.orderid;
                Item.containerid = $localStorage.containerid
                Item.usedweight = $localStorage.usedweight.toFixed(2);
                Item.usedvolume = $localStorage.usedvolume.toFixed(2);
                Item.currentcontainertype = $localStorage.containetype;
                if (Item.newcontianertype != "" && Item.orderid != "" && Item.containerid != "" && Item.usedweight != "" && Item.usedvolume != "" && Item.currentcontainertype != "") {
                    OrderService.NewContainer(Item.newcontianertype, Item.orderid, Item.containerid, Item.usedweight, Item.usedvolume, Item.currentcontainertype).success(function(response) {
                        //  $('.ajax_loader').hide();
                        result = response.message;
                        if (result == 'true') {

                            $('#MyModel2').modal('hide');
                            $localStorage.containerid = response.containerid;
                            $localStorage.containetype = Item.newcontianertype;
                            // $localStorage.combinedcontainerid = response.newcontinerid;
                            // $('.ajax_loader').hide();
                            toastr["success"]("New container added");
                            Item.subitemsave();
                        } else {
                            $('.ajax_loader').hide();
                            toastr.error('Container added failed');
                        }
                    })
                }
            }
            /**end new container**/

        /**function upgrade container**/
        Item.upgradeContainer = function() {
                $('.ajax_loader').show();
                Item.upgradecontainertype = $('#upgradecontainer').val();
                Item.orderid = $localStorage.orderid;
                Item.containerid = $localStorage.containerid;

                Item.totalcontainervolume = $localStorage.totalcontainervolumeforupgrade;
                Item.totalcontainerweight = $localStorage.totalcontainerweightforupgrade;


                if (Item.upgradecontainertype != "" && Item.orderid != "" && Item.containerid != "" && Item.totalcontainervolume != "" && Item.totalcontainerweight != "") {
                    OrderService.UpgradeContainer(Item.upgradecontainertype, Item.orderid, Item.containerid, Item.totalcontainervolume, Item.totalcontainerweight).success(function(response) {

                        result = response.message;
                        if (result == 'true') {
                            $('#MyModel2').modal('hide');
                            $localStorage.containetype = Item.upgradecontainertype;
                            $localStorage.balancevolume = $localStorage.totalcontainervolumeforupgrade;
                            $localStorage.balanceweight = $localStorage.totalcontainerweightforupgrade;
                            // $('.ajax_loader').hide();
                            toastr["success"]("Upgrade your container successfully");
                            //$localStorage.combinedcontainerid = $localStorage.containerid;
                            $localStorage.flag = 0;
                            Item.subitemsave();
                        } else {
                            $('.ajax_loader').hide();
                            toastr.error('Upgrade container failed');

                        }
                    })
                }
            }
            /**end upgrade container**/

        
        // Item.saveitem = function() {
        //         $localStorage.flag = 1;
        //         if ($localStorage.combinedcontainerid == undefined || $localStorage.combinedcontainerid == "") {
        //             $localStorage.combinedcontainerid = $localStorage.containerid;
        //         } else {
        //             $localStorage.combinedcontainerid = $localStorage.combinedcontainerid + ',' + $localStorage.containerid;
        //         }

        //         $('.ajax_loader').show();
        //         Item.itemno = $('#itemno').val();
        //         Item.description = $('#description').val();
        //         Item.altdescription = $('#altdescription').val();
        //         Item.cartonGrossWeight = $('#cartonGrossWeight').val();
        //         Item.Length = $('#Length').val();
        //         Item.Width = $('#Width').val();
        //         Item.Height = $('#Height').val();
        //         Item.cbm = $('#cbm').val();
        //         Item.usprice = $('#priceinus').val();
        //         Item.unitpercarton = $('#unitpercarton').val();
        //         Item.unitqty = $('#unitqty').val();
        //         Item.priceRMB = $('#ExchangeRate').val();
        //         Item.ExchangeRate3dCurrency = $('#ExchangeRate3dCurrency').val();
        //         if (Item.ExchangeRate3dCurrency == undefined) {
        //             Item.ExchangeRate3dCurrency = '0';
        //         }
        //         Item.cartonqty = $('#cartonqty').val();
        //         Item.itemimage = $localStorage.itemimage;
        //         if (Item.itemimage == undefined || Item.itemimage == "") {
        //             Item.itemimage = 'default.jpg';
        //         }
        //         Item.orderid = $localStorage.orderid;
        //         Item.boothid = $localStorage.boothid;
        //         Item.containerid = $localStorage.containerid;
        //         Item.containertype = $localStorage.containetype;


        //         if (Item.orderid != "" && Item.boothid != "" && Item.containerid != "" && Item.itemno != "" && Item.description != "" && Item.cartonGrossWeight != "" && Item.Length != "" && Item.Width != "" && Item.Height != "" && Item.cbm != "" && Item.usprice != "" && Item.unitpercarton != "" && Item.unitqty != "" && Item.priceRMB != "" && Item.cartonqty != "" && Item.containertype != "") {

        //             OrderService.NewItem(Item.orderid, Item.boothid, Item.containerid, Item.itemno, Item.description, Item.altdescription, Item.cartonGrossWeight, Item.Length, Item.Width, Item.Height, Item.cbm, Item.usprice, Item.unitpercarton, Item.unitqty, Item.priceRMB, Item.ExchangeRate3dCurrency, Item.cartonqty, Item.itemimage, Item.containertype, Item.itemimage).success(function(response) {

        //                 var result = response.message;

        //                 if (result == 'true') {
        //                     $('.ajax_loader').hide();
        //                     Item.addnewcontainerlist();
        //                     if (response.usedvolume == 0 && response.usedweight == 0) {
        //                     Item.newcontainerlist = [];
        //                     //alert("the first container is not filled");
        //                     }
        //                     $localStorage.balancevolume = response.balancevolume;
        //                     $localStorage.balanceweight = response.balanceweight;
        //                     $localStorage.usedweight = response.usedweight;
        //                     $localStorage.usedvolume = response.usedvolume;
        //                     $localStorage.totalcontainervolume = response.totalcontainervolume;
        //                     $localStorage.totalcontainerweight = response.totalcontainerweight;


        //                     $('#MyModel2').modal('hide');
        //                     Item.popupshowbalancevolume = response.balancevolume.toFixed(2);
        //                     Item.balancecartongs = response.balancecartons;

        //                     $containervolume = $localStorage.balancevolume + $localStorage.usedvolume;
        //                     $containerweight = $localStorage.balanceweight + $localStorage.usedweight;
        //                     $localStorage.totalcontainervolumeforupgrade = $containervolume;
        //                     $localStorage.totalcontainerweightforupgrade = $containerweight;


        //                     Item.selectcontainer($localStorage.containetype, $containervolume, $containerweight);

        //                     Item.volumecapacity = $containervolume.toFixed(2);
        //                     Item.weightcapacity = $containerweight.toFixed(2);

        //                     $('#MyModel2').modal('show');
        //                 } else if (result == 'false') {
        //                     $('.ajax_loader').hide();
        //                     toastr.error('Item save failed');
        //                 } else if (result == 'mfalse') {
        //                     $('.ajax_loader').hide();
        //                     toastr.error('missing values');
        //                 } else if (result == 'saved') {
        //                     $localStorage.combinedcontainerid = "";
        //                     $localStorage.itemimage = "";
        //                     $('.ajax_loader').hide();
        //                     $('#itemno').val('');
        //                     $('#description').val('');
        //                     $('#altdescription').val('');
        //                     $('#priceinus').val('');
        //                     $('#unitpercarton').val('');
        //                     $('#unitqty').val('');
        //                     $('#ExchangeRate').val('');
        //                     $('#ExchangeRate3dCurrency').val('');
        //                     $('#cartonqty').val('');
        //                     //imageObj.src = '../../QA/assets/images/image_icon2.png';
        //                     //var imageObj = new Image();
        //                     var c = document.getElementById("snapshot");
        //                     var ctx = c.getContext("2d");
        //                     ctx.strokeRect(400, 300, 320, 240);
        //                     ctx.clearRect(0, 0, 320, 240);
        //                     c.width = 200;
        //                     c.height = 200;
        //                     Item.getpurchasecurrentbooth();
        //                     Item.getcontainerfilledpercentage();
        //                     Item.gettotalordervalue();
        //                     Item.selectusedcontainers();
        //                     $('.ajax_loader').hide();
        //                     toastr["success"]("Item added successfully");
        //                     Item.getcartonunits();
        //                 } else {
        //                     $('.ajax_loader').hide();
        //                     toastr.error('Item added failed');
        //                 }
        //             })
        //         } else {
        //             $('.ajax_loader').hide();
        //             toastr.error('Fill all the fields');
        //         }
        //     }

            /**function saveitem**/

            Item.saveitem = function() {
            $localStorage.flag = 1;
            if ($localStorage.combinedcontainerid == undefined || $localStorage.combinedcontainerid == "") {
                $localStorage.combinedcontainerid = $localStorage.containerid;
            } else {
                $localStorage.combinedcontainerid = $localStorage.combinedcontainerid + ',' + $localStorage.containerid;
            }

            $('.ajax_loader').show();
            Item.itemno = $('#itemno').val();
            Item.description = $('#description').val();
            Item.altdescription = $('#altdescription').val();
            Item.cartonGrossWeight = $('#cartonGrossWeight').val();
            Item.Length = $('#Length').val();
            Item.Width = $('#Width').val();
            Item.Height = $('#Height').val();
            Item.cbm = $('#cbm').val();
            Item.usprice = $('#priceinus').val();
            Item.unitpercarton = $('#unitpercarton').val();
            Item.unitqty = $('#unitqty').val();
            Item.priceRMB = $('#ExchangeRate').val();
            Item.ExchangeRate3dCurrency = $('#ExchangeRate3dCurrency').val();
            if (Item.ExchangeRate3dCurrency == undefined) {
                Item.ExchangeRate3dCurrency = '0';
            }
            Item.cartonqty = $('#cartonqty').val();
            Item.itemimage = $localStorage.itemimage;
            if (Item.itemimage == undefined || Item.itemimage == "") {
                Item.itemimage = 'default.jpg';
            }
            Item.orderid = $localStorage.orderid;
            Item.boothid = $localStorage.boothid;
            Item.containerid = $localStorage.containerid;
            Item.containertype = $localStorage.containetype;


            if (Item.orderid != "" && Item.boothid != "" && Item.containerid != "" && Item.itemno != "" && Item.description != "" && Item.cartonGrossWeight != "" && Item.Length != "" && Item.Width != "" && Item.Height != "" && Item.cbm != "" && Item.usprice != "" && Item.unitpercarton != "" && Item.unitqty != "" && Item.priceRMB != "" && Item.cartonqty != "" && Item.containertype != "") {

                OrderService.NewItem(Item.orderid, Item.boothid, Item.containerid, Item.itemno, Item.description, Item.altdescription, Item.cartonGrossWeight, Item.Length, Item.Width, Item.Height, Item.cbm, Item.usprice, Item.unitpercarton, Item.unitqty, Item.priceRMB, Item.ExchangeRate3dCurrency, Item.cartonqty, Item.itemimage, Item.containertype, Item.itemimage).success(function(response) {

                    var result = response.message;

                    if (result == 'true') {

                        $('.ajax_loader').hide();
                        Item.addnewcontainerlist();
                        if (response.usedvolume == 0 && response.usedweight == 0) {
                            Item.newcontainerlist = [];
                            //alert("the first container is not filled");
                        }
                        $localStorage.balancevolume = response.balancevolume;
                        $localStorage.balanceweight = response.balanceweight;
                        $localStorage.usedweight = response.usedweight;
                        $localStorage.usedvolume = response.usedvolume;
                        $localStorage.totalcontainervolume = response.totalcontainervolume;
                        $localStorage.totalcontainerweight = response.totalcontainerweight;
                        $localStorage.balancecarton        = response.balancecartons;
                        if($localStorage.balancevolume == $localStorage.totalcontainervolume)
                        {
                            $localStorage.flag = 0;
                        }
                        else
                        {
                            $localStorage.flag = 1;
                        }

                        OrderService.Checkfornotfilledcontainers(Item.orderid, $localStorage.balancevolume, $localStorage.balanceweight,$localStorage.usedweight,$localStorage.usedvolume,Item.cartonqty,Item.cbm,Item.cartonGrossWeight,$localStorage.balancecarton,Item.containertype,Item.containerid,$localStorage.flag).success(function(filled_response) {
                            //debugger
                            Item.containerisnotfilled = filled_response.message;
                            if (Item.containerisnotfilled == true) {
                            Item.unfilledcontainerlist = filled_response.data[0];
                            $localStorage.unfilledcontainerid = Item.unfilledcontainerlist;
                           
                                swal({
                                    title: 'There is an unfilled container found?',
                                    text: "Do yo want to fill this item into that container!",
                                    type: 'info',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    showLoaderOnConfirm: true,
                                    // allowOutsideClick: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, fill it!',
                                    cancelButtonText: 'No, continue to next container!',
                                }).then(function() {
                                    Item.updatecontainer();

                                    
                                }, function(dismiss) {

                                    if (dismiss === 'cancel') {
                                        $('#MyModel2').modal('hide');
                                        Item.popupshowbalancevolume = response.balancevolume.toFixed(2);
                                        Item.balancecartongs = response.balancecartons;

                                        $containervolume = $localStorage.balancevolume + $localStorage.usedvolume;
                                        $containerweight = $localStorage.balanceweight + $localStorage.usedweight;
                                        $localStorage.totalcontainervolumeforupgrade = $containervolume;
                                        $localStorage.totalcontainerweightforupgrade = $containerweight;


                                        Item.selectcontainer($localStorage.containetype, $containervolume, $containerweight);

                                        Item.volumecapacity = $containervolume.toFixed(2);
                                        Item.weightcapacity = $containerweight.toFixed(2);

                                        $('#MyModel2').modal('show');
                                    }
                                })

                            } else {

                                $('#MyModel2').modal('hide');
                                Item.popupshowbalancevolume = response.balancevolume.toFixed(2);
                                Item.balancecartongs = response.balancecartons;

                                $containervolume = $localStorage.balancevolume + $localStorage.usedvolume;
                                $containerweight = $localStorage.balanceweight + $localStorage.usedweight;
                                $localStorage.totalcontainervolumeforupgrade = $containervolume;
                                $localStorage.totalcontainerweightforupgrade = $containerweight;


                                Item.selectcontainer($localStorage.containetype, $containervolume, $containerweight);

                                Item.volumecapacity = $containervolume.toFixed(2);
                                Item.weightcapacity = $containerweight.toFixed(2);

                                $('#MyModel2').modal('show');
                            }

                        })
                        //Item.Checkfornotfilledcontainers($localStorage.balancevolume,$localStorage.balanceweight);


                    } else if (result == 'false') {
                        $('.ajax_loader').hide();
                        toastr.error('Item save failed');
                    } else if (result == 'mfalse') {
                        $('.ajax_loader').hide();
                        toastr.error('missing values');
                    } else if (result == 'saved') {
                        $localStorage.combinedcontainerid = "";
                        $localStorage.itemimage = "";
                        $('.ajax_loader').hide();
                        $('#itemno').val('');
                        $('#description').val('');
                        $('#altdescription').val('');
                        $('#priceinus').val('');
                        $('#unitpercarton').val('');
                        $('#unitqty').val('');
                        $('#ExchangeRate').val('');
                        $('#ExchangeRate3dCurrency').val('');
                        $('#cartonqty').val('');
                        //imageObj.src = '../../QA/assets/images/image_icon2.png';
                        //var imageObj = new Image();
                        var c = document.getElementById("snapshot");
                        var ctx = c.getContext("2d");
                        ctx.strokeRect(400, 300, 320, 240);
                        ctx.clearRect(0, 0, 320, 240);
                        c.width = 200;
                        c.height = 200;
                        Item.getpurchasecurrentbooth();
                        Item.getcontainerfilledpercentage();
                        Item.gettotalordervalue();
                        Item.selectusedcontainers();
                        $('.ajax_loader').hide();
                        toastr["success"]("Item added successfully");
                        Item.getcartonunits();
                    } else {
                        $('.ajax_loader').hide();
                        toastr.error('Item added failed');
                    }
                })
            } else {
                $('.ajax_loader').hide();
                toastr.error('Fill all the fields');
            }
        }
            /**end save item**/

        /**function save item sub function**/
        Item.subitemsave = function() {
                $('.ajax_loader').show();
                if ($localStorage.flag == 1) {
                    if ($localStorage.combinedcontainerid == undefined || $localStorage.combinedcontainerid == "") {
                        $localStorage.combinedcontainerid = $localStorage.containerid;
                    } else {
                        $localStorage.combinedcontainerid = $localStorage.combinedcontainerid + ',' + $localStorage.containerid;
                    }
                }

                Item.itemno = $('#itemno').val();
                Item.description = $('#description').val();
                Item.altdescription = $('#altdescription').val();
                Item.cartonGrossWeight = $('#cartonGrossWeight').val();
                Item.Length = $('#Length').val();
                Item.Width = $('#Width').val();
                Item.Height = $('#Height').val();
                Item.cbm = $('#cbm').val();
                Item.usprice = $('#priceinus').val();
                Item.unitpercarton = $('#unitpercarton').val();
                Item.unitqty = $('#unitqty').val();
                Item.priceRMB = $('#ExchangeRate').val();
                Item.ExchangeRate3dCurrency = $('#ExchangeRate3dCurrency').val();
                if (Item.ExchangeRate3dCurrency == undefined) {
                    Item.ExchangeRate3dCurrency = '0';
                }
                Item.cartonqty = $('#cartonqty').val();
                Item.itemimage = $localStorage.itemimage;
                if (Item.itemimage == undefined || Item.itemimage == "") {
                    Item.itemimage = 'default.jpg';
                }
                Item.orderid = $localStorage.orderid;
                Item.boothid = $localStorage.boothid;
                Item.containerid = $localStorage.combinedcontainerid;
                Item.containertype = $localStorage.containetype;
                Item.balanceweight = $localStorage.balanceweight;
                Item.balancevolume = $localStorage.balancevolume;
                if (Item.orderid != "" && Item.boothid != "" && Item.containerid != "" && Item.itemno != "" && Item.description != "" && Item.cartonGrossWeight != "" && Item.Length != "" && Item.Width != "" && Item.Height != "" && Item.cbm != "" && Item.usprice != "" && Item.unitpercarton != "" && Item.unitqty != "" && Item.priceRMB != "" && Item.cartonqty != "" && Item.containertype != "") {

                    // && Item.balanceweight != "" && Item.balancevolume != ""){

                    OrderService.SubNewItem(Item.orderid, Item.boothid, Item.containerid, Item.itemno, Item.description, Item.altdescription, Item.cartonGrossWeight, Item.Length, Item.Width, Item.Height, Item.cbm, Item.usprice, Item.unitpercarton, Item.unitqty, Item.priceRMB, Item.ExchangeRate3dCurrency, Item.cartonqty, Item.itemimage, Item.containertype, Item.balanceweight, Item.balancevolume).success(function(response) {
                        var result = response.message;
                        if (result == 'true') {
                            Item.addnewcontainerlist();
                            $localStorage.balancevolume = response.balancevolume;
                            $localStorage.balanceweight = response.balanceweight;
                            $localStorage.usedweight = response.usedweight;
                            $localStorage.usedvolume = response.usedvolume;
                            Item.popupshowbalancevolume = response.balancevolume;
                            Item.balancecartongs = response.balancecartons;
                            $containervolume = $localStorage.balancevolume + $localStorage.usedvolume;
                            $containerweight = $localStorage.balanceweight + $localStorage.usedweight;
                            $localStorage.totalcontainervolumeforupgrade = $containervolume;
                            $localStorage.totalcontainerweightforupgrade = $containerweight;
                            Item.selectcontainer($localStorage.containetype, $containervolume, $containerweight);
                            Item.volumecapacity = $containervolume;
                            Item.weightcapacity = $containerweight;
                            $timeout(function() {
                                $('.ajax_loader').hide();
                                $('#MyModel2').modal('show');
                            }, 3000);

                        } else if (result == 'false') {
                            $('.ajax_loader').hide();
                            toastr.error('Item save failed');
                        } else if (result == 'mfalse') {
                            $('.ajax_loader').hide();
                            $('#MyModel2').modal('hide');
                            toastr.error('missing values');
                        } else if (result == 'saved') {
                            $localStorage.combinedcontainerid = "";
                            $localStorage.itemimage = "";
                            $('#MyModel2').modal('hide');
                            $('#itemno').val('');
                            $('#description').val('');
                            $('#altdescription').val('');
                            $('#priceinus').val('');
                            $('#unitpercarton').val('');
                            $('#unitqty').val('');
                            $('#ExchangeRate').val('');
                            $('#ExchangeRate3dCurrency').val('');
                            $('#cartonqty').val('');
                            var c = document.getElementById("snapshot");
                            var ctx = c.getContext("2d");
                            ctx.strokeRect(400, 300, 320, 240);
                            ctx.clearRect(0, 0, 320, 240);
                            c.width = 200;
                            c.height = 200;
                            Item.getpurchasecurrentbooth();
                            Item.getcontainerfilledpercentage();
                            Item.gettotalordervalue();

                            Item.selectusedcontainers();
                            $('.ajax_loader').hide();
                            toastr["success"]("Item added successfully");
                            Item.getcartonunits();

                        } else {
                            toastr.error('Item added failed');
                        }
                    })
                } else {
                    $('.ajax_loader').hide();
                    toastr.error('Fill all the fields');
                }

            }
            /**end saveitem sub function**/

        /**function add booth**/
        Item.Addbooth = function() {
                Item.BoothError = false;
                $('.ajax_loader').show();
                Item.newBoothName = $('#newboothname').val();
                Item.newBoothNo = $('#newboothno').val();
                Item.newTelephone = $('#newtelephone').val();
                Item.newBusinessScope = $('#newbusinessscope').val();
                // Item.businesscard = $localStorage.businesscard;
                Item.TradeId = $('#trade').val();
                Item.HallId = $('#hall').val();
                if (Item.businesscard == undefined || Item.businesscard == "") {
                   if(Item.BoothImage!="image_icon2.png"&&Item.BoothImage!=""){
                        Item.businesscard=Item.BoothImage;
                   }
                   else{
                     Item.businesscard="";
                   }
                }

                Item.orderid = $localStorage.orderid;
                if (Item.newBoothNo != "" && Item.TradeId != -1 && Item.HallId != -1) {

                    OrderService.NewBooth(Item.orderid, Item.newBoothName, Item.newBoothNo, Item.newTelephone, Item.newBusinessScope, Item.businesscard, Item.TradeId, Item.HallId).success(function(response) {
                        $('.ajax_loader').hide();
                        try {
                            var result = JSON.parse(response);
                            if (result != 0) {
                                Item.BoothAdded = true;
                                Item.isNoBooth = false;
                                $localStorage.boothid = result;
                                $localStorage.businesscard = "";
                                $('#newboothname').val('');
                                $('#newboothno').val('');
                                $('#newtelephone').val('');
                                $('#newbusinessscope').val('');
                                $('#MyModel1').modal('hide');
                                Item.getpurchasecurrentbooth();
                                var c = document.getElementById("boothsnapshot");
                                var ctx = c.getContext("2d");
                                ctx.strokeRect(400, 300, 320, 240);
                                ctx.clearRect(0, 0, 320, 240);
                                c.width = 200;
                                c.height = 200;
                                toastr["success"]("Booth added successfully");
                                Item.businesscard="";
                                if (Item.notSaved) {
                                    Item.saveitem();
                                }
                            } else {
                                toastr.error('Booth added failed');
                            }
                        } catch (e) {
                            toastr.error('Booth added failed');
                            console.log(e);
                        }
                    })
                } else {
                    $('.ajax_loader').hide();
                    Item.BoothError = true;
                    if (Item.TradeId == -1) {
                        Item.BoothTrade = true;
                    }
                    if (Item.HallId == -1) {
                        Item.BoothHall = true;
                    }
                }
            }
            /**end add booth function **/

        /**end order conformation function**/
        $('.endorder').click(function() {
            swal({
                title: 'Are you sure?',
                text: "Your will not be able to continue this order!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, End it!'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    swal(
                        "Ended!", "Your order has been ended.", "success"
                    )
                    var delay = 1500; //1 second
                    setTimeout(function() {
                        endorder();
                        window.location.href = "../Login/newOrder";
                    }, delay);
                } else {
                    swal("Cancelled", "Your order is safe :)", "error");
                }
            })

            
            
        });
        /**end endorder function **/

        /**function end order**/
        function endorder() {
            $('.ajax_loader').show();
            var orderid = $localStorage.orderid;
            
            OrderService.EndOrder(orderid).success(function(response) {
                $('.ajax_loader').hide();
                window.location.href = "../Login/newOrder";

            })
        }
        /*end function end order*/

        /*pause order function */
        $('.pauseorder').click(function() {
            swal({
                    title: 'Are you sure?',
                    text: "Your will be able to continue this order from order list!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Pause it!'
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        swal(
                            "Paused!", "Your order has been paused.", "success"
                        )
                        var orderid = $localStorage.orderid;
                        var url="../Login/vieworder?id="+orderid+"&status=0"
                        var delay = 1500; //1 second
                        setTimeout(function() {
                            window.location.href = url;
                        }, delay);
                    } else {
                        swal("Cancelled", "Your order is safe :)", "error");
                    }
                })
                // swal({
                //         title: "Are you sure?",
                //         text: "Your will be able to continue this order from order list!",
                //         type: "warning",
                //         showCancelButton: true,
                //         confirmButtonColor: "#DD6B55",
                //         confirmButtonText: "Yes, Pause it!",
                //         cancelButtonText: "No, cancel!"
                //         // closeOnConfirm: false,
                //         // closeOnCancel: false
                //     },
                //     function(isConfirm) {
                //         if (isConfirm) {
                //             swal("Paused!", "Your order has been paused.", "success");
                //             var delay = 1500; //1 second
                //             setTimeout(function() {
                //                 window.location.href = "../Login/newOrder";
                //             }, delay);

            //         } else {
            //             swal("Cancelled", "Your order is safe :)", "error");
            //         }
            //     });
        });
        /**end pause order function**/

        /**function view all the order details**/
        Item.Viewallorder = function() {
                //  $('.ajax_loader').show();
                OrderService.Viewallorder().success(function(response) {
                    // $('.ajax_loader').hide();
                    Item.result = response;
                })

            }
            // Item.Viewallorder();
            /**end view all the function**/
        Item.Vieworderitems = function() {
                // $('.ajax_loader').show();
                var i = 0;
                Item.orderid = $('#orderid').val();
                OrderService.Viewallorderitems(Item.orderid).success(function(response) {
                    Item.Loadorderdetails = true;
                    // $('.ajax_loader').hide();
                    // Item.orderitems = response.data;
                    // Item.containerid = response.containerid;
                    if (response.order.length > 0) {
                        var orderitems = response.orderitems;
                        var order = response.order;
                        var containerid = response.containerid;
                        for (var i = 0; i < orderitems.length; i++) {
                            orderitems[i].containername = containerid[i];

                        }
                        Item.orderitems = orderitems;
                        Item.containerid = containerid;
                        Item.vieworderid = order[0].OrderId;
                        Item.item_ordernumber = order[0].OrderNumber;
                        Item.item_orderusvalue = order[0].TotalOrderUsValue;
                        Item.item_orderrmbvalue = order[0].TotalOrderRmbValue;
                        Item.item_orderdate = order[0].LastUpdate;
                        Item.item_totalcbm = order[0].TotalOrderCBM;
                        Item.item_totalweight = order[0].TotalOrderWeight;
                        Item.item_orderid = order[0].OrderId;
                    } else {
                        console.log("No data");
                    }
                })
            }
            // Item.Vieworderitems();
            /**end function view all function**/

        /**function view order details**/
        Item.ViewOrderdetails = function() {
            // $('.ajax_loader').show();
            Item.orderid = $localStorage.orderid;
            OrderService.Vieworderdetails(Item.orderid).success(function(response) {
                // $('.ajax_loader').hide();
                Item.totalordervalueus = response[0].TotalOrderUsValue;
                Item.totalordervaluermb = response[0].TotalOrderRmbValue;
                Item.date = response[0].LastUpdate;
                Item.totalcbm = response[0].TotalOrderCBM;
                Item.totalweight = response[0].TotalOrderWeight;
                Item.status = response[0].OrderStatus;

            })
        }
        Item.ViewOrderdetails();
        /**end view order details**/

        /**functionget total container**/
        Item.Gettotalcontainer = function() {
            // $('.ajax_loader').show();
            Item.orderid = $('#orderid').val();

            OrderService.Getcontainer(Item.orderid).success(function(response) {
                // $('.ajax_loader').hide();
                Item.container = response;
            })
        }

        Item.Gettotalcontainer();
        /**end view total container function **/

        /**contioue order **/
        Item.Contioueorder = function(orderid) {
            // $('.ajax_loader').show();
            Item.orderid = orderid;
            $localStorage.orderid = orderid;
            OrderService.Getallorderdetails(Item.orderid).success(function(response) {
                // $('.ajax_loader').hide();
                if (response.message == true) {
                    $localStorage.boothid = response.data[0].BoothId;
                    $localStorage.orderno = response.data[0].OrderNumber;
                    $localStorage.orderid = response.data[0].OrderId;
                    window.location.href = "../Login/item";

                }
            })
        }
        Item.printorderid = function() {

            $localStorage.printorderid = $('#orderid').val();
        }

        /*end contioue order*/
        Item.printorder = function() {

                window.location.href = "../Login/printorder";

            }
            /**invoice order**/
        Item.printorderview = function() {
                // $('.ajax_loader').show();
                Item.orderid = $localStorage.printorderid;
                OrderService.Orderinvoice(Item.orderid).success(function(response) { //$('.ajax_loader').hide(); //document.getElementById(id_printpage).contentDocument.location.reload(true);
                    var result = response.message;
                    if (result == true) {
                        Item.Order = response.data;
                        Item.Order.ThirdCurrency = response.extractedthirdcurrencyvalue;
                    }

                })
            }
            //  Item.printorderview();
        Item.EmailOrder = function() {
                $('.ajax_loader').show();
                Item.orderid = $localStorage.printorderid;

                OrderService.checkemailitems(Item.orderid).success(function(response) {

                    var result = response.message;
                    if (result == "itemsfound") {

                        OrderService.emailinvoice(Item.orderid).success(function(response) {
                            $('.ajax_loader').hide();
                            var result = response.message;
                            //var result = JSON.parse(response);

                            if (result == "success") {

                                toastr["success"]("Mail has been sent");

                            } else {

                                toastr.error('Mail Senting Failed');

                            }

                        })
                    } else if (result == "noitem") {
                        $('.ajax_loader').hide();
                        toastr.error('Please add some items before senting email');
                    } else {
                        $('.ajax_loader').hide();
                        toastr.error('Something bad happend');
                    }
                 })
        }

            /*select used contianer*/
        Item.selectusedcontainers = function() {
                Item.orderid = $localStorage.orderid;
                OrderService.userdcontainers(Item.orderid).success(function(response) {
                    var result = response.message;
                    if (result == true) {
                        Item.containers = response.data;
                        Item.activecontainer = response.data[0];
                    } else {
                        toastr["error"]("No container selected");
                    }
                })

            }
            /*end*/
        Item.LoadModal = function() {
            if (!Item.isNoBooth) {
                $('#MyModel1').modal({ backdrop: false });
            } else {
                $('#MyModel1').modal({ backdrop: 'static', keyboard: false });
            }

            $("#businesscardimage").css("display", "none");
            $("#businesscardcamera").css("display", "block");
        }
        Item.contianerlist = function() {

                OrderService.containerlist().success(function(response) {
                    if (response.message == true) {
                        Item.containerlist = response.data;
                        Item.addnewcontainer = response.data[0];
                        Item.maincontainerlist = Item.containerlist;
                        Item.newcontainerlist = Item.containerlist;
                    }
                })
            }
            /*filter new containerlist */
        Item.addnewcontainerlist = function() {
            Item.tempContainerlist = [];
            var cartoncbm = parseFloat(Item.cbm, 10);
            var cartonweight = parseFloat(Item.cartonGrossWeight, 10);
            angular.forEach(Item.maincontainerlist, function(value, key) {
                var maxvolume = parseFloat(value.MaximumVolume, 10);
                var maxweight = parseFloat(value.MaximumWeight, 10);
                if (maxvolume < cartoncbm || maxweight < cartonweight) {

                } else {
                    Item.tempContainerlist.push(value);
                }
            });
            Item.newcontainerlist = Item.tempContainerlist;
            Item.addnewcontainer = Item.newcontainerlist[0];
        }

        Item.containerpercentage = function() {
            // alert("container select");
        }

        $scope.$watch('Item.activecontainer.ContainerId', function() {
            if (Item.activecontainer != undefined) {
                var val_contaierid = Item.activecontainer.ContainerId;
                Item.showactivecontainerstatus(val_contaierid);
            }
        });

        Item.showactivecontainerstatus = function(activecontainerid) {
            Item.orderid = $localStorage.orderid;
            Item.activecontainerid = activecontainerid;
            OrderService.showactivecontainer(Item.orderid, Item.activecontainerid).success(function(response) {
                var result = response.message;
                if (result == true) {
                    var val_volumepercentage = Number(response.data.Volumepercentage);
                    var val_weightpercentage = Number(response.data.Weightpercentage);
                    if (val_volumepercentage > val_weightpercentage) {
                        Item.volumepercentage = response.data.Volumepercentage;
                    } else if (val_volumepercentage < val_weightpercentage) {
                        Item.volumepercentage = response.data.Weightpercentage;
                    } else if (val_volumepercentage == val_weightpercentage) {
                        Item.volumepercentage = response.data.Weightpercentage;
                    } else {
                        Item.volumepercentage = response.data.Weightpercentage;
                    }
                } else {
                    toastr["error"](" Test No container selected");
                }
            })

        }

        /*web camera*/
        Item.channel = {};
        Item.webcamError = false;
        Item.Webcamerrormessage = true;
        Item.HideCaptureButton = false;
        //Item.Webcamerrormessage = false;
        Item.onError = function(err) {
            $scope.$apply(
                function() {
                    Item.webcamError = err;
                    Item.Webcamerrormessage = true;
                    Item.HideCaptureButton = false;
                    // toastr["warning"]("Failed to allocate videosource");
                }
            );
        };
        Item.onStream = function(stream) {};
        Item.onSuccess = function() {
            _video = $scope.channel.video;
            $scope.$apply(function() {
                Item.Webcamerrormessage = false;
                Item.HideCaptureButton = true;
                $scope.patOpts.w = _video.width;
                $scope.patOpts.h = _video.height;
                //$scope.showDemos = true;
            });
        };

        Item.makeSnapshot = function() {

            if (_video) {

                var patCanvas = document.querySelector("#snapshot");
                if (!patCanvas) return;

                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                sendSnapshotToServer(patCanvas.toDataURL());

                patData = idata;
            }
        };

        var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
            $scope.snapshotData = imgBase64;
            Item.imagebase64 = imgBase64;
            var imagestring = imgBase64.replace(/^data:image\/(png|jpg);base64,/, "");
            var imgData = JSON.stringify(imagestring);
            $localStorage.itemimage = imgData;

        };

        Item.makeBoothSnapshot = function() {


            $("#businesscardcamera").css("display", "none");
            $("#businesscardimage").css("display", "block");

            if (_video) {

                var patCanvas = document.querySelector("#boothsnapshot");
                if (!patCanvas) return;

                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                sendBoothSnapshotToServer(patCanvas.toDataURL());

                patData = idata;

            }
        };

        var sendBoothSnapshotToServer = function sendSnapshotToServer(imgBase64) {
            $scope.snapshotData = imgBase64;

            Item.imagebase64 = imgBase64;
            var imagestring = imgBase64.replace(/^data:image\/(png|jpg);base64,/, "");
            var imgData = JSON.stringify(imagestring);
            $localStorage.businesscard = imgData;
            Item.businesscard = imgData
        };


        var getVideoData = function getVideoData(x, y, w, h) {
            var hiddenCanvas = document.createElement('canvas');
            hiddenCanvas.width = _video.width;
            hiddenCanvas.height = _video.height;
            var ctx = hiddenCanvas.getContext('2d');
            ctx.drawImage(_video, 0, 0, _video.width, _video.height);
            return ctx.getImageData(x, y, w, h);
        };

        /*end*/

        /*get user company details*/
        Item.selectusercompanydetails = function() {
                OrderService.SelectUserCompanyDetails().success(function(response) {
                    var result = response.message;
                    if (result == true) {
                        $localStorage.languagefrom = response.data[0].TransilateFrom;
                        $localStorage.languageto = response.data[0].TransilateTo;
                        if ($localStorage.languagefrom == "" && $localStorage.languageto == "") {
                            $localStorage.languagefrom = "en";
                            $localStorage.languageto = "zh";
                        }
                    }
                });
            }
            /*end*/

        Item.text_transilate = function() {
            Item.text = $('#description').val();
            Item.source = $localStorage.languagefrom;
            Item.target = $localStorage.languageto;
            if (Item.source == Item.target) {
                Item.altdescription = Item.text;
            }
            OrderService.languagetrasilate(Item.source, Item.target, Item.text).success(function(response) {
                Item.altdescription = response.data.translations[0].translatedText;

            });
        }
        Item.changeboothpicture = function() {
            $("#businesscardcamera").css("display", "block");
            $("#businesscardimage").css("display", "none");
        }

        Item.getBoothInfoItem = function() {
            if (Item.newboothno != "" && Item.newboothno != undefined && Item.newboothno != null) {
                OrderService.getBoothInfo(Item.newboothno).success(function(response) {
                    if (response.message == "true") {
                        Item.newboothname = response.boothinfo.BoothName;
                        Item.newboothtelephone = response.boothinfo.Phone;
                        Item.newbusinesscope = response.boothinfo.BusinessScope;
                        Item.BoothImage = response.boothinfo.BusinessCard;
                        Item.IsCamera = false;
                        Item.changeboothpicture();
                    } else {
                        Item.newboothname = "";
                        Item.newboothtelephone = "";
                        Item.newbusinesscope = "";
                        Item.BoothImage = "image_icon2.png";
                        console.log(response.comment);
                    }

                });
            }
        }
        Item.selectcontainer = function(containertype, volume, weight) {

            Item.volume = volume;
            Item.weight = weight;
            Item.contianertype = containertype;

            if (Item.contianertype != "") {
                OrderService.getContainertypes(Item.contianertype, Item.volume, Item.weight).success(function(container_response) {

                    if (container_response.message == true) {

                        Item.upgradecontainerlist = container_response.data;
                        Item.addupgradecontainer = container_response.data[0];
                        Item.currentcotainername = container_response.currentcontainer;
                    } else {
                        Item.upgradecontainerlist = [];
                        Item.addupgradecontainer = "Container Upgrade is not possible";
                    }
                });
            }
        }


        /*check booth is select or not*/
        Item.checkboothIsselect = function() {
            Item.continouseorderid = $localStorage.orderid;
            if (Item.continouseorderid != "") {
                OrderService.checkboothIsset(Item.continouseorderid).success(function(boothcheck_response) {
                    Item.PageReady = true;
                    if (boothcheck_response.message == false) {
                        toastr["error"]("Booth is not selected, Choose a valid booth");
                        Item.isNoBooth = true;
                        Item.LoadModal();
                    } else {
                        $localStorage.boothid = boothcheck_response.data[0].BoothId;
                        Item.getpurchasecurrentbooth();
                        Item.BoothAdded = true;
                        Item.isNoBooth = false;
                    }

                });
            }
        }


        /*end*/
        Item.checkBoothSaved = function(form) {
            Item.addItemError = false;
            if (form.$invalid) {

                Item.addItemError = true;

            } else {
                if (Item.BoothAdded) {
                    Item.saveitem();
                } else {
                    Item.notSaved = true;
                    toastr["error"]("Booth is not selected, Choose a valid booth");
                    Item.LoadModal();
                }
            }
        }
        Item.calculateunitsqty = function() {
            Item.unitqty = Item.cartonqty * Item.unitpercarton;
        }

        Item.ignorecontainer = function() {
                Item.ignoreorderid = $localStorage.orderid;
                $localStorage.containerid = $localStorage.firstcontainerid;
                Item.firstcontainerid = $localStorage.firstcontainerid;
                if (Item.continouseorderid != "") {
                    OrderService.Ignorcontianer(Item.ignoreorderid, Item.firstcontainerid).success(function(response) {
                        console.log(response);
                    });

                } else {
                    toastr["error"]("Invalid order");
                }
            }
            /*Abhijith A Nair's Code */
        Item.getdefaultCartonValues = function() {
            OrderService.getCartonvalues().success(function(response) {

                Item.cartonGrossWeight = Number(response.GrossWeight); //store carton gross weight from response
                Item.Length = Number(response.Length); // store carton length from response
                Item.Height = Number(response.Height); // store carton height from response
                Item.Width = Number(response.Width); //  store carton width from response
                Item.cbm = Item.Length * Item.Height * Item.Width * 0.000001;

            });
        }
        Item.getTrades = function() {
            OrderService.getTradelist().success(function(response) {
                if (response.message == "true") {
                    Item.Trades = response.tradelist;
                    if (response.tradelist.length > 0) {
                        Item.Trade = Item.Trades[0];
                        Item.getHalls();
                    }
                } else {
                    console.log(response.comment);
                    Item.Trades = [{
                        "Id": -1,
                        "Name": "No Result"
                    }];

                    Item.Trade = Order.Trades[0];

                }

            });
        }

        /* function get halls on trade */
        Item.getHalls = function() {
            Item.Halls = [{
                "Id": -1,
                "Name": "Loading..."
            }];
            if (Item.Trade != undefined) {
                if (Item.Trade.Id > 0) {
                    OrderService.getHalllist(Item.Trade.Id).success(function(response) {
                        if (response.message == "true") {
                            Item.Halls = response.halllist;
                            if (response.halllist.length > 0) {
                                Item.Hall = response.halllist[0];
                            }
                        } else {
                            console.log(response.comment);
                            Item.Halls = [{
                                "Id": -1,
                                "Name": "No Result"
                            }];
                            Item.Hall = Order.Halls[0];

                        }

                    });
                } else {
                    Item.Halls = [{
                        "Id": -1,
                        "Name": "No Result"
                    }];
                    Item.Hall = Order.Halls[0];
                }
            }
        }
        Item.addTrade = function() {
            $('#MyModel1').modal('hide');
            swal({
                title: 'Enter your own Trade District/Area',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: function(trade) {
                    return new Promise(function(resolve, reject) {
                        if (trade === '') {
                            reject('Please Enter Trade District/Area')
                        } else {
                            var parent = 0;
                            OrderService.saveCustomTradeOrHall(trade, parent).success(function(response) {
                                if (response.message == "true") {
                                    var tempVal = {
                                        Id: response.Id,
                                        Name: trade,
                                    }
                                    Item.Trades.push(tempVal);
                                    Item.Trade = tempVal;

                                    resolve()
                                } else {
                                    resolve()
                                    console.log(response.comment);

                                }
                            });

                        }
                    })
                },
                allowOutsideClick: false
            }).then(function(trade) {
                swal({
                    type: 'success',
                    title: 'Success!',
                    html: 'New Trade Added'
                })
                $('#MyModel1').modal();
            }, function(dismiss) {
                $('#MyModel1').modal();
            })
        }

        /*Open popup for enter custom hall */
        Item.addHall = function() {
            $('#MyModel1').modal('hide');
            swal({
                title: 'Enter your own Hall',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: function(hall) {
                    return new Promise(function(resolve, reject) {
                        if (hall === '') {
                            reject('Please Enter Hall')
                        } else {
                            if (Item.Trade != undefined) {
                                if (Item.Trade.Id > 0) {
                                    var parent = Item.Trade.Id
                                    OrderService.saveCustomTradeOrHall(hall, parent).success(function(response) {
                                        if (response.message == "true") {

                                            var tempVal = {
                                                Id: response.Id,
                                                Name: hall,
                                            }
                                            Item.Hall = tempVal;
                                            resolve()
                                            Item.Halls.push(tempVal);
                                        } else {
                                            console.log(response.comment);
                                            resolve()

                                        }
                                    });
                                }
                            } else {
                                resolve()
                            }

                        }
                    })
                },
                allowOutsideClick: false
            }).then(function(trade) {
                swal({
                    type: 'success',
                    title: 'Success!',
                    html: 'New Hall Added'
                })
                $('#MyModel1').modal();
            }, function(dismiss) {
                $('#MyModel1').modal();
            })
        }
        $scope.$watch('Item.Trade.Id', function() {
            Item.getHalls();
        });
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollX', '100%');
    })
    ($scope.Item);
}]);
