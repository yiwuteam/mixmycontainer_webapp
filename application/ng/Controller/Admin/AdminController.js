adminmixMyContainer.controller('AdminController', ['$scope', '$rootScope', 'AdminService', '$window', '$location', '$http', '$timeout', function($scope, $rootScope, AdminService, $window, $location, $http, $timeout) {
    $scope.Admin = {};
    (function(Admin) {
        Admin.GoldAgentContacts=[{'Contact':'1'},{'Contact':'2'},{'Contact':'3'},{'Contact':'4'},{'Contact':'5'}];
        // Admin.GoldAgentContacts = [{ 'Contact': '1' }];
        Admin.FaxNumber = "";
        Admin.BussinessYear = "";
        Admin.GoldYear = "";
        Admin.Trades = [{
            "Id": -1,
            "Name": "Loading..."
        }];
        Admin.TradeId = Admin.Trades[0];
        Admin.LoginCheck = function() {
            Admin.loginLoading = true;
            AdminService.getLogin(Admin.UserName, Admin.Password).success(function(response) {
                if (response.message == "true") {
                    toastr["success"]("", "Welcome " + Admin.UserName + " to MixMyContainer");
                    window.location.href = "../../QA/Admin/manage";
                    //   window.location('dashboard');
                } else {
                    swal(
                        'Oops...',
                        'You entered an invalid username or password',
                        'error'
                    )
                    //console.log(response.comment);
                }
                Admin.loginLoading = false;
            })
        }
        Admin.AddGoldAgentContact = function() {
            var newItemNo = Admin.GoldAgentContacts.length + 1;
            Admin.GoldAgentContacts.push({ 'Contact': newItemNo });
        }
        Admin.RemoveGoldAgentContact = function() {
            if (Admin.GoldAgentContacts.length > 5) {
                var lastindex = Admin.GoldAgentContacts.length - 1;
                Admin.GoldAgentContacts.splice(lastindex, 1);
            }
        }
        Admin.AddGoldAgent = function() {
            $('.ajax_loader').show();
            Admin.golderror = false;
            Admin.agenterror = true;
            angular.forEach(Admin.GoldAgentContacts, function(value, key) {
                if (value.Name == undefined || value.Mobile == undefined || value.Email == undefined || value.Name == "" ||
                    value.Mobile == "" || value.Email == "") {
                    Admin.golderror = true;
                    Admin.agenterror = false;
                }
                if (value.Skype == undefined) {
                    value.Skype = "";
                }
                if (value.Image == undefined||value.Image=="") {
                    value.Image = ""; 
                } else  if(value.Image.compressed){
                    var imgstring = value.Image.compressed.dataURL;
                    imgstring = imgstring.replace(/^data:image\/[a-z]+;base64,/, "");
                    value.Image = imgstring;
                }
            });
            if (Admin.agenterror && Admin.GoldAgentName != "" && Admin.GoldAgentName != undefined && Admin.AgentOffice != undefined && Admin.AgentOffice != "" && Admin.CompanyPhone != undefined && Admin.CompanyPhone != "") {
                AdminService.saveGoldAgent(Admin.GoldAgentName, Admin.AgentOffice, Admin.CompanyPhone, Admin.GoldAgentContacts, Admin.FaxNumber, Admin.BussinessYear, Admin.GoldYear).success(function(response) {
                    $('.ajax_loader').hide();
                    if (response.message == "true") {
                        toastr["success"]("", "Gold Agent added successfully")
                        $scope.goldagent_form.$setPristine();
                        $scope.goldagent_form.$setUntouched();
                        Admin.GoldAgentName = "";
                        Admin.AgentOffice = "";
                        Admin.CompanyPhone = "";
                        Admin.FaxNumber = "";
                        Admin.BussinessYear = "";
                        Admin.GoldYear = "";
                        Admin.GoldAgentContacts = [{ 'Contact': '1' }, { 'Contact': '2' }, { 'Contact': '3' }, { 'Contact': '4' }, { 'Contact': '5' }];
                    } else {
                        toastr.error('Somethig bad happend, please try again');
                    }

                });

            } else {
                Admin.golderror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.getTrades = function() {
            AdminService.getTradelist().success(function(response) {
                if (response.message == "true") {
                    Admin.Trades = response.tradelist;
                    if (response.tradelist.length > 0) {
                        Admin.TradeId = Admin.Trades[0];
                    }
                } else {
                    //console.log(response.comment);
                    Admin.Trades = [{
                        "Id": -1,
                        "Name": "No Result"
                    }];

                    Admin.TradeId = Admin.Trades[0];

                }

            });
        }
        Admin.AddTrade = function() {
            $('.ajax_loader').show();
            Admin.tradeerror = false;
            if (Admin.Tradename != "" && Admin.Tradename != undefined) {
                trade = Admin.Tradename;
                parent = 0;
                AdminService.saveTradeOrHall(trade, parent).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Trade added successfully")
                        Admin.Tradename = "";
                        $scope.trade_form.$setPristine();
                        $scope.trade_form.$setUntouched();
                        var tempVal = {
                            Id: response.Id,
                            Name: trade,
                        }
                        Admin.Trades.push(tempVal);

                    } else {
                        if (response.comment == "Exists") {
                            toastr.error('Already Saved, please try another');

                        } else {
                            toastr.error('Somethig bad happend, please try again');
                            //console.log(response.comment);
                        }
                        $('.ajax_loader').hide();
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.tradeerror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.AddHall = function() {
            $('.ajax_loader').show();
            Admin.hallerror = false;
            if (Admin.HallName != "" && Admin.HallName != undefined && Admin.TradeId != undefined && Admin.TradeId.Id != "-1") {
                hall = Admin.HallName;
                parent = Admin.TradeId.Id;

                AdminService.saveTradeOrHall(hall, parent).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Hall added successfully")
                        Admin.HallName = "";
                        $scope.hall_form.$setPristine();
                        $scope.hall_form.$setUntouched();
                    } else {
                        if (response.comment == "Exists") {
                            toastr.error('Already Saved, please try another');

                        } else {
                            toastr.error('Somethig bad happend, please try again');
                            //console.log(response.comment);
                        }
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.hallerror = true;
                if (Admin.TradeId == undefined || Admin.TradeId.Id == "-1") {
                    Admin.tradeiderror = true;
                }
                $('.ajax_loader').hide();
            }
        }
        Admin.AddContainer = function() {
            $('.ajax_loader').show();
            Admin.conterror = false;
            if (Admin.Container != "" && Admin.Container != undefined && Admin.MaxVolume != undefined && Admin.MaxVolume != "" && Admin.MaxWeight != "" && Admin.MaxWeight != undefined) {
                AdminService.saveContainer(Admin.Container, Admin.MaxVolume, Admin.MaxWeight).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Container added successfully")

                        Admin.Container = "";
                        Admin.MaxVolume = "";
                        Admin.MaxWeight = "";
                        $scope.container_form.$setPristine();
                        $scope.container_form.$setUntouched();
                    } else {
                      if(response.comment=="Exists"){
                             toastr.error('Conatiner already exists');
                        }
                        else{
                            toastr.error('Somethig bad happend, please try again');
                        }
                        console.log(response.comment);
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.conterror = true;

                $('.ajax_loader').hide();
            }
        }
        Admin.ChangePassword = function(form) {
            Admin.Passerror = false;
            Admin.compareerror = false;
            if (form.$valid && Admin.NewPassword == Admin.ConfirmPassword) {
                Admin.passwordLoading = true;
                AdminService.ChangePassword(Admin.NewPassword, Admin.OldPassword).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Password changed successfully")
                        Admin.NewPassword = "";
                        Admin.OldPassword = "";
                        Admin.ConfirmPassword = "";
                        form.$setPristine();
                        form.$setUntouched();
                        $('#passwordmodal').modal('hide');
                    } else {
                        if (response.comment == "Old Password Wrong") {
                            toastr.error('You entered a wrong old password');
                        } else {
                            toastr.error('Error occured please try after some time');
                        }

                    }
                    Admin.passwordLoading = false;
                });
            } else {
                if (Admin.NewPassword != Admin.ConfirmPassword) {
                    Admin.Compareerror = true;
                }
                Admin.Passerror = true;
            }
        }
        Admin.GetDefaultData=function()
        {
             AdminService.getDefaultValues().success(function(response) {

                if(response.message=="true")
                {
                    Admin.Default=response.default;
                    Admin.Default.GrossWeight=parseFloat(Admin.Default.GrossWeight, 10);
                    Admin.Default.Length=parseFloat(Admin.Default.Length, 10);
                    Admin.Default.Width=parseFloat(Admin.Default.Width, 10);
                    Admin.Default.Height=parseFloat(Admin.Default.Height, 10);
                    Admin.Default.ExchangeRate=parseFloat(Admin.Default.ExchangeRate, 10);
                }
                else
                {
                    console.log(response.comment);
                }
                var $target=$("#default_form");
                $target.find('input').removeClass('loading');
             });
        }
        Admin.UpdateDefault = function(form) {
            $('.ajax_loader').show();
            Admin.deferror = false;
            if (form.$valid) {
               
                AdminService.updateDefault(Admin.Default).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Default values updated")
                        Admin.Tradename = "";
                        form.$setPristine();
                        form.$setUntouched();
                    } else {
                        toastr.error('Somethig bad happend, please try again');
                        console.log(response.comment);
                        $('.ajax_loader').hide();
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.deferror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.ClearPasswordModel=function()
        {
            Admin.OldPassword="";
            Admin.NewPassword="";
            Admin.ConfirmPassword="";
            $scope.pass_form.$setPristine();
            $scope.pass_form.$setUntouched();
            $scope.pass_form.$invalid=false;
            $scope.pass_form.confirmpass.$error.passwordVerify=false;
        }
        Admin.GetDefaultData();
    })
    ($scope.Admin);

}]);
