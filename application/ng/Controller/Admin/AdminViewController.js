adminmixMyContainer.controller('AdminViewController', ['$scope', '$rootScope', 'AdminService', '$window', '$location', '$http', '$timeout', function($scope, $rootScope, AdminService, $window, $location, $http, $timeout) {
    $scope.Admin = {};
    (function(Admin) {
        Admin.Trades = [];
        Admin.Halls = [];
        Admin.Containers = [];
        Admin.LoadingContainer = true;
        Admin.LoadingTrades = true;
        Admin.LoadingHalls = true;
        Admin.AgentPageNo = 0;
        Admin.UserPageNo = 0;
        Admin.ScrollLoader = false;
        Admin.GoldAgents = [];
        Admin.Users = [];
        Admin.AgentContentLength = 12;
        Admin.UserContentLength = 12;
        Admin.GetGoldAgents = function() {
            if (!Admin.ScrollLoader && !Admin.AgentLoader) {
                Admin.AgentLoader = true;
                Admin.ScrollLoader = true;
                AdminService.getGoldAgents(Admin.AgentPageNo).success(function(response) {
                    Admin.AgentLoader = false;
                    if (response.message == "true") {
                        Admin.AgentPageNo = Admin.AgentPageNo + Admin.AgentContentLength;
                        angular.forEach(response.goldagents, function(val, key) {
                            Admin.GoldAgents.push(val);
                        });
                        if (response.goldagents.length == Admin.AgentContentLength) {
                            Admin.ScrollLoader = false;
                        }

                    } else {
                        console.log(response.comment);
                    }

                })
            }
        }
        Admin.GetTrades = function() {
            //Loader Starts
            Admin.LoadingTrades = true;
            AdminService.getTradelist().success(function(response) {
                //Loader Ends
                Admin.LoadingTrades = false;
                if (response.message == "true") {
                    //Success
                    Admin.Trades = response.tradelist;
                    Admin.TradeId = Admin.Trades[0];
                } else {
                    //No Data Recieved
                    console.log(response.comment);
                }
            })
        }
        Admin.GetHalls = function() {
            //Loader Starts
            Admin.LoadingHalls = true;
            AdminService.getHalllist().success(function(response) {
                //Loader Ends
                Admin.LoadingHalls = false;
                if (response.message == "true") {
                    //Success
                    Admin.Halls = response.halllist;

                } else {
                    //No Data Recieved
                    console.log(response.comment);
                }
            })
        }
        Admin.GetContainers = function() {
            //Loader Starts
            Admin.LoadingContainer = true;
            AdminService.getContainerist().success(function(response) {
                //Loader Ends
                Admin.LoadingContainer = false;
                if (response.message == "true") {
                    //Success
                    Admin.Containers = response.containerlist;

                } else {
                    //No Data Recieved
                    Admin.Containers = [];
                    console.log(response.comment);
                }
            })
        }
        Admin.EditContainer = function(ID) {
            angular.forEach(Admin.Containers, function(value, key) {
                if (value.ID == ID) {
                    Admin.Container = value.Container;
                    Admin.MaxVolume = value.MaximumVolume;
                    Admin.MaxWeight = value.MaximumWeight;
                    Admin.ContainerId = value.ID;
                }
            });
            $('#editContainer').modal();
        }
        Admin.DeleteContainer = function(ID) {
            Admin.ContainerId = ID;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function(ID) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deletContainer(Admin.ContainerId).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var Id = "#container_" + Admin.ContainerId;
                                $(Id).remove();
                                swal(
                                    'Deleted!',
                                    'Container has been deleted.',
                                    'success'
                                )

                            } else {
                                console.log(response.comment);
                                if (response.comment == "Not Allowed") {
                                    swal(
                                            'Error!',
                                            "Container can't delete because it's already used",
                                            'error'
                                        )
                                        // reject("Container can't delete because it's already used")
                                } else {
                                    reject("Something bad happend, Try Again!")
                                }
                            }
                        })
                    })
                },
            }).then(function(trade) {
                // swal(
                //     'Deleted!',
                //     'Container has been deleted.',
                //     'success'
                // )
            })
        }
        Admin.UpdateContainer = function(form) {
            $('.ajax_loader').show();
            Admin.conterror = false;
            if (Admin.Container != "" && Admin.Container != undefined && Admin.MaxVolume != undefined && Admin.MaxVolume != "" && Admin.MaxWeight != "" && Admin.MaxWeight != undefined && form.$valid) {
                AdminService.updateContainer(Admin.ContainerId, Admin.Container, Admin.MaxVolume, Admin.MaxWeight).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Container updated successfully");
                        angular.forEach(Admin.Containers, function(value, key) {
                            if (value.ID == Admin.ContainerId) {
                                value.Container = Admin.Container;
                                value.MaximumVolume = Admin.MaxVolume;
                                value.MaximumWeight = Admin.MaxWeight;
                                Admin.Container = "";
                                Admin.MaxVolume = "";
                                Admin.MaxWeight = "";
                                Admin.ContainerId = 0;
                            }
                        });
                        $scope.container_form.$setPristine();
                        $scope.container_form.$setUntouched();
                        $('#editContainer').modal('hide');
                    } else {
                        if (response.comment == "Exists") {
                            toastr.error('Conatiner already exists');
                        } else {
                            toastr.error('Somethig bad happend, please try again');
                        }
                        console.log(response.comment);
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.conterror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.EditTrade = function(ID) {
            angular.forEach(Admin.Trades, function(value, key) {
                if (value.Id == ID) {
                    Admin.Tradename = value.Name;
                    Admin.TradeMainId = value.Id;
                }
            });
            $('#editTrade').modal();
        }
        Admin.DeleteTrade = function(ID) {
            Admin.Id = ID;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function(ID) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deletTrade(Admin.Id).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var Id = "#tradeorhall_" + Admin.Id;
                                $(Id).remove();
                                index = 0;
                                angular.forEach(Admin.Trades, function(value, key) {
                                    if (value.Id == Admin.Id) {
                                        Admin.Trades.splice(index, 1);
                                    }
                                    index++;
                                });
                                swal(
                                        'Deleted!',
                                        'Trade has been deleted.',
                                        'success'
                                    )
                                    // resolve()
                            } else {
                                console.log(response.comment);
                                if (response.comment == "Not Allowed") {
                                    swal(
                                            'Error!',
                                            "Trade can't delete because it's already used",
                                            'error'
                                        )
                                        // resolve()
                                        // reject("Trade can't delete because it's already used")
                                } else {
                                    reject("Something bad happend, Try Again!")
                                }
                            }
                        })
                    })
                },
            }).then(function() {

            })
        }
        Admin.DeleteHall = function(ID) {
            Admin.Id = ID;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function(ID) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deletTrade(Admin.Id).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var Id = "#tradeorhall_" + Admin.Id;
                                $(Id).remove();
                                swal(
                                    'Deleted!',
                                    'Hall has been deleted.',
                                    'success'
                                )
                            } else {
                                console.log(response.comment);
                                if (response.comment == "Not Allowed") {
                                    swal(
                                            'Error!',
                                            "Hall can't delete because it's already used",
                                            'error'
                                        )
                                        // reject()
                                } else {
                                    reject("Something bad happend, Try Again!")
                                }
                            }
                        })
                    })
                },
            }).then(function() {
                // swal(
                //     'Deleted!',
                //     'Trade has been deleted.',
                //     'success'
                // )
            })
        }
        Admin.UpdateTrade = function(form) {
            $('.ajax_loader').show();
            Admin.tradeerror = false;
            if (Admin.Tradename != "" && Admin.Tradename != undefined && form.$valid) {
                trade = Admin.Tradename;
                parent = 0;
                AdminService.updateTrade(Admin.TradeMainId, trade, parent).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Trade updated successfully")
                        $scope.trade_form.$setPristine();
                        $scope.trade_form.$setUntouched();
                        angular.forEach(Admin.Trades, function(value, key) {
                            if (value.Id == Admin.TradeMainId) {
                                value.Name = Admin.Tradename;
                                Admin.Tradename = "";
                                Admin.TradeMainId = 0;
                            }
                        });
                        form.$setPristine();
                        form.$setUntouched();
                        $('#editTrade').modal('hide');

                    } else {
                        if (response.comment == "Exists") {
                            toastr.warning('Already Exists,Try another name!');
                        } else {
                            toastr.error('Somethig bad happend,please try again');
                        }
                        console.log(response.comment);
                        $('.ajax_loader').hide();
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.tradeerror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.EditHall = function(ID) {
            angular.forEach(Admin.Halls, function(value, key) {
                if (value.Id == ID) {
                    Admin.HallName = value.Name;
                    Admin.HallId = value.Id;
                    if (value.Parent != Admin.TradeId.Id) {
                        Admin.TradeId = value.Parent
                    }
                }
            });
            if (Admin.Trades[0].Id != Admin.TradeId.Id) {
                angular.forEach(Admin.Trades, function(value, key) {
                    if (value.Id == Admin.TradeId) {
                        Admin.TradeId = value;
                    }
                });
            }
            $('#editHall').modal();
        }
        Admin.UpdateHall = function(form) {
            $('.ajax_loader').show();
            Admin.hallerror = false;
            if (Admin.HallName != "" && Admin.HallName != undefined && Admin.TradeId != undefined && Admin.TradeId.Id != "-1") {
                hall = Admin.HallName;
                parent = Admin.TradeId.Id;
                AdminService.updateTrade(Admin.HallId, hall, parent).success(function(response) {
                    if (response.message == "true") {
                        toastr["success"]("", "Hall updated successfully")
                        angular.forEach(Admin.Halls, function(value, key) {
                            if (value.Id == Admin.HallId) {
                                value.Name = hall;
                                value.Parent = parent;
                                Admin.HallName = value.Name;
                                Admin.HallId = 0;
                            }
                        });
                        $scope.hall_form.$setPristine();
                        $scope.hall_form.$setUntouched();
                        $('#editHall').modal('hide');
                    } else {
                        if (response.comment == "Exists") {
                            toastr.warning('Already Exists,Try another name!');
                        } else {
                            toastr.error('Somethig bad happend,please try again');
                        }
                        console.log(response.comment);
                    }
                    $('.ajax_loader').hide();
                });
            } else {
                Admin.hallerror = true;
                if (Admin.TradeId == undefined || Admin.TradeId.Id == "-1") {
                    Admin.tradeiderror = true;
                }
                $('.ajax_loader').hide();
            }
        }
        Admin.GetUsers = function() {
            if (!Admin.ScrollLoader && !Admin.UserLoader) {
                Admin.UserLoader = true;
                Admin.ScrollLoader = true;
                AdminService.getUserList(Admin.UserPageNo).success(function(response) {
                    Admin.UserLoader = false;
                    if (response.message == "true") {
                        Admin.UserPageNo = Admin.UserPageNo + Admin.UserContentLength;
                        angular.forEach(response.userlist, function(val, key) {
                            Admin.Users.push(val);
                        });
                        if (response.userlist.length == Admin.UserContentLength) {
                            Admin.ScrollLoader = false;
                        }
                    } else {
                        console.log(response.comment);
                    }
                })
            }
        }
        Admin.DeleteAgents = function(Agent) {
            Admin.tempAgent = Agent;
            Admin.AgentId = Agent.Id;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function(ID) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deleteGoldAgent(Admin.AgentId).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var Id = "#goldagent_" + Admin.AgentId;
                                $(Id).fadeOut();
                                var index = Admin.GoldAgents.indexOf(Admin.tempAgent);
                                Admin.GoldAgents.splice(index, 1);
                                // resolve()
                                swal(
                                    'Deleted!',
                                    'Gold Agent has been deleted.',
                                    'success'
                                )
                            } else {
                                console.log(response.comment);
                                if (response.comment == "Not Allowed") {
                                    swal(
                                            'Error!',
                                            "Gold Agent can't delete because it's already used",
                                            'error'
                                        )
                                        //  reject("Gold Agent can't delete because it's already used")
                                } else {
                                    reject("Something bad happend, Try Again!")
                                }
                            }
                        })
                    })
                },
            }).then(function() {

            })
        }
        Admin.DeleteUser = function(User) {
            Admin.tempUser = User;
            Admin.UserEmail = User.Email;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function(User) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deleteUser(Admin.UserEmail).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var index = Admin.Users.indexOf(Admin.tempUser);
                                Admin.Users.splice(index, 1);
                                resolve()
                            } else {
                                console.log(response.comment);
                                reject("Something bad happend, Try Again!")
                            }
                        })
                    })
                },
            }).then(function() {
                swal(
                    'Deleted!',
                    'User has been deleted.',
                    'success'
                )
            })
        }
        Admin.EditAgent = function(agent) {
            Admin.tempAgent = agent;
            Admin.AgentId = agent.Id;
            Admin.GoldAgentName = agent.CompanyName;
            Admin.AgentOffice = agent.OfficeAddress;
            Admin.CompanyPhone = agent.PhoneNumber;
            Admin.BussinessYear = agent.BusinessYear;
            Admin.FaxNumber = agent.FaxNumber;
            Admin.GoldYear = agent.GoldAgentYears;
            angular.forEach(agent.Contact, function(value, key) {
                value.Image = "";
                value.Contact = key;
            });
            Admin.GoldAgentContacts = agent.Contact;
            $('#editAgent').modal();
        }
        Admin.AddGoldAgentContact = function() {
            var newItemNo = Admin.GoldAgentContacts.length + 1;
            var ImageName = "";
            Admin.GoldAgentContacts.push({ 'Contact': newItemNo, 'ImageName': ImageName });
        }
        Admin.RemoveGoldAgentContact = function() {
            if (Admin.GoldAgentContacts.length > 5) {
                var lastindex = Admin.GoldAgentContacts.length - 1;
                Admin.GoldAgentContacts.splice(lastindex, 1);
            }
        }
        Admin.UpdateGoldAgent = function(form) {
            $('.ajax_loader').show();
            Admin.golderror = false;
            Admin.agenterror = true;
            angular.forEach(Admin.GoldAgentContacts, function(value, key) {
                if (value.Name == undefined || value.Mobile == undefined || value.Email == undefined || value.Name == "" ||
                    value.Mobile == "" || value.Email == "") {
                    Admin.golderror = true;
                    Admin.agenterror = false;
                }
                if (value.Skype == undefined) {
                    value.Skype = "";
                }
                if (value.Image == undefined || value.Image == "") {
                    value.Image = "";
                } else if (value.Image.compressed) {
                    var imgstring = value.Image.compressed.dataURL;
                    imgstring = imgstring.replace(/^data:image\/[a-z]+;base64,/, "");
                    value.Image = imgstring;
                }
            });
            if (Admin.agenterror && Admin.GoldAgentName != "" && Admin.GoldAgentName != undefined && Admin.AgentOffice != undefined && Admin.AgentOffice != "" && Admin.CompanyPhone != undefined && Admin.CompanyPhone != "") {
                AdminService.updateGoldAgent(Admin.AgentId, Admin.GoldAgentName, Admin.AgentOffice, Admin.CompanyPhone, Admin.GoldAgentContacts, Admin.FaxNumber, Admin.BussinessYear, Admin.GoldYear).success(function(response) {
                    $('.ajax_loader').hide();
                    if (response.message == "true") {
                        toastr["success"]("", "Gold Agent Updated successfully")
                        var index = Admin.GoldAgents.indexOf(Admin.tempAgent);
                        Admin.GoldAgents[index].Contact = response.Contact;
                        Admin.GoldAgents[index].CompanyName = Admin.GoldAgentName;
                        Admin.GoldAgents[index].OfficeAddress = Admin.AgentOffice;
                        Admin.GoldAgents[index].PhoneNumber = Admin.CompanyPhone;
                        Admin.GoldAgents[index].FaxNumber = Admin.FaxNumber;
                        Admin.GoldAgents[index].BusinessYear = Admin.BussinessYear;
                        Admin.GoldAgents[index].GoldAgentYears = Admin.GoldYear;
                        $scope.goldagent_form.$setPristine();
                        $scope.goldagent_form.$setUntouched();
                        Admin.GoldAgentName = "";
                        Admin.AgentOffice = "";
                        Admin.CompanyPhone = "";
                        Admin.FaxNumber = "";
                        Admin.BussinessYear = "";
                        Admin.GoldYear = "";
                        Admin.GoldAgentContacts = [{ 'Contact': '1' }, { 'Contact': '2' }, { 'Contact': '3' }, { 'Contact': '4' }, { 'Contact': '5' }];
                        $('#editAgent').modal('hide');
                    } else {
                        toastr.error('Somethig bad happend, please try again');
                    }

                });

            } else {
                Admin.golderror = true;
                $('.ajax_loader').hide();
            }
        }
        Admin.DeactivateAgents = function(Agent) {
            Admin.tempAgent = Agent;
            Admin.AgentId = Agent.Id;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, deactivate it!',
                showLoaderOnConfirm: true,
                preConfirm: function(ID) {
                    return new Promise(function(resolve, reject) {
                        AdminService.deactivateGoldAgent(Admin.AgentId).success(function(response) {
                            if (response.message == "true") {
                                //Success
                                var Id = "#goldagent_" + Admin.AgentId;
                                $(Id).fadeOut();
                                var index = Admin.GoldAgents.indexOf(Admin.tempAgent);
                                Admin.GoldAgents.splice(index, 1);
                                // resolve()
                                swal(
                                    'Deactivate!',
                                    'Gold Agent has been deactivated.',
                                    'success'
                                )
                            } else {
                                console.log(response.comment);
                                reject("Something bad happend, Try Again!")
                            }
                        })
                    })
                },
            }).then(function() {

            })
        }
        Admin.GetTrades();
        Admin.GetHalls();
        Admin.GetContainers();

    })
    ($scope.Admin);

}]);
