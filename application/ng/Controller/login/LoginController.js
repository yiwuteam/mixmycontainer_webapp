mixMyContainer.controller('LoginController', ['$scope', '$rootScope', 'LoginService', '$window','$location','$http','$timeout', function ($scope, $rootScope, LoginService, $window, $location,$http,$timeout) {
  $scope.Login = {};
  $scope.Register ={};

      (function (Login) {
        Login.check=[];
        Login.Credentials={};
         Login.LoginCheck = function () {
           Login.UserName = $('#username').val();
           Login.Password = $('#password').val();
           if (Login.UserName !="" && Login.Password !=""){
           LoginService.getLogin(Login.UserName,Login.Password).success(function (response) {
             Login.Check=JSON.parse(response);

             if(Login.Check==1)
             {
               toastr["success"]("", "Welcome " +Login.UserName+ " to MixMyContainer")
               window.location.href = "../Login/dashboard";
            //   window.location('dashboard');
             }
             else {
               swal({
                   title: "Failed!",
                   text: "Username or Password is incorrect"
                   
               });
             }
            //  alert("success");

          })
         }
         else{
          swal({
                   title: "Failed!",
                   text: "Username or Password is empty"
                   
               });
         }
         }
        //  Login.checkIfEnterKeyWasPressed = function($event){
        //   var keyCode = $event.which || $event.keyCode;
        //   if (keyCode === 13) {
        //       $("#button_login").trigger('click');
        //       Login.LoginCheck();
        //       //Login.LoginCheck();
        //       // Do that thing you finally wanted to do
        //   }

        // }
         function validateEmail(email) {
             var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
             return re.test(email);
         }
         Login.Resetpassword =function(){
          $('.ajax_loader').show();
           Login.Email=$('#email').val() ;
           if(Login.Email!=""){
              if (validateEmail(Login.Email)){
                   LoginService.resetpassword(Login.Email).success(function (response) {
                     var result=JSON.parse(response);
                     if(result=="1"){
                      $('.ajax_loader').hide();
                       swal({
                           title: "Success!",
                           text: "Your password reset link has been sent successfully to your email address",
                           type: "success",
                            closeOnConfirm: false,
                          },
                          function(isConfirm){
                          if (isConfirm) {
                            window.location.href="../Login/reset"
                           }
                       });
                       //window.location.href="../Login/reset";
                     }
                     else {
                      $('.ajax_loader').hide();
                       {
                         swal({
                             title: "Failed!",
                             text: "Email does not exist please register first",
                             type: "danger"
                         });
                       }
                     }
                   })
              }
              else {
                  $('.ajax_loader').hide();
                   toastr.error('Please enter a valid Email');
              }
           }
           else {
                $('.ajax_loader').hide();
                toastr.error('Please fill the columns first');

           }
         }
         Login.Changepassword =function(){
          $('.ajax_loader').show();
           //Login.Email=$('#email').val() ;
           Login.otp = $('#otp').val() ;
           //$.cookie("otp", Login.otp,{ expires : 10 });
           Login.Password=$('#newpwd').val() ;
           Login.Confirmpassword=$('#cpwd').val();
           if(Login.otp!=""  && Login.Password !="" && Login.Confirmpassword !=""){
           if(Login.Password==Login.Confirmpassword){
             LoginService.changepassword(Login.otp,Login.Password).success(function (response){
               var result = response.message;
               //var result=JSON.parse(response);
               if(result=="true"){
                $('.ajax_loader').hide();
                 swal({
                     title: "Success!",
                     text: "Your password successfully changed",
                     type: "success",
                    closeOnConfirm: false,
                  },
                   function(isConfirm){
                          if (isConfirm) {
                            window.location.href="../"
                           } 
                 });

               }

              else if(result=="false"){
                $('.ajax_loader').hide();
                    toastr.error('Invalid OTP');
               }

             })
           }
           else{
            $('.ajax_loader').hide();
             toastr.error('Password Mismatches');
           }
         }
         else{
          $('.ajax_loader').hide();
           toastr.error('Please Fill All The Fields');
         }
         }
      })
      ($scope.Login);
      (function (Register) {
        Register.userregister = function () {
          $('.ajax_loader').show();
          //Register.UserName = $('#username').val();
          Register.Email    = $('#useremail').val() ;
          Register.Password = $('#userpassword').val();
          Register.Password.length = $("#userpassword").val().length;
          Register.Confirmpassword = $('#confirmpassword').val();
          Register.Agreecheck = $('#bc1').is(":checked");
          Register.Companyname = $('#companyname').val();
          Register.Country = $('#registercountry').val();
          Register.Zipcode = $('#zipcode').val();
          Register.Phone = $('#phonenumber').val();
          Register.Mobile = $('#mobilenumber').val();
          Register.Address = $('#address').val();
          Register.Towncity = $('#towncity').val();
          if(Register.Country == "? undefined:undefined ?"){
            Register.Country ="";
          }
           
            if(Register.Agreecheck && Register.Email != "" && Register.Password != "" && Register.Confirmpassword != "" && Register.Companyname != "" && Register.Country != "" && Register.Zipcode != "" && Register.Phone != "" && Register.Mobile != "" && Register.Address != "" && Register.Towncity != ""){
            if(Register.Password.length >=6){
             if (Register.Password == Register.Confirmpassword) {  
              if (validateEmail(Register.Email)){
                  LoginService.userRegistration(Register.Email, Register.Password, Register.Companyname, Register.Country, Register.Zipcode, Register.Phone, Register.Mobile, Register.Address,Register.Towncity).success(function (response) {
                  var result=JSON.parse(response);
                    if(result==1){
                      $('.ajax_loader').hide();
                     swal({
                          title: "User Registration is Successful",
                          text: "Please Login With Your Unique Id And Password!",
                          type: "success",
                          closeOnConfirm: false,
                      },
                      function(isConfirm){
                          if (isConfirm) {
                            window.location.href="../Login"
                           } 
                          }); 
                    }
                    else{
                      $('.ajax_loader').hide();
                      toastr.error('This email already exists. Please provide another valid email address');
                      $('#email').val('');
                      $("#username").val('');
                    }
                     
                 })
              }
              else {
                $('.ajax_loader').hide();
                toastr.error('Email is not valid');
              }
          }
            else {
              $('.ajax_loader').hide();
                    toastr.error('Password Mismatch');
                }
            }
            else{
                $('.ajax_loader').hide();
                    toastr.error('Password length should be atleast 6 characters');
              }
              }
          else {
            $('.ajax_loader').hide();
            toastr.error('Please fill the fields');
          }
        
        }
        Register.getContries = function() {
                LoginService.getcountries().success(function(response) {
                    Register.ContryNames = response;
                    Register.Country = Register.ContryNames[0].CountryName;
                })
            }

        // Register.checkIfEnterKeyWasPressed = function($event){
        //   var keyCode = $event.which || $event.keyCode;
        //   if (keyCode === 13) {
        //       Register.userregister();
        //       // Do that thing you finally wanted to do
        //   }
        // }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
      })
        ($scope.Register);

}]);
