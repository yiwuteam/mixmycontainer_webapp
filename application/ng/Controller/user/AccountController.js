mixMyContainer.controller('AccountController', ['$scope', '$rootScope', 'AccountService', '$window', '$location', '$http', '$localStorage', '$filter', function($scope, $rootScope, AccountService, $window, $location, $http, $localStorage, $filter) {
    $scope.Account = {};
    $scope.Company = {};
    $scope.myImage = '';
    $scope.email = '';
    $scope.form = [];
    $scope.files = [];
    //StudentProfile.graphitems = {};

    var handleFileSelect = function(evt) {

        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
                //  $('#My_Img').attr('src',evt.target.result);
            });
        };
        reader.readAsDataURL(file);
    };

    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
    (function(Account) {
        Account.ContryNames = [];
        Account.ContryCorrency = [];
        Account.file = "";
        Account.AccountDetails = [];
        $('#emailcheckbox').addClass('loadinggif');
        Account.chengepassword = function() {
            $('.ajax_loader').show();
            Account.Email = $('#email').val();
            $scope.email = Account.Email;
            Account.password = $('#password').val();
            Account.newPassword = $('#newpassword').val();
            Account.confirmpassword = $('#confirmpassword').val();
            if (Account.password != "" && Account.newPassword != "" && Account.confirmpassword != "") {
                if (Account.newPassword == Account.confirmpassword) {
                    // if (validateEmail(Account.Email)){
                    AccountService.chengepassword(Account.Email, Account.newPassword, Account.password).success(function(response) {
                            var result = response.message;
                            if (result == 'true') {
                                $('.ajax_loader').hide();
                                swal({
                                    title: "Success!",
                                    text: "Your password has been changed!",
                                    type: "success"
                                });
                                // $('#passwordmatch').html('');
                                //  $('#passstrength').html('');
                                $('#email').val("");
                                $('#password').val("");
                                $('#newpassword').val("");
                                $('#confirmpassword').val("");
                            } else if (result == 'false') {
                                toastr.error('Incorrect Current password');
                                $("#password").val('');
                            } else {
                                toastr.error('Incorrect Current password');
                                $('.ajax_loader').hide();
                            }
                        })
                        // }
                        // else {
                        //   toastr.error('Provide a valid Email');
                        // }
                } else {
                    toastr.error('Password Mismatch');
                    $('.ajax_loader').hide();
                }
            } else {
                if (Account.Email == "") {
                    toastr.error('Email is required');
                }
                if (Account.password == "") {
                    toastr.error('Provide your old password');
                }
                if (Account.newPassword == "") {
                    toastr.error('Enter your new password');
                }
                if (Account.confirmpassword == "") {
                    toastr.error('Confirm your new password');
                }
                $('.ajax_loader').hide();
            }
            //$('.ajax_loader').hide();
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        Account.ImageUpload = function() {
            $('.ajax_loader').show();
            var file = document.getElementById("fileInput").files[0];
            if (file != undefined) {
            var size = file.size;
                if (size < 2097152) {
                    if (file.type == 'image/png' || file.type == 'image/jpeg' || file.type == 'image/gif') {
                        Account.Image = file.name;
                        //  $scope.form.image = $scope.files[0];

                        $http({
                            method: 'POST',
                            url: '../profile/imageUpload',
                            processData: false,
                            transformRequest: function(data) {
                                var formData = new FormData();
                                formData.append("image", file);
                                formData.append("name", Account.Image);
                                return formData;
                            },
                            data: $scope.form,
                            headers: {
                                'Content-Type': undefined
                            }
                        }).success(function(data) {
                            $('.ajax_loader').hide();
                            toastr["success"]("", "Image Uploaded Successfully");
                            // $('.modal-body').find('input').val('');
                            //window.location.reload();
                            $("#modalclose").trigger('click');
                            Account.getImage();
                        });
                    } else {
                        $('.ajax_loader').hide();
                        toastr.error('Please select an Image File');
                    }
                } else {
                $('.ajax_loader').hide();
                toastr.error('Please select an Image File less than 2 MB');
                }    
            } else {
                $('.ajax_loader').hide();
                toastr.error('Please select an Image');
            }
            
        }

        Account.ImageModalClose = function() {

            $('.modal-body').find('input').val('');
            image = "userdummypic.png";
            $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
            Account.getAccount();
            $('#My_Imgmodal').attr('ng-src', '../assets/images/profilepic/' + image);
            $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);

            // $('.modal-body').find('input').val('');
            // //Account.getAccount();
            // $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);

        }

        Account.getfunctions = function() {
            Account.getImage();
            Account.getAccount();
            Account.getLocale();
        }

        Account.accountdetails = function() {
            $('.ajax_loader').show();
            Account.languages = $("input[name='radio1']:checked").val();
            // Account.emailsettings = $("input[name='email']:checked").val();
            //Account.emailsettings=$('#email').prop('checked');
             Account.emailsettings=$('#email').is(':checked');
            Account.locale = $('#localesettings').val();
            if (Account.languages == null || undefined) {
                Account.languages = "";
            } else {
                Account.languages = $("input[name='radio1']:checked").val();
            }
            if (Account.emailsettings == null || undefined||!Account.emailsettings) {
                Account.emailsettings = 0;
            } else {
                Account.emailsettings = 1;
            }
            if (Account.locale == "?") {
                Account.locale = "";
            } else {
                Account.locale = $('#localesettings').val();
            }

            AccountService.accountdetails(Account.languages, Account.emailsettings, Account.locale).success(function(response) {
                var result = response;
                //var result = JSON.parse(response);
                if (result == "true") {
                    $('.ajax_loader').hide();
                    toastr["success"]("", "Account Saved Successfully");
                    Account.getAccount();
                    //window.location.reload();
                } else {
                    $('.ajax_loader').hide();
                    toastr.error('Could not find User,Please login in again');
                }

            })
        }

        Account.getImage = function() {
            AccountService.getaccount().success(function(response) {
                if (response.length != 0) {
                    $('.ajax_loader').show();
                    $("#ProfileButton").html('Update');
                    var image = response[0].Image;

                    if (image == "default.jpg") {
                        $('.ajax_loader').hide();
                        image = "userdummypic.png";
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);

                    } else if (image == "") {
                        $('.ajax_loader').hide();
                        image = "userdummypic.png";
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
                    } else {
                        $('.ajax_loader').hide();
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
                    }
                } else {
                    $("#ProfileButton").html('Save');
                }
            })
        }
        // Account.getImage();
        Account.getAccount = function() {
            AccountService.getaccount().success(function(response) {
                if (response.length != 0) {
                    $('#emailcheckbox').removeClass('loadinggif');

                    $('.ajax_loader').show();
                    $("#ProfileButton").html('Update');
                    var image = response[0].Image;

                    if (image == "default.jpg") {
                        $('.ajax_loader').hide();
                        image = "userdummypic.png";
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);

                    } else if (image == "") {
                        $('.ajax_loader').hide();
                        image = "userdummypic.png";
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
                    } else {
                        $('.ajax_loader').hide();
                        $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
                        $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
                    }

                    if (response[0].EmailSettings == "1") {
                        $('.icheckbox_square-green').addClass('checked');
                        //$("#mailset").html('Plaintext email only');
                        //Account.emailsettings = true;
                        // $('.myCheckbox').prop('checked', true);
                    } else {
                        document.getElementById("email").checked = false;
                        $('.icheckbox_square-green').removeClass('checked');
                        //$("#mailset").html('No Plaintext emails');
                        //Account.emailsettings = false;
                    }
                    var value = response[0].Language;

                    if (response[0].Language == "English") {
                        $('#rd1').find('.iradio_square-green').addClass('checked');
                        $('#rd1').addClass('checked');
                        $("input[name=radio1][value=" + value + "]").prop('checked', true);
                    }
                    else if(response[0].Language == "Chinese") {
                        $('.ajax_loader').hide();
                        $('#rd2').find('.iradio_square-green').addClass('checked');
                        $('#rd2').addClass('checked');
                        $("input[name=radio1][value=" + value + "]").prop('checked', true);
                    }
                     else {
                        $('.ajax_loader').hide();
                        // $('#rd2').find('.iradio_square-green').addClass('checked');
                        // $('#rd2').addClass('checked');
                        //$("input[name=radio1][value=" + value + "]").prop('checked', true);
                    }
                    var Locale = response[0].Locale;
                    angular.forEach(Account.Locales, function(value, key) {
                        if (value.countryCode === Locale) {
                          Account.AccountDetails.Locale = value;
                        //$('#localesettings').removeClass('loadinggiflocale');
                             }
                      });
                } else {
                    $('#emailcheckbox').removeClass('loadinggif');
                     //$('#localesettings').removeClass('loadinggiflocale');
                    $('.icheckbox_square-green').removeClass('checked');
                    $("#ProfileButton").html('Save');
                }

            })
        }
        //Account.getAccount();

        // Account.getAccount = function() {
        //     AccountService.getaccount().success(function(response) {
        //         if (response.length != 0) {
        //             $('.ajax_loader').show();
        //             $("#ProfileButton").html('Update');
        //             var image = response[0].Image;

        //             if (image == "default.jpg") {
        //                 $('.ajax_loader').hide();
        //                 image = "userdummypic.png";
        //                 $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);

        //             } else if (image == "") {
        //                 $('.ajax_loader').hide();
        //                 image = "userdummypic.png";
        //                 $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
        //             } else {
        //                 $('.ajax_loader').hide();
        //                 $('#My_Img').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgsidebar').attr('src', '../assets/images/profilepic/' + image);
        //                 $('#My_Imgmodal').attr('src', '../assets/images/profilepic/' + image);
        //             }

        //             if (response[0].EmailSettings == "1") {
        //                 $('.icheckbox_square-green').addClass('checked');
        //                 $("#mailset").html('Plainetext email only');
        //                 // $('.myCheckbox').prop('checked', true);
        //             } else {
        //                 document.getElementById("email").checked = false;
        //                 $('.icheckbox_square-green').removeClass('checked');
        //                 $("#mailset").html('No Plainetext emails');
        //             }
        //             var value = response[0].Language;

        //             if (response[0].Language == "English") {
        //                 $('#rd1').find('.iradio_square-green').addClass('checked');
        //                 $('#rd1').addClass('checked');
        //                 $("input[name=radio1][value=" + value + "]").prop('checked', true);
        //             } else if (response[0].Language == "Chinese") {
        //                 $('.ajax_loader').hide();
        //                 $('#rd2').find('.iradio_square-green').addClass('checked');
        //                 $('#rd2').addClass('checked');
        //                 //$("input[name=radio1][value=" + value + "]").prop('checked', true);
        //             } else {
        //                 $('.ajax_loader').hide();
        //                 // $('#rd2').find('.iradio_square-green').addClass('checked');
        //                 // $('#rd2').addClass('checked');
        //                 //$("input[name=radio1][value=" + value + "]").prop('checked', true);
        //             }
        //             var Locale = response[0].Locale;
        //             angular.forEach(Account.Locales, function(values, key) {
        //                 if (values.countryCode === Locale) {
        //                     Account.AccountDetails.Locale = values;
        //                 }
        //             });
        //         } else {
        //             $('.icheckbox_square-green').removeClass('checked');
        //             $("#ProfileButton").html('Save');
        //         }

        //     })
        // }
        // Account.getAccount();

        Account.getLocale = function() {
            AccountService.getlocale().success(function(response) {
                for (var i = 0; i < response.zones.length; i++) {
                    //alert(response.zones.timestamp[i]);
                    var date = new Date(response.zones[i].timestamp * 1000);
                    var hours = date.getHours();
                    var minutes = "0" + date.getMinutes();
                    var seconds = "0" + date.getSeconds();
                    var Timezone = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
                    var CountryName = response.zones[i].countryName;
                    response.zones[i].countryName = CountryName + " " + Timezone;
                }
                Account.Locales = response.zones;
                //Account.AccountDetails.Locale=response.zones[0];
                //Account.AccountDetails.Locale=$localStorage.userlocale;
            })
        }

        // Account.getLocale();

        Account.getContries = function() {
                AccountService.getcontries().success(function(response) {
                    Account.ContryNames = response;
                })
            }
            //  Account.getContries();
        Account.getCorrency = function() {
                AccountService.getCorrency().success(function(response) {
                    Account.ContryCorrency = response;
                })
            }
            //  Account.getCorrency();
    })
    ($scope.Account);

    (function(Company) {
        Company.CompanySetupDetails = {};
        Company.CompanySetup = {};
        Company.ExchangeRate = {};
        Company.CartonSetup = {};
        Company.ContryNames = [];
        Company.CountryCurrency = [];
        Company.test = "";
        //Company.Activethirdcurrencylabel =false;
        // Company.EnableButton2=true;
        // Company.EnableButton3=true;
        // Company.GoldAgentError=false;
        //Company.IsEnable = true;
        (function(CompanySetup) {
            CompanySetup.CompanyName = "";
            CompanySetup.Contry = "";
            CompanySetup.Documentation = "";
            CompanySetup.Address = "";
            CompanySetup.Zipcode = "";
            CompanySetup.Phone = "";
            CompanySetup.Mobile = "";
            CompanySetup.Fax = "";

            CompanySetup.SkypeName = "";
            CompanySetup.MSNid = "";
            CompanySetup.QQid = "";
            CompanySetup.OrderEmail = "";
            CompanySetup.AgentEmail = "";
            CompanySetup.AgentId = "";
        })(Company.CompanySetup);
        (function(ExchangeRate) {
            ExchangeRate.DollarRMB = "";
            ExchangeRate.Thirdcurrency = "";
            ExchangeRate.Currency = "";
            ExchangeRate.Dollerthirdcurrency = "";
            ExchangeRate.TranslateFrom = "";
            ExchangeRate.TranslateTo = "";
        })(Company.ExchangeRate);
        (function(CartonSetup) {
            CartonSetup.Grossweight = "";
            CartonSetup.Lenght = "";
            CartonSetup.Width = "";
            CartonSetup.Height = "";
            CartonSetup.Volume = "";
        })(Company.CartonSetup);
        Company.getContries = function() {
            AccountService.getcontries().success(function(response) {
                Company.ContryNames = response;
            })
        }
        Company.getContries();
        Company.getCorrency = function() {
            AccountService.getCorrency().success(function(response) {
                Company.CountryCurrency = response;
            })
        }
        Company.getCorrency();

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        Company.companyDetails = function() {
            $('.ajax_loader').show();
            Company.CompanySetup.AgentId = $localStorage.AgentId;
            Company.CompanySetup.AgentName = $localStorage.AgentIdName;
            if (Company.ExchangeRate.Thirdcurrency == '1') {
                Company.ExchangeRate.Thirdcurrency = "1";

            } else {
                Company.ExchangeRate.Currency = "";
                Company.ExchangeRate.Dollerthirdcurrency = "0";
                Company.ExchangeRate.Thirdcurrency = "0";

            }

            Company.error = false;

            Company.CartonSetup.Volume = Company.CartonSetup.Width * Company.CartonSetup.Height * Company.CartonSetup.Lenght * 0.000001;
            if (Company.ExchangeRate.Thirdcurrency != "1" || Company.ExchangeRate.Thirdcurrency == true && Company.ExchangeRate.Currency != "" && Company.ExchangeRate.Dollerthirdcurrency != "") {
                if (Company.CompanySetup.CompanyName != "" && Company.CompanySetup.CompanyName != undefined && Company.CompanySetup.OrderEmail != "" && Company.CompanySetup.OrderEmail != undefined && Company.ExchangeRate.DollarRMB != "" && Company.ExchangeRate.DollarRMB != undefined) {

                    AccountService.companyDetails(Company.CompanySetup, Company.ExchangeRate, Company.CartonSetup).success(function(response) {
                        var result = response;
                        if (result == "true") {
                            $('.ajax_loader').hide();
                            toastr["success"]("", "Company Details Saved Successfully");
                            Company.getCompanyDetails();
                            //$("#redirectsetup").trigger('click');

                        } else {
                            $('.ajax_loader').hide();
                            toastr.error('Oops! Something went wrong please try again later');
                        }
                    })
                } else {
                    Company.error = true;
                    $('.ajax_loader').hide();
                    if (Company.CompanySetup.CompanyName == "" || Company.CompanySetup.CompanyName == undefined) {
                        toastr.error('Please enter Company Name');
                    }
                    if (Company.CompanySetup.OrderEmail == "" || Company.CompanySetup.OrderEmail == undefined) {
                        toastr.error('Please type your orderemail address');
                    }
                    if (Company.ExchangeRate.DollarRMB == "" || Company.ExchangeRate.DollarRMB == undefined) {
                        toastr.error('Please enter Dollar RMB value');
                    }

                }
            } else {
                if (Company.ExchangeRate.Currency == "" || undefined) {
                    Company.Activethirdcurrencylabel = true;
                    toastr.error('Please select Exchange Rate Currency');
                }
                if (Company.ExchangeRate.Dollerthirdcurrency == "" || undefined) {
                    toastr.error('Please enter Dollar third Currency');
                }
                //Company.Activethirdcurrencylabel = false;
                $('.ajax_loader').hide();
            }
        }

        Company.hideActivethirdcurrency = function() {
            if (Company.ExchangeRate.Currency != "" && Company.ExchangeRate.Currency != undefined) {
                Company.Activethirdcurrencylabel = false;
            } else {
                Company.Activethirdcurrencylabel = true;
            }
        }

        // Company.Changetonullvalue = function(){
        //     if(Company.ExchangeRate.Thirdcurrency == false){
        //          Company.ExchangeRate.Currency = "";
        //     Company.ExchangeRate.Dollerthirdcurrency = "0";
        //     }

        // }

        Company.getCompanyDetails = function() {
            $('.ajax_loader').show();
            AccountService.getCompanyDetails().success(function(response) {
                $('.ajax_loader').show();
                if (response.length != 0) {
                    $('.ajax_loader').show();
                    if (response[0].CompanyName != "") {
                        $('.ajax_loader').show();
                        Company.CompanySetupDetails = response;
                        //Company.CompanySetup.IsHidden = false;
                        Company.IsEnable = false;
                        $("#setupbutton").html('Update');
                        Company.CompanySetup.CompanyName = response[0].CompanyName;
                        Company.CompanySetup.Contry = response[0].Country;
                        Company.CompanySetup.Documentation = response[0].AddressType;
                        Company.CompanySetup.Address = response[0].Address;
                        Company.CompanySetup.Zipcode = response[0].ZipCode;
                        Company.CompanySetup.Phone = response[0].Phone;
                        Company.CompanySetup.Mobile = response[0].Mobile;
                        Company.CompanySetup.Fax = response[0].Fax;
                        Company.CompanySetup.SkypeName = response[0].Skype;
                        Company.CompanySetup.MSNid = response[0].MsnID;
                        Company.CompanySetup.QQid = response[0].QqID;
                        Company.CompanySetup.OrderEmail = response[0].MailOrder;
                        Company.CompanySetup.AgentEmail = response[0].AgentEmail;
                        Company.CompanySetup.AgentId = response[0].AgentName;
                        $localStorage.AgentId = response[0].AgentId;
                        $localStorage.AgentIdName = response[0].AgentName;
                        var country = response[0].Country;
                        Company.ExchangeRate.DollarRMB = response[0].ExchangeRate;
                        Company.ExchangeRate.Thirdcurrency = response[0].Active3dCurrency;
                        Company.ExchangeRate.Currency = response[0].ThirdCurrency;
                        Company.ExchangeRate.Dollerthirdcurrency = response[0].ExchangeRate3dCurrency;
                        Company.ExchangeRate.TranslateFrom = response[0].TransilateFrom;
                        Company.ExchangeRate.TranslateTo = response[0].TransilateTo;
                        // Company.EnableButton2=false;
                        // Company.EnableButton3=false;
                        if (Company.ExchangeRate.TranslateFrom == "") {
                            Company.ExchangeRate.TranslateFrom = "en";
                        }
                        if (Company.ExchangeRate.TranslateTo == "") {
                            Company.ExchangeRate.TranslateTo = "zh";
                        }
                        if (Company.CompanySetup.Zipcode == "0") {
                            Company.CompanySetup.Zipcode = "";
                        } else {
                            Company.CompanySetup.Zipcode = response[0].ZipCode;
                        }
                        if (Company.CompanySetup.AgentEmail == "" && Company.CompanySetup.AgentId == "") {
                            Company.Hideagentemail = true;
                            //Company.Hideagentemail = !Company.Hideagentemail;
                            // Company.IsHiddenagentname = false;
                            // Company.Hideagentemail = !Company.Hideagentemail;
                            //Company.CompanySetup.AgentId = response[0].AgentName;
                        } else if (Company.CompanySetup.AgentEmail == "") {
                            Company.IsHiddenagentname = true;
                            Company.CompanySetup.AgentId = response[0].AgentName;
                        } else {
                            Company.CompanySetup.AgentEmail = response[0].AgentEmail;
                            Company.Hideagentemail = true;
                        }
                        if (response[0].GrossweightCarton == 0) {
                            Company.CartonSetup.Grossweight = $localStorage.admingrossweight;
                        } else {
                            Company.CartonSetup.Grossweight = response[0].GrossweightCarton;
                        }
                        if (response[0].Length == 0) {

                            Company.CartonSetup.Lenght = $localStorage.length;
                        } else {

                            Company.CartonSetup.Lenght = response[0].Length;
                        }
                        if (response[0].Width == 0) {

                            Company.CartonSetup.Width = $localStorage.width;
                        } else {

                            Company.CartonSetup.Width = response[0].Width;
                        }
                        if (response[0].Height == 0) {

                            Company.CartonSetup.Height = $localStorage.height;
                        } else {

                            Company.CartonSetup.Height = response[0].Height;
                        }
                        if (response[0].ExchangeRate == 0) {

                            Company.ExchangeRate.DollarRMB = $localStorage.ExchangeRate;
                        } else {
                            Company.ExchangeRate.DollarRMB = response[0].ExchangeRate
                        }
                        Company.CartonSetup.Volume = response[0].Volume;

                        //  $('#country').val(country);
                        // $("#country").select2().select2('val', country);
                        if (Company.ExchangeRate.Thirdcurrency == "1") {
                            $('#currency').val(Company.ExchangeRate.Currency);
                            // $("#corrency").select2().select2('val', Company.ExchangeRate.Currency);
                            Company.ExchangeRate.Thirdcurrency = true;
                        } else {
                            Company.ExchangeRate.Thirdcurrency = false;
                        }
                        $('#tfrom').val(Company.ExchangeRate.TranslateFrom);
                        // $("#tfrom").select2().select2('val', Company.ExchangeRate.TranslateFrom);
                        $('#tto').val(Company.ExchangeRate.TranslateTo);
                        //$("#tto").select2().select2('val', Company.ExchangeRate.TranslateTo);
                    } else {
                        $('.ajax_loader').hide();
                        Company.Hideagentemail = true;
                        if (Company.ExchangeRate.TranslateFrom == "") {
                            Company.ExchangeRate.TranslateFrom = "en";
                        }
                        if (Company.ExchangeRate.TranslateTo == "") {
                            Company.ExchangeRate.TranslateTo = "zh";
                        }
                        $("#setupbutton").html('Submit');
                         Company.CompanySetup.AgentId = response[0].AgentName;
                        if(Company.CompanySetup.AgentId !=""){
                        Company.Hideagentemail = false;
                        Company.IsHiddenagentname = true;
                        Company.CompanySetup.AgentId = response[0].AgentName;
                        }
                        Company.CompanySetup.OrderEmail = response[0].MailOrder;
                        Company.CartonSetup.Grossweight = $localStorage.admingrossweight;
                        Company.CartonSetup.Lenght = $localStorage.length;
                        Company.CartonSetup.Width = $localStorage.width;
                        Company.CartonSetup.Height = $localStorage.height;
                        Company.ExchangeRate.DollarRMB = $localStorage.ExchangeRate;
                    }
                } else {
                    $('.ajax_loader').hide();
                    Company.Hideagentemail = true;
                    if (Company.ExchangeRate.TranslateFrom == "") {
                        Company.ExchangeRate.TranslateFrom = "en";
                    }
                    if (Company.ExchangeRate.TranslateTo == "") {
                        Company.ExchangeRate.TranslateTo = "zh";
                    }
                    $("#setupbutton").html('Submit');
                    //Company.CompanySetup.OrderEmail = response[0].OrderEmail;
                    Company.CompanySetup.OrderEmail = response[0].MailOrder;
                    Company.CartonSetup.Grossweight = $localStorage.admingrossweight;
                    Company.CartonSetup.Lenght = $localStorage.length;
                    Company.CartonSetup.Width = $localStorage.width;
                    Company.CartonSetup.Height = $localStorage.height;
                    Company.ExchangeRate.DollarRMB = $localStorage.ExchangeRate;
                }
                // console.log(Company.CompanySetupDetails);
                $('.ajax_loader').hide();
            })
            $('.ajax_loader').hide();
        }
        Company.getCompanyDetails();



        Company.Getcartondefaultvalues = function() {
            AccountService.getCartonvalues().success(function(response) {
                $localStorage.admingrossweight = response.GrossWeight;
                $localStorage.length = response.Length;
                $localStorage.width = response.Width;
                $localStorage.height = response.Height;
                $localStorage.ExchangeRate = response.ExchangeRate;

            })
        }
        Company.Getcartondefaultvalues();


        Company.ShowHide = function() {
            AccountService.getallgoldagents().success(function(response) {
                Company.result = response;
            })


        }
        Company.ShowHide();


        Company.selectagent = function(name, id) {
            $localStorage.AgentId = "0";
            $localStorage.AgentIdName = "";
            //Company.modalShown = !Company.modalShown;
            $localStorage.AgentId = id;
            Company.CompanySetup.AgentEmail = "";
            Company.CompanySetup.AgentId = name;
            $localStorage.AgentIdName = name;
            Company.CompanySetup.AgentsId = id;
            Company.Hideagentemail = false;
            Company.IsHiddenagentname = true;
            $("#modalclose").trigger('click');

        }
        Company.removegoldagentname = function() {
            Company.IsHiddenagentname = false;
            Company.Hideagentemail = !Company.Hideagentemail;
            Company.CompanySetup.AgentId = "";
            $localStorage.AgentId = "0";
            $localStorage.AgentIdName = "";
        }

    })
    ($scope.Company);

}]);
