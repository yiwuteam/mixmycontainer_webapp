
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

</style>
</head>
<body>
<table border='1'>
  <tr style="font-weight: 900;">
    <td>Order Number</td>
    <!-- <td>User Phone Number</td> -->
    <td>Order Date</td>
    <td>Booth Count</td>
    <td>Order US value</td>
    <td>Container Count</td>
    <td>Order RMB Value</td>
    <td>TOtal CBM</td>
    <td>Price In RMB</td>
    <td>Item Number</td>
    <td>Booth Id</td> 
    <td>Unit Per Carton</td>
    <td>Unit Quantity</td>
    <td>Price In US</td>
    <td>Total Price In US</td> 
  </tr>
  
  <?php
foreach ($email as $key => $value) {
	?>
    <tr>
    <td><?php echo $value['OrderNumber'];
?></td>
    <td><?php echo $value['Date'];
?></td>
    <td><?php echo $value['TotalBooth'];
?></td>
    <td><?php echo $value['TotalOrderUsValue'];
?></td>
    <td><?php echo $value['TotalContainer'];
?></td>
    <td><?php echo $value['TotalOrderRmbValue'];
?></td>
    <td><?php echo $value['TotalOrderCBM'];
?></td>
    <td><?php echo $value['PriceInRmb'];
?></td>
    <td><?php echo $value['ItemNo'];
?></td>
    <td><?php echo $value['BoothId'];
?></td>
    <td><?php echo $value['UnitperCarton'];
?></td>
   <td><?php echo $value['UnitQuantity'];
?></td>
   <td><?php echo $value['PriceInUs'];
?></td>
   <td><?php echo $value['TotalPriceInUS'];
?></td> 
    </tr>
  <?php
}
?>

 
</table> 

</body>
</html>