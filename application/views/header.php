<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:48:58 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>LOGIN</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

     <link rel="stylesheet" href="<?php echo asset_url();?>css/animate.css">
     <link rel="stylesheet" href="<?php echo asset_url();?>css/jquery.gritter.css">
      <link rel="stylesheet" href="<?php echo asset_url();?>css/sweet-alert.css" />
      <link rel="stylesheet" href="<?php echo asset_url();?>css/toastr.min.css" /> 
     <link rel="stylesheet" href="<?php echo asset_url();?>css/quirk.css">

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo asset_url();?>fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>styles/style.css">


    <style>
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/images/loader/loader.gif) center no-repeat;
   
}


.ajax_loader{

  display: none;
   position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/images/loader/loader.gif) center no-repeat;
}

}
</style>

 <script>
  
//     $(window).load(function() {
     
//         $(".se-pre-con").fadeOut("slow");;
//     });
// </script>

</head>

<body class="blank">
   <div class="ajax_loader"></div>