<?php $this->load->view('layouts/adminheader'); ?>
<?php $this->load->view('layouts/adminsidebar'); ?>
<title>USERS</title>
<style type="text/css">
    .wrapcontent{
            word-wrap: break-word;
            /*font-size:18px;*/
    }
    button.btn.dropdown-toggle {
    background-color: transparent;
}
</style>
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="#">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                   <!--  <li><a href="#!">Dashboard</a></li> -->
                    <li class="active">
                        <span>Users</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Users
            </h2>
            <small>List of users</small>
        
        </div>
    </div>
</div>

<div class="content animate-panel" ng-controller="AdminViewController" infinite-scroll="Admin.GetUsers()">

<div class="row" ng-show="Admin.Users.length>0">
    <div class="col-lg-3" ng-repeat-start="user in Admin.Users" >
        <div class="hpanel hgreen contact-panel">
            <div class="panel-body">
             <div class="btn-group pull-right">
                    <button data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false"><i class="pe-7s-more"></i></button>
                    <ul class="dropdown-menu">
                     <!--    <li><a href="#">Edit</a></li> -->
                        <li><a href="#!" ng-click="Admin.DeleteUser(user)">Delete</a></li>
                    </ul>
                </div>
                <!-- <span class="label label-success pull-right"></span> -->
                <img alt="PrfilePic" class="img-circle m-b" src="../assets/images/profilepic/{{user.Image}}" ng-if="user.Image">
             

                <img alt="PrfilePic" class="img-circle m-b" src="../assets/images/userdummypic.png" ng-if="!user.Image">
                <h3 class="wrapcontent"><!-- <a href="#" class="wrapcontent"> --> {{user.Email}}<!-- </a> --></h3>
                <!-- <div class="text-muted font-bold m-b-xs">California, LA</div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.
                </p> -->
            </div>
           <!--  <div class="panel-footer contact-footer">
                <div class="row">
                    <div class="col-md-4 border-right"> <div class="contact-stat"><span>Projects: </span> <strong>200</strong></div> </div>
                    <div class="col-md-4 border-right"> <div class="contact-stat"><span>Messages: </span> <strong>300</strong></div> </div>
                    <div class="col-md-4"> <div class="contact-stat"><span>Views: </span> <strong>400</strong></div> </div>
                </div>
            </div> -->
        </div>
    </div>
      <div class="clearfix" ng-if="($index+1)%4==0"></div>
      <div ng-repeat-end=""></div>
</div>
<div class="row">
    <div id="loader" class="loading row text-center" ng-show="Admin.UserLoader&&Admin.ScrollLoader"> <img src="../assets/images/loader.svg" alet="Loading.."/></div>
    <div id="nocontent" class="nocontent row text-center"  ng-show="Admin.Users.length==0&&!Admin.UserLoader"> <img src="../assets/images/warning.png" alet="Loading.."/><br/><h1>No Users</h1></div>
</div>

    

</div>
<?php $this->load->view('layouts/footer'); ?>