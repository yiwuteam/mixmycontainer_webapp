<!DOCTYPE html>

<html>
<head>
<title>Yiwutrader</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all">
<style type="text/css">
/* BackToTop button css */
#scroll {
    position:fixed;
    right:10px;
    bottom:10px;
    cursor:pointer;
    width:50px;
    height:50px;
    background-color:#3498db;
    text-indent:-9999px;
    display:none;
    -webkit-border-radius:60px;
    -moz-border-radius:60px;
    border-radius:60px
}
#scroll span {
    position:absolute;
    top:50%;
    left:50%;
    margin-left:-8px;
    margin-top:-12px;
    height:0;
    width:0;
    border:8px solid transparent;
    border-bottom-color:#ffffff
}
#scroll:hover {
    background-color:#e74c3c;
    opacity:1;filter:"alpha(opacity=100)";
    -ms-filter:"alpha(opacity=100)";
}
</style>
</head>
<body id="top">

<div class="wrapper row0">
  <div id="topbar" class="clear">
  </div>
</div>

<div class="wrapper row1">
  <header id="header" class="clear">
    <div id="logo" class="fl_left">
      <h1><a><img src="assets/images/homepage/demo/logo.png"/></a></h1>
    </div>
    <nav id="mainav" class="fl_right">
      <ul class="clear">
        <li class="active"><a class="clickactive" href="#home">Home</a></li>
        <li><a class="clickactive" href="#about">About Us</a></li>
        <li><a class="clickactive" href="#gold">Gold Agent</a></li>
        <li><a href="../Login/register">Sign Up</a></li>
        <li><a href="../Login">Login</a></li>
      </ul>
    </nav>
  </header>
</div>
<div class="wrapper">
  <a><img class="stretch" src="assets/images/homepage/demo/1900x500-min.png" alt=""></a>
</div>
<div class="wrapper row2">

</div>
<div class="wrapper row3">
  <main class="container clear">
    <!-- main body -->
    <div class="group">
      <div class="first">
        <p class="heading" id="home">Yiwu City in China is home to the largest wholesale market of small commodities in the world.
There are 59000 Chinese suppliers exhibiting and selling goods from the Yiwu International Trade City Market -all of them permanently located within the International Trade city districts.
</p>
<a><p class="btmspace-20">There are 5 main advantages of buying and sourcing goods from Yiwu:</p></a>
        <ul class="nospace group">
          <li class="one_half first btmspace-30">
            <article class="service largeicon"><i class="icon nobg circle fa fa-check"></i>
              <h6 class="heading"><a>sourcing & shopping</a></h6>
              <p>lt is the worlds largest commodity market that allows for 'one stop sourcing & shopping.</p>
            </article>
          </li>
          <li class="one_half btmspace-30">
            <article class="service largeicon"><i class="icon nobg circle fa fa fa-check"></i>
              <h6 class="heading"><a>Large no of Suppliers </a></h6>
              <p>The ability to contact and deal with over 59000 suppliers easily</p>
            </article>
          </li>
          <li class="one_half first btmspace-30">
            <article class="service largeicon"><i class="icon nobg circle fa fa fa-check"></i>
              <h6 class="heading"><a>Multiple Purchases</a></h6>
              <p>The ease and convenience that allow buyers to consolidate (Mix) multiple purchases from multiple suppliers into shipping containers.</p>
            </article>
          </li>
          <li class="one_half">
            <article class="service largeicon"><i class="icon nobg circle fa fa fa-check"></i>
              <h6 class="heading"><a>Deliver Fast</a></h6>
              <p>Delivery and consolidation of goods is fast, because most items are in stock and can be delivered within a week.</p>
            </article>
          </li>
          <li class="first btmspace-50">
            <article class="service largeicon"><i class="icon nobg circle fa fa fa-check"></i>
              <h6 class="heading"><a>Minimum Order Quantity </a></h6>
              <p>The ability to purchase small quantities of many items (at wholesale prices) rather
than large quantities and a small range - as in the case of direct factory
purchases. (There are seldom MOQ-Minimum Order Quantity restrictions with Yiwu suppliers.)
With direct factory purchases, customers usually have to buy whole containers of just
a few items from the same factory and are obligated to purchase minimum order quantities of
these products as well.
</p>
            </article>
          </li>
        </ul>
      </div>
    </div>

    <div id="comments">
        <a>  <h2 id="about">What is yiwutrader ? How does it help</h2></a>
        <address>Yiwutrader is a free tool that allows buyers visiting Yiwu and the International Trade City to:</address>
          <ul>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar.png" alt=""></figure>
                  <div class="comcont">
                    <p>Easily keep track of the suppliers they purchased from.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar.png" alt=""></figure>
                  <div class="comcont">
                    <p>Consolidate their containers in real time and change the configuration of shipping containers chosen to optimize cost savings. Buyers can view and edit their purchase orders and actually see how many containers (and what type 20'; 40', 40'HQ) they have filled up with their purchases. The buyer is also able to see how much of a container he/she has stuffed visually and as a percentage.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar.png" alt=""></figure>
                  <div class="comcont">
                    <p>Buyers can generate purchase orders with pictures and 'English to Chinese; translation on the product descriptions . This is a valuable tool and allows the buyer to make sure that the seller (booth owner) understands specific instructions given ensuring accurate purchase orders.
The pictures help ensure that the correct items are shipped and help minimize misunderstanding: and instructions lost in translation.
</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar.png" alt=""></figure>
                  <div class="comcont">
                    <p>The system has the ability to achieve 3 way currency conversion, (Yiwu Traders usually quote in Chinese Yaun - RMB), the system automatically converts this to US dollars (Which is the currency that buyers usually pay in for their purchases. It also converts each cost into a factored (estimatec landing cost) in the buyers home currency. This makes for accurate buying with proper accountability and gives the buyer confidence to plan for the purchases financially. It minimizes over buying or under buying and helps buyers stick to purchasing budgets.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar.png" alt=""></figure>
                  <div class="comcont">
                    <p>It saves time, eliminates mistakes, enhances communication and its just plain fun to use.</p>
                  </div>
                </header>
              </article>
            </li>
          </ul>
          <a>  <h2 id="gold">What is a Gold Agent? Why do we recommend using them?</h2></a>
            <address class="btmspace-10">A Yiwutrader Gold Agent is a shipping agent in Yiwu that has been audited and verified by the Yiwutrader business auditing team. These Yiwutrader Gold Agents sign a code of conduct and have been doing business worldwide for a number of years . Yiwutrader even audits and veifies past dealings with Importers and customers. Only the best export agents make the Gold Agent status. This ensures that your shipment from Yiwu is well managed and executed to satisfaction.</address>
            <a>Each Yiwutrader Gold agent: </a>
            <ul>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Is verified and checked by a competent audit team.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Has been doing business in China and worldwide for a number of years and holds a valid business licence.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Has signed a code of conduct and guarantee of service/supply agreement.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Has an impeccable amd independently verified reputation. They agree to allow for independent arbitration in the case of any disputes that may arise with customers.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Provides the very best service and translation facilities.</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><img src="assets/images/homepage/demo/avatar1.png" alt=""></figure>
                  <div class="comcont">
                    <p>Ensures that buyers are well looked after and their needs are met when visiting Yiwu.</p>
                  </div>
                </header>
              </article>
            </li>
          </ul>
            <a>  <h2>What is the difference between the free version of Yiwutrader and the premium buyer subscription ?</h2></a>
            <address>Yiwutrader is currently in beta release. Our free version is already completely functional.<br/>
              For a premium buyer subscription of just $30 a year, we hope to provide the following services: </address>
          <ul>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>The ability to export purchase orders to Excel (Available mid 2017)</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>The right to access a Yiwu Chat facility that enables buyers, gold agents and booth owners to talk, exchange ideas and compare prices and ratings of products. This chat facility will not be available to free users. (Available mid 2017)</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>To allow advertising and company listings on our worldwide Yiwutrader's database for $20 a year.
Your information as a Yiwu Buyer can be made available to the public if you choose. This enables you to promote your business as a direct importer from Yiwu in your country. People looking for goods imported from Yiwu will naturally contact our premium Yiwu Buyers. (Available Now)
</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>To view special offers made by Booth/Factory owners to our premium buyer database. (Available Now)</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>A direct dial local number (in your country) for your company that is forwarded to any Gold Agent of your choice. This will enable you to save on international calls by paying a local call rate to speak to a Gold Agent. (Available end 2017)</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>Iphone and Android application, allowing you to create orders offline. (Available end 2017)</p>
                  </div>
                </header>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar"><i class="icon nobg circle fa fa-hand-o-right"></i></figure>
                  <div class="comcont">
                    <p>To view trends and Hot Items based on cumulative sales of comodities from Yiwu. Category based (Available end 2017 )</p>
                  </div>
                </header>
              </article>
            </li>
          </ul>
        </div>
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>







<div class="wrapper row5">
  <div id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">yiwutrader.com</a></p>
  </div>
</div>
<!-- JAVASCRIPTS -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.mobilemenu.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type='text/javascript'>
$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});

$('.clickactive').on('click', function() {
  $('li').removeClass('active');
  $(this).closest('li').addClass('active');
});


$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 750);
    return false;
});
</script>
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>
</body>
</html>
