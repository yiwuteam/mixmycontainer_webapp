<!DOCTYPE html>
<html ng-app="adminmixMyContainer">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <!-- <title>MMC</title> -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="../assets/images/favicon.ico" />
    <!-- Vendor styles -->
    <link rel="stylesheet" href="../assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="../assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweetalert2.css" />
    <link rel="stylesheet" href="../assets/css/toastr.min.css" />
    <!-- App styles -->
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../assets/styles/style.css">
   <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en">
    <!-- App styles -->
   
    
    <link rel="stylesheet" href="../assets/css/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
   
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="../assets/css/ladda.min.css">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>
      <!--Jquery  plugin-->
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <script src="../assets/js/jquery.dataTables.min.js"></script>
      <script src="../assets/js/select2.js"></script>
      <script src="../assets/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
      <script src="../assets/js/intlTelInput.js"></script>
      <script src="../assets/js/utils.js"></script>
      <script src="../assets/js/ng-intl-tel-input.js"></script>
      <script src="../assets/js/ng-intl-tel-input.directive.js"></script>
      <script src="../assets/js/ng-intl-tel-input.module.js"></script>
      <script src="../assets/js/ng-intl-tel-input.provider.js"></script>
      <script src="../assets/js/spin.min.js"></script>
      <script src="../assets/js/ladda.min.js"></script>
      <script src="../assets/js/angular-ladda.js"></script>
      <script src="../assets/js/angular-base64.js"></script>
      <script src="../assets/js/ng-infinite-scroll.min.js"></script>
      
      <!-- MY App -->
      <script src="../application/ng/adminapp.js"></script>
     
      <script src="../assets/js/ngStorage.min.js"></script>

      <script src="../application/ng/Services/Admin/AdminService.js"></script>
      

      <!-- App Controller -->
      <script src="../application/ng/Controller/Admin/AdminController.js"></script>
      <script src="../application/ng/Controller/Admin/AdminViewController.js"></script>
     
      <!--App directive -->
       <script src="../application/ng/Directive/fileread.js"></script>
       <script src="../application/ng/Directive/select3.js"></script>
       <script src="../application/ng/Directive/digitonly.js"></script>
       <script src="../application/ng/Directive/passwordverify.js"></script>
      <script src="../application/ng/Directive/ng-image-compress.js"></script>
      <!--Angular Datatable-->
      <link rel="stylesheet" href="../assets/css/angular-datatables.min.css">
      <script src="../assets/js/angular-datatables.min.js"></script>
      


<style>
/*.ajax_loader{
  display: none;
   position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/images/ripple.svg) center no-repeat;
}*/
.ajax_loader{
   display: none;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: #FFFFFF;
    opacity: 0.8;
    /*background: rgba(0, 0, 0, 0.8);
    background: url(../assets/images/loader/reload.svg) center no-repeat;*/
}
span.important {
    color: red;
}
button.ladda-button.btn.btn-primary {
    padding: 6px 12px;
    font-size: 14px;
}
input[type="file"] {
    display: block;
    cursor: pointer;
}
</style>

</head>
<body class="hide-sidebar">
<div class="ajax_loader"><img src="../assets/images/loader/page_loading.gif" style="margin-left:600px;margin-top: 220px" /> </div>

<!-- <div class="ajax_loader" style="background: rgba(0, 0, 0, 0.8);"><img src="../assets/images/loader/reload.svg" style="margin-left:600px;margin-top: 220px" /> </div> -->
<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/page_loading.gif" style="color: white;" width="" height="" /> </div> </div>
<!-- <div class="splash" style="background: rgba(0, 0, 0, 1);"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/reload.svg" style="color: white;" width="" height="" /> </div> </div> -->
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->
<div id="header" ng-controller="AdminController">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
      <img src="../assets/images/logo.png" class="img-circle m-b" alt="logo" style="width: 100ox;width: 100px;margin-right: 30px;margin-top: -16px;">
    </div>
    <nav role="navigation">
       <!--  <div class="header-link hide-menu"><i class="fa fa-bars"></i></div> -->
        <!-- <div class="small-logo">
            <span class="text-primary">HOMER APP</span>
        </div>
        <form role="search" class="navbar-form-custom" method="post" action="#">
            <div class="form-group">
                <input type="text" placeholder="Search something special" class="form-control" name="search">
            </div>
        </form> -->
        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                  <!--   <li>
                        <a class="" href="login.html">Login</a>
                    </li> -->
                    <li>
                        <a href="../Admin/manage">Setup</a>
                    </li>
                    <li>
                        <a href="../Admin/viewdata">Update Setup</a>
                    </li>
                     <li>
                        <a href="../Admin/agents">Gold Agents</a>
                    </li>
                     <li>
                        <a href="../Admin/user">Users</a>
                    </li>
                     <li id="logout" class="dropdown">
                      <a href="../Admin/logout">
                         Logout
                      </a>
                   </li>
                </ul>
            </div>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav no-borders">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#!" data-toggle="dropdown">
                      <span class="menu">Menu</span> <i class="pe-7s-keypad  text-danger"></i> 
                    </a>

                    <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <a href="../Admin/manage">
                                        <i class="pe pe-7s-config text-info"></i>
                                        <h5>Setup</h5>
                                    </a>
                                </td>
                               
                                 <td>
                                    <a href="../Admin/viewdata">
                                        <i class="pe pe-7s-tools text-danger"></i>
                                        <h5>Update Setup</h5>
                                    </a>
                                </td>
                                 <td>
                                    <a href="../Admin/agents">
                                        <i class="pe pe-7s-id text-success"></i>
                                        <h5>Gold Agents</h5>
                                    </a>
                                </td>
                                </tr>
                                <tr>
                                 <td>
                                    <a href="../Admin/user">
                                        <i class="pe pe-7s-users text-primary"></i>
                                        <h5>Users</h5>
                                    </a>
                                </td>
                                 <td>
                                    <a href="#!" data-toggle="modal" data-target="#passwordmodal" ng-click="Admin.ClearPasswordModel()">
                                        <i class="pe pe-7s-key text-warning"></i>
                                        <h5>Change Password</h5>
                                    </a>
                                </td>
                             <!--    <td>
                                    <a href="contacts.html">
                                        <i class="pe pe-7s-users text-success"></i>
                                        <h5>Contacts</h5>
                                    </a>
                                </td> -->
                            </tr>
                            
                            </tbody>
                        </table>
                    </div>
                </li>
                <li id="logout" class="dropdown">
                    <a href="../Admin/logout">
                        <i class="pe-7s-upload pe-rotate-90" title="LOGOUT" ></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
     <div class="modal fade" id="passwordmodal" tabindex="-1" role="dialog" aria-hidden="true"  >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header text-center">
                                    <h4 class="modal-title">Change Password</h4>
                                    <small class="font-bold">Change your password.</small>
                                </div>
                            <form name="pass_form" method="post" novalidate ng-submit="Admin.ChangePassword(pass_form)">
                                <div class="modal-body">
                                    <div class="form-group row">
                                       <label class="col-sm-4 control-label">Old Password</label>
                                        <div class="col-sm-8"><input type="password" name="oldpass" id="oldpass" required="" class="form-control" ng-class="{ 'error' : (pass_form.oldpass.$invalid && pass_form.oldpass.$touched)||(
                                                       Admin.Passerror && pass_form.oldpass.$invalid)}" ng-model="Admin.OldPassword">
                                                       <label ng-show=" (pass_form.oldpass.$invalid && pass_form.oldpass.$touched)||(
                                                       Admin.Passerror && pass_form.oldpass.$invalid)" class="error">Enter Old Password</label>
                                                       </div>
                                      
                                    </div>
                                    <div class="form-group row">
                                      <label class="col-sm-4 control-label">New Password</label>
                                       <div class="col-sm-8"><input type="password" class="form-control" name="newpass" id="newpass"  ng-class="{ 'error' : (pass_form.newpass.$invalid && pass_form.newpass.$touched)||(
                                                       Admin.Passerror && pass_form.newpass.$invalid)||(
                                                       Admin.Passerror && Admin.Compareerror)}" ng-model="Admin.NewPassword" required="true">
                                                      <label ng-show=" (pass_form.newpass.$error.required && pass_form.newpass.$touched)||(
                                                       Admin.Passerror && pass_form.newpass.$error.required)" class="error">Enter New Password</label>
                                                       <!--  <label ng-show="pass_form.newpass.$error.passwordVerify" class="error">Password didn't match</label>-->
                                                       </div> 
                                                       
                                    </div>
                                    <div class="form-group row">
                                      <label class="col-sm-4 control-label">Confirm Password</label>
                                      <div class="col-sm-8"><input type="password" class="form-control" name="confirmpass" id="confirmpass"  ng-class="{ 'error' : (pass_form.confirmpass.$invalid && pass_form.confirmpass.$touched)||(
                                                       Admin.Passerror && pass_form.confirmpass.$invalid)||(
                                                       Admin.Passerror && Admin.Compareerror)}" ng-model="Admin.ConfirmPassword" required="true" password-verify="Admin.NewPassword">
                                                       <label ng-show=" (pass_form.confirmpass.$error.required && pass_form.confirmpass.$touched)||(
                                                       Admin.Passerror && pass_form.confirmpass.$error.required)" class="error">Enter Confirm Password</label>
                                                        <label ng-show="(pass_form.confirmpass.$error.passwordVerify)||(
                                                       Admin.Passerror && Admin.Compareerror)" class="error">Password didn't match</label>
                                                       </div>
                                                       </div>
                                                      
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" ladda="Admin.passwordLoading" class="ladda-button btn btn-primary"  data-spinner-size="30" data-style="slide-left">Save Changes </button>
                                </div>
                              </form>
                            </div>
                        </div>
</div>
</div>
