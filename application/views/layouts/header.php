<!DOCTYPE html>
<html  ng-app="mixMyContainer">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <!-- <title>MMC</title> -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="../assets/images/favicon.ico" />
    <!-- Vendor styles -->
    <link rel="stylesheet" href="../assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en" />
    <link rel="stylesheet" href="../assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="../assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.css" />

    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweetalert2.css" />
    <link rel="stylesheet" href="../assets/css/toastr.min.css" />
      <link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
    <!-- App styles -->
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../assets/styles/style.css">

    <!-- App styles -->
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../assets/styles/style.css">
    <link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="../assets/css/select2.css" />


    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="../assets/css/intlTelInput.css" />
     <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script> -->
      <!--Jquery  plugin-->
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    
    


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
       <script src="../assets/js/jquery.js"></script> 
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>
 <!--       <script src="../assets/js/jquery.dataTables.min.js"></script>
      <script src="../assets/vendor/select2-3.5.2/select2.min.js"></script> -->
   
      <!--Jquery dataTable plugin-->
  <!--     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> -->
      <script src="../assets/js/jquery.dataTables.min.js"></script>

      <script src="../assets/js/select2.js"></script> 
      <script src="../assets/vendor/select2-3.5.2/select2.min.js"></script>
  <script src="../assets/js/datepicker.min.js"></script>
      <script src="../assets/js/utils.js"></script>
      <script src="../assets/js/intlTelInput.js"></script> 
      

      <script src="../assets/js/ng-intl-tel-input.js"></script>
      <script src="../assets/js/ng-intl-tel-input.module.js"></script>
      <script src="../assets/js/ng-intl-tel-input.provider.js"></script>
      <script src="../assets/js/ng-intl-tel-input.directive.js"></script>

      <!-- MY App -->
      <script src="../application/ng/app.js"></script>
      <script src="../application/ng/routes.js"></script>
      <script src="../assets/js/ngStorage.min.js"></script>

      <script src="../application/ng/Services/Login/LoginService.js"></script>
      <script src="../application/ng/Services/user/AccountService.js"></script>
      <script src="../application/ng/Services/Order/OrderService.js"></script>

      <!-- App Controller -->
      <script src="../application/ng/Controller/login/LoginController.js"></script>
      <script src="../application/ng/Controller/user/AccountController.js"></script>
      <script src="../application/ng/Controller/Order/OrderController.js"></script>
      <!--App directive -->
      <script src="../application/ng/Directive/select2.js"></script>
      <script src="../application/ng/Directive/StringToNumber.js"></script>
      <script src="../application/ng/Directive/numberOnly.js"></script>
      <script src="../application/ng/Directive/onlyDigits.js"></script>
      <script src="../application/ng/Directive/datetime.js"></script>
      <script src="../application/ng/Directive/validNumber.js"></script>
      <script src="../application/ng/Directive/getNumber.js"></script>
      <!--Angular Datatable-->
      <link rel="stylesheet" href="../assets/css/angular-datatables.min.css">
      <script src="../assets/js/angular-datatables.min.js"></script>



<style>
.ajax_loader{
    display: none;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: #FFFFFF;
    opacity: 0.8;
    /*background: rgba(0, 0, 0, 0.8);
    background: url(../assets/images/loader/reload.gif) center no-repeat;*/
}
</style>

</head>
<body>
<div class="ajax_loader"><img src="../assets/images/loader/page_loading.gif" style="margin-left:600px;margin-top: 220px" /> </div>

<!-- <div class="ajax_loader" style="background: rgba(0, 0, 0, 0.8);"><img src="../assets/images/loader/reload1.gif" style="margin-left:600px;margin-top: 220px" /> </div>
 --><!-- Simple splash screen-->
 <div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/page_loading.gif" style="color: white;" width="" height="" /> </div> </div>
<!-- <div class="splash" style="background: rgba(0, 0, 0, 1);"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/reload1.gif" style="color: white;" width="" height="" /> </div> </div> -->

<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
      <a href="../Login/dashboard"><img src="../assets/images/yiwutrader_logo.png" class="img-circle m-b" alt="logo" style="width: 100ox;width: 100px;margin-right: 30px;margin-top: -16px;"></a>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="pull-right">
            <ul class="nav navbar-nav no-borders">

                <li id="logout" class="dropdown">
                    <a href="../Login/logout">
                        <i class="pe-7s-upload pe-rotate-90" title="LOGOUT" ></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- <div class="small-logo">
            <span class="text-primary">HOMER APP</span>
        </div>
        <form role="search" class="navbar-form-custom" method="post" action="#">
            <div class="form-group">
                <input type="text" placeholder="Search something special" class="form-control" name="search">
            </div>
        </form> -->
        <!-- <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="" href="login.html">Login</a>
                    </li>
                    <li>
                        <a class="" href="login.html">Logout</a>
                    </li>
                    <li>
                        <a class="" href="#!">Profile</a>
                    </li>
                </ul>
            </div>
        </div> -->
        <!-- <div class="navbar-right">
            <ul class="nav navbar-nav no-borders">

                <li id="logout" class="dropdown">
                    <a href="../Login/logout">
                        <i class="pe-7s-upload pe-rotate-90" title="LOGOUT" ></i>
                    </a>
                </li>
            </ul>
        </div> -->
    </nav>
</div>
