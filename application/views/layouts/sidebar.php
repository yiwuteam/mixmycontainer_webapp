<aside id="menu">
    <div class="tab-pane active" id="navigation" ng-controller="AccountController" ng-init="Account.getImage()">
        <div class="profile-picture" style="height: 168px;">
             <form id="image">
                <img id="My_Imgsidebar" src="../assets/images/userdummypic.png" style="height: 100px;width:100px;margin-top: 15px;" class="img-circle m-b" alt="logo">
                </form> 


        </div>

        <ul class="nav" id="side-menu">
            <li id="dashboard">
                <a href="../Login/dashboard"> <span class="nav-label">Dashboard</span>  </a>
            </li>
            <li id="myaccount">
                <a href="../Login/profile"> <span class="nav-label">My Account</span> </a>
            </li>
            <li id="companysetup">
                <a href="../Login/setup"> <span class="nav-label">Company setup</span> </a>
            </li>
             <li class="nav-parent" id="mainmenu">
                <a href=""><span class="nav-label" id="myorders">My Orders</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level" id="level">
                    <li id="neworder"><a href="../Login/newOrder">New Order</a></li>
                    <li ng-controller="OrderController" id="vieworderdetails"><a href="../Login/orders" ng-click="Order.MenuReset()">View order details</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
<!-- <div class="modal fade" id="MyModel" tabindex="-1" role="dialog" ng-controller="AccountController" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Image Upload</h4>

            </div>
            <div class="modal-body">
              <div id="image-holder"><img id="My_Img" ng-src="{{myImage}}" class="img-circle m-b" /></div>
              <div id="wrapper" style="margin-top: 20px;"><input type="file" id="fileInput" accept="image/x-png, image/gif, image/jpeg" class="file" ng-model="Account.Image" name="files[]" />
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="Account.ImageUpload()">Save changes</button>
            </div>
        </div>
    </div>
</div> -->