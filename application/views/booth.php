<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>
<div id="wrapper">
  <div class="normalheader transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>

                        <li class="active">
                            <span>Booth and Container setup</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Booth and Container Setup
                </h2>
                <small>Provide your Booth and Container details</small>
            </div>
        </div>
    </div>
 <div class="content animate-panel">
   <div class="content">
       <div class="row">
        <div class="col-lg-12">
               <div class="hpanel">
                   <div class="panel-heading">


                   </div>
                   <div class="panel-body" style="display: block;">

                       <form name="simpleForm" novalidate="" id="simpleForm" action="#" method="post">

                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="row">
                                     <div class="form-group col-lg-12">
                                         <label>Order Number</label>
                                         <input type="" value="" id="" class="form-control" name="username" placeholder="Order Number">
                                     </div>
                                     <div class="form-group col-lg-6">
                                         <label>Container Type</label>
                                         <select class="js-source-states" style="width: 100%">
                                             <optgroup label="Alaskan/Hawaiian Time Zone">
                                                 <option value="AK">Alaska</option>
                                                 <option value="HI">Hawaii</option>
                                             </optgroup>
                                         </select>
                                     </div>
                                     <div class="form-group col-lg-6">
                                         <label>Trade</label>
                                         <select class="js-source-states" style="width: 100%">
                                             <optgroup label="Alaskan/Hawaiian Time Zone">
                                                 <option value="AK">Alaska</option>
                                                 <option value="HI">Hawaii</option>
                                             </optgroup>
                                         </select>
                                     </div>


                                     <div class="form-group col-lg-6">
                                         <label>Booth company name</label>
                                         <input type="text" value="" id="" class="form-control" name="" placeholder="postalcode">
                                     </div>
                                     <div class="form-group col-lg-6">
                                         <label>Booth No</label>
                                         <input type="text" value="" id="" class="form-control" name="" placeholder="phone">
                                     </div>
                                     <div class="form-group col-lg-6">
                                         <label>Telephone</label>
                                         <input type="text" value="" id="" class="form-control" name="" placeholder="mobile">
                                     </div>
                                     <div class="form-group col-lg-6">
                                         <label>Business scope</label>
                                         <input type="text" value="" id="" class="form-control" name="" placeholder="fax">
                                     </div>
                                 </div>
                             </div>
                         </div>


                       </form>

                       <div class="m-t-md">

                         <button class="btn btn-sm btn-primary m-t-n-xs pull-right" style="margin-bottom: 20px;" type="submit"><strong>Save & continue</strong></button>


                       </div>

                   </div>
               </div>
           </div>
         </div>
       </div>
</div>
</div>
<?php $this->load->view('layouts/footer'); ?>
