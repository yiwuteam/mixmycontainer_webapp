
<link rel="stylesheet" href="../assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css" />

<style>
button.btn.btn-info.btn-circle.pull-right {
  margin-right: 5px;
}
button.btn.btn-danger.btn-circle.pull-right{
  margin-right: 20px;
}
.col-md-6.pull-right {
  text-align: right;
}
button.btn.btn-warning {
    margin-bottom: 10px;
}
button.btn.btn-info {
    margin-bottom: 10px;
}
</style>
<title>ORDER DETAILS</title>
<div id="wrapper">

      <div class="normalheader transition animated fadeIn">
          <div class="hpanel">
              <div class="panel-body">
                  <a class="small-header-action" href="#">
                      <div class="clip-header">
                          <i class="fa fa-arrow-up"></i>
                      </div>
                  </a>

                  <div id="hbreadcrumb" class="pull-right m-t-lg">
                      <ol class="hbreadcrumb breadcrumb">
                          <li><a href="../Login/dashboard">Dashboard</a></li>
                          <li><a href="../Login/orders">Order Details</a></li>
                          <li class="active">
                              <span>View Order Details</span>
                          </li>
                      </ol>
                  </div>
                  <h2 class="font-light m-b-xs">
                      View Order Details
                  </h2>
                  <small>View All Order Details</small>
              </div>
          </div>
      </div>


  <div class="content animate-panel" ng-controller="OrderController" ng-app="mixMyContainer">
    <div class="row">
      <div class="col-md-6 pull-left">
        <p>
          Order number: <?php echo $orderid; ?>
        </p>
        <input type="hidden" value="<?php echo $orderid; ?>" id="orderid" >
        <p>Order value: <span>US${{Item.totalordervalueus}}</span> (RMB{{Item.totalordervaluermb}})</p>
        <p>Total containers:<span ng-repeat="item in Item.container"><span ng-if="item.ContainerType==1"><span>20' FCL (32CBM): {{item.Count}}</span>|</span>
        <span ng-if="item.ContainerType==2"><span>40' FCL (66CBM): {{item.Count}}</span>|</span>
        <span ng-if="item.ContainerType==3"><span>40' HQ FCL (75CBM): {{item.Count}}</span></span></span></p>
        <button class="btn btn-info " style="margin-top:20px" type="button"><i class="fa fa-plus"></i>Add New Order</button>
        <button class="btn btn-info " style="margin-top:20px" type="button"><i class="fa fa-plus circle"></i>Add New Product</button>
      </div>
      <div class="col-md-6 pull-right">
        <p>Order Date:{{Item.date}}</p>
        <p>Total CBM on this order: {{Item.totalcbm}}</p>
        <p>Total weight of order: {{Item.totalweight}}</p> 

      <a href="#" onClick ="$('#divid').tableExport({type:'pdf',escape:'false',pdfFontSize:'10', columns: ':visible'});"><button class="btn btn-info" data-toggle="dropdown" type="button"><i class="fa fa-print"></i><br>Print</button></a>
      <a href="../pdf_creator"><button class="btn btn-info" data-toggle="dropdown" type="button"><i class="fa fa-print"></i><br>Print</button></a>
       <!-- <ul class="dropdown-menu">
       <li></li>
       </ul> -->
       <a ng-href="../Login/mailorder?id=<?php echo $orderid; ?>"><button class="btn btn-warning" type="button"><i class="pe-7s-mail"></i> <br>Email Order</button></a>
      </div>

    </div>


      <div class="row">
          <div class="col-lg-12">


              <div class="hpanel">

                 <div id="divid" class="panel-body">
                    <table id="example2" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Booth Number</th>
                        <th>Item Number</th>
                        <th>Description</th>
                        <th>Container</th>
                        <th>Unit Quantity</th>
                        <th>Cartons Quantity</th>
                        <th>USD</th>
                        <th>RMB</th>
                        <th>Total USD</th>
                        <th>Total RMB</th>
                        <th class="abc"></th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in Item.orderitems ">
                        <td>{{item.BoothNumber}}</td>
                        <td>{{item.ItemNo}}</td>
                        <td>{{item.DescriptionFrom}}</td>
                        <td ng-if="item.ContainerType==1">20' FCL (32 CBM)</td>
                        <td ng-if="item.ContainerType==2">40' FCL (66 CBM)</td>
                        <td ng-if="item.ContainerType==3">40' HQ (75 CBM)</td>
                        <td>{{item.UnitQuantity}}</td>
                        <td>{{item.CartonQuantity}}</td>
                        <td>{{item.PriceInUs}}</td>
                        <td>{{item.PriceInRmb}}</td>
                        <td>{{item.TotalPriceInUS}}</td>
                        <td>{{item.TotalPriceInRMB}}</td>
                        <td><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button>
                          </td>
                    </tr>

                    </tbody>
                  </table>
                  </div>

              </div>
          </div>

      </div>



      </div>

      <!-- Right sidebar -->
      <div id="right-sidebar" class="animated fadeInRight">

          <div class="p-m">
              <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md"><i class="pe pe-7s-close"></i>
              </button>
              <div>
                  <span class="font-bold no-margins"> Analytics </span>
                  <br>
                  <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
              </div>
              <div class="row m-t-sm m-b-sm">
                  <div class="col-lg-6">
                      <h3 class="no-margins font-extra-bold text-success">300,102</h3>

                      <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                  </div>
                  <div class="col-lg-6">
                      <h3 class="no-margins font-extra-bold text-success">280,200</h3>

                      <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                  </div>
              </div>
              <div class="progress m-t-xs full progress-small">
                  <div style="width: 25%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar"
                       class=" progress-bar progress-bar-success">
                      <span class="sr-only">35% Complete (success)</span>
                  </div>
              </div>
          </div>
          <div class="p-m bg-light border-bottom border-top">
              <span class="font-bold no-margins"> Social talks </span>
              <br>
              <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
              <div class="m-t-md">
                  <div class="social-talk">
                      <div class="media social-profile clearfix">
                          <a class="pull-left">
                              <img src="images/a1.jpg" alt="profile-picture">
                          </a>

                          <div class="media-body">
                              <span class="font-bold">John Novak</span>
                              <small class="text-muted">21.03.2015</small>
                              <div class="social-content small">
                                  Injected humour, or randomised words which don't look even slightly believable.
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="social-talk">
                      <div class="media social-profile clearfix">
                          <a class="pull-left">
                              <img src="images/a3.jpg" alt="profile-picture">
                          </a>

                          <div class="media-body">
                              <span class="font-bold">Mark Smith</span>
                              <small class="text-muted">14.04.2015</small>
                              <div class="social-content">
                                  Many desktop publishing packages and web page edtors.
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="social-talk">
                      <div class="media social-profile clearfix">
                          <a class="pull-left">
                              <img src="images/a4.jpg" alt="profile-picture">
                          </a>

                          <div class="media-body">
                              <span class="font-bold">Marica Morgan</span>
                              <small class="text-muted">21.03.2015</small>

                              <div class="social-content">
                                  There are many variations of passages of Lorem Ipsum available, but the majority have
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="p-m">
              <span class="font-bold no-margins"> Sales in last week </span>
              <div class="m-t-xs">
                  <div class="row">
                      <div class="col-xs-6">
                          <small>Today</small>
                          <h4 class="m-t-xs">$170,20 <i class="fa fa-level-up text-success"></i></h4>
                      </div>
                      <div class="col-xs-6">
                          <small>Last week</small>
                          <h4 class="m-t-xs">$580,90 <i class="fa fa-level-up text-success"></i></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-xs-6">
                          <small>Today</small>
                          <h4 class="m-t-xs">$620,20 <i class="fa fa-level-up text-success"></i></h4>
                      </div>
                      <div class="col-xs-6">
                          <small>Last week</small>
                          <h4 class="m-t-xs">$140,70 <i class="fa fa-level-up text-success"></i></h4>
                      </div>
                  </div>
              </div>
              <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.
                  Many desktop publishing packages and web page Viewors.
              </small>
          </div>

      </div>

      <!-- Footer-->


</div>

<script>

    $(function () {
        $('#example2').dataTable();

    });

</script>

<?php $this->load->view('layouts/footer'); ?>
<script src="../assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>

<script src="../assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script src="../assets/scripts/homer.js"></script>
 <script type="text/javascript" src="../assets/js/plugins/tableexport/tableExport.js"></script>
  <script type="text/javascript" src="../assets/js/plugins/tableexport/jquery.base64.js"></script>
  <script type="text/javascript" src="../assets/js/plugins/tableexport/html2canvas.js"></script>
  <script type="text/javascript" src="../assets/js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
  <script type="text/javascript" src="../assets/js/plugins/tableexport/jspdf/jspdf.js"></script>
  <script type="text/javascript" src="../assets/js/plugins/tableexport/jspdf/libs/base64.js"></script> 