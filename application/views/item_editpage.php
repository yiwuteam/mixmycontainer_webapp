<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>
<link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
<link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="../assets/css/intlTelInput.css" />

<style>
    .col-md-6.pull-right {
        text-align: right;
    }

    .form-group.col-lg-6.pull-right {
        padding-top: 30px;
    }

    button.padding {
        margin-right: 10px;
    }
</style>
<title>New Item</title>

<div id="wrapper">
    <div id="withmodel" ng-controller="OrderController" ng-app="mixMyContainer" data-ng-init="Item.itemeditpagefunctions()">
        <div class="normalheader transition animated fadeIn">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="#">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>

                    <div id="hbreadcrumb" class="pull-right m-t-lg">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="../Login/dashboard">Dashboard</a>
                            </li>
                            <li>
                                <span><a href="../Login/neworder">New Order</a></span>
                            </li>
                            <li class="active">
                                <span>Edit Item</span>
                            </li>
                        </ol>
                    </div>
                    <h2 class="font-light m-b-xs">
                        Edit Items
                    </h2>
                    <small>edit items</small>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <!-- <form name="itemform" ng-submit="submitForm()"> -->
            <div class="row">
                <div class="col-md-6 pull-left">
                    <input type="hidden" value="<?php echo $itemid; ?>" id="itemid">
                  <span>{{Item.imagebase64}}</span>
                    <p>
                        Order number: <span>{{Item.Orderno}}</span>
                    </p>
                    <p>Order value: <span>US${{Item.Usprice}}</span> (<span>RMB{{Item.Rmbprice}}</span>)</p>
                    <p>Value of purchases in current Booth: <span>US${{Item.Usboothprice}}</span> (<span>RMB{{Item.Rmbboothprice}}</span>)</p>

                </div>
                <div class="col-md-6 pull-right">

                    <p>Total CBM on this order: <span>{{Item.totalcbm}}</span>
                    </p>
                    <p>Total weight of order: <span>{{Item.totalweight}}</span>
                    </p>
                    <select class="js-source-states" style="width: 30%" select2="" ng-model="Item.activecontainer" id="showusedcontainer" name="showusedcontainer"
                      ng-options="usedcontainer.Container for usedcontainer in Item.containers track by usedcontainer.ContainerId">
                </select>

                </div>
                <div class="col-md-12">
                    <p class="pull-right">Container full:<span>{{Item.volumepercentage}}%</span>
                    </p>
                    <div class="m">
                        <div class="progress m-t-xs full progress-striped">

                            <div style="width: {{Item.volumepercentage}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class=" progress-bar progress-bar-warning">
                                <span>{{Item.volumepercentage}}%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-sm btn-primary m-t-n-xs pull-right" style="margin-bottom: 20px;" ng-click="Item.LoadModal()"><strong>New Booth</strong>
                </button>
                    <button class="btn btn-sm btn-success m-t-n-xs pull-right padding endorder" style="margin-bottom: 20px;" type="submit"><strong>End/New Order</strong>
                </button>
                    <button class="btn btn-primary btn-sm m-t-n-xs  padding pull-right pauseorder" style="margin-bottom: 20px;"><strong>Pause Order</strong>
                </button>

                </div>
            </div>
            <form name="item_form" method="post" novalidate ng-submit="Item.updateitem()">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <ul class="nav nav-tabs">

                                <li class="active"><a class="primary" data-toggle="tab" href="#tab1">Item description</a>
                                </li>
                                <li class=""><a class="default" data-toggle="tab" href="#tab2">packing </a>
                                </li>
                                <li class=""><a class="default" data-toggle="tab" href="#tab3">Cost and order quantity</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                              <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group ">
                                                      <label>Item Number<span style="color:red;">*</span></label>
                                                      <input type="text" required="" id="itemno" class="form-control" ng-model="Item.edit_itemno" name="itemno" placeholder="item number">
                                                      <label ng-show="item_form.itemno.$invalid && !item_form.itemno.$pristine" class="error">Item number is required.</label>
                                                  </div>

                                                    <div class="form-group">
                                                      <label>Item Description</label>
                                                      <textarea type="text" required="" ng-blur="Item.text_transilate();" rows="5" id="description" ng-model="Item.edit_itemdescription" name="description" id="description" class="form-control" name="" placeholder="item description">
                                                      </textarea>
                                                      <label ng-show="item_form.description.$invalid && !item_form.description.$pristine" class="error">Item description is required.</label>
                                                  </div>

                                                    <div class="form-group">
                                                      <label>Item Description(secondary language)</label>
                                                      <textarea type="text" rows="5" id="altdescription" ng-model="Item.altdescription" class="form-control" name="altdescription" placeholder="item description">
                                                      </textarea>
                                                  </div>
                                                </div>
                                                <div class="form-group col-lg-4">

                                                      <div class="form-group col-lg-6">
                                                        <label>Item Image</label>
                                                        <canvas id="snapshot" name="snapshot" ng-model="Item.snapshot" height="240"  class="text-center"></canvas>
                                                        <a data-target="#MyModel" data-toggle="modal">Take a Picture</a>
                                                          <!-- <a href="#a" data-toggle="modal" required="" data-target="#MyModel"><img ng-src="{{myImage}}" onerror="this.src='../../../MMC/assets/images/upload.png'" alt="Item image" id="imagefeild" height="100" width="100"> -->
                                                          <!-- </a> -->
                                                      </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right m-t-xs">
                                            <!-- <a class="btn btn-default prev" href="#">Previous</a> -->
                                            <a class="btn btn-default next" href="#!">Next</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label>Gross weight of carton<span style="color:red;">*</span></label>
                                                         <div class="input-group m-b">
                                                        <input type="number" only-digits required="" id="cartonGrossWeight" class="form-control" ng-model="Item.edit_itemgrossweight" name="grossweight" placeholder="gross weight of carton">
                                                        
                                                        <span class="input-group-addon">kg</span>
                                                        </div>
                                                        <label ng-show="item_form.grossweight.$invalid && !item_form.grossweight.$pristine" class="error">Carton gross weight is required.</label>
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                        <label>Length<span style="color:red;">*</span></label>
                                                        <div class="input-group m-b">
                                                        <input type="number" only-digits required=""  id="editLength" class="form-control" ng-change="Item.calculatecartonsize_edit()"  name="length" ng-model="Item.edit_itemlength" placeholder="length of carton">
                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                        <label ng-show="item_form.length.$invalid && !item_form.length.$pristine" class="error">Carton Length is required.</label>
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                        <label>Width<span style="color:red;">*</span></label>
                                                        <div class="input-group m-b">
                                                        <input type="number" only-digits required=""  id="editWidth" class="form-control" ng-change="Item.calculatecartonsize_edit()"  name="width" ng-model="Item.edit_itemwidth" placeholder="width of the carton">
                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                        <label ng-show="item_form.width.$invalid && !item_form.width.$pristine" class="error">Carton Width is required.</label>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Height<span style="color:red;">*</span></label>
                                                        <div class="input-group m-b">
                                                        <input type="number" only-digits required=""  id="editHeight" class="form-control"  ng-change="Item.calculatecartonsize_edit()" name="height" ng-model="Item.edit_itemheight" placeholder="height of the carton">
                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                        <label ng-show="item_form.height.$invalid && !item_form.height.$pristine" class="error">Carton Height is required.</label>
                                                    </div>
                                                    <div class="form-group col-lg-6 pull-right">
                                                        <label>CBM(Carton Volume)</label>
                                                        <div class="input-group m-b">
                                                         <input type="text"  required="" value="{{Item.edit_cbm}}"id="editHeight" class="form-control"  name="height" ng-model="Item.edit_cbm" readonly placeholder="CBM of carton">
                                                        <span class="input-group-addon">cbm</span>
                                                      
                                                    </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right m-t-xs">
                                            <a class="btn btn-default prev" href="#!">Previous</a>
                                            <a class="btn btn-default next" href="#!">Next</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab3" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">

                                                    <div class="form-group col-lg-6">
                                                        <label>Price in US$ <span style="color:red;">*</span></label>
                                                        <input type="number" numbers-Only required=""  id="priceinus" submit-required="true" ng-model="Item.edit_priceinus" ng-change="Item.Getuspriceconversion()" class="form-control" name="priceinus" placeholder="Price in US Dollar">
                                                        <label ng-show="item_form.priceinus.$invalid && !item_form.priceinus.$pristine" class="error">Price in US Dollar is required.
                                                    </div>

                                                    <div class="form-group col-lg-6">
                                                    <label>Units per carton <span style="color:red;">*</span></label>
                                                    <div class="input-group m-b">
                                                        <input type="number" numbers-Only required=""  id="editunitpercarton" ng-model="Item.edit_unitpercarton" ng-change="Item.editcalculatecartonqty()" class="form-control" name="unitpercarton" placeholder="Unit per carton">
                                                        <span class="input-group-addon">no</span>
                                                    </div>
                                                        <label ng-show="item_form.unitpercarton.$invalid && !item_form.unitpercarton.$pristine" class="error">Unit per carton is required.</label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label>Price in RMB <span style="color:red;">*</span></label>
                                                        <!-- <input type="number" numbers-Only required=""  id="ExchangeRate" ng-model="Item.edit_ExchangeRate" ng-keyup="Item.Getrmbpriceconversion()" class="form-control" name="priceinzar" placeholder="Price in RMB"> -->
                                                         <input type="number" required="" numbers-Only  id="priceinrmb" ng-change="" ng-model="Item.edit_ExchangeRate" class="form-control" name="ExchangeRate" placeholder="Price in RMB">
                                                        <label ng-show="item_form.ExchangeRate.$invalid && !item_form.ExchangeRate.$pristine" class="error">Exchange rate is required.
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Units qty <span style="color:red;">*</span></label>
                                                        <div class="input-group m-b">
                                                        <input type="number" required="" numbers-Only  id="editunitqty" ng-change="Item.editcalculatecartonqty()" ng-model="Item.edit_unitqty" class="form-control" name="unitqty" placeholder="unit quantity">
                                                        <span class="input-group-addon">no</span>
                                                    </div>
                                                        <label ng-show="item_form.unitqty.$invalid && !item_form.unitqty.$pristine" class="error">Unit quanity is required.</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-6" ng-if="Item.thirdcurrencystatus == 1">
                                                        <label>Price in <span>{{Item.thirdcurrencyname}}</span></label>
                                                        <input type="number" required="" numbers-Only  id="ExchangeRate3dCurrency" ng-model="Item.edit_ExchangeRate3dCurrency" class="form-control" name="priceinzar" placeholder="Third currency price" readonly>
                                                        <label ng-show="item_form.ExchangeRate3dCurrency.$invalid && !item_form.ExchangeRate3dCurrency.$pristine" class="error">Third currency exchange is required.</label>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Cartons qty <span style="color:red;">*</span></label>
                                                        <div class="input-group m-b">
                                                        <input type="text" required=""   id="editcartonqty" ng-model="Item.edit_cartonqty" class="form-control" name="cartonqty" placeholder="carton quantity" readonly>
                                                        <span class="input-group-addon">no</span>
                                                    </div>
                                                        <label ng-show="item_form.cartonqty.$invalid && !item_form.cartonqty.$pristine" class="error">Carton quantity is required.</label>
                                                    </div>
                                                </div>

                                                <div class="text-right m-t-xs">
                                                    <a class="btn btn-default prev" style="margin-top: -5px; margin-right: 20px;" href="#!">Previous</a>
                                                    <!-- <a class="btn btn-success submitWizard" ng-click="Company.companyDetails()" href="#">Submit</a> -->
                                                    <input type="submit" ng-disabled="item_form.$invalid" class="btn btn-sm btn-primary m-t-n-xs pull-right" style="margin-right: 20px;" value="Update Item" />
                                                </div>
                                            </div>
                                            <!--   <div class="form-group col-lg-6 pull-right">

                                                <a class="btn btn-default prev" href="#!">Previous</a> -->


                                            <!-- ng-click="Item.saveitem()" -->

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="MyModel2" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Container Full</h4>
                    </div>
                    <div class="modal-body" data-ng-init="Item.contianerlist()">
                      <h5>You have filled up your current container and hove some cartons left over. (<span>{{Item.balancecartongs}}</span> cartons = <span>{{Item.popupshowbalancevolume}}</span> cbm)</h5>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <div class="form-group col-lg-12 text-center" style="border-left: 6px solid #55ffff;
    background-color: lightgrey;padding-bottom: 10px;">

                                    <h5>Allocate Overflow to a New Container</h5>

                                <select class="js-source-states" style="width: 100%" select2="" ng-model="Item.addnewcontainer" id="newcontainer" name="newcontainer"
                                  ng-options="containerkey.Container for containerkey in Item.containerlist track by containerkey.ID">
                            </select>
                                <!-- <span>balancevolume : {{Item.popupbalancevolume}}</span>
                                <span>balanceweight : {{Item.popupbalanceweight}}</span> -->
                                    </br>
                                    </br>
                                    <button type="button" class="btn btn-primary" ng-click="Item.newContianer()" >Start filling new container</button>
                                </div>

                                <div class="form-group col-lg-12 text-center" style="border-left: 6px solid #55ffff;
    background-color: lightgrey;padding-bottom: 10px;">
                                    <h5>You can upgrade your current container <span>{{Item.currentcotainername}}</span> container to large one</h5>

                               <select class="js-source-states" style="width: 100%" select2="" ng-model="Item.addupgradecontainer" id="upgradecontainer" name="upgradecontainer"
                                  ng-options="containerkey.Container for containerkey in Item.upgradecontainerlist track by containerkey.ID">
                            </select>
                                    </br>
                                    </br>
                                    <button type="button" class="btn btn-primary" ng-click="Item.upgradeContainer()">Upgrade current container</button>
                                </div>

                                <div class="form-group col-lg-12 text-center" style="border-left: 6px solid #55ffff;
    background-color: lightgrey;padding-bottom: 10px;">
                                    <h5>Ignore and Continoue to Shop</h5>
                                    </br>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" >Ignore & Continoue</button>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="../assets/vendor/select2-3.5.2/select2.min.js"></script>
<script src="../assets/vendor/iCheck/icheck.min.js"></script>
<script src="../assets/js/intlTelInput.js"></script>

<script>
    $(document).ready(function() {

        var image = $('#imagefeild').val();

        if (image == '') {
            //  image.src = "../assets/images/item-defult.jpg";
            //  $('#myImage').attr('src','../assets/images/item-defult.jpg');
            $('#imagefeild').attr('src', '../assets/images/item-defult.jpg');
        }
    });

    //$(".js-source-states").select2();

    $(function() {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $('a[data-toggle="tab"]').removeClass('primary');
            $('a[data-toggle="tab"]').addClass('default');
            $(this).removeClass('default');
            $(this).addClass('primary');
        })

        $('.next').click(function() {
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href=#' + nextId + ']').tab('show');
        })

        $('.prev').click(function() {
            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href=#' + prevId + ']').tab('show');
        })


    });
</script>

<?php $this->load->view('layouts/footer'); ?>

<div class="modal fade" id="MyModel" tabindex="-1" role="dialog" ng-controller="OrderController" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Image Upload</h4>

            </div>
            <!-- <div class="modal-body">
                <div id="image-holder"><img id="My_Img" ng-src="{{myImage}}" class="img-circle m-b" onerror="this.src='../../../MMC/assets/images/upload.png'" height="100" width="100" /></div>
                <input type="file" accept="image/*" id="fileInput" class="file" ng-model="Item.Image" name="files[]" />
            </div> -->
            <div class="modal-body" align="center">
            <webcam placeholder="'../assets/images/ripple.svg'"  channel="channel" on-stream="Item.onStream(stream)" on-streaming="Item.onSuccess()" on-error="Item.onError(err)">
            </webcam>
            <button ng-click="Item.makeSnapshot()" class="btn btn-sm btn-success " data-dismiss="modal">Click</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="Item.ImageUpload()">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="MyModel1" tabindex="-1" role="dialog" ng-controller="OrderController" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">New Booth</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-lg-12">

                        <div class="form-group col-lg-6">
                            <label>Booth No</label>
                            <input type="text" value="" id="newboothno" ng-model="Item.newboothno" ng-blur="Item.getBoothInfoItem()"  class="form-control" name="newboothno" placeholder="Booth number">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Booth company name</label>
                            <input type="text" value="" id="newboothname" ng-model="Item.newboothname" class="form-control" name="newboothname" placeholder="Company Name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-12">
                        <div class="form-group col-lg-6">
                            <label>Telephone</label>
                            <input type="text" value="" id="newtelephone" ng-model="Item.newboothtelephone" class="form-control" name="newtelephone" placeholder="Telephone">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Business scope</label>
                            <input type="text" value="" id="newbusinessscope" ng-model="Item.newbusinesscope" class="form-control" name="newbusinessscope" placeholder="Business scope">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-12" align="center">
                      <label>Business card</label>
                      <div class="form-group col-lg-12"  id="businesscardimage">
                        <canvas id="boothsnapshot" name="boothsnapshot" ng-model="Item.boothsnapshot" width="200" height="200" style="background: url('../../../MMC/assets/images/image_icon2.png')" class="text-center"></canvas>
                      </br><a><span ng-click="Item.changeboothpicture()">Take a picture</span></a>
                    </div>

                    <div class="modal-body" align="center" id="businesscardcamera">
                    <webcam placeholder="'../assets/images/ripple.svg'"  channel="channel" on-stream="Item.onStream(stream)" on-streaming="Item.onSuccess()" on-error="Item.onError(err)">
                    </webcam>
                    <button ng-click="Item.makeBoothSnapshot()" class="btn btn-sm btn-success ">Click</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="Item.Addbooth()">Save Booth</button>
            </div>
        </div>
    </div>
</div>
</div>
