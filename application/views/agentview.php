<?php $this->load->view('layouts/adminheader');?>
<?php $this->load->view('layouts/adminsidebar');?>
<title>GOLD AGENTS</title>
<style type="text/css">
  textarea#officeaddress {
    height: 105px;
}
.modal-content {
    overflow-x: auto;
    max-height: 600px;
}
small.ng-binding {
    word-wrap: break-word;
}
</style>
<div id="wrapper" ng-controller="AdminViewController">
   <div class="normalheader transition animated fadeIn">
      <div class="hpanel">
         <div class="panel-body">
            <a class="small-header-action" href="#">
               <div class="clip-header">
                  <i class="fa fa-arrow-up"></i>
               </div>
            </a>
            <div id="hbreadcrumb" class="pull-right m-t-lg">
               <ol class="hbreadcrumb breadcrumb">
                 <!--  <li><a href="#!">Dashboard</a></li> -->
                  <li>
                     <span>Gold Agents</span>
                  </li>
               </ol>
            </div>
            <h2 class="font-light m-b-xs">
               Gold Agent
            </h2>
            <small>List of gold agents.</small>
         </div>
      </div>
   </div>
   <div class="content animate-panel" infinite-scroll="Admin.GetGoldAgents()" infinite-scroll-distance="3" >
      <div class="row projects" ng-show="Admin.GoldAgents.length>0">
         <div class="col-lg-6" ng-repeat-start="goldagent in Admin.GoldAgents" id="goldagent_{{goldagent.Id}}">
            <div class="hpanel hgreen">
               <div class="panel-body">
                  <!-- <span class="label label-success pull-right">NEW</span> -->
                  <div class="row">
                     <div class="col-sm-8">
                        <h4><a href="#"> {{goldagent.CompanyName}}</a></h4>
                        <p>
                          Address: {{goldagent.OfficeAddress}}
                          <br/>
                          Phone: {{goldagent.PhoneNumber}}
                          <br/>
                          Fax: {{goldagent.FaxNumber}}
                          <br/>
                          No of years in Business: {{goldagent.BusinessYear}}
                          <br/>
                          No of years as Gold Agent: {{goldagent.GoldAgentYears}}
                        </p>
                        <hr/>


                     </div>

                     <div class="col-sm-4 project-info">
                        <div class="project-action m-t-md">
                           <div class="btn-group">
                            <!--   <button class="btn btn-xs btn-default"> View</button> -->
                              <button class="btn btn-xs btn-info" ng-click="Admin.EditAgent(goldagent)"> Edit</button>
                               <button class="btn btn-xs btn-warning" ng-click="Admin.DeactivateAgents(goldagent)">Deactivate</button>
                              <button class="btn btn-xs btn-danger" ng-click="Admin.DeleteAgents(goldagent)"> Delete</button>
                           </div>
                        </div>
                        <div class="project-value">
                           <h2 class="text-success">
                             <!--  {{goldagent.PhoneNumber}} -->
                           </h2>
                        </div>
                        <div class="project-people">
                           <img alt="logo" class="img-circle" ng-repeat="contact in goldagent.Contact" ng-if="contact.ImageName" src="../assets/images/{{contact.ImageName}}">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                           <div class="col-sm-3">
                              <div class="project-label">Name</div>
                           </div>
                           <div class="col-sm-3">
                              <div class="project-label">Email</div>
                           </div>
                           <div class="col-sm-3">
                              <div class="project-label">Contact</div>
                           </div>
                           <div class="col-sm-3">
                              <div class="project-label">Skype</div>
                           </div>
                  </div>
                   <div class="row" ng-repeat="contact in goldagent.Contact">
                           <div class="col-sm-3">
                              <small>{{contact.Name}}</small>
                           </div>
                           <div class="col-sm-3">
                              <small>{{contact.Email}}</small>
                           </div>
                           <div class="col-sm-3">
                              <small>{{contact.Mobile}}</small>
                           </div>
                           <div class="col-sm-3">
                              <small>{{contact.Skype}}</small>
                           </div>
                        </div>
               </div>
               <!-- <div class="panel-footer">
                  Additional information about project in footer
                  </div> -->
            </div>
         </div>
         <div class="clearfix" ng-if="($index+1)%2==0"></div>
         <div ng-repeat-end=""></div>
      </div>
   </div>
   <div id="loader" class="loading row text-center"  ng-show="Admin.AgentLoader&&Admin.ScrollLoader">
      <img src="../assets/images/loader.svg" alet="Loading.."/>
   </div>
   <div id="nocontent" class="nocontent row text-center" ng-if="Admin.GoldAgents.length==0&&!Admin.AgentLoader"><img src="../assets/images/warning.png" alet="Loading.."/><br/><h1>No Agents</h1></div>
     <div class="modal fade" id="editAgent" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
               <h4 class="modal-title">Edit GoldAgent</h4>
               <small class="font-bold">Edit your goldagent and save.</small>
            </div>
            <div class="modal-body">
               <form name="goldagent_form" method="post" novalidate ng-submit="Admin.UpdateGoldAgent(goldagent_form)">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row">
                           <div class="row col-lg-12">
                              <div class="form-group col-lg-12">
                                 <label>Company Name<span class="important">*</span></label>
                                 <input type="text" required="" ng-class="{ 'error' : (goldagent_form.companyname.$invalid && goldagent_form.companyname.$touched)||(
                                    Admin.golderror && goldagent_form.companyname.$invalid)}" ng-model="Admin.GoldAgentName"   id="companyname" class="form-control" name="companyname" placeholder="Company Name" maxlength="25">
                                 <label ng-show=" (goldagent_form.companyname.$invalid && goldagent_form.companyname.$touched)||(
                                    Admin.golderror && goldagent_form.companyname.$invalid)" class="error">Enter Company Name</label>
                              </div>
                           </div>
                           <div class="row col-lg-12">
                              <div class="hpanel pd10">
                                 <div class="panel-heading hbuilt">
                                    CONTACTS
                                 </div>
                                 <div class="panel-body">
                                    <div class="row" ng-repeat="GoldAgentContact in Admin.GoldAgentContacts">
                                       <div class="col-lg-3">
                                          <div class="form-group ">
                                             <label>Name<span class="important">*</span></label>
                                             <input type="text"  required="" id="name_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Name" class="form-control" name="name_{{GoldAgentContact.Contact}}" placeholder="Name" maxlength="25" ng-class="{ 'error' : (goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.name_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid) }" >
                                             <label ng-show="(goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.name_{{GoldAgentContact.Contact}}.$touched)||(Admin.golderror && goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid)" class="error">Name is required.</label>
                                          </div>
                                       </div>
                                       <div class="col-lg-3">
                                          <div class="form-group ">
                                             <label>Email<span class="important">*</span></label>
                                             <input type="email"  required="" id="email_{{GoldAgentContact.Contact}}" name="email_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Email" class="form-control" placeholder="Email" maxlength="40" ng-class="{ 'error' : (goldagent_form.email_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern)||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required) }" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" >
                                             <label ng-show=" (goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern) " class="error">Enter valid Email</label>
                                             <label ng-show=" (goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required) " class="error">Email is required</label>
                                          </div>
                                       </div>
                                       <div class="col-lg-2">
                                          <div class="form-group">
                                             <label>Mobile<span class="important">*</span></label>
                                             <input type="text"  required="" id="phone_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Mobile" class="form-control" name="phone_{{GoldAgentContact.Contact}}" placeholder="Moble" maxlength="25" digit-only  ng-class="{ 'error' : (goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.phone_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid) }" >
                                             <label ng-show="(goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.phone_{{GoldAgentContact.Contact}}.$touched)||(Admin.golderror && goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid)" class="error"> Mobile Number is required.</label>
                                          </div>
                                       </div>
                                       <div class="col-lg-2">
                                          <div class="form-group">
                                             <label>Skype</label>
                                             <input type="text"  required="" id="skype_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Skype" class="form-control" name="skype_{{GoldAgentContact.Contact}}" placeholder="Skype ID"  maxlength="25" >
                                          </div>
                                       </div>
                                       <div class="col-lg-2">
                                          <div class="form-group">
                                             <label>Image</label>
                                             <input type="file"  required="" id="image_{{GoldAgentContact.Contact}}" image="GoldAgentContact.Image" class="form-control" name="image_{{GoldAgentContact.Contact}}"
                                                resize-max-height="200" resize-max-width="150" resize-quality="0.7" resize-type="image/jpg" accept="image/x-png,image/jpeg" >
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-6">
                                          The Gold Agent require to have atleast 5 sales contacts
                                       </div>
                                       <div class="col-lg-6">
                                          <div class="col-lg-6 pull-right">
                                             <a href="#!"  class="btn btn-sm btn-success m-t-n-xs " ng-click="Admin.AddGoldAgentContact()">Add another sales contact
                                             </a>
                                          </div>
                                          <div class="col-lg-5 pull-right" ng-if="Admin.GoldAgentContacts.length>5">
                                             <a href="#!"  class="btn btn-sm btn-danger m-t-n-xs " ng-click="Admin.RemoveGoldAgentContact()">Remove sales contact
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="panel-footer">
                                    </div> -->
                              </div>
                           </div>
                           <div class="row col-lg-12">
                              <div class="form-group col-lg-4">
                                 <label>Office Address<span class="important">*</span></label>
                                 <textarea class="form-control" name="officeaddress" id="officeaddress" ng-model="Admin.AgentOffice" required="" ng-class="{ 'error' : (goldagent_form.officeaddress.$invalid && goldagent_form.officeaddress.$touched)||(Admin.golderror && goldagent_form.officeaddress.$invalid) }"></textarea>
                                 <label ng-show="(goldagent_form.officeaddress.$invalid && goldagent_form.officeaddress.$touched)||(Admin.golderror && goldagent_form.officeaddress.$invalid)" class="error">Office Address is required.</label>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="form-group col-lg-6">
                                       <label>Phone Number<span class="important">*</span></label>
                                       <input type="text" required="" id="companyphne" ng-model="Admin.CompanyPhone" class="form-control" name="companyphne" placeholder="Phone Number" maxlength="30" digit-only ng-class="{ 'error' : (goldagent_form.companyphne.$invalid && goldagent_form.companyphne.$touched)||(Admin.golderror && goldagent_form.companyphne.$invalid) }">
                                       <label ng-show="(goldagent_form.companyphne.$invalid && goldagent_form.companyphne.$touched)||(Admin.golderror && goldagent_form.companyphne.$invalid)" class="error">Phone Number is required.</label>
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>Number of years in business</label>
                                       <input type="text" required="" id="bussinessyear" digit-only  ng-model="Admin.BussinessYear" class="form-control" name="bussinessyear" placeholder="Business Year" maxlength="4">
                                       <!-- <label ng-show="(goldagent_form.boothname.$invalid && goldagent_form.boothname.$touched)||(Order.error && goldagent_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="form-group col-lg-6">
                                       <label>Fax Number</label>
                                       <input type="text" required="" id="faxnumber" digit-only  ng-model="Admin.FaxNumber" class="form-control" name="faxnumber" placeholder="Fax Number" maxlength="30">
                                       <!-- <label ng-show="(goldagent_form.boothname.$invalid && goldagent_form.boothname.$touched)||(Order.error && goldagent_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>Number of years in as Gold Agent</label>
                                       <input type="text" required="" id="goldagentyear" digit-only  ng-model="Admin.GoldYear" class="form-control" name="goldagentyear" placeholder="Gold Agent Year" maxlength="4">

                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary" ng-click="Admin.UpdateGoldAgent(goldagent_form)">Save changes</button>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('layouts/footer');?>