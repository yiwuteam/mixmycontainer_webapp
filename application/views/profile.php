<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>

<link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
<link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
<style>
.hpanel {
    background-color: none;
    border: none;
    box-shadow: none;
    margin-bottom: 3px !important;
}
#passstrength {
    color:red;
    font-family:verdana;
    font-size:10px;
    font-weight:bold;
}
img#My_Img {
    width: 47%;
    height: 52%;
}
input#fileInput {
    margin-left: -180px;
}
#passwordmatch{
  color:red;
  font-family:verdana;
  font-size:10px;
  font-weight:bold;
}
image{float:left;width:100px;position:relative;padding:5px;}
.btn-default {
    border: 1px solid transparent;
    /*border-radius: 4px 4px 0 0;*/
    background-color: transparent;
}
input#fileInput {
    cursor: pointer !important;
}
.container {
    height: 200px;
    width: 250px;
    float: left;
    overflow: hidden;
    margin: -15px;
}
.container img {
    display: block;
}
.landscape img {
    height: 100%;
}
.portrait img {
    width: 101%;
    height: 159px;
}
.edit {
  padding-top: 26px; 
  padding-right: 120px;
  position: absolute;
  right: 0;
  top: 0;
  display: none;
}

.edit a {
  color: #000;
}
.profile-pic:hover .edit {
  /*position: relative;*/
  display: block;
}
.profile-pic {
  position: relative;
  display: inline-block;
    padding: 0px 0px;
}
.loadinggif {
    background:url('http://www.hsi.com.hk/HSI-Net/pages/images/en/share/ajax-loader.gif') no-repeat 150px center;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php $username = $this->session->userdata('username'); ?>
<?php $usermail = $this->session->userdata('useremail'); ?>
<script>

window.onload = function() {
 $('#dashboard').removeClass('active');
 $('#companysetup').removeClass('active');
 $('#neworder').removeClass('active');
 $('#vieworderdetails').removeClass('active');
 document.getElementById('myaccount').className = 'active';
};
</script>
<title>MY ACCOUNT</title>

<div>
  <div id="wrapper" style="min-height: 1860px;" ng-controller="AccountController" ng-init="Account.getfunctions()">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="#">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="../Login/dashboard">Dashboard</a></li>

                    <li class="active">
                        <span>My Account </span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                My Account
            </h2>
            <small>User account Details</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
    <div class="col-lg-4">
        <div class="hpanel hgreen">
            <div class="panel-body" style="height: 198px;">
               <form id="image">
                <a data-toggle="modal" data-target="#MyModel">
                <div class="profile-pic">
                 <div class="container portrait">
                <img id="My_Img" style="margin-top: 25px;" src="../assets/images/userdummypic.png" class="img-circle" alt="logo">
                <div class="edit"><a data-toggle="modal" data-target="#MyModel"><i class="fa fa-pencil fa-lg"></i></a></div>
                </div>
                </div>
                </a>

            
                <div class="text-muted font-bold m-b-xs"></div>
                <!-- <h5><a href="#"><?php //echo $username ?></a></h5> -->
                <div class="text-muted font-bold m-b-xs"></div>
              </form>
            </div>
            <div class="panel-body">
                <dl>
                    <dt>Email</dt>
                     <div class="text-muted font-bold m-b-xs"><?php echo $usermail ?></div>
                    <dt></dt>
                    <dd></dd>
                    <dd></dd>
                </dl>
            </div>

        </div>
    </div>
    <div class="col-lg-8">
        <div class="hpanel">
            <div class="hpanel">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id="profiletab" href="#tab-1">Profile Details</a></li>
                <li class=""><a data-toggle="tab" id="changepasswordtab" href="#tab-2">Change Password</a></li>
              <!--   <li class="active"><a data-toggle="tab" href="#tab-1"> This is tab</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">This is second tab</a></li> -->
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                  <div class=hpanel>
                    <div class="panel-header hbuilt">
                      <h5>Email Settings</h5>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                         <div class="checkbox" id="emailcheckbox" style="margin-left: 12px;"><label id="mailset" style="margin-left: -15px;"><input name="email" checked="true" type="checkbox" id="email" class="i-checks">  Plaintext email only</label></div>
                            <!-- <div class="checkbox"><label><input name="email" type="checkbox"  id="email" checked="true" class="i-checks"> Plaintext email only </label></div> -->
                        </div>

                    </div>
                  </div>
                  <div class=hpanel>
                    <div class="panel-header hbuilt">
                      <h5>Language Settings</h5>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                                        <div class="radio" id="rd1"><label> <input type="radio" name="radio1" id="radio1" class="i-checks" value="English">   English </label></div>
                                        <div class="radio" id="rd2"><label> <input type="radio" name="radio1" id="radio2" class="i-checks" value="Chinese">   Chinese, Simplified (简体中文) </label></div>
                        </div>
                    </div>
                  </div>
                  <div class=hpanel>
                    <div class="panel-header hbuilt">
                    <h5>Locale settings</h5>
                    </div>


                    <div class="panel-body">
                        <div class="col-md-12">
                        
                       <select ng-model="Account.AccountDetails.Locale" class="js-source-states" id="localesettings" style="width: 100%" select2="" ng-options="value.countryName for value in Account.Locales track by value.countryCode"> 
                       </select>

                        </div>
                        <div class="col-md-12 paddingtop10">
                          <!-- <button class="btn w-xs btn-info demo4 pull-right">Run example</button> -->
                          <button type="button" id="ProfileButton" class="btn w-xs btn-info pull-right demo4" ng-click="Account.accountdetails()" style="margin-top:10px">Save</button>
                        </div>
                    </div>
                  </div>
                </div>

                <div id="tab-2" class="tab-pane">
                    <div class="panel-body no-padding col-md-12">
                      <form role="form" id="form" novalidate="novalidate" style="margin-top:20px">
                        <!-- <div class="form-group col-md-12"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control" required="" id="email" aria-required="true"></div> -->
                        <div class="form-group col-md-12"><label>Current Password</label> <input type="password" placeholder="Current Password" class="form-control" id="password"  name="oldpassword"></div>
                      <div class="form-group col-md-12"><label>Password</label> <input type="password" placeholder="New Password" class="form-control" id="newpassword" name="newpassword" required> </div>
                      <!-- <div class="form-group col-md-3"><span><br><br></span><span id="passstrength"></span></div> -->
                      <div class="form-group col-md-12"><label>Confirm Password</label> <input type="password" placeholder="Confirm Password" class="form-control" id="confirmpassword" name="confirmpassword"></div>
                      <!--  <div class="form-group col-md-3"><span><br><br></span><span id="passwordmatch"></span></div> -->
                      <div class="row col-md-12">
                          <button class="btn btn-sm btn-primary m-t-n-xs pull-right" type="button" style="margin-bottom: 20px;" ng-click="Account.chengepassword()" type="submit"><strong>Submit</strong></button>
                      </div>
                  </form>


                        </div>

                    </div>

                </div>
            </div>


            </div>
        </div>
    </div>
</div>

</div>
<!-- Right sidebar -->


</div>


<?php $this->load->view('layouts/footer'); ?>



<div class="modal fade" id="MyModel" tabindex="-1" role="dialog" ng-controller="AccountController" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Image Upload</h4>

            </div>
            <div class="modal-body">
              <div id="image-holder"><img id="My_Imgmodal" src="../assets/images/userdummypic.png" ng-src="{{myImage}}" style="height: 100px;width:100px;" class="img-circle m-b" /></div>
              <div id="wrapper" style="margin-top: 20px;"><input type="file" id="fileInput" accept="image/x-png, image/gif, image/jpeg" class="file" ng-model="Account.Image" name="files[]" />
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="modalclose"  ng-click="Account.ImageModalClose()" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="Account.ImageUpload()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script src="../assets/vendor/jquery-validation/jquery.validate.min.js"></script> 

<script>
var toValidate = $('#newpassword'),
        valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 5) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('button[type=button]').prop('disabled', false);
        }else{
            $('button[type=button]').prop('disabled', true);        
        }
    });   
  $(function(){

        $("#form").validate({
            rules: {
                newpassword: {
                    required: true,
                    minlength: 6
                },
                confirmpassword: {
                    required: true,
                    minlength: 6
                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

