


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>
  <script src="../assets/js/ng-intl-tel-input.js"></script>
    
      <script src="../assets/js/ng-intl-tel-input.module.js"></script>
      <script src="../assets/js/ng-intl-tel-input.provider.js"></script>
      <script src="../assets/js/ng-intl-tel-input.directive.js"></script>
  <!-- MY App -->
  <script src="../application/ng/app.js"></script>
  <script src="../application/ng/routes.js"></script>
  <script src="../assets/js/ngStorage.min.js"></script>
     <script src="../assets/js/angular-datatables.min.js"></script>
     <script src="../dist/saveHtmlToPdf.js"></script>
     <script src="../assets/js/webcam.min.js"></script>
  <script src="../application/ng/Services/Login/LoginService.js"></script>
  <script src="../application/ng/Services/user/AccountService.js"></script>
  <script src="../application/ng/Services/Order/OrderService.js"></script>

  <!-- App Controller -->
  <script src="../application/ng/Controller/login/LoginController.js"></script>
  <script src="../application/ng/Controller/user/AccountController.js"></script>
  <script src="../application/ng/Controller/Order/OrderController.js"></script>
  <!--App directive -->
  <script src="../application/ng/Directive/stringToNumber.js"></script>
<!-- Main Wrapper -->
<!-- <div id="wrapper"> -->
<!-- <style type="text/css">
  table.border_bottom  {
  border-bottom:1pt solid black;
}
tr.spaceUnder > td
{
  padding-bottom: 1em;
}
  /*tr.page-break  {page-break-inside:auto }*/
/*table.page-break-inside {
  table-layout: auto;
}*/
/* table { page-break-inside:auto }*/
}
</style> -->
<div ng-controller="OrderController" ng-app="mixMyContainer" data-ng-init="Item.printorderview()">
<table width="970">
<tbody>
<tr>
<td colspan="2" width="219">
<p><img style="float: left;" src="../assets/images/logo.png" alt="" width="200" height="100" /></p>
</td>
<!-- <td width="100">
<p></p>
</td> -->
<td width="120">
<p><b>PURCHASE ORDER</b></p>
</td>
<td width="100">
<p></p>
</td>
<!-- <td width="100">
<p><strong>PAGE: </strong></p>
</td> -->
</tr>
<tr>
<td width="200">
<p><strong>Order No</strong> : {{Item.Order.OrderNumber}}</p>
</td>
<td width="121">
<p></p>
</td>
<td width="150">
<p><strong>No of Booths visited</strong> :</p>
</td>
<td width="88">
<p> {{Item.Order.booth.length}}</p>
</td>
</tr>
<tr>
<td width="73">
<p><strong>Date</strong> : {{Item.Order.LastUpdate}}</p>
</td>
<td width="121">
<p></p>
</td>
<td width="74">
<p><strong>Total Value of order</strong>:</p>
</td>
<td width="88" colspan="3">
<p>USD: {{Item.Order.TotalOrderUsValue| number}}/RMB:{{Item.Order.TotalOrderRmbValue| number}}/{{Item.Order.ThirdCurrency}} {{Item.Order.TotalOrderZarValue| number}}</p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="73" ng-repeat="User in Item.Order.user">
<p><strong>Agent</strong> : {{User.AgentName}}</p>
</td>
<td width="121>
<p></p>
</td>

<td width="74">
<p>&nbsp;</p>
</td>
<!-- <td width="88">
<p>RMB:{{Item.Order.TotalOrderRmbValue| number}}</p>
</td> -->
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="2" width="200">
<p><strong>USER DETAILS</strong></p>
</td>
<!-- <td width="74">
<p>Third Currency:</p>
</td>
<td width="88">
<p>{{Item.Order.TotalOrderZarValue| number}}</p>
</td> -->
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tbody ng-repeat="User in Item.Order.user">
<tr>
<td width="100" colspan="2">
<p><strong>User</strong> : <span style="color:blue;"><u>{{User.OrderEmail}}</u></span></p>
</td>
<td width="73">
<p><strong>Tel</strong> : {{User.Phone}}</p>
</td>
<td width="121">
<p></p>
</td>
<td width="74">
<p></p>
</td>
<td width="88">
<p>&nbsp;</p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="75">
<p><strong>Name</strong> : {{User.CompanyName}}</p>
</td>
<td width="144">
<p></p>
</td>
<td width="88" colspan="3">
<p><strong>Country</strong> : {{User.Country}}</p>
</td>
<td width="121">
<p></p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="75" colspan="1">
<p><strong>No of Containers Filled</strong> :</p>
</td>
<td width="200" colspan="1" ng-repeat="Container in Item.Order.container" ng-show="$index<=2">
<p>{{Container.Count}}X{{Container.Container}}{{$last ? '' : ', '}}</p>
<tr>
<td ng-repeat="Container in Item.Order.container" ng-show="$index>2 && $index<=7">{{Container.Count}}X{{Container.Container }}{{$last ? '' : ', '}}
</td>
</tr>
<tr>
<td ng-repeat="Container in Item.Order.container" ng-show="$index>7 && $index<=12">{{Container.Count}}X{{Container.Container }}{{$last ? '' : ', '}}
</td>
</tr>
<tr>
<td ng-repeat="Container in Item.Order.container" ng-show="$index>12 && $index<=17">{{Container.Count}}X{{Container.Container }}{{$last ? '' : ', '}}
</td>
</tr>
</td>
</tr>
<!-- <tr>
<td width="75" colspan="1">
<p><strong>No of Containers Filled</strong> :</p>
</td>
<td width="150" ng-repeat="Container in Item.Order.container">
<p>{{Container.Count}}X{{Container.Container}}{{$last ? '' : ', '}}</p>
</td>
</tr> -->
</tbody>
</tbody>
</table>
<tr>
<td colspan=""><hr color = "blue" width="135%"/></td>
</tr>
<table style="page-break-after: always" width="970"  ng-repeat="Booth in Item.Order.booth" ng-if="Booth.orderitems.length>0">
<tbody>
<tr>
<td width="94">
<p><strong>Booth No</strong> : {{Booth.BoothNumber}}</p>
</td>
<td width="100">
<p><strong>Tel</strong> : {{Booth.Phone}}</p>
</td>
<td colspan="2" width="1">
<p></p>
</td>
<td colspan="3" width="132">
<p><strong>No of items ordered (this Booth)</strong> : {{Booth.orderitems.length}}</p>
</td>
<td colspan="3" rowspan="4" width="211">
<p><img src="http://mixmycontainer.com/QA/assets/images/{{Booth.BusinessCard}}" alt="" width="200" height="140" /></p>
</td>
</tr>
<tr>
<td colspan="4" width="286">
<p><strong>Booth Name</strong> :{{Booth.BoothName}}</p>
</td>
</tr>
<tr>
<td colspan="4" width="230">
<p><strong>Trade District</strong> : {{Booth.Name}}</p>
</td>
<td colspan="6" width="388">
<p></p>
</td>
</tr>
<tr>
<td colspan="5" width="400">
<p><strong>Total Purchases for this Booth</strong> : USD: ${{Booth.ValuePurchase}}&nbsp; /RMB: {{Booth.ValuePurchaseRMB}}</p>
</td>
</tr>
<tr>
<td colspan="12"><hr color = "#919090" /></td>
</tr>
<tbody ng-repeat="item in Booth.orderitems">
<tr>
<td style="width: 20%;" rowspan="4"><img src="http://mixmycontainer.com/QA/assets/images/{{item.Picture}}" alt="" width="200" height="140" /></td>
<td colspan="3"><strong>Item No:</strong></td>
<td>{{item.ItemNo}}</td>
<td style="width: 20%;" colspan="2"><strong>Units Ordered:</strong></td>
<td>{{item.UnitQuantity}}</td>
</tr>
<tr>
<td colspan="3"><strong>Description:</strong></td>
<td colspan="4">{{item.DescriptionFrom}}/{{item.DescriptionTo}}</td>
</tr>
<tr>
<td style="width: 20%;" colspan="3"><strong>QTY in Case:</strong></td>
<td>{{item.UnitperCarton}}</td>
<td style="width: 20%;" colspan="2"><strong>Cases Ordered:</strong></td>
<td>{{item.CartonQuantity}}</td>
</tr>
<tr>
<td style="width: 30%;" colspan="3"><strong>Total Item Price:</strong></td>
<td style="width: 20%;">${{item.TotalPriceInUS | number}}/&yen;{{item.TotalPriceInRMB | number}}/{{Item.Order.ThirdCurrency}} {{item.TotalPriceIn3rdCurrency | number}}</td>
<td style="width: 30%;" colspan="2"><strong>Unit Price:</strong></td>
<td colspan="6">${{item.PriceInUs | number}}/&yen;{{item.PriceInRmb | number}}/{{Item.Order.ThirdCurrency}} {{item.PriceInThirdCurrency | number}}</td>
</tr>
<tr>
<td colspan="12"><hr color = "#919090" /></td>
</tr>
</tbody>
<tr>
<td colspan="4" width="425">
<p></p>
</td>
<td colspan="3" width="100" >
<p><strong>Total Price in Dollar</strong>:</p>
</td>
<td colspan="4" width="72">
<p><strong>Total Price in RMB</strong>:</p>
</td>
</tr>
<tr>
<td colspan="4" width="425">
<p><strong>Booth Totals</strong></p>
</td>
<td colspan="3" width="80">
<p>{{Booth.ValuePurchase}}</p>
</td>
<td colspan="4" width="66">
<p>{{Booth.ValuePurchaseRMB}}</p>
</td>
</tr>
</tbody>
</table>
</div>
<script src="../assets/js/mix.js"></script>
<script src="../assets/js/angular-datatables.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="dist/saveHtmlToPdf.js"></script>


<!-- App scripts -->
