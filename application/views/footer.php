
<!-- Vendor scripts -->
<script src="<?php echo asset_url();?>js/jquery.js"></script>
<script src="<?php echo asset_url();?>vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo asset_url();?>js/jquery.gritter.js"></script>
<script src="<?php echo asset_url();?>js/sweet-alert.min.js"></script>
<script src="<?php echo asset_url();?>js/toastr.min.js"></script>
<script src="<?php echo asset_url();?>js/mix.js"></script> 


<script src="<?php echo asset_url();?>vendor/slimScroll/jquery.slimscroll.min.js"></script>

<script src="<?php echo asset_url();?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo asset_url();?>vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo asset_url();?>vendor/iCheck/icheck.min.js"></script>
<script src="<?php echo asset_url();?>vendor/sparkline/index.js"></script>

<!-- App scripts -->
<script src="<?php echo asset_url();?>js/homer.js"></script>

</body>

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:48:58 GMT -->
</html>