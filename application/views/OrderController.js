mixMyContainer.controller('OrderController', ['$scope', '$rootScope', 'OrderService', '$window','$location','$http','$localStorage',function ($scope, $rootScope, OrderService, $window, $location,$http,$localStorage) {
/*declare scopes*/
	$scope.Order = {};
	$scope.Item = {};
	$scope.Orderid = {};
	$scope.Boothid = {};
	$scope.Containerid = {};
	$scope.myImage = {};
	$scope.files = [];

	var handleFileSelect = function (evt) {
	    debugger
	    var file = evt.currentTarget.files[0];
	    var reader = new FileReader();
	    reader.onload = function (evt) {
	        $scope.$apply(function ($scope) {
	            $scope.myImage = evt.target.result;
	        });
	    };
	    reader.readAsDataURL(file);
	};
angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);


/*function save order*/
	  (function (Order) {

    Order.saveorder = function () {
				$('.ajax_loader').show();
      Order.OrderNo       = $('#orderno').val();
      Order.ProductType	  = $('#productType').val();
      Order.ContainerType = $('#ordertype').val() ;
      Order.Trade         = $('#trade').val() ;
      Order.BoothName     = $('#boothname').val() ;
      Order.BoothNo       = $('#boothno').val() ;
      Order.Telephone     = $('#telephone').val() ;
      Order.Businessscope = $('#businessscope').val();
			Order.Businesscard	= $localStorage.imagename; // image name is from localstorage
			$localStorage.orderno = $('#orderno').val()
      if(Order.OrderNo!="" && Order.ProductType !="" && Order.ContainerType!="" && Order.Trade!="" && Order.BoothName!="" && Order.BoothNo!="" && Order.Telephone!="" && Order.Businessscope!="" && Order.Businesscard != ""){

              OrderService.NewOrder(Order.OrderNo,Order.ProductType,Order.ContainerType,Order.Trade,Order.BoothName,Order.BoothNo,Order.Telephone,Order.Businessscope,Order.Businesscard).success(function (response) {
								result = response.message;
								$('.ajax_loader').hide();
              if(result == 'true')
              {
                $localStorage.orderid     = response.orderid; //store order id to localstorage from response
                $localStorage.boothid     = response.boothid; //store booth id to localstorage from response
                $localStorage.containerid = response.containerid; //store container id to localstorage from reponse
								$localStorage.containetype = Order.ContainerType; // store container type to localstorage from response
                window.location.href = "../Login/item";
								toastr["success"]("New order added");
              }
              else if(result == 'sameon'){
								toastr.error('Order No already exists');
              }
             })
      }
      else {
				toastr.error('fill the fields');
      }
    }
  })
    ($scope.Order);
/**end scope order**/

/**start scope item**/
 (function (Item) {
	 /**get carton units**/
      Item.getcartonunits = function () {
				// $('.ajax_loader').show();
                OrderService.Cartonunits().success(function (response) {
									// $('.ajax_loader').hide();
                if(response)
                {
                  Item.cartonGrossWeight=Number(response[0].GrossweightCarton); //store carton gross weight from response
                  Item.Length=Number(response[0].Length); // store carton length from response
                  Item.Height=Number(response[0].Height);// store carton height from response
                  Item.Width=Number(response[0].Width); //  store carton width from response
                  Item.cbm = Item.Length * Item.Height * Item.Width * 0.000001; // calculate cbm by multiply length, height and width
									Item.Orderno = $localStorage.orderno; // store order no
                }
                else{
									toastr.error('There is no default carton settings');
                }
               })
      }
    Item.getcartonunits();
/**end carton units**/

/**function get totalorder value**/
		Item.gettotalordervalue = function () {
			// $('.ajax_loader').show();
			Item.orderid = $localStorage.orderid;
							OrderService.GetOrderValues(Item.orderid).success(function (response) {
								// $('.ajax_loader').hide();
							if(response)
							{
									Item.Usprice = Number(response[0].TotalOrderUsValue); // store total order value from response
									Item.Rmbprice = Number(response[0].TotalOrderRmbValue); // store totalrmb value from response
									Item.totalcbm = Number(response[0].TotalOrderCBM); // store total order cbm from response
									Item.totalweight = Number(response[0].TotalOrderWeight); // store order weight
							}
							else{
								toastr.error('There is no default carton settings');
							}
						 })
		}
	Item.gettotalordervalue();
/**end get total order value**/

/**function purchage current booth**/
	Item.getpurchasecurrentbooth = function () {
		// $('.ajax_loader').show();
		Item.boothid = $localStorage.boothid;
		Item.orderid = $localStorage.orderid;
						OrderService.GetBoothValue(Item.orderid,Item.boothid).success(function (response) {
							// $('.ajax_loader').hide();

						if(response)
						{
								Item.Usboothprice = response[0].ValuePurchase; // store usbooth price from response
								Item.Rmbboothprice = response[0].ValuePurchaseRMB; // store value rbm pursechase response
						}

						else{
							toastr.error('There is no default carton settings');
						}
					 })
	}
Item.getpurchasecurrentbooth();
/***get purchase current booth*/

/**get container filled percentage**/
Item.getcontainerfilledpercentage = function () {
	// $('.ajax_loader').show();
	Item.orderid = $localStorage.orderid;
					OrderService.GetContainerPercentageValue(Item.orderid).success(function (response) {
						// $('.ajax_loader').hide();
					if(response)
					{
							if(response[0].Volumepercentage > response[0].Weightpercentage)
							{
							Item.percentage = response[0].Volumepercentage;
							}
							else
							{
								Item.percentage = response[0].Weightpercentage;  
							}
							

					}
					else{
						toastr.error('There is no default carton settings');
					}
				 })
}
Item.getcontainerfilledpercentage();
/**end function container filled percentage **/

/*function calculate carton quantity*/
		Item.calculatecartonqty = function () {
			$('.ajax_loader').show();
			var unitpercarton = $('#unitpercarton').val();
			var unitqty       = $('#unitqty').val();
			var mod           = Number(unitqty) % Number(unitpercarton);
			console.log(mod);
			var division      = Number(unitqty) / Number(unitpercarton);
			$('.ajax_loader').hide();
			if(mod == 0)
			{
				// alert(division);
				Item.cartonqty = Number(division);
			}
			if(mod != 0)
			{
				if( Number(unitpercarton) >  Number(unitqty))
				{
					Item.cartonqty = Number('1');
				}
			 if(Number(unitpercarton) < Number(unitqty))
				{
						var division1  = parseInt(division) + 1 ;
						Item.cartonqty = Number(division1);
				}
			}

		}
/**calculate carton size**/
Item.calculatecartonsize = function () {
	$('.ajax_loader').show();
// calculate carton cbm
	var cbm = $('#Length').val() * $('#Width').val() * $('#Height').val() * 0.000001;
	Item.cbm = Number(cbm);
	$('.ajax_loader').hide();
}

/**function for get price conversion**/
  Item.Getuspriceconversion = function () {
		$('.ajax_loader').show();
            OrderService.Priceunits().success(function (response) {
							$('.ajax_loader').hide();
            if(response)
            {
			Item.thirdcurrency  = response[0].ThirdCurrency;
			Item.activethridcurrency = response[0].Active3dCurrency;
              var usprice = $('#priceinus').val();
              var exchangerate = response[0].ExchangeRate;
              Item.ExchangeRate =  exchangerate * usprice;
              var exchangethirdcurrency = response[0].ExchangeRate3dCurrency;
              Item.ExchangeRate3dCurrency = exchangethirdcurrency * usprice;
              Item.priceinus = usprice;
            }
            else{
							toastr.error('There is no default price settings');
            }
           })
  }
/**end getusprice conversion function**/

/**function rmb price conversion**/
  Item.Getrmbpriceconversion = function () {
		$('.ajax_loader').show();
            OrderService.Priceunits().success(function (response) {
							$('.ajax_loader').hide();
            if(response)
            {
				Item.thirdcurrency  = response[0].ThirdCurrency;
				Item.activethridcurrency = response[0].Active3dCurrency;
              var rmbprice = $('#priceinrmb').val();
              var exchangerate = response[0].ExchangeRate;
              Item.ExchangeRate =  exchangerate * rmbprice;
              var exchangethirdcurrency = response[0].ExchangeRate3dCurrency;
              Item.ExchangeRate3dCurrency = exchangethirdcurrency * rmbprice;
              Item.priceinus = rmbprice;
            }
            else{
							toastr.error('There is no default price settings');
            }
           })
  }
/**end get rmbprice conversion**/

/**function image upload**/
	Item.ImageUpload = function () {
		$('.ajax_loader').show();
					debugger
					var file = document.getElementById("fileInput").files[0];
					Item.Image = file.name;
				//  $scope.form.image = $scope.files[0];

				$http({
			method  : 'POST',
			url     : '../Order/imageUpload',
			processData: false,
			transformRequest: function (data) {
					var formData = new FormData();
					formData.append("image", file);
					formData.append("name",Item.Image);
					return formData;
			},
			data : $scope.form,
			headers: {
						 'Content-Type': undefined
			}
		 }).success(function(data){
		 	$('.ajax_loader').hide();
			 $('#MyModel2').modal('hide');
			 var str1 = data;
			 str1.replace(/\"/g,"");
			 var str2 = str1.replace(/\"/g,"");
			  $localStorage.imagename = str2;
		 });

			}
/**end function image upload**/

/**function new container**/
			Item.newContianer = function () {
				$('.ajax_loader').show();
				Item.newcontianertype     = $('#newcontainer').val();
				Item.orderid              = $localStorage.orderid;
				Item.containerid          = $localStorage.containerid
				Item.usedweight           = $localStorage.usedweight;
				Item.usedvolume           = $localStorage.usedvolume;
				Item.currentcontainertype = $localStorage.containetype;
					if(Item.newcontianertype != "" && Item.orderid != "" && Item.containerid != "" && Item.usedweight != "" && Item.usedvolume != "" && Item.currentcontainertype != "")
					{
						OrderService.NewContainer(Item.newcontianertype,Item.orderid,Item.containerid,Item.usedweight,Item.usedvolume,Item.currentcontainertype).success(function (response) {
							$('.ajax_loader').hide();
							result = response.message;
		            if(result == 'true')
		            {

										$('#MyModel2').modal('hide');
										$localStorage.containerid = response.containerid;
										$localStorage.containetype = Item.newcontianertype;
										$localStorage.combinedcontainerid = response.combinedcontainerid;
										toastr["success"]("New container added");
										Item.subitemsave();
		            }
		            else
								{
									toastr.error('Container added failed');
		            }
		          })
					}
		  }
/**end new container**/

/**function upgrade container**/
			Item.upgradeContainer = function () {
				$('.ajax_loader').show();
				Item.upgradecontainertype = $('#upgradecontainer').val();
				Item.orderid = $localStorage.orderid;
				Item.containerid = $localStorage.containerid;

				Item.totalcontainervolume  = $localStorage.totalcontainervolume;
				Item.totalcontainerweight  = $localStorage.totalcontainerweight;


					if(Item.upgradecontainertype != "" && Item.orderid != "" && Item.containerid != "" && Item.totalcontainervolume != "" && Item.totalcontainerweight != "")
					{
						OrderService.UpgradeContainer(Item.upgradecontainertype,Item.orderid,Item.containerid,Item.totalcontainervolume,Item.totalcontainerweight).success(function (response) {
							$('.ajax_loader').hide();
							  result = response.message;
		            if(result == 'true')
		            {
											$('#MyModel2').modal('hide');
											$localStorage.containetype = Item.upgradecontainertype;
											toastr["success"]("Upgrade your container successfully");
											$localStorage.combinedcontainerid = $localStorage.containerid;
											Item.subitemsave();
		            }
		            else
					{
		              toastr.error('Upgrade container failed');
		            }
		           })
					}
		  }
/**end upgrade container**/

/**function saveitem**/
      Item.saveitem = function () {
				$('.ajax_loader').show();
      Item.itemno            = $('#itemno').val();
      Item.description       = $('#description').val();
      Item.altdescription    = $('#altdescription').val() ;
      Item.cartonGrossWeight = $('#cartonGrossWeight').val() ;
      Item.Length            = $('#Length').val() ;
      Item.Width             = $('#Width').val() ;
      Item.Height            = $('#Height').val() ;
      Item.cbm               = $('#cbm').val();
			Item.usprice	     	   = $('#priceinus').val();
			Item.unitpercarton     = $('#unitpercarton').val();
			Item.unitqty           = $('#unitqty').val();
			Item.priceRMB  				 = $('#ExchangeRate').val();
			Item.ExchangeRate3dCurrency = $('#ExchangeRate3dCurrency').val();
			Item.cartonqty        = $('#cartonqty').val();
			Item.itemimage				= $localStorage.imagename;
			Item.orderid          = $localStorage.orderid;
			Item.boothid				  = $localStorage.boothid;
			Item.containerid		  = $localStorage.containerid;
			Item.containertype		= $localStorage.containetype;

      if(Item.orderid != "" && Item.boothid != "" && Item.containerid != "" &&  Item.itemno!="" && Item.description !="" && Item.altdescription!="" && Item.cartonGrossWeight !="" && Item.Length!="" && Item.Width!="" && Item.Height!="" && Item.cbm!="" && Item.usprice != "" && Item.unitpercarton !="" && Item.unitqty != "" && Item.priceRMB != "" && Item.ExchangeRate3dCurrency !="" && Item.cartonqty !="" && Item.itemimage != "" && Item.containertype != ""){

              OrderService.NewItem(Item.orderid,Item.boothid,Item.containerid,Item.itemno,Item.description,Item.altdescription,Item.cartonGrossWeight,Item.Length,Item.Width,Item.Height,Item.cbm,Item.usprice,Item.unitpercarton,Item.unitqty,Item.priceRMB,Item.ExchangeRate3dCurrency,Item.cartonqty,Item.itemimage,Item.containertype).success(function (response) {
								$('.ajax_loader').hide();
						  var result = response.message;

              if(result == 'true')
              {
									$localStorage.balancevolume 		   = response.balancevolume;
									$localStorage.balanceweight 			 = response.balanceweight;
									$localStorage.usedweight					 = response.usedweight;
									$localStorage.usedvolume					 = response.usedvolume;
									$localStorage.totalcontainervolume			 =response.totalcontainervolume;
									$localStorage.totalcontainerweight			= response.totalcontainerweight;
									$('#MyModel2').modal('show');
             }
              else if(result == 'false')
							{
                toastr.error('Item save failed');
              }
							else if(result == 'mfalse')
							{
								toastr.error('missing values');
							}
							else if(result == 'saved')
							{
								 $('#itemno').val('');
								 $('#description').val('');
								 $('#altdescription').val('') ;
								 $('#priceinus').val('');
								 $('#unitpercarton').val('');
								 $('#unitqty').val('');
								 $('#ExchangeRate').val('');
								 $('#ExchangeRate3dCurrency').val('');
								 $('#cartonqty').val('');
								 Item.getpurchasecurrentbooth();
								 Item.getcontainerfilledpercentage();
								 Item.gettotalordervalue();
									toastr["success"]("Item added successfully");
							}
							else
							{
										toastr.error('Item added failed');
							}
             })
     }
      else {
				toastr.error('Fill all the fields');
      }
    }
/**end save item**/

/**function save item sub function**/
Item.subitemsave = function(){
	$('.ajax_loader').show();
	Item.itemno            = $('#itemno').val();
	Item.description       = $('#description').val();
	Item.altdescription    = $('#altdescription').val() ;
	Item.cartonGrossWeight = $('#cartonGrossWeight').val() ;
	Item.Length            = $('#Length').val() ;
	Item.Width             = $('#Width').val() ;
	Item.Height            = $('#Height').val() ;
	Item.cbm               = $('#cbm').val();
	Item.usprice	     	   = $('#priceinus').val();
	Item.unitpercarton     = $('#unitpercarton').val();
	Item.unitqty           = $('#unitqty').val();
	Item.priceRMB  				 = $('#ExchangeRate').val();
	Item.ExchangeRate3dCurrency = $('#ExchangeRate3dCurrency').val();
	Item.cartonqty        = $('#cartonqty').val();
	Item.itemimage				= $localStorage.imagename;
	Item.orderid          = $localStorage.orderid;
	Item.boothid				  = $localStorage.boothid;
	Item.containerid		  = $localStorage.combinedcontainerid;
	Item.containertype		= $localStorage.containetype;
	Item.balanceweight			= $localStorage.balanceweight;
	Item.balancevolume			= $localStorage.balancevolume;

	if(Item.orderid != "" && Item.boothid != "" && Item.containerid != "" &&  Item.itemno!="" && Item.description !="" && Item.altdescription!="" && Item.cartonGrossWeight !="" && Item.Length!="" && Item.Width!="" && Item.Height!="" && Item.cbm!="" && Item.usprice != "" && Item.unitpercarton !="" && Item.unitqty != "" && Item.priceRMB != "" && Item.ExchangeRate3dCurrency !="" && Item.cartonqty !="" && Item.itemimage != "" && Item.containertype != ""){
	// && Item.balanceweight != "" && Item.balancevolume != ""){

					OrderService.SubNewItem(Item.orderid,Item.boothid,Item.containerid,Item.itemno,Item.description,Item.altdescription,Item.cartonGrossWeight,Item.Length,Item.Width,Item.Height,Item.cbm,Item.usprice,Item.unitpercarton,Item.unitqty,Item.priceRMB,Item.ExchangeRate3dCurrency,Item.cartonqty,Item.itemimage,Item.containertype,Item.balanceweight,Item.balancevolume).success(function (response) {
						$('.ajax_loader').hide();
					var result = response.message;

					if(result == 'true')
					{
							$localStorage.balancevolume 		   = response.balancevolume;
							$localStorage.balanceweight 			 = response.balanceweight;
							$localStorage.usedweight					 = response.usedweight;
							$localStorage.usedvolume					 = response.usedvolume;

							$('#MyModel2').modal('show');
				  }
					else if(result == 'false')
					{
						toastr.error('Item save failed');
					}
					else if(result == 'mfalse')
					{
						toastr.error('missing values');
					}
					else if(result == 'saved')
					{
						 $('#itemno').val('');
						 $('#description').val('');
						 $('#altdescription').val('') ;
						 $('#priceinus').val('');
						 $('#unitpercarton').val('');
						 $('#unitqty').val('');
						 $('#ExchangeRate').val('');
						 $('#ExchangeRate3dCurrency').val('');
						 $('#cartonqty').val('');
						 Item.getpurchasecurrentbooth();
						 Item.getcontainerfilledpercentage();
						 Item.gettotalordervalue();
							toastr["success"]("Item added successfully");
					}
					else
					{
								toastr.error('Item added failed');
					}
				 })
 }
	else {
		toastr.error('Fill all the fields');
	}

}
/**end saveitem sub function**/

/**function add booth**/
Item.Addbooth = function(){
	$('.ajax_loader').show();
	Item.newBoothName = $('#newboothname').val();
	Item.newBoothNo = $('#newboothno').val();
	Item.newTelephone = $('#newtelephone').val();
	Item.newBusinessScope = $('#newbusinessscope').val();
	Item.orderid  = $localStorage.orderid;
	if(Item.newBoothName != "" && Item.newBoothNo != "" && Item.newTelephone != "" &&  Item.newBusinessScope != "" && Item.orderid != ""){

					OrderService.NewBooth(Item.orderid,Item.newBoothName,Item.newBoothNo,Item.newTelephone,Item.newBusinessScope).success(function (response) {
						$('.ajax_loader').hide();
				 var result=JSON.parse(response);
					if(result)
					{
						 $localStorage.boothid = result;
						 $('#newboothname').val('');
						 $('#newboothno').val('');
						 $('#newtelephone').val('') ;
						 $('#newbusinessscope').val('');
					$('#MyModel1').modal('hide');
						toastr["success"]("Booth added successfully");
				 }
					else{
						toastr.error('Booth added failed');
					}
				 })
 }
	else {
		toastr.error('Fill all the fields');
	}
}
/**end add booth function **/

/**end order conformation function**/
$('.endorder').click(function () {

            swal({
                        title: "Are you sure?",
                        text: "Your will not be able to continue this order!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, End it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Ended!", "Your order has been ended.", "success");
														var delay=1500; //1 second
														setTimeout(function() {
															endorder();
														}, delay);
                        } else {
                            swal("Cancelled", "Your order is safe :)", "error");
                        }
                    });
        });
/**end endorder function **/

/**function end order**/
				function endorder() {
					$('.ajax_loader').show();
				  var orderid = $localStorage.orderid;

					OrderService.EndOrder(orderid).success(function (response) {
						$('.ajax_loader').hide();
					window.location.href = "../Login/newOrder";

				 })
				}
/*end function end order*/

/*pause order function */
				$('.pauseorder').click(function () {
				            swal({
				                        title: "Are you sure?",
				                        text: "Your will be able to continue this order from order list!",
				                        type: "warning",
				                        showCancelButton: true,
				                        confirmButtonColor: "#DD6B55",
				                        confirmButtonText: "Yes, Pause it!",
				                        cancelButtonText: "No, cancel!",
				                        closeOnConfirm: false,
				                        closeOnCancel: false },
				                    function (isConfirm) {
				                        if (isConfirm) {
				                            swal("Paused!", "Your order has been paused.", "success");
																		var delay=1500; //1 second
																		setTimeout(function() {
																		window.location.href = "../Login/newOrder";
																		}, delay);

				                        } else {
				                            swal("Cancelled", "Your order is safe :)", "error");
				                        }
				                    });
				        });
/**end pause order function**/

/**function view all the order details**/
			Item.Viewallorder = function () {
				$('.ajax_loader').show();
						OrderService.Viewallorder().success(function (response) {
							$('.ajax_loader').hide();
							  Item.result  = response;
		           })

		  }
			// Item.Viewallorder();
/**end view all the function**/
		 Item.Vieworderitems = function ()
			{
				// $('.ajax_loader').show();
				Item.orderid = $('#orderid').val();
				OrderService.Viewallorderitems(Item.orderid).success(function (response) {
					// $('.ajax_loader').hide();
						Item.orderitems  = response;
						Item.vieworderid = response[0].OrderId;
						Item.item_ordernumber = response[0].OrderNumber;
						Item.item_orderusvalue = response[0].TotalOrderUsValue;
						Item.item_orderrmbvalue = response[0].TotalOrderRmbValue;
						Item.item_orderdate = response[0].LastUpdate;
						Item.item_totalcbm = response[0].TotalOrderCBM;
						Item.item_totalweight = response[0].TotalOrderWeight;
					 })
			}
     Item.Vieworderitems();
/**end function view all function**/

/**function view order details**/
		 Item.ViewOrderdetails = function ()
		 {
			 // $('.ajax_loader').show();
			 Item.orderid = $localStorage.orderid;
			 OrderService.Vieworderdetails(Item.orderid).success(function(response)
			 {
				 // $('.ajax_loader').hide();
				 Item.totalordervalueus = response[0].TotalOrderUsValue;
				 Item.totalordervaluermb = response[0].TotalOrderRmbValue;
				 Item.date = response[0].LastUpdate;
				 Item.totalcbm = response[0].TotalOrderCBM;
				 Item.totalweight = response[0].TotalOrderWeight;
				 Item.status = response[0].OrderStatus;

			 })
		 }
		 Item.ViewOrderdetails();
/**end view order details**/

/**functionget total container**/
		 Item.Gettotalcontainer = function()
		 {
			 // $('.ajax_loader').show();
			 Item.orderid = $('#orderid').val();

			 OrderService.Getcontainer(Item.orderid).success(function(response)
		 {
			 // $('.ajax_loader').hide();
			 Item.container = response;
		 })
		 }

		 Item.Gettotalcontainer();
/**end view total container function **/

/**contioue order **/
		Item.Contioueorder = function(orderid)
		{
			$('.ajax_loader').show();
			Item.orderid = orderid;
			OrderService.Getallorderdetails(Item.orderid).success(function(response)
		{
			$('.ajax_loader').hide();
			if(response.message == true)
			{
			 $localStorage.boothid = response.data[0].BoothId;
			 $localStorage.orderno = response.data[0].OrderNumber;
			 $localStorage.orderid = response.data[0].OrderId;
			 window.location.href = "../Login/continoueorder";

			}
		})
		}
		Item.printorderid = function()
		{

		 $localStorage.printorderid = $('#orderid').val();
		}

/*end contioue order*/
Item.printorder = function()
{

		window.location.href = "../Login/printorder";

}
/**invoice order**/
Item.printorderview = function()
{
	// $('.ajax_loader').show();
 	Item.orderid = $localStorage.printorderid;
  OrderService.Orderinvoice(Item.orderid).success(function(response)
  {   //$('.ajax_loader').hide(); //document.getElementById(id_printpage).contentDocument.location.reload(true);
 			 var result = response.message;
			 if(result == true)
			 {
			  //Item.orderno		    = response.data[0].OrderNumber;
				Item.orderdate 	    = response.data[0].LastUpdate;
				Item.orderusvalue   = response.data[0].TotalOrderUsValue;
				Item.orderrmbvalue	= response.data[0].TotalOrderRmbValue;
				Item.totalcbm       = response.data[0].TotalOrderCBM;
				Item.boothcount     = response.data[0].TotalBooth;
				Item.containercount = response.data[0].TotalContainer;
				Item.repeat					= response.data;
			 }

  })
}
Item.printorderview();

/**email invoice**/
Item.emailinvoice = function()
{
	$('.ajax_loader').show();
	Item.orderid = $localStorage.printorderid;
	alert(Item.orderid);
	OrderService.Orderinvoice(Item.orderid).success(function(response)
  {
		$('.ajax_loader').hide();
 			 var result = response.message;
			 if(result == true)
			 {
				Item.repeat					= response.data;
				Item.orderno		    = response.data[0].OrderNumber;
				Item.orderdate 	    = response.data[0].LastUpdate;
				Item.orderusvalue   = response.data[0].TotalOrderUsValue;
				Item.orderrmbvalue	= response.data[0].TotalOrderRmbValue;
				Item.totalcbm       = response.data[0].TotalOrderCBM;
				Item.boothcount     = response.data[0].TotalBooth;
				Item.containercount = response.data[0].TotalContainer;
				Item.repeat					= response.data;
		    var html = "";
				html = html + '<table width="958" height="361" border="0">';
				html = html + '<tr><td colspan="7"><h1><span class="style4">Invoice</span></h1></td></tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2" rowspan="5"><img id="My_Img" src="../assets/images/logo.png" class="img-circle m-b" width="150" height="100"/></td>';
				html = html + '<td colspan="2">ORDER NO</td>';
				html = html + '<td width="159"><span>'+Item.orderno+'</span></td>';
				html = html + '<td colspan="2">BUYER</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2">DATE</td>';
				html = html + '<td>'+Item.orderdate+'</td>';
				html = html + '<td colspan="2">No of Booth Visited : '+Item.boothcount+'</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2">VALUE OF ORDER</td>';
				html = html + '<td>$'+Item.orderusvalue+'</td>';
				html = html + '<td colspan="2">No of Container filled : '+Item.containercount+'</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2">&nbsp;</td>';
				html = html + '<td>RMB'+Item.orderrmbvalue+'</td>';
				html = html + '<td colspan="2">&nbsp;</td>';
				html = html + '</tr>';
				html = html +'<tr>';
				html = html +'<td colspan="2">TOTAL CBM</td>';
				html = html +'<td>'+Item.totalcbm+'</td>';
				html = html +'<td colspan="2">&nbsp;</td>';
				html = html +'</tr>';
				html = html +'</table>';

				for (i = 0; i < response.data.length; i++) {
				html = html +'<table width="958" height="361" border="0" ng-repeat="item in Item.repeat">';
				html = html +'<tr><td  colspan="7">&nbsp</td></tr>';
				html = html +'<tr><td  colspan="7">&nbsp</td></tr>';
				html = html +'<tr><td  colspan="7">&nbsp</td></tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2" rowspan="5"><img src="../assets/images/{{item.Picture}}" height="200" width="150"></td>';
				html = html + '<td colspan="2">ITEM NUMBER</td>';
				html = html + '<td colspan="3">'+response.data[i].ItemNo+'</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2">DESCRIPTION</td>';
				html = html + '<td colspan="3">'+response.data[i].DescriptionFrom+'</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td colspan="2">BOOTH NUMBER</td>';
				html = html + '<td colspan="3">'+reponse.data[i].BoothId+'</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td width="138">QTYINCASE</td>';
				html = html + '<td width="150">TOTUNITQTY</td>';
				html = html + '<td>UNITPRICE</td>';
				html = html + '<td width="163">TOTAL QTY</td>';
				html = html + '<td width="136">ITEM TOTAL</td>';
				html = html + '</tr>';
				html = html + '<tr>';
				html = html + '<td>'+reponse.data[i].UnitperCarton+'</td>';
				html = html + '<td>'+response.data[i].UnitQuantity+'</td>';
				html = html + '<td>$'+reponse.data[i].PriceInUs+'</td>';
				html = html + '<td>'+reponse.data[i].UnitQuantity+'</td>';
				html = html + '<td>$'+response.data[i].TotalPriceInUS+'</td>';
				html = html + '</tr>';
				html = html + '</table>';
				}
			 }
			  $("#emailorder").append(html);

  })

}
/**end email invoice**/

mixMyContainer.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});



})
($scope.Item);
 }]);
