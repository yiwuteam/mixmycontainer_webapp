<?php
$this->load->view('layouts/header');
?>
<?php
$this->load->view('layouts/sidebar');
?>
<link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
<link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="../assets/css/intlTelInput.css" />
<script>
   window.onload = function() {
    $('#dashboard').removeClass('active');
    $('#companysetup').removeClass('active');
    $('#myaccount').removeClass('active');
     $('#vieworderdetails').removeClass('active');

   $('#mainmenu').addClass('active');
    $("#level").addClass('in');
    $("#level").css("display", "block");
   $('#neworder').addClass('active');
   };
</script>
<style type="text/css">
   select2-choice.style {
   margin-right: 30px;
   }
   .mrt24{
   margin-top: 24px;
   }
   .ml24
   {
      margin-left: 24px;
   }
   canvas#snapshot {
   /* width: 200px;
    height: 200px;*/
   }
   span.important {
    color: red;
   }
   .loadinggif {
    background:url('http://www.hsi.com.hk/HSI-Net/pages/images/en/share/ajax-loader.gif') no-repeat right center;
   }
   @media screen and (max-width: 396px) {
    #snapshot {
    width: 240px
    }
  }
   @media screen and (max-width: 370px) {
    .webcam-live {
    width: 210px
    }
  }
</style>
<title>NEW ORDER</title>
<div id="wrapper">
   <div class="normalheader transition animated fadeIn">
      <div class="hpanel">
         <div class="panel-body">
            <a class="small-header-action" href="#">
               <div class="clip-header">
                  <i class="fa fa-arrow-up"></i>
               </div>
            </a>
            <div id="hbreadcrumb" class="pull-right m-t-lg">
               <ol class="hbreadcrumb breadcrumb">
                  <li><a href="../Login/dashboard">Dashboard</a></li>
                  <li class="active">
                     <span>New Order</span>
                  </li>
               </ol>
            </div>
            <h2 class="font-light m-b-xs">
               New Order
            </h2>
            <small>Provide your Order,Booth and Container details</small>
         </div>
      </div>
   </div>
   <div class="content animate-panel" ng-controller="OrderController" ng-init="Order.checkCompanyDetails()">
      <!-- <div class="content"> -->
         <div class="row">
            <div class="col-lg-12">
               <div class="hpanel" >
                  <div class="panel-heading">
                  </div>
                  <div class="panel-body" style="display: block;">
                     <form name="order_form" method="post" novalidate ng-submit="Order.saveOrder()">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="row">
                                 <div class="row col-lg-12">
                                    <div class="form-group col-lg-12">
                                       <label>Order Number<span class="important">*</span></label>
                                       <input type="text" required="" ng-class="{ 'error' : (order_form.orderno.$invalid && order_form.orderno.$touched)||(Order.error && order_form.orderno.$invalid)}" ng-model="Order.OrderNumber"   id="orderno" class="form-control" name="orderno" placeholder="Order Number" ng-blur="Order.checkOrderno()" maxlength="25">
                                     <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                       <label ng-show="(order_form.orderno.$invalid && order_form.orderno.$touched)||(Order.error && order_form.orderno.$invalid)" class="error">Enter OrderNo</label>
                                    </div>
                                 </div>
                                 <div class="row col-lg-12">
                                    <div class="form-group col-lg-6" >
                                       <label>Container Type<span class="important">*</span></label>
                                       <select class="js-source-states" style="width: 100%" id="ContainerType" name="ContainerType" ng-init="Order.getContainers()"
                                          ng-model="Order.ContainerType" select2="" ng-options="Conatinertypes.Container  for Conatinertypes in Order.Conatinertypes track by Conatinertypes.ID">
                                       </select>
                                       <label ng-show="(Order.error && Order.containererror)" class="error" >Please select valid Conatiner</label>
                                    </div>
                                    <div class="form-group col-lg-5">
                                       <label>Trade District/Area<span class="important">*</span></label>
                                       <select class="js-source-states" style="width: 100%" id="trade" name="trade" ng-init="Order.getTrades()"
                                          ng-model="Order.Trade" select2="" ng-options="Trades.Name  for Trades in Order.Trades track by Trades.Id">
                                       </select>
                                       <label ng-show="(Order.error && Order.tradeerror)" class="error" >Please select valid trade</label>
                                    </div>
                                    <div class="form-group col-lg-1">
                                       <button class="btn btn-info btn-circle mrt24" type="button" ng-click="Order.addTrade()"><i class="fa fa-plus"></i></button>
                                    </div>
                                 </div>
                                 <div class="row col-lg-12">
                                    <div class="form-group col-lg-11">
                                       <label>Hall<span class="important">*</span></label>
                                       <select class="js-source-states" style="width: 100%;" id="hall" name="hall"
                                          ng-model="Order.Hall" select2="" ng-options="Halls.Name  for Halls in Order.Halls track by Halls.Id">
                                       </select>
                                       <label ng-show="(Order.error && Order.hallerror)" class="error" >Please select valid hall</label>
                                    </div>
                                    <div class="form-group col-lg-1">
                                       <button class="btn btn-info btn-circle mrt24" type="button" ng-click="Order.addHall()" ><i class="fa fa-plus"></i></button>
                                    </div>
                                 </div>
                                 <div class="row col-lg-12">
                                    <div class="form-group col-lg-6">
                                       <label>Booth Number<span class="important">*</span></label>
                                       <input type="text"  required="" id="boothno" ng-model="Order.BoothNo" class="form-control" name="boothno" placeholder="Booth number" ng-blur="Order.getBoothInfo()" maxlength="25" ng-class="{ 'error' : (order_form.boothno.$invalid && order_form.boothno.$touched) ||(Order.error && order_form.boothno.$invalid) }" >
                                    <!--    <span class="help-block" id="boothnumbers" style="color: red;"></span> -->
                                       <label ng-show="(order_form.boothno.$invalid && order_form.boothno.$touched)||(Order.error && order_form.boothno.$invalid)" class="error">Booth Number is required.</label>
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>Booth company name</label>
                                       <input type="text" required="" id="boothname" ng-model="Order.BoothInfo.BoothName" class="form-control" name="boothname" placeholder="Company Name" maxlength="30" >
                                     <!--   <label ng-show="(order_form.boothname.$invalid && order_form.boothname.$touched)||(Order.error && order_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                    </div>
                                 </div>
                                 <div class="row col-lg-12">
                                    <div class="form-group col-lg-6">
                                       <label>Telephone</label>
                                       <input type="text" required="" id="telephone" ng-model="Order.BoothInfo.Phone" class="form-control" name="telephone" placeholder="Telephone" get-Number >
                                     <!--   <span class="help-block" id="telephonenumber" style="color: red;"></span> -->
                                    <!--    <label ng-show="(order_form.telephone.$invalid && order_form.telephone.$touched)||(Order.error && order_form.telephone.$invalid)" class="error">Booth Telephone is required.</label> -->
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>Business scope</label>
                                       <input type="text" required="" id="businessscope" ng-model="Order.BoothInfo.BusinessScope" class="form-control" name="businessscope" placeholder="Business scope"  maxlength="50">
                                      <!--  <label ng-show="(order_form.businessscope.$invalid && order_form.businessscope.$touched)||(Order.error && order_form.businessscope.$invalid)" class="error" >Business Scope is required.</label> -->
                                    </div>
                                 </div>
                                 <div class="row col-lg-12">
                                    <div class="text-center col-lg-12">
                                       <div class="hpanel">
                                          <div class="panel-body file-body">
                                                <canvas  id="snapshot" name="snapshot" ng-model="Item.snapshot"  style="background: url('../../../QA/assets/images/{{Order.BoothImage.replace('_thumb.jpg', '.jpg')}}') center no-repeat" class="text-center"></canvas>
                                          </div>
                                          <div class="panel-footer">
                                             <a data-target="#MyModel" data-toggle="modal">Scan business card</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-t-md">
                           <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      <!-- </div> -->
      <div class="modal fade" id="MyModel" tabindex="-1" role="dialog"  aria-hidden="true">
         <div class="modal-dialog modal-md">
            <div class="modal-content">
               <div class="color-line"></div>
               <div class="modal-header text-center">
                  <h4 class="modal-title">Image Upload</h4>
               </div>
               <div class="modal-body" align="center">
               <div class="normalheader transition animated fadeIn" ng-show="Order.Webcamerrormessage">
              <div class="panel-body">
                  <h2 class="font-light m-b-xs">
                      No Preview Available
                  </h2>
                  <small></small>
              </div>
             </div>
                  <webcam placeholder=""  channel="channel" on-stream="Order.onStream(stream)" on-streaming="Order.onSuccess()" on-error="Order.onError(err)">
                  </webcam>
                  <button ng-show="Order.HideCaptureButton" ng-click="Order.makeSnapshot()" class="btn btn-sm btn-success " data-dismiss="modal">Capture</button>
               </div>
               <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="Item.ImageUpload()">Save changes</button> -->
               </div>
         </div>
      </div>
      </div>

   </div>
   </div>
</div>
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="../assets/vendor/select2-3.5.2/select2.min.js"></script>
<script src="../assets/vendor/iCheck/icheck.min.js"></script>
<script src="../assets/js/intlTelInput.js"></script>
<?php
$this->load->view('layouts/footer');
?>
