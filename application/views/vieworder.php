<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>
<style>
button.btn.btn-info.btn-circle.pull-right {
  margin-right: 5px;
}
button.btn.btn-danger.btn-circle.pull-right{
  margin-right: 20px;
}
.col-md-6.pull-right {
  text-align: right;
}
button.btn.btn-warning {
    margin-bottom: 10px;
}
button.btn.btn-info {
    margin-bottom: 10px;
}
</style>
<?php $usermail = $this->session->userdata('useremail'); ?>
<title>Order Details</title>
<div ng-controller="OrderController"  ng-app="mixMyContainer">
<div id="wrapper">

      <div class="normalheader transition animated fadeIn">
          <div class="hpanel">
              <div class="panel-body">
                  <a class="small-header-action" href="#">
                      <div class="clip-header">
                          <i class="fa fa-arrow-up"></i>
                      </div>
                  </a>

                   <div id="hbreadcrumb" class="pull-right m-t-lg">
                      <ol class="hbreadcrumb breadcrumb">
                          <li><a href="../Login/dashboard">Dashboard</a></li>
                          <li><a href="../Login/orders">My Orders</a></li>
                          <li class="active">
                              <span>View Order Details</span>
                          </li>
                      </ol>
                  </div>
                  <h2 class="font-light m-b-xs">
                      Order Details
                  </h2>
                  <small>all order details</small>
              </div>
          </div>
      </div>

      <div class="normalheader transition animated fadeIn" ng-show="!Item.Loadorderdetails">
          <div class="hpanel">
              <div class="panel-body">
                  <a class="small-header-action" href="#">
                  </a>

                   <div id="hbreadcrumb" class="pull-right m-t-lg">
                      <ol class="hbreadcrumb breadcrumb">
                          <li class="active">
                              <span></span>
                          </li>
                      </ol>
                  </div>
                  <h2 class="font-light m-b-xs">
                      Loading..
                  </h2>
                  <small></small>
              </div>
          </div>
      </div>


  <div class="content animate-panel" ng-show="Item.Loadorderdetails"  data-ng-init="Item.printorderid()">
   <div class="row">
      <div class="col-md-6 pull-left">
        <p>
          Order number: <strong>{{Item.item_ordernumber}}</strong>
          <?php //echo $orderid; ?>
        </p>
        <input type="hidden" value="<?php echo $orderid; ?>" id="orderid" >
        <p>Order value: <span><strong>US${{Item.item_orderusvalue}}</strong></span> (<strong>RMB{{Item.item_orderrmbvalue}}</strong>)</p>
        <p>Total containers:<span ng-repeat="item in Item.container"><strong>{{item.ContainerName}}</strong> : <strong>{{item.Count}}</strong> |</span></p>
      
        <a href="../Login/newOrder"><button class="btn btn-info" style="margin-top:20px" type="button"><i class="fa fa-plus"></i>Add New Order</button></a>
        <?php if($status == 0) {?>
        <button class="btn btn-info " style="margin-top:20px" ng-click="Item.Contioueorder(Item.item_orderid)" type="button"><i class="fa fa-plus circle"></i>Add New Product</button>
        <?php } ?>
      </div>
      <div class="col-md-6 pull-right">
        <p>Order Date:<strong>{{Item.item_orderdate}}</strong></p>
        <p>Total CBM on this order: <strong>{{Item.item_totalcbm}}</strong></p>
        <p>Total weight of order: <strong>{{Item.item_totalweight}}</strong></p>

        <button class="btn btn-info" type="button" onclick="frames['printpage'].print()"><i class="fa fa-print"></i> <br>Print</button>
        <!-- <button class="btn btn-info" type="button" ng-click="Item.printorder()"><i class="fa fa-print"></i> <br>Print</button> -->
        <button class="btn btn-warning" ng-click="Item.EmailOrder(item.OrderId)" type="button"><i class="pe-7s-mail"></i> <br>Email Order</button>
      </div>

    </div>


      <div class="row">
          <div class="col-lg-12">


              <div class="hpanel">

                  <div class="panel-body">
                    <table id="example2" class="table table-striped table-bordered table-hover" datatable="ng" dt-options="dtOptions"data-ng-init="Item.Vieworderitems()">
                    <thead>
                    <tr>
                        <th>Booth Number</th>
                        <th>Item Number</th>
                        <th>Description</th>
                        <th>Container</th>
                        <th>Unit Quantity</th>
                        <th>Cartons Quantity</th>
                        <th>USD</th>
                        <th>RMB</th>
                        <th>Total USD</th>
                        <th>Total RMB</th>
                        <!-- <th class="abc"></th> -->

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in Item.orderitems ">
                      <td>{{item.BoothNumber}}</td>
                          <td>{{item.ItemNo}}</td>
                          <td>{{item.DescriptionFrom}}</td>
                          <td>{{item.containername}}</td>
                          <td>{{item.UnitQuantity}}</td>
                          <td>{{item.CartonQuantity}}</td>
                          <td>${{item.PriceInUs}}</td>
                          <td>¥{{item.PriceInRmb}}</td>
                          <td>${{item.TotalPriceInUS}}</td>
                          <td>¥{{item.TotalPriceInRMB}}</td>
                          <!-- <td><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button>
                            </td> -->
                    </tr>

                    </tbody>
                  </table>
                  </div>
              </div>
          </div>

      </div>



      </div>

      <!-- iframe -->
      <iframe src="../Login/printorder" name="printpage" id="id_printpage" style="display:none;"></iframe>
      <!-- iframe close-->
</div>
<div id="emailorder"></div>
</div>


<script>



</script>

<?php $this->load->view('layouts/footer'); ?>
<!-- <script src="../assets/scripts/homer.js"></script> -->
