<?php
	$content = ob_get_clean();
	$content	=	$_REQUEST['divid'];	
     // convert in PDF
    require_once('assets/pdf-creator/html2pdf.class.php');
	
	/*
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple00.pdf');
    }
    catch(HTML2PDF_exception $e) 
	{
        echo $e;
        exit;
    }*/
	
	
	// create new PDF document
	$pdf = new TCPDF("A4", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Basil eldo');
	$pdf->SetTitle('Test');
	$pdf->SetSubject('TCPDF');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	
	// set default header data
	$pdf->SetHeaderData("logo.jpg", "25px", "TCPDF example", "Basil eldo\nbasileldo@gmail.com");
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	// set margins
	$pdf->SetMargins("10px", "10px", "10px");
	$pdf->SetHeaderMargin(5);
	
	
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->SetFont('dejavusans', '', 10);
	// add a page
	$pdf->AddPage();
			
   	$pdf->SetXY(100, 40);
    try
    {	
        $pdf->writeHTML($content, 10, false, true, false, '');
        //$pdf->Output('../wuploads/'.$file_name.'.pdf',"F"); save to folder
        $pdf->Output($file_name.'.pdf');

       
    }
    catch(HTML2PDF_exception $e)
	{
        echo $e;
        exit;
    }


?>