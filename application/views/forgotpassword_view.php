<!DOCTYPE html>
<html  ng-app="mixMyContainer">

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:53:53 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->

    <title>MMC||Forgot Password</title>

 <style>
 body.blank {
     background-color: #7ececd !important;
 }
 .m-b-md {
     color: #ffffff !important;
 }
 .ajax_loader{
    display: none;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
     background-color: #FFFFFF;
    opacity: 0.8;
    /*background: rgba(0, 0, 0, 0.8);
    /*opacity: 0.8;*/
    /*background: url(../assets/images/loader/reload1.gif) center no-repeat;*/
}
 </style>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="../assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="../assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="../assets/vendor/toastr/build/toastr.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>

        <script src="../assets/js/ng-intl-tel-input.js"></script>
      <script src="../assets/js/ng-intl-tel-input.module.js"></script>
      <script src="../assets/js/ng-intl-tel-input.provider.js"></script>
      <script src="../assets/js/ng-intl-tel-input.directive.js"></script>


      <!-- MY App -->
      <script src="../application/ng/app.js"></script>
    	<script src="../application/ng/routes.js"></script>
       <script src="../assets/js/ngStorage.min.js"></script>

    	<!-- App Controller -->
          	<script src="../application/ng/Controller/login/LoginController.js"></script>
          	<script src="../application/ng/Services/Login/LoginService.js"></script>

    <!-- App styles -->
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../assets/styles/style.css">
      <link rel="stylesheet" href="../assets/styles/static_custom.css">


</head>
<body class="blank" ng-controller="LoginController">
<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/page_loading.gif" style="color: white;" width="" height="" /> </div> </div><!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>
<div class="back-link">
    <a href="../" class="btn btn-primary">Back to Login</a>
</div>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
       <div class="ajax_loader"><img src="../assets/images/loader/page_loading.gif" style="margin-left:600px;margin-top: 220px" /> </div>

            <div class="text-center m-b-md">
                <h3>Reset Password</h3>
                <!-- <small>Full suported AngularJS WebApp/Admin template with very clean and aesthetic style prepared for your next app. </small> -->
            </div>
            <div class="hpanel">
                <div class="panel-body">
                     <form  id="loginForm">

                            <div class="row">
                            <div class="form-group col-lg-12">
                                <label class="text-center">Please enter the email address that associated with your account to reset your password</label>
                                <input type="" value="" id="email" class="form-control" name="">
                            </div>
                            </div>
                            <div class="text-center">


                                <button class="btn btn-success" type="button" ng-click="Login.Resetpassword()">Reset Password</button>
                                <a class="btn btn-default" href="../">Cancel</a>

                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Vendor scripts -->
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="../assets/vendor/iCheck/icheck.min.js"></script>
<script src="../assets/vendor/sparkline/index.js"></script>
<script src="../assets/vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="../assets/vendor/toastr/build/toastr.min.js"></script>
<script src="../assets/js/angular-datatables.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>


<script src="../assets/js/webcam.min.js"></script>


<!-- App scripts -->
<script src="../assets/scripts/homer.js"></script>

</body>

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:53:53 GMT -->
</html>
