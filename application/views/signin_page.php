<?php $this->load->view('layouts/header'); ?>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="<?php echo asset_url();?>images/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<!-- <div class="back-link">
    <a href="index.html" class="btn btn-primary">Back to Dashboard</a>
</div> -->

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>PLEASE LOGIN TO APP</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                        <?php
               $attributes = array('id' => 'login_form','action' => 'form-validation.html','novalidate' => 'novalidate','enctype'=>'multipart/form-data');
                echo form_open('Login/loginverification',$attributes);
                ?>
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" placeholder="example@gmail.com" title="Please enter you username" onblur="checkuseremailexist()" required="" value="" name="username" id="username" class="form-control">
                                <span class="help-block small">Your unique username to app</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                                <span class="help-block small">Your strong password</span>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>
                            <button class="btn btn-success btn-block" id="button_login" ng-click="Login.LoginCheck()" type="button">Login</button>
                            <a class="btn btn-default btn-block" href="../../MixMyContainer_WebApp/C_UserRegister/view_registation_page">Register</a>
                            <a href="../../Mixmycontainer/C_UserRegister/forgotpassword_view"><span>forgot password?</span></a>
                        <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong></strong><br/> 2015 Copyright Company Name
        </div>
    </div>
</div>
