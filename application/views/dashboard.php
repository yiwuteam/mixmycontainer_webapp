<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>
<script>

window.onload = function() {
 $('#myaccount').removeClass('active');
 $('#companysetup').removeClass('active');
 $('#neworder').removeClass('active');
 $('#vieworderdetails').removeClass('active');


 document.getElementById('dashboard').className = 'active';
};</script>
<title>DASHBOARD</title>
<div>
  <div id="wrapper">

   <div class="content animate-panel">

          <div class="row">
              <div class="col-lg-6">
                  <div class="hpanel">
                      <div class="panel-body text-center h-200">
                      <a href="../Login/profile">
                          <i class="pe-7s-user fa-4x"></i>

                          <h1 class="m-xs"></h1>

                          <h3 class="font-extra-bold no-margins text-success">
                              My Account
                          </h3>
                          <small>You can change the password or descriptive details of the MixMyContainer account you are currently logged in with.</small>
                          </a>
                      </div>

                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="hpanel">
                      <div class="panel-body text-center h-200">
                      <a href="../Login/setup">
                          <i class="pe-7s-tools fa-4x"></i>

                          <h1 class="m-xs"></h1>

                          <h3 class="font-extra-bold no-margins text-success">
                              Company Setup
                          </h3>
                          <small>   You can change the descriptive details of your company  account you are currently logged in with.</small>
                          </a>
                      </div>

                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="hpanel">
                      <div class="panel-body text-center h-200">
                      <a href="../Login/newOrder">
                          <i class="pe-7s-paper-plane fa-4x"></i>

                          <h1 class="m-xs"></h1>
                          <h3 class="font-extra-bold no-margins text-success">
                              New Order
                          </h3>
                          <small>You can add your new order here</small>
                          </a>
                      </div>

                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="hpanel">
                      <div class="panel-body text-center h-200">
                      <a href="../Login/orders">
                          <i class="pe-7s-note2 fa-4x"></i>

                          <h1 class="m-xs"></h1>

                          <h3 class="font-extra-bold no-margins text-success">
                              Order Details
                          </h3>
                          <small>You can manage your orders </small>
                          </a>
                      </div>

                  </div>
              </div>
          </div>


      </div>

      <!-- Right sidebar -->
    

      <!-- Footer-->

  </div>
</div>
<?php $this->load->view('layouts/footer'); ?>
