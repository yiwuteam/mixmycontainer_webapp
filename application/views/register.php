<!DOCTYPE html>
<html  ng-app="mixMyContainer">

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:53:53 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Register</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="../assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="../assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="../assets/vendor/toastr/build/toastr.min.css" />
    <link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="../assets/css/select2.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
    
    	<!-- MY App -->
       <script src="../assets/js/select2.js"></script> 
      <script src="../assets/vendor/select2-3.5.2/select2.min.js"></script>
         <script src="../assets/js/ng-intl-tel-input.js"></script>
      <script src="../assets/js/ng-intl-tel-input.module.js"></script>
      <script src="../assets/js/ng-intl-tel-input.provider.js"></script>
      <script src="../assets/js/ng-intl-tel-input.directive.js"></script>
      
      <script src="../application/ng/app.js"></script>
      <script src="../application/ng/routes.js"></script>
       <script src="../assets/js/ngStorage.min.js"></script>
      <!-- App Controller -->
      <script src="../application/ng/Controller/login/LoginController.js"></script>
      <script src="../application/ng/Services/Login/LoginService.js"></script>

      
    <!-- App styles -->
    <script src="../application/ng/Directive/getNumber.js"></script>
    <script src="../application/ng/Directive/select2.js"></script>
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="../assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="../assets/styles/style.css">
    
      <link rel="stylesheet" href="../assets/styles/static_custom.css">
      <style>
      body.blank {
          background-color: #7ececd !important;
      }
      .m-b-md {
          color: #ffffff !important;
      }
      .ajax_loader{
    display: none;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: #FFFFFF;
    opacity: 0.8;
    /*background: rgba(0, 0, 0, 0.8);
    color: white;*/
    /*opacity: 0.5;*/
    
    }
     #div_Register {
            margin-top: -40px;
        }
      </style>

</head>
<body class="blank" ng-controller="LoginController" data-ng-init="Register.getContries()">
<!-- Simple splash screen-->

<div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/page_loading.gif" style="color: white;" width="" height="" /> </div> </div>
<!-- <div class="ajax_loader" style="background: rgba(0, 0, 0, 0.8);"><img src="../assets/images/loader/reload1.gif" style="margin-left:600px;margin-top: 220px" /> </div> -->
<!-- <div class="splash" style="background: rgba(0, 0, 0, 0.8);"> <div class="color-line"></div><div class="splash-title"><img src="../assets/images/loader/reload1.gif" style="color: white;" width="" height="" /> -->
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>
<div class="back-link">
    <a href="../Login" class="btn btn-primary">Back to Login</a>
</div>
<div class="register-container">
    <div class="row">
        <div id="div_Register" class="col-md-12">
       <div class="ajax_loader"><img src="../assets/images/loader/page_loading.gif" style="margin-left:600px;margin-top: 220px" /> </div>
            <div class="text-center m-b-md">
                <h3>Registration</h3>
                <!-- <small>Full suported AngularJS WebApp/Admin template with very clean and aesthetic style prepared for your next app. </small> -->
            </div>
            <div class="hpanel">
                <div class="panel-body">
                        <form role="form" name="registeruser" novalidate ng-submit="Register.userregister()">
                            <div class="row">

                                <div class="form-group col-lg-6">
                                    <label>Email Address</label>
                                    <input type="email" value="" id="useremail" class="form-control" name="email" placeholder="example@gmail.com">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Company Name</label>
                                    <input type="text" value="" id="companyname" class="form-control" name="companyname" placeholder="Company Name">
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Password</label>
                                    <input type="password" value="" id="userpassword" class="form-control" name="password" placeholder="******"  required>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Country</label>
                                    <select class="js-source-states" id="registercountry" ng-model="Register.Country" placeholder="Select contry" select2="" style="width: 100%" required="">

                                        <option ng-repeat="value in Register.ContryNames" value="{{value.CountryName}}">{{value.CountryName}}</option>

                                    </select>
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Confirm Password</label>
                                    <input type="password" value="" id="confirmpassword" class="form-control" name="confirmpassword" placeholder="******">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Address</label>
                                    <input type="text" value="" id="address" class="form-control" class="validate" name="address" placeholder="Address">
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Zip/Postal Code </label>
                                    <input type="text" id="zipcode" class="form-control" ng-model="Register.Zipcode" get-Number name="zip" placeholder="Postal code">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Town/City</label>
                                     <input type="text" value="" id="towncity" class="form-control" class="validate" name="towncity" placeholder="Town/City">
                                    <!-- <input type="text"  id="towncity" class="form-control" ng-model="Register.Towncity" get-Number name="number" placeholder="Phone Number"> -->
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="tel">Mobile</label>
                                    <input type="text"  id="mobilenumber" class="form-control" ng-model="Register.Mobile" get-Number name="tel" placeholder="Mobile Number">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Phone</label>
                                    <input type="text"  id="phonenumber" class="form-control" ng-model="Register.Phone" get-Number name="number" placeholder="Phone Number">
                                </div>
                                </div>
                                <div class="checkbox col-lg-12" style="margin-left: -15px;">
                                    <input type="checkbox" id="bc1" name="bc1" class="i-checks">
                                    <label for="group">Agree the terms and condition</label>
                                </div>
                                 <div class="text-center">
                                <button class="btn btn-success" type="button" id="register" ng-click="Register.userregister()">Register</button>
                                <a class="btn btn-default" href="../">Cancel</a>
                            </div>
                            </div>
                           
                        </form>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Vendor scripts -->
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="../assets/vendor/iCheck/icheck.min.js"></script>
<script src="../assets/vendor/sparkline/index.js"></script>
<script src="../assets/js/webcam.min.js"></script>
<script src="../assets/vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="../assets/vendor/toastr/build/toastr.min.js"></script>
<script src="../assets/js/angular-datatables.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/vendor/jquery-validation/jquery.validate.min.js"></script>
<!-- App scripts -->
<script src="../assets/scripts/homer.js"></script>
<script>
 // function disablesubmit(){
 //         document.getElementById("register").disabled = true;
 //    }

  // var toValidate = $('#userpassword'),
  //       valid = false;
  //   toValidate.keyup(function () {
  //       if ($(this).val().length > 5) {
  //           $(this).data('valid', true);
  //       } else {
  //           $(this).data('valid', false);
  //       }
  //       toValidate.each(function () {
  //           if ($(this).data('valid') == true) {
  //               valid = true;
  //           } else {
  //               valid = false;
  //           }
  //       });
  //       if (valid === true) {
  //           $('button[type=button]').prop('disabled', false);
  //       }else{
  //           $('button[type=button]').prop('disabled', true);        
  //       }
  //   });   
// $('input').on('ifChecked', function(event){
//    document.getElementById("register").disabled = false;
// });
// $('input').on('ifUnchecked', function(event){
//    document.getElementById("register").disabled = true;
// });

  $(function(){

        $("#loginForm").validate({
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                 confirmpassword: {
                    required: true,
                    minlength: 6
                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

</script>
<script type="text/javascript">
$(document).keypress(function(e) {
    if(e.which == 13) {

        $("#register").trigger('click');
    }
});

</script>
</body>

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:53:53 GMT -->
</html>
