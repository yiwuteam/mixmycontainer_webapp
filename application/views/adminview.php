<?php $this->load->view('layouts/adminheader'); ?>
<?php $this->load->view('layouts/adminsidebar'); ?>
<title>ADMIN UPDATE SETUP</title>
<style type="text/css">
    .pd10
    {
        padding: 10px;
    }
    .pl-pr-30
    {
        padding-left: 30px;
        padding-right: 30px;
    }
</style>
<div id="wrapper" ng-controller="AdminViewController">
    <div class="normalheader transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                        <!-- <li><a href="#!">Dashbaord</a></li> -->
                        <li class="active">
                            <span>Admin Update Setup</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Admin Update Setup
                </h2>
                <small>Gold Agents, Trades, Halls and Containers.</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <ul class="nav nav-tabs">
                      <!--   <li class="active"><a data-toggle="tab" href="#tab-1">Gold Agent</a></li> -->
                        <li class="active"><a data-toggle="tab" href="#tab-2"> Trade & Hall</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Containers</a></li>
                    </ul>
                    <div class="tab-content">
                       <!--  <div id="tab-1" class="tab-pane active">
                            <div class="panel-body" ng-init="Admin.GetGoldAgents()">
                                <div >{{item.CompanyName}}</div>
                            </div>
                        </div> -->
                        <div id="tab-2" class="tab-pane active">
                            <div class="panel-body">
                                <div class="hpanel hblue">
                                    <div class="panel-heading hbuilt">
                                      Trade District/Area
                                    </div>
                                    <div class="panel-body">
                                        <div ng-if="Admin.LoadingTrades" class="loading"><!--Loading-->
                                        Loading..
                                        </div>
                                        <div class="table-responsive" ng-if="Admin.Trades.length>0&&!Admin.LoadingTrades">
                                            <table cellpadding="1" cellspacing="1" class="table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="trade in Admin.Trades" id="tradeorhall_{{trade.Id}}">
                                                    <td>{{trade.Name}}</td>
                                                    <td>
                                                    <a href="#!" ng-click="Admin.EditTrade(trade.Id)"><i class="pe-7s-pen"></i></a>
                                                    <a href="#!" ng-click="Admin.DeleteTrade(trade.Id)"><i class="pe-7s-trash"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div ng-if="!Admin.LoadingTrades&&Admin.Trades.length==0" class="nocontent">
                                            <!--No Content-->
                                        </div>
                                    </div>  
                                    <!-- <div class="panel-footer">
                                       
                                    </div> -->
                                </div>
                                 <div class="hpanel hgreen">
                                    <div class="panel-heading hbuilt">
                                        Hall
                                    </div>
                                    <div class="panel-body">
                                        <div ng-if="Admin.LoadingHalls" class="loading"><!--Loading-->
                                        Loading..
                                        </div>
                                        <div class="table-responsive" ng-if="Admin.Halls.length>0&&!Admin.LoadingHalls">
                                            <table cellpadding="1" cellspacing="1" class="table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <!-- <th>Trade</th> -->
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="hall in Admin.Halls" id="tradeorhall_{{hall.Id}}">
                                                    <td>{{hall.Name}}</td>
                                                    <!-- <td>{{trade.ParentName}}</td> -->
                                                    <td>
                                                    <a href="#!" ng-click="Admin.EditHall(hall.Id)"><i class="pe-7s-pen"></i></a>
                                                    <a href="#!" ng-click="Admin.DeleteHall(hall.Id)"><i class="pe-7s-trash"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div ng-if="!Admin.LoadingHalls&&Admin.Halls.length==0" class="nocontent">
                                            <!--No Content-->
                                        </div>
                                    </div>  
                                   <!--  <div class="panel-footer">
                                       Add your hall
                                    </div> -->
                                </div>
                            </div>
                        </div>
                
                         <div id="tab-3" class="tab-pane ">
                                <div class="panel-body">
                                    <div ng-if="Admin.LoadingContainer" class="loading"><!--Loading-->
                                    Loading..
                                    </div>
                                    <div class="table-responsive" ng-if="Admin.Containers.length>0&&!Admin.LoadingContainer">
                                        <table cellpadding="1" cellspacing="1" class="table">
                                            <thead>
                                            <tr>
                                                <th>Contaier Name</th>
                                                <th>Max Volume</th>
                                                <th>Max Weight</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="containers in Admin.Containers" id="container_{{containers.ID}}">
                                                <td>{{containers.Container}}</td>
                                                <td>{{containers.MaximumVolume}} CBM</td>
                                                <td>{{containers.MaximumWeight}} KG</td>
                                                <td>
                                                <a href="#!" ng-click="Admin.EditContainer(containers.ID)"><i class="pe-7s-pen"></i></a>
                                                <a href="#!" ng-click="Admin.DeleteContainer(containers.ID)"><i class="pe-7s-trash"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div ng-if="!Admin.LoadingContainer&&Admin.Containers.length==0" class="nocontent">
                                        <!--No Content-->
                                    </div>
                                </div>
                        </div>
                 
                      
                    </div>


                </div>
            </div> 
        </div>
    </div>
    <div class="modal fade" id="editContainer" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header text-center">
                                    <h4 class="modal-title">Edit Container</h4>
                                    <small class="font-bold">Edit your container and save.</small>
                                </div>
                                <div class="modal-body">
                                   <form name="container_form" method="post" novalidate ng-submit="Admin.UpdateContainer(container_form)">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Container<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (container_form.conatiner.$invalid && container_form.conatiner.$touched)||(Admin.conterror && container_form.conatiner.$invalid)}" ng-model="Admin.Container"   id="conatiner" class="form-control" name="conatiner" maxlength="30" placeholder="Container">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(container_form.conatiner.$invalid && container_form.conatiner.$touched)||(Admin.conterror && container_form.conatiner.$invalid)" class="error">Enter Container</label>
                                                            </div>
                                                         </div>
                                                           <div class="row col-lg-12">
                                                            <div class="form-group col-lg-6">
                                                               <label>Max Volume<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="text" required="" ng-class="{ 'error' : (container_form.maxvolume.$invalid && container_form.maxvolume.$touched)||(Admin.conterror && container_form.maxvolume.$invalid)}" digit-only ng-model="Admin.MaxVolume"   id="maxvolume"  class="form-control" name="maxvolume" placeholder="Max volume"  maxlength="25"><span class="input-group-addon">CBM</span></div>
                                                               
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show=" (container_form.maxvolume.$invalid && container_form.maxvolume.$touched)||(Admin.conterror && container_form.maxvolume.$invalid)" class="error">Max Volume is required</label>
                                                            </div>
                                                             <div class="form-group col-lg-6">
                                                               <label>Max Weight<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="text" required="" ng-class="{ 'error' :( container_form.maxweight.$invalid && container_form.maxweight.$touched)||(Admin.conterror && container_form.maxweight.$invalid)}" digit-only  ng-model="Admin.MaxWeight"   id="maxweight" class="form-control" name="maxweight" placeholder="Max Weight"  maxlength="25"><span class="input-group-addon">KG</span></div>
                                                               
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="( container_form.maxweight.$invalid && container_form.maxweight.$touched)||(Admin.conterror && container_form.maxweight.$invalid)" class="error">Max Weight is required</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>               
                                  </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" ng-click="Admin.UpdateContainer(container_form)">Save changes</button>
                                </div>
                            </div>
                        </div>
    </div>
    <div class="modal fade" id="editTrade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header text-center">
                                    <h4 class="modal-title">Edit Trade District/Area</h4>
                                    <small class="font-bold">Edit your Trade District/Area and save.</small>
                                </div>
                                <div class="modal-body">
                                    <form name="trade_form" method="post" novalidate ng-submit="Admin.UpdateTrade(trade_form)">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Trade District/Area<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (trade_form.tradename.$invalid && trade_form.tradename.$touched)||(Admin.tradeerror && trade_form.tradename.$invalid)}" ng-model="Admin.Tradename"   id="tradename" class="form-control" name="tradename" placeholder="Trade" maxlength="150" required="">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(trade_form.tradename.$invalid && trade_form.tradename.$touched)||(Admin.tradeerror && trade_form.tradename.$invalid)" class="error">Enter Trade District/Area</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                        </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" ng-click="Admin.UpdateTrade(trade_form)">Save changes</button>
                                </div>
                            </div>
                        </div>
    </div>
    <div class="modal fade" id="editHall" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header text-center">
                                    <h4 class="modal-title">Edit Hall</h4>
                                    <small class="font-bold">Edit your hall and save.</small>
                                </div>
                                <div class="modal-body">
                                  <form name="hall_form" method="post" novalidate ng-submit="Admin.UpdateHall(hall_form)">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12 pl-pr-30">
                                                            <div class="form-group col-lg-12">
                                                               <label>Hall<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (hall_form.hall.$invalid && hall_form.hall.$touched)||(Admin.hallerror && hall_form.hall.$invalid)}" ng-model="Admin.HallName" id="hall" class="form-control" name="hall" placeholder="Hall" maxlength="150">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(hall_form.hall.$invalid && hall_form.hall.$touched)||(Admin.hallerror && hall_form.hall.$invalid)" class="error">Enter Hall</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                       <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Trade District/Area</label>
                                                               <select class="js-source-states" style="width: 100%" id="TradeId" name="tradeid"
                                                                  ng-model="Admin.TradeId" select3="" ng-options="Trades.Name  for Trades in Admin.Trades track by Trades.Id">
                                                               </select>
                                                               <label ng-show="(Admin.tradeiderror && Admin.hallerror)" class="error" >Please select valid trade</label>
                                                            </div>
                                                          
                                                         </div>
                                                   </div>
                                                </div>
                                          </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" ng-click="Admin.UpdateHall(hall_form)">Save changes</button>
                                </div>
                            </div>
                        </div>
    </div>
</div>
<?php $this->load->view('layouts/footer'); ?>