<!DOCTYPE html>
<html  ng-app="mixMyContainer">

<!-- Mirrored from webapplayers.com/homer_admin-v1.8/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Nov 2015 06:48:58 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>LOGIN</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/toastr/build/toastr.min.css" />
     <link rel="stylesheet" href="<?php echo asset_url();?>css/animate.css">
     <link rel="stylesheet" href="<?php echo asset_url();?>css/jquery.gritter.css">
      <link rel="stylesheet" href="<?php echo asset_url();?>css/sweet-alert.css" />
      <link rel="stylesheet" href="<?php echo asset_url();?>css/toastr.min.css" />
     <link rel="stylesheet" href="<?php echo asset_url();?>css/quirk.css">

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo asset_url();?>fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo asset_url();?>styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="<?php echo asset_url();?>css/ladda.min.css">
      <!-- MY App -->
    	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="<?php echo asset_url();?>css/ladda.min.css">
    	<!-- MY App -->
       <script src="<?php echo base_url();?>assets/js/ng-intl-tel-input.js"></script>
      <script src="<?php echo base_url();?>application/ng/app.js"></script>
      <script src="<?php echo base_url();?>application/ng/routes.js"></script>
       <script src="<?php echo base_url();?>assets/js/ngStorage.min.js"></script>
      <!-- App Controller -->
            <script src="<?php echo base_url();?>application/ng/Controller/login/LoginController.js"></script>
            <script src="<?php echo base_url();?>application/ng/Services/Login/LoginService.js"></script>
              <!-- <script src="assets/css/angular-datatables.css"> -->
    <style>
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript,
if it's not present, don't show loader */
body.blank {
    background-color: #7ececd !important;
}
.no-js #loader { display: none;  }
a.pull-right {
    padding-top: 10px;
    color: cornflowerblue;
    font-size: 14px;
}
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/images/loader/loader.gif) center no-repeat;

}
.text-center.m-b-md {
    color: #ffffff;
}

.ajax_loader{

  display: none;
   position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: #FFFFFF;
    opacity: 0.8;
    /*background: url(<?php //echo base_url();?>assets/images/loader/loader.gif) center no-repeat;*/
}

}
</style>
</head>

<body class="blank">
   <div class="ajax_loader"></div>


<div ng-controller="LoginController">
<div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="assets/images/loader/page_loading.gif" style="color: white;" width="" height="" /> </div> </div>

<!-- <div class="splash" style="background: rgba(0, 0, 0, 1);"> <div class="color-line"></div><div class="splash-title"><img src="assets/images/loader/reload1.gif" style="color: white;" width="" height="" /> </div> </div> -->
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>
<div class="back-link">
    <a href="Homepage" class="btn btn-primary">Back to Homepage</a>
</div>
<!-- <div class="back-link">
    <a href="index.html" class="btn btn-primary">Back to Dashboard</a>
</div> -->

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>PLEASE LOGIN TO MIXMYCONTAINER</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                       <form role="form" novalidate ng-submit="Login.LoginCheck()">
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" onclick="hideme()" placeholder="example@gmail.com" title="Please enter your username" ng-model="Login.UserName" required="" value="" name="username" id="username" class="form-control">
                                  <span id="uname" style="display:none;" class="help-block small">Your unique username to app</span>
                            </div>
                           <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" onclick="showmetest()" title="Please enter your password" ng-enter="Register.enterbuttonlogin()" placeholder="******" ng-model="Login.Password" required="" value="" name="password" id="password" class="form-control">
                              <span id="pwd" style="display:none;"  class="help-block small">Your strong password</span>
                            </div>
                            <!-- <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <div class="form-group">
                            <button class="btn btn-success btn-block" id="button_login" data-style="slide-left" type="submit" >Login</button>
                            <a class="btn btn-default btn-block" href="Login/register">Register</a>
                            <a class="pull-right" href="Login/forgot"><span>forgot password?</span></a>
                          </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- <div class="col-md-12 text-center">
            <strong></strong><br/> 2015 Copyright Company Name
        </div> -->
    </div>
</div>
</div>
</div>


<script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/sparkline/index.js"></script>
<script src="<?php echo base_url();?>assets/vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/toastr/build/toastr.min.js"></script>
<!-- App scripts -->
<script src="<?php echo base_url();?>assets/scripts/homer.js"></script>
<script src="<?php echo base_url();?>assets/js/webcam.min.js"></script>
<!-- App scripts -->
<script src="<?php echo base_url();?>assets/scripts/homer.js"></script>
<script src="<?php echo base_url();?>assets/js/spin.min.js"></script>
<script src="<?php echo base_url();?>assets/js/ladda.min.js"></script>
<script src="<?php echo base_url();?>assets/js/angular-datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

<script>

            // Bind normal buttons
            Ladda.bind( '.form-group button', { timeout: 3000 } );

            // Bind progress buttons and simulate loading progress
            Ladda.bind( '.progress-demo button', {
                callback: function( instance ) {
                    var progress = 0;
                    var interval = setInterval( function() {
                        progress = Math.min( progress + Math.random() * 0.1, 1 );
                        instance.setProgress( progress );

                        if( progress === 1 ) {
                            instance.stop();
                            clearInterval( interval );
                        }
                    }, 200 );
                }
            } );

        </script>


<script>
function hideme(){
  document.getElementById("uname").style.display = 'block';
}
function showmetest(){
  document.getElementById("pwd").style.display = 'block';
}
</script>
