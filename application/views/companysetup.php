<?php
$this->load->view('layouts/header');
?>
<?php
$this->load->view('layouts/sidebar');
?>
<style>
/*.error {
  border:2px solid red;
}*/
.radio.radio-inline {
    margin-top: 5px;
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.intl-tel-input {
      display: table-cell;
    }
/*.modal{
    display: block !important;
}*/
.modal-dialog{
      overflow-y: initial !important
}
.modal-body{
  height: 400px;
  overflow-y: auto;
}
.modal-open .modal {
    overflow-x: hidden;
    overflow-y: auto;
    margin-top: -65px;
}
span.important {
    color: red;
}
div.contact-stat {
    width: 12em; 
    word-wrap: break-word;
}      
</style>
<link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
<link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="../assets/css/intlTelInput.css" />

<script>

window.onload = function() {
 $('#dashboard').removeClass('active');
 $('#myaccount').removeClass('active');
 $('#neworder').removeClass('active');
 $('#vieworderdetails').removeClass('active');


 document.getElementById('companysetup').className = 'active';
};</script>

<title>COMPANY SETUP</title>
<div id="wrapper" ng-controller="AccountController" data-ng-init="Company.Getcartondefaultvalues()">
   <div class="normalheader transition animated fadeIn">
      <div class="hpanel">
         <div class="panel-body">
            <a class="small-header-action" href="#">
               <div class="clip-header">
                  <i class="fa fa-arrow-up"></i>
               </div>
            </a>
            <div id="hbreadcrumb" class="pull-right m-t-lg">
               <ol class="hbreadcrumb breadcrumb">
                  <li><a href="../Login/dashboard">Dashboard</a></li>
                  <li class="active">
                     <span>company setup</span>
                  </li>
               </ol>
            </div>
            <h2 class="font-light m-b-xs">
               Company Setup
            </h2>
            <small>Update your company details</small>
         </div>
      </div>
   </div>
   <div class="content animate-panel">

      <div class="row">
         <div class="col-lg-12">
            <div class="hpanel">
               <div class="panel-body" style="display: block;">

                  <!-- <div class="text-center m-b-md" id="wizardControl">
                     <a class="btn btn-primary" id="redirectsetup" ng-disabled="companycarton_form.$invalid" href="#step1" data-toggle="tab">Company Setup</a> -->
                     <!-- <a class="btn btn-default" ng-disabled="company_form.$invalid || companycarton_form.$invalid||     Company.EnableButton2" href="#step2" data-toggle="tab">Exchange Rate Setup</a>
                     <a class="btn btn-default" ng-disabled="company_form.$invalid || companyexchange_form.$invalid ||
                     Company.EnableButton3"  href="#step3" data-toggle="tab">Average Carton Setup</a> -->
                  <!-- </div> -->
                  <form role="form" class="form" novalidate  ng-submit="Company.companyDetails()" name="company_form" id="form_2">
                  <div class="tab-content">

                  <div id="step1" class="p-m tab-pane active">
                      
                        <div class="row">
                           <div class=hpanel>
                              <div class="panel-header hbuilt">
                                 <h5>Company Setup</h5>
                              </div>
                              <div class="panel-body">
                                 <div class="col-lg-12">
                                    <div class="row">
                                       <div class="form-group col-lg-12">
                                       <label>Company Name<span class="important">*</span></label>
                                       <input type="" value="" id="companyname" onchange="validation()" class="form-control validate"
                                          ng-model="Company.CompanySetup.CompanyName" name="companyname" ng-class="{ 'error' : (company_form.companyname.$invalid && company_form.companyname.$touched) ||(Company.error && company_form.companyname.$invalid) }"
                                          placeholder="Company name" required="">
                                         <!--  <label ng-show="company_form.companyname.$invalid && company_form.companyname.$touched && !company_form.companyname.$pristine" class="error">Company Name is required.</label> -->
                                           <label ng-show="(company_form.companyname.$invalid && company_form.companyname.$touched)||(Company.error && company_form.companyname.$invalid)" class="error">Company Name is required.</label>
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>Country</label>
                                       <select class="js-source-states" id="country"
                                      ng-model="Company.CompanySetup.Contry" placeholder="Select contry"  select2="" style="width: 100%">
                                          <option ng-repeat="value in Company.ContryNames" value="{{value.CountryName}}" >{{value.CountryName}}</option>

                                       </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                       <label>I would like correspondence / documentation to be sent to my</label>
                                       <div class="radio-inline">
                                          <label for="inlineRadio1"><input type="radio"
                                             ng-model="Company.CompanySetup.Documentation"
                                             id="inlineRadio1" value="postal address"
                                             name="radioInline" checked="">
                                          Postal address</label>
                                       </div>
                                       <div class="radio-inline">
                                          <label for="inlineRadio2"> <input type="radio" ng-model="Company.CompanySetup.Documentation" id="inlineRadio2" value="physical address" name="radioInline">
                                          Physical address </label>
                                       </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                       <label>Address</label>
                                       <input type="textarea" value="" id="companyaddress" ng-model="Company.CompanySetup.Address"
                                          class="form-control" class="validate" name="address" placeholder="Address">
                                         <!--  <label ng-show="company_form.address.$invalid && company_form.address.$touched && !company_form.address.$pristine" class="error">Address is required.</label> -->
                                    </div>
                                    <div class="row col-lg-12">
                                       <div class="form-group col-lg-6">
                                          <label>Zip/Postal Code </label>
                                          <input type="text" ng-model="Company.CompanySetup.Zipcode" id="companyzipcode"
                                             class="form-control" get-Number  name="zip" placeholder="Postal code">
                                              <!-- <label ng-show="company_form.zip.$invalid && company_form.zip.$touched && !company_form.zip.$pristine" class="error">ZipCode is required.</label> -->

                                          <span class="help-block" id="zipcode" style="color: red;"></span>
                                       </div>
                                       <div class="form-group col-lg-6">
                                          <label>Phone</label>
                                       <input type="text" ng-model="Company.CompanySetup.Phone"
                                          id="phonenum" class="form-control" get-Number  name="number" placeholder="Phone number" >
                                          <!-- <label ng-show="company_form.number.$invalid && company_form.number.$touched && !company_form.number.$pristine" class="error">Phone Number is required.</label>
                                          <span class="help-block" id="companyphone" style="color: red;"></span> -->
                                       </div>
                                    </div>
                                    <div class="row col-lg-12">
                                     <div class="form-group col-lg-6">
                                       <!-- <div class="form-group col-lg-6" ng-class="{'has-error': company_form.tel.$invalid && company_form.tel.$touched, 'has-success': company_form.tel.$valid}"> -->
                                          <label for="tel">Mobile</label>
                                           <!-- <input type="text" class="form-control" id="tel" name="tel"  ng-model="Company.CompanySetup.Mobile" value="" class="form-control"
                                           placeholder="" required="" ng-intl-tel-input> -->
                                           <input type="text" ng-model="Company.CompanySetup.Mobile"
                                          id="tel" class="form-control" get-Number name="tel" placeholder="Mobile Number">
                      
                                         <!-- <span class="help-block" ng-show="company_form.tel.$invalid && company_form.tel.$touched && !company_form.tel.$pristine">Invalid Number</span>
                                         <span class="help-block" ng-show="company_form.tel.$valid && company_form.tel.$touched && !company_form.tel.$pristine">Valid Number</span> -->


                                         <!--  <label ng-show="company_form.mobilenumber.$invalid && !company_form.mobilenumber.$pristine" class="error">Mobile Number is required.</label> -->
                                       </div>
                                       <div class="form-group col-lg-6">
                                          <label>Fax </label>
                                          <input type="text"  ng-model="Company.CompanySetup.Fax" value="" get-Number id="companyfax" class="form-control" name="fax" placeholder="Fax">
                                           <!-- <label ng-show="company_form.fax.$invalid && company_form.fax.$touched && !company_form.fax.$pristine" class="error">Fax is required.</label> -->
                                       </div>
                                    </div>
                                    <div class="row col-lg-12">
                                       <div class="form-group col-lg-6">
                                          <label>Skype Name</label>
                                          <input type="text" ng-model="Company.CompanySetup.SkypeName"  value="" id="SkypeName" class="form-control" name="SkypeName" placeholder="Skype name">
                                          <!--  <label ng-show="company_form.SkypeName.$invalid && !company_form.SkypeName.$pristine" class="error">Skype Name is required.</label> -->
                                       </div>
                                       <div class="form-group col-lg-6">
                                          <label>MSN Id</label>
                                          <input type="text" ng-model="Company.CompanySetup.MSNid" value="" id="MSNidMSNid" class="form-control" name="MSNid" placeholder="Msn Id">
                                         <!--  <label ng-show="company_form.MSNid.$invalid && !company_form.MSNid.$pristine" class="error">MSNId is required.</label> -->
                                       </div>
                                    </div>
                                    <div class="row col-lg-12">
                                       <div class="form-group col-lg-6">
                                          <label>QQ Id</label>
                                          <input type="text" ng-model="Company.CompanySetup.QQid" value="" id="" class="form-control" name="QQid" placeholder="QQ Id">
                                         <!--  <label ng-show="company_form.QQid.$invalid && !company_form.QQid.$pristine" class="error">QQId is required.</label> -->
                                       </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                       <label>Order email address<span class="important">*</span> (If more than 1 separate by ',')</label>
                                       <input type="text"  ng-model="Company.CompanySetup.OrderEmail" multiple-emails required value="" id="OrderEmail" class="form-control" name="OrderEmail"  ng-class="{ 'error' : (company_form.OrderEmail.$invalid && company_form.OrderEmail.$touched) ||(Company.error && company_form.OrderEmail.$invalid) }" placeholder="Order email address" required="">
                                        <label ng-show="(company_form.OrderEmail.$invalid && company_form.OrderEmail.$touched)||(Company.error && company_form.OrderEmail.$invalid)" class="error">Order Email is required.</label>

                                        <!-- <label ng-show="company_form.OrderEmail.$invalid && company_form.OrderEmail.$touched && !company_form.OrderEmail.$pristine" class="error">Order Email is required.</label> -->
                                    </div>
                                    <div class="row col-lg-12">
                                    <div class="form-group col-lg-6" ng-show="Company.Hideagentemail">
                                       <label id="changegoldagentlabel">Gold Agent Email Address </label>
                                       <input type="text" ng-model="Company.CompanySetup.AgentEmail" value="" id="AgentEmail" class="form-control" name="AgentEmail" placeholder="Gold agent email address">
                                      <!--  <label ng-show="Company.GoldAgentError" class="error">Enter Gold Agent</label> -->
                                      <!--  <label ng-show="company_form.AgentEmail.$invalid && !company_form.AgentEmail.$pristine" class="error">Agent Email is required.</label> -->
                                         </div>
                                          <div class="form-group col-lg-6" ng-show="Company.IsHiddenagentname">
                                       <label id="changegoldagentlabel">Gold Agent Name </label>
                                       <div class="input-group m-b"><a class="input-group-addon" ng-click="Company.removegoldagentname()">x</a> <input type="text" readonly="" ng-model="Company.CompanySetup.AgentId" value="" id="AgentId" class="form-control tagsinput" name="AgentEmail" placeholder="gold agent name"></div>
                                       <!-- <label ng-show="Company.GoldAgentError" class="error">Select Gold Agent</label> -->
                                       <!--  <label ng-show="company_form.AgentId.$invalid && !company_form.AgentId.$pristine" class="error">Agent Name is required.</label>  -->
                                         </div>
                                          <div class="form-group col-lg-6">
                                          <label></label>
                                          <div class="row">
                                          <a href="#!" class="btn btn-info" id="modalopen" data-toggle="modal" data-target="#myModal6" class="btn btn-primary" ng-click="" style="margin-top: 4px;">Select Any Gold Agent</a>
                                           </div>
                                       </div>
          <div class="modal fade" show='modalShown' id="myModal6" tabindex="-1"   role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="color-line"></div>
                  <div class="modal-header">
                    <h4 class="modal-title">Agent Details</h4>
                      <small class="font-bold">Select your gold rated agent</small>
                  </div>
                      <div class="modal-body">
                        <div class="row"  ng-repeat="company in Company.result">
                          <div class="col-lg-12">
                            <div class="hpanel hgreen contact-panel">
                              <div class="panel-body">
                                <h3>{{company.CompanyName}}</h3>
                                  <div class="text-muted font-bold m-b-xs">{{company.PhoneNumber}}</div>
                                    <p>{{company.OfficeAddress}}</p>
                              </div>
                                    <div class="panel-footer contact-footer">
                                      <div class="row">
                                        <div class="col-md-4 border-right"> <div class="contact-stat"><span>Business Year:</span><strong>{{company.BusinessYear}}</strong></div> 
                                        </div>
                                          <div class="col-md-4 border-right"> <div class="contact-stat"><span>Fax Number:</span><strong>{{company.FaxNumber}}</strong></div> 
                                          </div>
                                            <div class="col-md-4"> <div class="contact-stat"><span>Gold Agent Years:</span><strong>{{company.GoldAgentYears}}</strong></div>
                                            </div>
                                      </div>
                                              <div class="row" ng-repeat="companycontact in company.contact">
                                                <div class="col-md-3 border-right"> <div class="contact-stat"><span>Name:</span><strong>{{companycontact.Name}}</strong></div> </div>
                                                <div class="col-md-3 border-right"> <div class="contact-stat"><span>Email:</span><strong>{{companycontact.Email}}</strong></div> </div>
                                                <div class="col-md-3 border-right"> <div class="contact-stat"><span>Mobile:</span><strong>{{companycontact.Mobile}}</strong></div> </div>
                                                <div class="col-md-3"> <div class="contact-stat"><span>Skype:</span><strong>{{companycontact.Skype}}</strong></div></div>
                                              </div>
                                                <div class="modal-footer">
                                      <button class="btn btn-info btn-sm"  ng-click="Company.selectagent(company.CompanyName,company.Id)">Select Agent</button>
                                                </div> 
                                    </div>
                            </div>
                          </div>
                        </div>
                                                
   
                      </div>
                        <div class="modal-footer">
                                                     <button type="button" class="btn btn-default" id="modalclose" data-dismiss="modal">Close</button>
                                                       </div>

          </div>
        </div>
      </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                          
                     <!-- <div id="step3" class="tab-pane"> -->
                      <!-- <form role="form" class="form" novalidate  name="companycarton_form" id="form_2"> -->
                        <div class="row">
                           <div class=hpanel>
                              <div class="panel-header hbuilt">
                                 <h5>Exchange Rate Setup</h5>
                              </div>
                              <div class="panel-body">
                                 <div class="col-lg-12">
                                    <div class="row">
                                       <div class="form-group col-lg-12">
                                          <label>Dollar/RMB exchange<span class="important">*</span></label>
                                           <input type="text"  id="dollar"
                                           ng-class="{ 'error' : (company_form.dollar.$invalid && company_form.dollar.$touched) ||(Company.error && company_form.dollar.$invalid) }" ng-model="Company.ExchangeRate.DollarRMB"  id="rmb"  class="form-control" name="dollar" valid-number required="">
                                             <label ng-show="(company_form.dollar.$invalid && company_form.dollar.$touched)||(Company.error && company_form.dollar.$invalid)" class="error">Dollar RMB value is required.</label>
                                           <!-- <label ng-show="companyexchange_form.card_name.$invalid && companyexchange_form.card_name.$touched && !companyexchange_form.card_name.$pristine" class="error">Dollar/RMB is required.</label> -->
                                          <span class="help-block" id="dollarexchange" style="color: red;"></span>
                                       </div>
                                       <div class="form-group col-lg-12">
                                          <input type="checkbox"
                                             ng-model="Company.ExchangeRate.Thirdcurrency" value="option1"
                                             id="inlineCheckbox1" ng-checked="Company.ExchangeRate.Thirdcurrency=='1'" >
                                          <label>Activate 3rd currency</label>
                                       </div>
                                       <div class="form-group col-lg-6" id="show1"  ng-show="Company.ExchangeRate.Thirdcurrency=='1'">
                                          <label>Currency</label>
                                          <select id="currency" ng-change="Company.hideActivethirdcurrency()" ng-model="Company.ExchangeRate.Currency"
                                              select2="" class="js-source-states"
                                             style="width: 100%">
                                             <option  ng-repeat="value in Company.CountryCurrency"
                                                value="{{value.CurrencyName}}">{{value.CurrencyName}}</option>
                                          </select>
                                          <label ng-show="Company.Activethirdcurrencylabel" class="error">Select Exchange Rate Currency</label>
                                       </div>
                                       <div class="form-group col-lg-6" id="show2" ng-show="Company.ExchangeRate.Thirdcurrency=='1'">
                                          <label>Dollar/3rd Currency exchange</label>
                                          <input type="text" ng-model="Company.ExchangeRate.Dollerthirdcurrency" id="thirdollarcurrency"
                                             class="form-control" name="thirddollarrate" valid-number>
                                      <!--   <label ng-show="company_form.thirddollarrate.$invalid && !company_form.thirddollarrate.$pristine" class="error">Dollar Rmb is required.</label> -->
                                          <span class="help-block" id="thirddollarcurrency" style="color: red;"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class=hpanel>
                              <div class="panel-header hbuilt">
                                 <h5>Translation setup</h5>
                              </div>
                              <div class="panel-body">
                                 <div class="col-lg-12">
                                    <div class="row">
                                       <div class="form-group col-lg-6">
                                          <label>Translate from</label>
                                          <select class="js-source-states" id="tfrom" select2="" ng-model="Company.ExchangeRate.TranslateFrom" style="width: 100%">
                                             <option value="en">English</option>
                                             <option value="zh">Chinese</option>
                                          </select>
                                       </div>
                                       <div class="form-group col-lg-6">
                                          <label>Translate to</label>
                                          <select class="js-source-states" id="tto"  select2="" data-placeholder="Select Role Type" ng-model="Company.ExchangeRate.TranslateTo" style="width: 100%">
                                             <option value="en">English</option>
                                             <option value="zh">Chinese</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class=hpanel>
                              <div class="panel-header hbuilt">
                                 <h5>Average Carton Setup</h5>
                              </div>
                              <div class="panel-body">
                                 <div class="col-lg-12">
                                    <div class="row">
                                      <div class="col-lg-12">
                              <div class="row">
                                 <div class="form-group col-lg-6">
                                    <label>Gross weight of carton</label>
                                    <div class="input-group m-b">
                                       <input type="text"
                                          ng-model="Company.CartonSetup.Grossweight"
                                          class="form-control"  get-Number  id="weight" name="Grossweight">
                                       <span class="input-group-addon">kg</span> 
                                    </div>
                                    <!--  <label ng-show="companycarton_form.Grossweight.$invalid && companycarton_form.Grossweight.$touched && !companycarton_form.Grossweight.$pristine" class="error">Gross Weight is required.</label> -->
                                    <span class="help-block" id="grossweight" style="color: red;"></span>
                                 </div>
                                 <div class="form-group col-lg-6">
                                    <label>Length</label>
                                    <div class="input-group m-b">
                                       <input type="text"  class="form-control" get-Number id="length" ng-model="Company.CartonSetup.Lenght" name="length">
                                       <span class="input-group-addon">cm</span>
                                    </div>
                                    <!--  <label ng-show="companycarton_form.length.$invalid && companycarton_form.length.$touched && !companycarton_form.length.$pristine" class="error">Length of carton is required.</label> -->
                                    <span class="help-block" id="cartonlength" style="color: red;"></span>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group col-lg-6">
                                    <label>Width</label>
                                    <div class="input-group m-b">
                                       <input type="text" id="width" get-Number step="0.01" ng-model="Company.CartonSetup.Width" class="form-control" name="Width">
                                       <span class="input-group-addon">cm</span>
                                    </div>
                                     <!-- <label ng-show="companycarton_form.Width.$invalid && companycarton_form.Width.$touched && !companycarton_form.Width.$pristine" class="error">Length of carton is required.</label> -->
                                    <span class="help-block" id="cartonwidth" style="color: red;"></span>
                                 </div>
                                 <div class="form-group col-lg-6">
                                    <label>Height</label>
                                    <div class="input-group m-b">
                                       <input type="text" id="height" get-Number step="0.01" ng-model="Company.CartonSetup.Height" class="form-control" name="Height">
                                       <span class="input-group-addon">cm</span>
                                    </div>
                                    <!-- <label ng-show="companycarton_form.Height.$invalid && companycarton_form.Height.$touched && !companycarton_form.Height.$pristine" class="error">Length of carton is required.</label> -->
                                    <span class="help-block" id="cartonheight" style="color: red;"></span>
                                 </div>
                              </div>
                           </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="text-right m-t-xs">

                        <!-- <input type="submit" ng-disabled="Order.IsEnable" class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/> -->


                        <button class="btn btn-success submitWizard" ng-disabled="Company.IsEnable" id="setupbutton" type="submit"><strong>Submit</strong></button>

                           <!-- <button class="btn btn-success submitWizard" ng-disabled="companycarton_form.$invalid" id="setupbutton" ng-click="Company.companyDetails()" type="submit"><strong>Submit</strong></button> -->


                        </div>
                        </div>
                        </div>
                         </form> 
                       <!--  </div> -->
                       
                     </div>
                   
                  </div>
                  <div class="m-t-md">
                     <!-- <p>
                        This is an example of a wizard form which can be easy adjusted. Since each step is a tab, and each clik to next tab is a function you can easily add validation or any other functionality.
                        </p> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script> -->
<!-- <script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script> -->
<!-- <script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script> -->
<!-- <script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script> -->
<!-- <script src="../assets/vendor/iCheck/icheck.min.js"></script> -->
<!-- <script src="../assets/js/jquery.maskedinput.js"></script> -->

<!-- <script src="../assets/vendor/sparkline/index.js"></script>
 -->
<!-- <script src="../assets/js/intlTelInput.js"></script>  -->
<!-- <script src="../assets/vendor/jquery-validation/jquery.validate.min.js"></script> -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<!-- <script src="../assets/js/intlTelInput.min.js"></script> -->


<!-- App scripts -->
<!-- <script src="../assets/scripts/homer.js"></script> -->
<script>

function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }


function validation(){
  if (document.getElementById ("companyname").value == '') {
     document.getElementById("companyname").className =
      document.getElementById("companyname").className + "error";
  }
}

$(document).ready(function(){
  $('#show1').fadeOut('slow');
  $('#show2').fadeOut('slow');
  // $('#weight').val('119');
  // $('#length').val('114');
  // $('#width').val('112');
  // $('#height').val('113');
    $('#inlineCheckbox1').change(function(){
        if(this.checked){
          $('#show2').fadeIn('slow');
          $('#show1').fadeIn('slow');
           //document.getElementById("setupbutton").disabled = true;
        }

        else{
            $('#show2').fadeOut('slow');
            $('#show1').fadeOut('slow');
           // document.getElementById("setupbutton").disabled = false;
        }
    });
});


</script>
<?php
$this->load->view('layouts/footer');
?>
<!-- <script type="text/javascript">
   //$(".js-source-states").select2();
</script -->>


