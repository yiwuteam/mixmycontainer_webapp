<?php
$this->load->view('layouts/header');
?>
<?php
$this->load->view('layouts/sidebar');
?>

<style>
button.btn.btn-info.btn-circle.pull-right {
  margin-right: 5px;
}
button.btn.btn-danger.btn-circle.pull-right{
  margin-right: 20px;
}
.mt25
{
  margin-top: 25px;
}
i.fa.fa-reply-all {
    padding-right: 5px !important;
}
#orderstable{
  overflow: auto;
}
@media screen and (max-width: 1225px) {
  table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 10px;
}
}
#table_orders{
/*  overflow: auto;*/
}
@media screen and (max-width: 1225px) {
/*  table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 10px;
}*/
}
</style>
<script>
  window.onload = function() {
    $('#dashboard').removeClass('active');
    $('#companysetup').removeClass('active');
    $('#myaccount').removeClass('active');
    $('#neworder').removeClass('active');
    $('#mainmenu').addClass('active');
    $("#level").addClass('in');
    $("#level").css("display", "block");
    $('#vieworderdetails').addClass('active');
  };
</script>
<title>VIEW ORDERS</title>
<!--Wrapper -->
<div id="wrapper">
  <!--Header Panel-->
  <div class="normalheader transition animated fadeIn">
          <div class="hpanel">
              <div class="panel-body">
                  <a class="small-header-action" href="#">
                      <div class="clip-header">
                          <i class="fa fa-arrow-up"></i>
                      </div>
                  </a>
                  <div id="hbreadcrumb" class="pull-right m-t-lg">
                      <ol class="hbreadcrumb breadcrumb">
                          <li><a href="../Login/dashboard">Dashboard</a></li>
                          <li class="active">
                              <span>Order Details</span>
                          </li>
                      </ol>
                  </div>
                  <h2 class="font-light m-b-xs">
                      Orders
                  </h2>
                  <small>Search and manage orders here.</small>
              </div>
          </div>
  </div>
  <!--End of Header Panel-->
  <!--Content panel-->
  <div class="content animate-panel" ng-controller="OrderController" data-ng-init="Order.Viewallorder()">
   <!--Search Panel -->
      <div class="row">
        <div class="col-lg-12">
           <div class="hpanel">
            <div class="panel-body">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="row">
                          <div class="col-md-4">
                              <h5>From Date</h5>
                              <div class="input-group date" date-time ng-model="Order.FromDate">
                                  <input type="text" class="form-control" id="#fromdate" value="{{Order.FromDate}}"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                              </div>
                          </div>   
                           <div class="col-md-4">
                              <h5>To Date</h5>
                              <div class="input-group date" ng-model="Order.ToDate" date-time>
                                  <input type="text" class="form-control" id="#todate" value="{{Order.ToDate}}"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                              </div>
                          </div>  
                          <div class="col-md-2">
                            <button class="btn btn-info mt25 btn-block" type="button" ng-click="Order.Filterbydate()"><i class="fa fa-search"></i> Search</button>
                          </div>     
                           <div class="col-md-2">
                            <button class="btn btn-info mt25 btn-block" type="button" ng-click="Order.ResetList()"><i class="fa fa-reply-all"></i>Reset</button>
                          </div>    
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!--End of Search Panel-->
    <!--Table Panel-->
      <div class="row">
          <div class="col-lg-12">
               <div class="hpanel">
               <!--    <div class="panel-heading hbuilt">
                       <div class="panel-tools">
                          <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                          <a class="fullscreen"><i class="fa fa-expand"></i></a>
                          <a class="closebox"><i class="fa fa-times"></i></a>
                      </div> 
                       
                  </div> -->
                  <div class="panel-body" id="orderstable">
                      <!--  <table id="example2" datatable="ng" class="row-border hover table table-striped table-bordered table-hover"  dt-rows >
                          <thead>
                            <tr>
                                <th>Date</th>
                                <th>Order Number</th>
                                <th>Number of booths</th>
                                <th>CBM</th>
                                <th>Order Value</th>
                                <th>No of Cartons</th>
                                <th class="abc"></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="order in ::Order.Orders ">
                                <td>{{order. LastUpdate}}</td>
                                <td>{{order. OrderNumber}}</td>
                                <td>{{order. TotalBooth}}</td>
                                <td>{{order. TotalOrderCBM}}</td>
                                <td>{{order. TotalOrderUsValue}}</td>
                                <td>{{order. TotalCartons}}</td>
                                <td>
                                  <button ng-if="order.OrderStatus == 0" class="btn btn-danger btn-circle" ng-click="Item.Contioueorder(order.OrderId)" type="button"><i class="fa fa-plus"></i></button>
                                  <a ng-href="../Login/vieworder?id={{order.OrderId}} & status={{order.OrderStatus}}"><button class="btn btn-info btn-circle pull-right" type="button"><i class="fa fa-eye"></i></button></a>
                                </td>
                            </tr>
                          </tbody>
                      </table> -->
                       <table datatable="" id="table_orders" dt-options="Order.dtOptions"  dt-instance="Order.dtInstance" dt-columns="Order.dtColumns" class="row-border hover table table-striped table-bordered table-hover"></table>
                  </div>
                
                </div>
          </div>
      </div>
    <!--End Of table panel-->
  </div>
  <!--End of Content Panel-->
</div>
<!--End of Wrapper -->


<!-- <script src="../assets/scripts/homer.js"></script> -->


<?php
$this->load->view('layouts/footer');
?>

