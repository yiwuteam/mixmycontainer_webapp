<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>

<style>
 .mrt24{
   margin-top: 24px;
   }
   .ml24
   {
      margin-left: 24px;
   }
    .col-md-6.pull-right {
        text-align: right;
    }
    .form-group.col-lg-6.pull-right {
        padding-top: 30px;
    }
    button.padding {
        margin-right: 10px;
    }
    .mr20 {
        margin-right: 20px;
    }
    .hpanel .hbuilt.panel-heading {
        background-color: #f7f9fa !important
    }
    textarea#altdescription {
    line-height: 16px !important;
    }
    textarea#description {
        line-height: 16px !important;
    }
    #loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }
    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
    .boothmodel {
        overflow-x: auto;
        max-height: 550px;
    }
    #snapshot {
        background: url('../../QA/assets/images/image_icon2.png')

    }
    #boothsnapshot {
        background: url('../../QA/assets/images/image_icon2.png')
    }
    #newcontainer {
        width: 100%
    }
    #upgradecontainer {
        width: 100%
    }
    .btn.btn-sm.btn-primary.m-t-n-xs.pull-right {
        margin-bottom: 20px;
    }
    .btn.btn-sm.btn-success.m-t-n-xs.pull-right.padding.endorder {
        margin-bottom: 20px;
    }
    .btn.btn-primary.btn-sm.m-t-n-xs.padding.pull-right.pauseorder {
        margin-bottom: 20px;
    }
    .form-group.col-lg-12.text-center {
        border-left: 6px solid #55ffff;
        background-color: lightgrey;
        padding-bottom: 10px;
    }
    #showusedcontainer {
        width: 36%;
    }
    .important
    {
        color:red;
    }
    @media screen and (max-width: 992px) {
        #scrollerdiv {
        visibility: hidden;
        display: none;
        }
    }
    @media screen and (max-width: 435px) {
        .webcam-live {
        width: 220px
        }
    }
    @media screen and (max-width: 335px) {
        .webcam-live {
        width: 150px
        }
    }
    @media screen and (max-width: 435px) {
        #snapshot {
        width: 220px
        }
    }
    @media screen and (max-width: 335px) {
        #snapshot {
        width: 150px
        }
    }
    #div_itemfillprogressbar{
    width:100%;
    }
</style>

<title>New Item</title>
<div id="wrapper">
    <div id="withmodel" ng-controller="OrderController" ng-app="mixMyContainer" data-ng-init="Item.itempagefunctions()">
     <div class="scroller_anchor"></div> 
        <div class="normalheader transition animated fadeIn">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="#">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>

                    <div id="hbreadcrumb" class="pull-right m-t-lg">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="../Login/dashboard">Dashboard</a>
                            </li>
                            <li>
                                <span><a href="../Login/neworder">New Order</a></span>
                            </li>
                            <li class="active">
                                <span>New Item</span>
                            </li>
                        </ol>
                    </div>
                    <h2 class="font-light m-b-xs">
                        Items
                    </h2>
                    <small>place new items</small>
                </div>
            </div>
        </div>
        <div style="display:none" class="scroller" id="scrollerdiv">
        <div class="content animate-panel" ng-show="Item.PageReady">
            <!-- <form name="itemform" ng-submit="submitForm()"> -->
            <div class="row">
            <div class="col-md-6">
            <div class="row">
                    <p class="pull-left">Container full:<strong>{{Item.unfilledvolumepercentage}}%</strong>
                    </p>
                    </div>
                    <div class="row">
                        <div class="progress m-t-xs full progress-striped">

                            <div style="width: {{Item.unfilledvolumepercentage}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class=" progress-bar progress-bar-warning">
                                <span>{{Item.unfilledvolumepercentage}}%</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                <div class="col-lg-6" style="margin-left: -6px;">
                <div class="row">
                    <button class="btn btn-sm btn-primary m-t-n-xs pull-right" ng-click="Item.LoadModal()"><strong>New Booth</strong>
                    </button>
                    <button class="btn btn-sm btn-success m-t-n-xs pull-right padding endorder" type="submit"><strong>End/New Order</strong>
                    </button>
                    <button class="btn btn-primary btn-sm m-t-n-xs  padding pull-right pauseorder"><strong>Pause Order</strong>
                    </button>
                    </div>
                </div>
            </div>
                </div>
                <div class="col-md-3" style="margin-left: 30px;">
                <p>Total CBM on this order: <strong>{{Item.totalcbm}}</strong>
                    </p>
                    <p>Total weight of order: <strong>{{Item.totalweight}}</strong>
                    </p>
                    <p>Currently Filling Container:
                    </p>
                    <!-- <p> -->
                    <strong>{{Item.unfilledcontainer}}</strong>
                    <!-- </p> -->
                   <!--  <select class="js-source-states" style="width: 53%;display: inline-block;" select2="" ng-model="Item.activecontainer" id="showusedcontainer" name="showusedcontainer" ng-options="usedcontainer.Container for usedcontainer in Item.containers track by usedcontainer.ContainerId">
                    </select> -->
                    <!-- <span>{{Item.imagebase64}}</span> -->
                    

                </div>
                <div class="col-md-3" style="margin-right: -30px;margin-left: -30px;">
                    <p>
                        Order number: <strong>{{Item.Orderno}}</strong>
                    </p>
                    <p>Order value: <strong>US${{Item.Usprice}}</strong> (<strong>RMB{{Item.Rmbprice}}</strong>)</p>
                    <p>Value of purchases in current Booth: <strong>US${{Item.Usboothprice}}</strong> (<strong>RMB{{Item.Rmbboothprice}}</strong>)</p>
                    

                </div>

            </div>
            
            </div>
            </div>

       <!--  <div class="scroller" id="scrollerdiv"> -->
       <div class="normalheader transition animated fadeIn" ng-show="!Item.PageReady">
          <div class="hpanel">
              <div class="panel-body">
                  <a class="small-header-action" href="#">
                     <!--  <div class="clip-header">
                          <i class="fa fa-arrow-up"></i>
                      </div> -->
                  </a>

                   <div id="hbreadcrumb" class="pull-right m-t-lg">
                      <ol class="hbreadcrumb breadcrumb">
                         <!--  <li><a href="../Login/dashboard"></a></li>
                          <li><a href="../Login/orders"></a></li> -->
                          <li class="active">
                              <span></span>
                          </li>
                      </ol>
                  </div>
                  <h2 class="font-light m-b-xs" style="margin-top: 5px;">
                      Loading...
                  </h2>
                  <small></small>
              </div>
          </div>
      </div>
        <div class="content animate-panel" id="itemcontent" ng-show="Item.PageReady">
            <!-- <form name="itemform" ng-submit="submitForm()"> -->
            <div class="row">
                <div class="col-md-6 pull-left">
                    <!-- <span>{{Item.imagebase64}}</span> -->
                    <p>
                        Order number: <strong>{{Item.Orderno}}</strong>
                    </p>
                    <p>Order value: <strong>US${{Item.Usprice}}</strong> (<strong>RMB{{Item.Rmbprice}}</strong>)</p>
                    <p>Value of purchases in current Booth: <strong>US${{Item.Usboothprice}}</strong> (<strong>RMB{{Item.Rmbboothprice}}</strong>)</p>

                </div>
                <div class="col-md-6 pull-right">

                    <p>Total CBM on this order: <strong>{{Item.totalcbm}}</strong>
                    </p>
                    <p>Total weight of order: <strong>{{Item.totalweight}}</strong>
                    </p>
                     <p>Total containers filled:<span ng-repeat="item in Item.containers" ng-if="item.Status !=0"><strong>{{item.Count}}</strong> x <strong>{{item.Container}}</strong> |</span></p>
                    <!-- <select class="js-source-states" style="width: 30%" select2="" ng-model="Item.activecontainer" id="showusedcontainer" name="showusedcontainer" ng-options="usedcontainer.Container for usedcontainer in Item.containers track by usedcontainer.ContainerId">
                    </select> -->

                </div>
                <div class="col-md-12">
                    <p class="pull-right">Container full:<strong>{{Item.unfilledvolumepercentage}}%</strong>
                    </p>
                    <div class="m">
                        <div id="div_itemfillprogressbar" class="progress m-t-xs full progress-striped">

                            <div style="width: {{Item.unfilledvolumepercentage}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class=" progress-bar progress-bar-warning">
                                <span>{{Item.unfilledvolumepercentage}}%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <p>Currently filling Container: <strong>{{Item.unfilledcontainer}}</strong></p>
                    <button class="btn btn-sm btn-primary m-t-n-xs pull-right" ng-click="Item.LoadModal()"><strong>New Booth</strong>
                    </button>
                    <button class="btn btn-sm btn-success m-t-n-xs pull-right padding endorder" type="submit"><strong>End/New Order</strong>
                    </button>
                    <button class="btn btn-primary btn-sm m-t-n-xs  padding pull-right pauseorder"><strong>Pause Order</strong>
                    </button>

                </div>
            </div>
            </div>
            <!-- </div> -->
            <div class="content animate-panel" id="itemcontent" ng-show="Item.PageReady">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                     <form name="item_form" method="post" novalidate ng-submit="Item.checkBoothSaved(item_form)">
                        <div class="panel-heading">
                            <!--Heading-->
                        </div>
                        <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="hpanel">
                                                    <div class="panel-heading hbuilt">
                                                        Item No and Description
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group ">
                                                                    <label>Item Number<span class="important">*</span></label>
                                                                    <input type="text" required="" id="itemno" class="form-control" ng-model="Item.itemno" name="itemno" placeholder="item number" ng-class="{ 'error' : (item_form.itemno.$invalid && !item_form.itemno.$pristine) ||(item_form.itemno.$invalid && Item.addItemError) }">
                                                                    <label ng-show="(item_form.itemno.$invalid && !item_form.itemno.$pristine) ||(item_form.itemno.$invalid && Item.addItemError)" class="error">Item number is required.</label>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Item Description<span class="important">*</span></label>
                                                                    <textarea type="text" ng-blur="Item.text_transilate();" rows="5" maxlength="120"id="description" ng-model="Item.description" name="description" id="description" class="form-control" name="" placeholder="item description" required="" ng-class="{ 'error' : (item_form.description.$invalid && !item_form.description.$pristine) ||(item_form.description.$invalid && Item.addItemError) }">
                                                                    </textarea>
                                                                    <label ng-show="(item_form.description.$invalid && !item_form.description.$pristine) ||(item_form.description.$invalid && Item.addItemError)" class="error">Item description is required.</label>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Item Description(secondary language)</label>
                                                                    <textarea type="text" rows="5" id="altdescription" ng-model="Item.altdescription" maxlength="120" class="form-control" name="altdescription" placeholder="item description">
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center col-lg-6">
                                                <div class="hpanel">
                                                    <div class="panel-body file-body">
                                                        <canvas id="snapshot" name="snapshot" ng-model="Item.snapshot" width="200" height="200" style="background-repeat: no-repeat;"></canvas>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <a data-target="#MyModel" data-toggle="modal">Take a Picture</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="hpanel">
                                            <div class="panel-heading hbuilt">
                                                Packing
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Units per carton <span class="important">*</span></label>
                                                            <input type="number" numbers-only min="1"  required="" ng-class="{ 'error' : (item_form.unitpercarton.$invalid && !item_form.unitpercarton.$pristine)||(item_form.unitpercarton.$invalid &&Item.addItemError) }" id="unitpercarton" ng-model="Item.unitpercarton" ng-change="Item.calculatecartonqty()" class="form-control" name="unitpercarton" placeholder="Unit per carton">
                                                            <label ng-show="(item_form.unitpercarton.$invalid && !item_form.unitpercarton.$pristine)||(item_form.unitpercarton.$invalid &&Item.addItemError)" class="error">Unit per carton is required.</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Gross weight of carton<span class="important">*</span></label>
                                                            <div class="input-group m-b">
                                                                <input type="number" numbers-only min="1" required="" ng-class="{ 'error' : (item_form.grossweight.$invalid && !item_form.grossweight.$pristine)||(item_form.grossweight.$invalid &&Item.addItemError) }" id="cartonGrossWeight" class="form-control" ng-model="Item.cartonGrossWeight" name="grossweight" placeholder="gross weight of carton">
                                                                <span class="input-group-addon">kg</span>
                                                            </div>
                                                            <label ng-show="(item_form.grossweight.$invalid && !item_form.grossweight.$pristine)||(item_form.grossweight.$invalid &&Item.addItemError)" class="error">Carton gross weight is required.</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Length<span class="important">*</span></label>
                                                            <div class="input-group m-b">
                                                                <input type="number" numbers-only min="1" required="" ng-class="{ 'error' : (item_form.length.$invalid && !item_form.length.$pristine)||(item_form.length.$invalid &&Item.addItemError) }" id="Length" class="form-control" ng-change="Item.calculatecartonsize()" name="length" ng-model="Item.Length" placeholder="length of carton">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                            <label ng-show="(item_form.length.$invalid && !item_form.length.$pristine)||(item_form.length.$invalid &&Item.addItemError)" class="error">Carton Length is required.</label>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Width<span class="important">*</span></label>
                                                            <div class="input-group m-b">
                                                                <input type="number" numbers-only min="1" required="" ng-class="{ 'error' : (item_form.width.$invalid && !item_form.width.$pristine)||(item_form.width.$invalid &&Item.addItemError) }" id="Width" class="form-control" ng-change="Item.calculatecartonsize()" name="width" ng-model="Item.Width" placeholder="width of the carton">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                            <label ng-show="(item_form.width.$invalid && !item_form.width.$pristine)||(item_form.width.$invalid &&Item.addItemError)" class="error">Carton Width is required.</label>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>Height<span class="important">*</span></label>
                                                            <div class="input-group m-b">
                                                                <input type="number" numbers-only min="1"  required="" ng-class="{ 'error' : (item_form.height.$invalid && !item_form.height.$pristine)||(item_form.height.$invalid &&Item.addItemError) }" id="Height" class="form-control" ng-change="Item.calculatecartonsize()" name="height" ng-model="Item.Height" placeholder="height of the carton">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                            <label ng-show="(item_form.height.$invalid && !item_form.height.$pristine)||(item_form.height.$invalid &&Item.addItemError)" class="error">Carton Height is required.</label>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>CBM(Carton Volume)</label>
                                                            <div class="input-group m-b">
                                                                <input type="text"  value="" id="cbm" class="form-control" name="cbm" ng-model="Item.cbm" placeholder="CBM of carton" readonly>
                                                                <span class="input-group-addon">cbm</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="hpanel">
                                                <div class="panel-heading hbuilt">
                                                    Cost
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group ">

                                                                <label>Price in US$ <span class="important">*</span></label>
                                                                <div class="input-group m-b">
                                                                    <input type="text" valid-number required="" ng-class="{ 'error' : (item_form.priceinus.$invalid && !item_form.priceinus.$pristine)||(item_form.priceinus.$invalid &&Item.addItemError) }" id="priceinus" submit-required="true" ng-model="Item.priceinus" ng-change="Item.Getuspriceconversion()" class="form-control" name="priceinus" placeholder="Price in US Dollar"> <span class="input-group-addon">$</span>
                                                                </div>
                                                                <label ng-show="(item_form.priceinus.$invalid && !item_form.priceinus.$pristine)||(item_form.priceinus.$invalid &&Item.addItemError)" class="error">Price in US Dollar is required.</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Price in RMB <span class="important">*</span></label>
                                                                <div class="input-group m-b">
                                                                    <input type="text" valid-number  id="ExchangeRate" ng-model="Item.ExchangeRate" ng-keyup="Item.Getrmbpriceconversion()" class="form-control" name="priceinzar" placeholder="Price in RMB" required="" ng-class="{ 'error' : (item_form.priceinzar.$invalid && !item_form.priceinzar.$pristine)||(item_form.priceinzar.$invalid &&Item.addItemError) }">
                                                                    <span class="input-group-addon">¥</span>
                                                                </div>
                                                                <label ng-show="(item_form.priceinzar.$invalid && !item_form.priceinzar.$pristine)||(item_form.priceinzar.$invalid &&Item.addItemError)" class="error">Price in RMB is required.</label>
                                                                
                                                            </div>
                                                            <div class="form-group ">
                                                                <div ng-if="Item.thirdcurrencystatus == 1">
                                                                    <label>Price in <span>{{Item.thirdcurrencyname}}</span></label>
                                                                    <input type="text" id="ExchangeRate3dCurrency" ng-model="Item.ExchangeRate3dCurrency" class="form-control" name="priceinzar" placeholder="Third currency price" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="hpanel">
                                                <div class="panel-heading hbuilt">
                                                   Order Quantity
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Units qty <span class="important">*</span></label>
                                                                <input type="number" numbers-only min="1" required=""  ng-class="{ 'error' : (item_form.unitqty.$invalid && !item_form.unitqty.$pristine)||(item_form.unitqty.$invalid &&Item.addItemError) }" id="unitqty" ng-change="Item.calculatecartonqty()" ng-model="Item.unitqty" class="form-control" name="unitqty" placeholder="unit quantity">
                                                                <label ng-show="(item_form.unitqty.$invalid && !item_form.unitqty.$pristine)||(item_form.unitqty.$invalid &&Item.addItemError)" class="error">Unit quanity is required.</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Cartons qty <span class="important">*</span></label>
                                                                <input type="number" numbers-only min="1" required=""  ng-class="{ 'error' : (item_form.cartonqty.$invalid && !item_form.cartonqty.$pristine)||(item_form.cartonqty.$invalid &&Item.addItemError) }" id="cartonqty"  ng-change="Item.calculateunitsqty()" ng-focus="Item.calculateunitsqty()" ng-model="Item.cartonqty" class="form-control" name="cartonqty" placeholder="carton quantity" >

                                                                <label ng-show="(item_form.cartonqty.$invalid && !item_form.cartonqty.$pristine)||(item_form.cartonqty.$invalid &&Item.addItemError)" class="error">Carton quantity is required.</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                           

                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <input type="submit" class="btn btn-primary pull-right mr20" value="Save Item">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>

        <!-- <div id="loader" ng-show="!Item.PageReady">

        </div> -->
      

        <div class="modal fade" id="MyModel1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content boothmodel">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">New Booth</h4>
                    </div>
                    <div class="modal-body">
                    <div class="row col-lg-12">
                          <div class="form-group col-lg-11">
                                           <label>Trade District/Area<span class="important">*</span></label>
                                           <select class="js-source-states" style="width: 100%" id="trade" name="trade" ng-init="Item.getTrades()"
                                              ng-model="Item.Trade" select2="" ng-options="Trades.Name  for Trades in Item.Trades track by Trades.Id">
                                           </select>
                                           <label ng-show="Item.BoothTrade" class="error" >Please select valid trade district/Area</label>
                                        </div>
                                        <div class="form-group col-lg-1">
                                           <button class="btn btn-info btn-circle mrt24" type="button" ng-click="Item.addTrade()"><i class="fa fa-plus"></i></button>
                                        </div>
                                       
                        </div>
                        <div class="row col-lg-12">
                           <div class="form-group col-lg-11">
                                           <label>Hall<span class="important">*</span></label>
                                           <select class="js-source-states" style="width: 100%;" id="hall" name="hall"
                                              ng-model="Item.Hall" select2="" ng-options="Halls.Name  for Halls in Item.Halls track by Halls.Id">
                                           </select>
                                           <label ng-show="Item.BoothHall" class="error" >Please select valid hall</label>
                                        </div>
                                        <div class="form-group col-lg-1">
                                           <button class="btn btn-info btn-circle mrt24" type="button" ng-click="Item.addHall()" ><i class="fa fa-plus"></i></button>
                                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">

                                <div class="form-group col-lg-6">
                                    <label>Booth No<span class="important">*</span></label>
                                    <input type="text" value="" id="newboothno" ng-model="Item.newboothno" ng-blur="Item.getBoothInfoItem()" class="form-control" name="newboothno" placeholder="Booth number" required="" ng-class="{ 'error' : Item.BoothError }">
                                    <label ng-show="Item.BoothError" class="error">Booth No is required.</label>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Booth company name</label>
                                    <input type="text" value="" id="newboothname" ng-model="Item.newboothname" class="form-control" name="newboothname" placeholder="Company Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <div class="form-group col-lg-6">
                                    <label>Telephone</label>
                                    <input type="text" value=""  maxlength="15" id="newtelephone" ng-model="Item.newboothtelephone" class="form-control" name="newtelephone" placeholder="Telephone">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Business scope</label>
                                    <input type="text" value="" id="newbusinessscope" ng-model="Item.newbusinesscope" class="form-control" name="newbusinessscope" placeholder="Business scope">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12" align="center">
                                <label>Business card</label>
                                <div class="form-group col-lg-12" id="businesscardimage">
                                    <canvas id="boothsnapshot" name="boothsnapshot" ng-model="Item.boothsnapshot" style="background-repeat: no-repeat;" width="50" height="50" class="text-center"></canvas>
                                    </br><a><span ng-click="Item.changeboothpicture()">Take a picture</span></a>
                                </div>
                                <div class="modal-body" align="center" id="businesscardcamera" ng-show="Item.IsCamera">
                                <div class="normalheader transition animated fadeIn" ng-show="Item.Webcamerrormessage">
                                  <div class="panel-body">
                                      <h2 class="font-light m-b-xs">
                                          No Preview Available
                                      </h2>
                                      <small></small>
                                  </div>
                                 </div>
                                    <webcam  channel="channel" on-stream="Item.onStream(stream)" on-streaming="Item.onSuccess()" on-error="Item.onError(err)">
                                    </webcam>
                                    <button ng-show="Item.HideCaptureButton" ng-click="Item.makeBoothSnapshot()" class="btn btn-sm btn-success ">Capture</button>
                                </div>
                                <div class="text-center col-lg-12" ng-show="!Item.IsCamera">
                                       <div class="hpanel">
                                          <div class="panel-body file-body">
                                                <canvas  id="snapshot" name="snapshot" ng-model="Item.snapshot" style="background: url('../../../QA/assets/images/{{Item.BoothImage}}') center no-repeat" class="text-center"></canvas>
                                          </div>
                                          <div class="panel-footer">
                                             <a href="#!" ng-click="Item.CameraOn()">Scan business card</a>
                                          </div>
                                       </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" ng-if=!Item.isNoBooth>Close</button>
                            <button type="button" class="btn btn-primary" ng-click="Item.Addbooth()">Save Booth</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  <div class="modal fade" id="MyModel" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Image Upload</h4>

                    </div>
                    <!-- <div class="modal-body">
                        <div id="image-holder"><img id="My_Img" ng-src="{{myImage}}" class="img-circle m-b" onerror="this.src='../../../MMC/assets/images/upload.png'" height="100" width="100" /></div>
                        <input type="file" accept="image/*" id="fileInput" class="file" ng-model="Item.Image" name="files[]" />
                    </div> -->
                    <div class="modal-body" align="center">
                       <div class="normalheader transition animated fadeIn" ng-show="Item.Webcamerrormessage">
                    <div class="panel-body">
                  <h2 class="font-light m-b-xs">
                      No Preview Available
                          </h2>
                          <small></small>
                      </div>
                     </div>
                        <webcam  channel="channel" on-stream="Item.onStream(stream)" on-streaming="Item.onSuccess()" on-error="Item.onError(err)">
                                    </webcam>
                        <button ng-show="Item.HideCaptureButton" ng-click="Item.makeSnapshot()" class="btn btn-sm btn-success " data-dismiss="modal">Capture</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="Item.ImageUpload()">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="MyModel2"  role="dialog" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-md">
                <div class="modal-content ">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Container Full</h4>
                    </div>
                    <div class="modal-body" data-ng-init="Item.contianerlist()">
                        <h5>You have filled up your current container and have some cartons left over. (<span>{{Item.balancecartongs}}</span> cartons = <span>{{Item.popupshowbalancevolume}}</span> cbm)</h5>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <div class="form-group col-lg-12 text-center" s>
                                     <h5 ng-show="Item.newcontainerlist.length>0" >Allocate Overflow to a New Container</h5>
                                    <!-- <h5>Allocate Overflow to a New Container</h5> -->
                                    <h5 ng-if="Item.newcontainerlist.length==0">Your item cannot be allocated in this container please upgrade to a bigger container  </h5>
                                    <select class="js-source-states" style="width: 100%" select2="" ng-model="Item.addnewcontainer" id="newcontainer" name="newcontainer" ng-options="containerkey.Container for containerkey in Item.newcontainerlist track by containerkey.ID">
                                    </select>
                                    <!-- <span>balancevolume : {{Item.popupbalancevolume}}</span>
                                        <span>balanceweight : {{Item.popupbalanceweight}}</span> -->
                                    </br>
                                    </br>
                                    <button type="button" class="btn btn-primary" ng-show="Item.newcontainerlist.length>0" ng-click="Item.newContianer()">Start filling new container</button>
                                </div>

                                 <div class="form-group col-lg-12 text-center">
                                    <h5 ng-show="Item.upgradecontainerlist.length>0">You can upgrade your current container <span>{{Item.currentcotainername}}</span> container to large one</h5>
                                    <h5 ng-if="Item.upgradecontainerlist.length==0">No Containers available for upgrade container</h5>
                                    <select class="js-source-states" style="width: 100%" select2="" ng-model="Item.addupgradecontainer" id="upgradecontainer" name="upgradecontainer" ng-options="containerkey.Container for containerkey in Item.upgradecontainerlist track by containerkey.ID" ng-show="Item.upgradecontainerlist.length>0">
                                    </select>
                                    </br>
                                    </br>
                                    <button type="button" class="btn btn-primary" ng-click="Item.upgradeContainer()" ng-if="Item.upgradecontainerlist.length>0">Upgrade current container</button>
                                </div>

                                <div class="form-group col-lg-12 text-center">
                                    <h5>Ignore and continue to Shop</h5>
                                    </br>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="Item.ignorecontainer()">Ignore & continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">
     $(window).scroll(function(e) {
    // Get the position of the location where the scroller starts.
    var scroller_anchor = $(".scroller_anchor").offset().top;
     
    // Check if the user has scrolled and the current position is after the scroller start location and if its not already fixed at the top 
    if ($(this).scrollTop() >= scroller_anchor && $('.scroller').css('position') != 'fixed') 
    {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
        //alert("hai");
        //document.getElementById("pagecontent").style.visibility = "hidden";
         $('#itemcontent').css({
           'display': 'none'
        });
        $('.scroller').css({
            'display': 'block',
            'background': '#FFF',
            'webkit-box-shadow': '0px 4px 10px -2px rgba(0, 0, 0, 0.3)',
            'moz-box-shadow': '0px 4px 10px -2px rgba(0, 0, 0, 0.3)',
            'box-shadow':  '0 4px 10px -2px rgba(0, 0, 0, 0.3)',
            'height': '136px',
            'position': 'fixed',
            'margin-left': '-180px',
            'width': '100%',
            'top': '-10px',
            'z-index':100
            
        });
        // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
        $('.scroller_anchor').css('height', '50px');
        //  $('.pull-left').css ({
        //     'float': 'left',
        //     'margin-left': '-24px'
        // });
    } 
    else if ($(this).scrollTop() < scroller_anchor && $('.scroller').css('position') != 'relative') 
    {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.
         $('#itemcontent').css({
           'display': 'block'
        });
        // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
        $('.scroller_anchor').css('height', '0px');
         
        //Change the CSS and put it back to its original position.
         //*.scroller{background:#FFF; border:1px solid #CCC; margin:0 0 10px; z-index:100; height:200px; text-align:center; }

        $('.scroller').css({
        'display': 'none',
        'background': 'rgb(241, 243, 246)',
        'webkit-box-shadow': '',
        'moz-box-shadow': '',
        'box-shadow':  '',
        'top': '0px',
        'width': '',
        'margin-left': '',
        'position': 'relative'
        });
        // $('.pull-left').css ({
        //     'float': '',
        //     'margin-left': ''
        // });
    }
});
// function changeimage(){
//     // alert("hai");
//     var c = document.getElementById("snapshot");
//     var ctx = c.getContext("2d");
//     // var img = new Image();
//     // img.src = "../../MMC/assets/images/default.jpg";
//      ctx.strokeRect(0,0,400,300);
//     //   ctx.drawImage(img, 0, 0);
//     ctx.clearRect(0, 0, 320, 240);
// // ctx.clearRect(0, 0, 0, 0);
// }
</script>