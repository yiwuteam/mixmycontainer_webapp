<?php $this->load->view('layouts/adminheader'); ?>
<?php $this->load->view('layouts/adminsidebar'); ?>
<title>ADMIN SETUP</title>
<style type="text/css">
    .pd10
    {
        padding: 10px;
    }
    .pl-pr-30
    {
        padding-left: 30px;
        padding-right: 30px;
    }
    .loading {    
    background-color: #ffffff;
    background-image: url("http://loadinggif.com/images/image-selection/34.gif");
    background-size: 15px 15px;
    background-position:right center;
    background-repeat: no-repeat;
}
</style>
<div id="wrapper" ng-controller="AdminController">
    <div class="normalheader transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                       <!--  <li><a href="#!">Dashbaord</a></li> -->
                        <li class="active">
                            <span>Admin Setup</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Admin Setup
                </h2>
                <small>Gold Agents, Trades, Halls and Containers.</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">Gold Agent</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Trade & Hall</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Containers</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Default Values</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                 <form name="goldagent_form" method="post" novalidate ng-submit="Admin.AddGoldAgent()">
                                        <div class="row">
                                           <div class="col-lg-12">
                                              <div class="row">
                                                 <div class="row col-lg-12">
                                                    <div class="form-group col-lg-12">
                                                       <label>Company Name<span class="important">*</span></label>
                                                       <input type="text" required="" ng-class="{ 'error' : (goldagent_form.companyname.$invalid && goldagent_form.companyname.$touched)||(
                                                       Admin.golderror && goldagent_form.companyname.$invalid)}" ng-model="Admin.GoldAgentName"   id="companyname" class="form-control" name="companyname" placeholder="Company Name" maxlength="25">
                                                       <label ng-show=" (goldagent_form.companyname.$invalid && goldagent_form.companyname.$touched)||(
                                                       Admin.golderror && goldagent_form.companyname.$invalid)" class="error">Enter Company Name</label>
                                                    </div>
                                                 </div>
                                                 <div class="row col-lg-12">
                                                    <div class="hpanel pd10">
                                                        <div class="panel-heading hbuilt">
                                                            CONTACTS
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row" ng-repeat="GoldAgentContact in Admin.GoldAgentContacts">
                                                                <div class="col-lg-3">
                                                                   <div class="form-group ">
                                                                       <label>Name<span class="important">*</span></label>
                                                                       <input type="text"  required="" id="name_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Name" class="form-control" name="name_{{GoldAgentContact.Contact}}" placeholder="Name" maxlength="25" ng-class="{ 'error' : (goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.name_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid) }" >
                                                                       <label ng-show="(goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.name_{{GoldAgentContact.Contact}}.$touched)||(Admin.golderror && goldagent_form.name_{{GoldAgentContact.Contact}}.$invalid)" class="error">Name is required.</label>
                                                                   </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                   <div class="form-group ">
                                                                       <label>Email<span class="important">*</span></label>
                                                                       <input type="email"  required="" id="email_{{GoldAgentContact.Contact}}" name="email_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Email" class="form-control" placeholder="Email" maxlength="40" ng-class="{ 'error' : (goldagent_form.email_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern)||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required) }" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" >
                                                                       <label ng-show=" (goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.pattern) " class="error">Enter valid Email</label>
                                                                         <label ng-show=" (goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required && goldagent_form.email_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.email_{{GoldAgentContact.Contact}}.$error.required) " class="error">Email is required</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                   <div class="form-group">
                                                                       <label>Mobile<span class="important">*</span></label>
                                                                       <input type="number"  required="" id="phone_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Mobile" class="form-control" name="phone_{{GoldAgentContact.Contact}}" placeholder="Moble" maxlength="25" digit-only  ng-class="{ 'error' : (goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.phone_{{GoldAgentContact.Contact}}.$touched) ||(Admin.golderror && goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid) }" >
                                                                       <label ng-show="(goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid && goldagent_form.phone_{{GoldAgentContact.Contact}}.$touched)||(Admin.golderror && goldagent_form.phone_{{GoldAgentContact.Contact}}.$invalid)" class="error"> Mobile Number is required.</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                   <div class="form-group">
                                                                       <label>Skype</label>
                                                                       <input type="text"  required="" id="skype_{{GoldAgentContact.Contact}}" ng-model="GoldAgentContact.Skype" class="form-control" name="skype_{{GoldAgentContact.Contact}}" placeholder="Skype ID"  maxlength="25" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                   <div class="form-group">
                                                                       <label>Image</label>
                                                                       <input type="file"  required="" id="image_{{GoldAgentContact.Contact}}" image="GoldAgentContact.Image" class="form-control phone" name="image_{{GoldAgentContact.Contact}}"
                                                                       resize-max-height="200" resize-max-width="150" resize-quality="0.7" resize-type="image/jpg" accept="image/x-png,image/jpeg" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                              <div class="col-lg-8">
                                                                 The Gold Agent require to have atleast 5 sales contacts
                                                               </div>
                                                               <div class="col-lg-4">
                                                                   <div class="col-lg-6 pull-right">
                                                                     <a href="#!"  class="btn btn-sm btn-success m-t-n-xs " ng-click="Admin.AddGoldAgentContact()">Add another sales contact
                                                                     </a>
                                                                    </div>
                                                                    <div class="col-lg-5 pull-right" ng-if="Admin.GoldAgentContacts.length>5">
                                                                     <a href="#!"  class="btn btn-sm btn-danger m-t-n-xs " ng-click="Admin.RemoveGoldAgentContact()">Remove sales contact
                                                                     </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="panel-footer">
                                                          
                                                        </div> -->
                                                    </div>
                                                 </div>
                                                
                                                 <div class="row col-lg-12">
                                                    <div class="form-group col-lg-6">
                                                       <label>Office Address<span class="important">*</span></label>
                                                        <textarea class="form-control" name="officeaddress" id="officeaddress" ng-model="Admin.AgentOffice" required="" ng-class="{ 'error' : (goldagent_form.officeaddress.$invalid && goldagent_form.officeaddress.$touched)||(Admin.golderror && goldagent_form.officeaddress.$invalid) }"></textarea>
                                                       <label ng-show="(goldagent_form.officeaddress.$invalid && goldagent_form.officeaddress.$touched)||(Admin.golderror && goldagent_form.officeaddress.$invalid)" class="error">Office Address is required.</label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                      <div class="row">
                                                          <div class="form-group col-lg-6">
                                                           <label>Phone Number<span class="important">*</span></label>
                                                           <input type="text" required="" id="companyphne" ng-model="Admin.CompanyPhone" class="form-control" name="companyphne" placeholder="Phone Number" maxlength="30" digit-only ng-class="{ 'error' : (goldagent_form.companyphne.$invalid && goldagent_form.companyphne.$touched)||(Admin.golderror && goldagent_form.companyphne.$invalid) }">
                                                           <label ng-show="(goldagent_form.companyphne.$invalid && goldagent_form.companyphne.$touched)||(Admin.golderror && goldagent_form.companyphne.$invalid)" class="error">Phone Number is required.</label>
                                                          </div>
                                                            <div class="form-group col-lg-6">
                                                           <label>Number of years in business</label>
                                                           <input type="text" required="" id="bussinessyear" digit-only  ng-model="Admin.BussinessYear" class="form-control" name="bussinessyear" placeholder="Business Year" maxlength="4">
                                                           <!-- <label ng-show="(goldagent_form.boothname.$invalid && goldagent_form.boothname.$touched)||(Order.error && goldagent_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                                          </div>
                                                      </div>
                                                       <div class="row">
                                                          <div class="form-group col-lg-6">
                                                           <label>Fax Number</label>
                                                           <input type="text" required="" id="faxnumber" digit-only  ng-model="Admin.FaxNumber" class="form-control" name="faxnumber" placeholder="Fax Number" maxlength="30">
                                                           <!-- <label ng-show="(goldagent_form.boothname.$invalid && goldagent_form.boothname.$touched)||(Order.error && goldagent_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                                          </div>
                                                           <div class="form-group col-lg-6">
                                                           <label>Number of years in as Gold Agent</label>
                                                           <input type="text" required="" id="goldagentyear" digit-only  ng-model="Admin.GoldYear" class="form-control" name="goldagentyear" placeholder="Gold Agent Year" maxlength="4">
                                                           <!-- <label ng-show="(goldagent_form.boothname.$invalid && goldagent_form.boothname.$touched)||(Order.error && goldagent_form.boothname.$invalid)" class="error">Booth company name is required.</label> -->
                                                          </div>
                                                      </div>
                                                      
                                                    </div>
                                                 </div>
                                                
                                              </div>
                                           </div>
                                        </div>
                                        <div class="m-t-md">
                                           <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/>
                                        </div>
                                 </form>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="hpanel hblue">
                                    <div class="panel-heading hbuilt">
                                        Trade District/Area
                                    </div>
                                    <div class="panel-body">
                                          <form name="trade_form" method="post" novalidate ng-submit="Admin.AddTrade()">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Trade District/Area<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (trade_form.tradename.$invalid && trade_form.tradename.$touched)||(Admin.tradeerror && trade_form.tradename.$invalid)}" ng-model="Admin.Tradename"   id="tradename" class="form-control" name="tradename" placeholder="Trade" maxlength="150" required="">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(trade_form.tradename.$invalid && trade_form.tradename.$touched)||(Admin.tradeerror && trade_form.tradename.$invalid)" class="error">Enter Trade District/Area</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="m-t-md">
                                                   <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/>
                                                </div>
                                        </form>
                                    </div>  
                                    <div class="panel-footer">
                                       Add your Trade District/Area
                                    </div>
                                </div>
                                 <div class="hpanel hgreen">
                                    <div class="panel-heading hbuilt">
                                        Hall
                                    </div>
                                    <div class="panel-body">
                                          <form name="hall_form" method="post" novalidate ng-submit="Admin.AddHall()">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12 pl-pr-30">
                                                            <div class="form-group col-lg-12">
                                                               <label>Hall<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (hall_form.hall.$invalid && hall_form.hall.$touched)||(Admin.hallerror && hall_form.hall.$invalid)}" ng-model="Admin.HallName" id="hall" class="form-control" name="hall" placeholder="Hall" maxlength="150">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(hall_form.hall.$invalid && hall_form.hall.$touched)||(Admin.hallerror && hall_form.hall.$invalid)" class="error">Enter Hall</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                       <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Trade District/Area</label>
                                                               <select class="js-source-states" style="width: 100%" id="TradeId" name="tradeid" ng-init="Admin.getTrades()"
                                                                  ng-model="Admin.TradeId" select3="" ng-options="Trades.Name  for Trades in Admin.Trades track by Trades.Id">
                                                               </select>
                                                               <label ng-show="(Admin.tradeiderror && Admin.hallerror)" class="error" >Please select valid trade district/area</label>
                                                            </div>
                                                          
                                                         </div>
                                                   </div>
                                                </div>

                                                <div class="m-t-md">
                                                   <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/>
                                                </div>
                                          </form>
                                    </div>  
                                    <div class="panel-footer">
                                       Add your hall
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                 <form name="container_form" method="post" novalidate ng-submit="Admin.AddContainer()">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Container<span class="important">*</span></label>
                                                               <input type="text" required="" ng-class="{ 'error' : (container_form.conatiner.$invalid && container_form.conatiner.$touched)||(Admin.conterror && container_form.conatiner.$invalid)}" ng-model="Admin.Container"   id="conatiner" class="form-control" name="conatiner" maxlength="30" placeholder="Container">
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="(container_form.conatiner.$invalid && container_form.conatiner.$touched)||(Admin.conterror && container_form.conatiner.$invalid)" class="error">Enter Container</label>
                                                            </div>
                                                         </div>
                                                           <div class="row col-lg-12">
                                                            <div class="form-group col-lg-6">
                                                               <label>Max Volume<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (container_form.maxvolume.$invalid && container_form.maxvolume.$touched)||(Admin.conterror && container_form.maxvolume.$invalid)}" ng-model="Admin.MaxVolume"   id="maxvolume"  class="form-control" name="maxvolume" placeholder="Max volume"  maxlength="25"><span class="input-group-addon">CBM</span></div>
                                                               
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show=" (container_form.maxvolume.$invalid && container_form.maxvolume.$touched)||(Admin.conterror && container_form.maxvolume.$invalid)" class="error">Max Volume is required</label>
                                                            </div>
                                                             <div class="form-group col-lg-6">
                                                               <label>Max Weight<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' :( container_form.maxweight.$invalid && container_form.maxweight.$touched)||(Admin.conterror && container_form.maxweight.$invalid)}"  ng-model="Admin.MaxWeight"   id="maxweight" class="form-control" name="maxweight" placeholder="Max Weight"  maxlength="25"><span class="input-group-addon">KG</span></div>
                                                               
                                                             <!--   <span class="help-block" id="ordernumbers" style="color: red;"></span> -->
                                                               <label ng-show="( container_form.maxweight.$invalid && container_form.maxweight.$touched)||(Admin.conterror && container_form.maxweight.$invalid)" class="error">Max Weight is required</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="m-t-md">
                                                   <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Save & continue"/>
                                                </div>
                                </form>
                                </div>
                        </div>
                         <div id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                 <form name="default_form" id="default_form" method="post" novalidate ng-submit="Admin.UpdateDefault(default_form)">
                                                <div class="row">
                                                   <div class="col-lg-12">
                                                      <div class="row">
                                                           <div class="row col-lg-12">
                                                            <div class="form-group col-lg-6">
                                                               <label>Gross Weight<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (default_form.grossweight.$invalid && default_form.grossweight.$touched)||(Admin.deferror && default_form.grossweight.$invalid)}"  ng-model="Admin.Default.GrossWeight"   id="grossweight"  class="form-control loading" name="grossweight" placeholder="Gross Weight"  maxlength="10"><span class="input-group-addon">KG</span></div>
                                                               <label ng-show=" (default_form.grossweight.$invalid && default_form.grossweight.$touched)||(Admin.deferror && default_form.grossweight.$invalid)" class="error">Gross Weight is required</label>
                                                            </div>
                                                             <div class="form-group col-lg-6">
                                                               <label>Length<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (default_form.length.$invalid && default_form.length.$touched)||(Admin.deferror && default_form.length.$invalid)}"  ng-model="Admin.Default.Length"  id="length"  class="form-control loading" name="length" placeholder="Length"  maxlength="10"><span class="input-group-addon">CM</span></div>
                                                               <label ng-show="  (default_form.length.$invalid && default_form.length.$touched)||(Admin.deferror && default_form.length.$invalid)" class="error">Length is required</label>
                                                            </div>
                                                         </div>
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-6">
                                                               <label>Width<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (default_form.width.$invalid && default_form.width.$touched)||(Admin.deferror && default_form.width.$invalid)}"  ng-model="Admin.Default.Width"   id="width"  class="form-control loading" name="width" placeholder="Width"  maxlength="10"><span class="input-group-addon">CM</span></div>
                                                               <label ng-show="  (default_form.width.$invalid && default_form.width.$touched)||(Admin.deferror && default_form.width.$invalid)" class="error">Width is required</label>
                                                            </div>
                                                             <div class="form-group col-lg-6">
                                                               <label>Height<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (default_form.height.$invalid && default_form.height.$touched)||(Admin.deferror && default_form.height.$invalid)}"  ng-model="Admin.Default.Height"  id="height"  class="form-control loading" name="height" placeholder="Height"  maxheight="10"><span class="input-group-addon">CM</span></div>
                                                               <label ng-show="  (default_form.height.$invalid && default_form.height.$touched)||(Admin.deferror && default_form.height.$invalid)" class="error">Height is required</label>
                                                            </div>
                                                         </div>
                                                         <div class="row col-lg-12">
                                                            <div class="form-group col-lg-6">
                                                               <label>Exchange Rate<span class="important">*</span></label>
                                                               <div class="input-group m-b"><input type="number" required="" ng-class="{ 'error' : (default_form.exchangerate.$invalid && default_form.exchangerate.$touched)||(Admin.deferror && default_form.exchangerate.$invalid)}"  ng-model="Admin.Default.ExchangeRate"   id="exchangerate"  class="form-control loading" name="exchangerate" placeholder="Exchange Rate"  maxlength="10"><span class="input-group-addon">¥</span></div>
                                                               <label ng-show=" (default_form.exchangerate.$invalid && default_form.exchangerate.$touched)||(Admin.deferror && default_form.exchangerate.$invalid)" class="error">Exchange Rate is required</label>
                                                            </div>
                                                             <div class="form-group col-lg-6">
                                                              <input type="submit"  class="btn btn-sm btn-primary m-t-n-xs pull-right"   value="Update"/>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>  
                                </form>
                                </div>
                        </div>
                      
                    </div>


                </div>
            </div> 
        </div>
    </div>
</div>
<?php $this->load->view('layouts/footer'); ?>