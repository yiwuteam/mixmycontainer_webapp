<!DOCTYPE html>
<html  ng-app="adminmixMyContainer">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <title>ADMIN LOGIN</title>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/sweetalert/lib/sweetalert2.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/toastr/build/toastr.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/jquery.gritter.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/toastr.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/quirk.css">
    
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/ladda.min.css">
      <!-- MY App -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/ladda.min.css">
    <script src="<?php echo base_url();?>/assets/js/spin.min.js"></script>
      <!-- MY App -->
    <script src="<?php echo base_url();?>/assets/js/ladda.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/angular-ladda.js"></script>
    <script src="<?php echo base_url();?>/assets/js/ng-intl-tel-input.js"></script>
    <script src="<?php echo base_url();?>/assets/js/ngStorage.min.js"></script>
   <script src="<?php echo base_url();?>/assets/js/angular-base64.js"></script>
   <script src="<?php echo base_url();?>/assets/js/ng-infinite-scroll.min.js"></script>
    <script src="<?php echo base_url();?>/application/ng/adminapp.js"></script>
 <!--    <script src="../application/ng/routes.js"></script> -->
  
    
      <!-- App Controller -->
    <script src="<?php echo base_url();?>/application/ng/Controller/Admin/AdminController.js"></script>
    <script src="<?php echo base_url();?>/application/ng/Services/Admin/AdminService.js"></script>
              <!-- <script src="assets/css/angular-datatables.css"> -->
    <style>
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript,
if it's not present, don't show loader */
body.blank {
    background-color: #7ececd !important;
}
.no-js #loader { display: none;  }
a.pull-right {
    padding-top: 10px;
    color: cornflowerblue;
    font-size: 14px;
}
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?php echo base_url();?>/assets/images/loader/loader.gif) center no-repeat;

}
.text-center.m-b-md {
    color: #ffffff;
}

.ajax_loader{

  display: none;
   position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?php echo base_url();?>/assets/images/loader/reaload.svg) center no-repeat;
}

}
</style>
</head>

<body class="blank">
   <div class="ajax_loader"></div>


<div ng-controller="AdminController">
<!-- <div class="splash"> <div class="color-line"></div><div class="splash-title"><img src="<?php
echo asset_url();
?>images/loading-bars.svg" width="64" height="64" /> </div> </div> -->
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<!-- <div class="back-link">
    <a href="index.html" class="btn btn-primary">Back to Dashboard</a>
</div> -->

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>PLEASE LOGIN TO MIXMYCONTAINER</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                       <form role="form" ng-submit="Admin.LoginCheck()">
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" onclick="hideme()" placeholder="" title="Please enter your username" ng-model="Admin.UserName" required="" value="" name="username" id="username" class="form-control">
                                  <span id="uname" style="display:none;" class="help-block small">Your unique username to app</span>
                           </div>
                           <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" onclick="showmetest()" title="Please enter your password"  placeholder="" ng-model="Admin.Password" required="" value="" name="password" id="password" class="form-control">
                              <span id="pwd" style="display:none;"  class="help-block small">Your strong password</span>
                            </div>
                            <!-- <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <div class="form-group">
                            <button type="submit" ladda="Admin.loginLoading" class="ladda-button btn btn-success btn-block" id="button_login" data-spinner-size="30" data-style="slide-left" type="button">Login</button>
                          
                          </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- <div class="col-md-12 text-center">
            <strong></strong><br/> 2015 Copyright Company Name
        </div> -->
    </div>
</div>
</div>
</div>


<script src="<?php echo base_url();?>/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/sparkline/index.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/sweetalert/lib/sweetalert2.js"></script>
<script src="<?php echo base_url();?>/assets/vendor/toastr/build/toastr.min.js"></script>
<!-- App scripts -->
<script src="<?php echo base_url();?>/assets/scripts/homer.js"></script>
<script src="<?php echo base_url();?>/assets/js/webcam.min.js"></script>
<!-- App scripts -->
<script src="<?php echo base_url();?>/assets/scripts/homer.js"></script>


<script src="<?php echo base_url();?>/assets/js/angular-datatables.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/jquery.dataTables.min.js"></script>




<script>
function hideme(){
  document.getElementById("uname").style.display = 'block';
}
function showmetest(){
  document.getElementById("pwd").style.display = 'block';
}
</script>

<script type="text/javascript">


</script>
