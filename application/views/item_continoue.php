<?php $this->load->view('layouts/header'); ?>
<?php $this->load->view('layouts/sidebar'); ?>
<link rel="stylesheet" href="../assets/vendor/select2-3.5.2/select2.css" />
<link rel="stylesheet" href="../assets/vendor/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="../assets/css/intlTelInput.css" />

<style>
.col-md-6.pull-right {
  text-align: right;
}
.form-group.col-lg-6.pull-right {
    padding-top: 30px;
}

button.padding {
    margin-right: 10px;
}
</style>
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel" >
        <div class="panel-body">
            <a class="small-header-action" href="#">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li>
                        <span>Interface</span>
                    </li>
                    <li class="active">
                        <span>Components</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Items
            </h2>
            <small>place new items</small>
        </div>
    </div>
</div>
<div class="content animate-panel" ng-controller="OrderController" ng-app="mixMyContainer">
   <!-- <form name="itemform" ng-submit="submitForm()"> -->
  <div class="row">
    <div class="col-md-6 pull-left">
      <p>
        Order number: <span>{{Item.Orderno}}</span>
      </p>
      <p>Order value: <span>US${{Item.Usprice}}</span> (<span>RMB{{Item.Rmbprice}}</span>)</p>
      <p>Value of purchases in current Booth: <span>US${{Item.Usboothprice}}</span> (<span>RMB{{Item.Rmbboothprice}}</span>)</p>

    </div>
    <div class="col-md-6 pull-right">

      <p>Total CBM on this order: <span>{{Item.totalcbm}}</span></p>
      <p>Total weight of order: <span>{{Item.totalweight}}</span></p>

    </div>
<div class="col-md-12">
    <p class="pull-right">Container full:<span>{{Item.volumepercentage}}%</span></p>
  <div class="m">
      <div class="progress m-t-xs full progress-striped">

          <div style="width: {{Item.volumepercentage}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class=" progress-bar progress-bar-warning">
              <span>{{Item.volumepercentage}}%</span>
          </div>
      </div>
  </div>
</div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        <button class="btn btn-sm btn-primary m-t-n-xs pull-right" style="margin-bottom: 20px;" data-toggle="modal" data-target="#MyModel1"><strong>New Booth</strong></button>
        <button class="btn btn-sm btn-success m-t-n-xs pull-right padding endorder" style="margin-bottom: 20px;" type="submit"><strong>End/New Order</strong></button>
        <button class="btn btn-primary btn-sm m-t-n-xs  padding pull-right pauseorder"  style="margin-bottom: 20px;"><strong>Pause Order</strong></button>

      </div>
    </div>
<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <ul class="nav nav-tabs">

                <li class="active"><a data-toggle="tab" href="#tab-2">Item description</a></li>
                <li class=""><a data-toggle="tab" href="#tab-3">packing </a></li>
                <li class=""><a data-toggle="tab" href="#tab-4">Cost and order quantity</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-2" class="tab-pane active">
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="row">
                              <!-- <input type="text" id="oriderid" name="orderid" ng-model="Item.orderid">
                              <input type="text" id="boothid" name="boothid" ng-model="Item.boothid">
                              <input type="text" id="containerid" name="conatinerid" ng-model="Item.containerid"> -->
                                  <div class="form-group col-lg-6">
                                      <label>Item Number</label>
                                      <input type="text" value="" id="itemno" class="form-control" ng-model="Item.itemno" name="itemno" placeholder="item number">
                                  </div>
                                  <div class="form-group col-lg-6">
                                    <div class="form-group col-lg-4">
                                       <label>Upload image</label>
                                      <!-- <input type="file" value="" id="fileInput" class="file" name="files[]" ng-model="Item.image" placeholder="item image"> -->
                                  </div>
                                  <div class="form-group col-lg-6 ">
                                  <a href="#a" data-toggle="modal" data-target="#MyModel"><img ng-src="{{myImage}}" onerror="this.src='../../../MMC/assets/images/upload.png'" id="imagefeild"  height="100" width="100"></a>
                                  </div>
                                </div>

                                  <div class="form-group col-lg-6">
                                      <label>Item Description</label>
                                      <textarea type="text" rows="5" id="description" ng-model="Item.description" name="description" class="form-control" name="" placeholder="item description">
                                      </textarea>
                                  </div>
                                  <div class="form-group col-lg-6">
                                          <label>Item Description(seconddary language)</label>
                                          <textarea type="text" rows="5" id="altdescription" ng-model="Item.altdescription" class="form-control" name="altdescription" placeholder="item description">
                                          </textarea>
                                  </div>

                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="row">


                                  <div class="form-group col-lg-6">
                                      <label>Gross weight of carton</label>
                                      <input type="text" value="" id="cartonGrossWeight" class="form-control" ng-model="Item.cartonGrossWeight" name="grossweight" placeholder="gross weight of carton">
                                  </div>

                                  <div class="form-group col-lg-6">
                                      <label>Length</label>
                                      <input type="text" value="" id="Length" class="form-control" ng-change = "Item.calculatecartonsize()" name="length" ng-model="Item.Length" placeholder="lenght of carton">

                                  </div>

                                  <div class="form-group col-lg-6">
                                          <label>Width</label>
                                        <input type="text" value="" id="Width" class="form-control" ng-change = "Item.calculatecartonsize()" name="width" ng-model="Item.Width" placeholder="width of the carton">
                                  </div>
                                  <div class="form-group col-lg-6">
                                          <label>Height</label>
                                        <input type="text" value="" id="Height" class="form-control" ng-change = "Item.calculatecartonsize()" name="height" ng-model="Item.Height" placeholder="height of the carton">
                                  </div>
                                  <div class="form-group col-lg-6 pull-right">
                                          <label>CBM(Carton Volume)</label>
                                        <input type="text" value="" id="cbm" class="form-control" name="cbm"  ng-model="Item.cbm" placeholder="CBM of carton" readonly>
                                  </div>

                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <div id="tab-4" class="tab-pane">
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="row">

                                  <div class="form-group col-lg-6">
                                      <label>Price in US$ *</label>
                                      <input type="text" value="" id="priceinus" submit-required="true" ng-model="Item.priceinus" ng-change = "Item.Getuspriceconversion()" class="form-control" name="priceinus" placeholder="prince in us">
                                  </div>

                                  <div class="form-group col-lg-6">
                                      <label>Units per carton *</label>
                                      <input type="text" value="" id="unitpercarton" ng-model="Item.unitpercarton"   class="form-control" name="unitpercarton" placeholder="unit per carton">
                                  </div>
                                  <div class="form-group col-lg-6">
                                      <label>Price in RMB </label>
                                      <input type="text" value="" id="ExchangeRate" ng-model = "Item.ExchangeRate" ng-change = "Item.Getrmbpriceconversion()"  class="form-control" name="priceinzar" placeholder="price in RMB">

                                  </div>
                                  <div class="form-group col-lg-6">
                                          <label>Units qty *</label>
                                        <input type="text" value="" id="unitqty" ng-change = "Item.calculatecartonqty()" ng-model="Item.unitqty" class="form-control" name="unitqty" placeholder="unit quantity">
                                  </div>
                                  <div class="form-group col-lg-6">
                                          <label>Price in ZAR</label>
                                        <input type="text" value="" id="ExchangeRate3dCurrency" ng-model = "Item.ExchangeRate3dCurrency" class="form-control" name="priceinzar" placeholder="Third currency p" readonly>
                                  </div>
                                  <div class="form-group col-lg-6">
                                          <label>Cartons qty * </label>
                                          <input type="text" value="" id="cartonqty" ng-model = "Item.cartonqty" class="form-control" name="cartonqty" placeholder="carton quantity" readonly>
                                  </div>

                                  <div class="form-group col-lg-6 pull-right">

                                        <button class="btn btn-sm btn-primary pull-right" style="margin-bottom: 20px;" type="button" ng-click="Item.saveitem()" ><strong>Save</strong></button>
                                        <!-- ng-click="Item.saveitem()" -->

                                  </div>
                                </div>

                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- </form> -->

<!-- popup  -->
<div class="modal fade" id="MyModel2" tabindex="-1" role="dialog" ng-controller="OrderController" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Container Full</h4>

            </div>
            <div class="modal-body">
              <div class = "row">
                <div class="form-group col-lg-12">
                      <div class="form-group col-lg-12 text-center">

                          <h4> ALLOCATE NEW CONTAINER</h4>
                          <select class="js-source-states" style="width: 100%" id="newcontainer" name="newcontainer">
                                  <option value="1">20' FCL (32CBM)</option>
                                  <option value="2">40' FCL (66CBM)</option>
                                 <option value="3">40'HQ FCL (75CBM)</option>
                              </optgroup>
                          </select>
                          </br>
                          </br>
                          <button type="button" class="btn btn-primary" ng-click="Item.newContianer()">Start filling new container</button>
                      </div>
                      <div class="form-group col-lg-12 text-center">
                          <h4>UPGRADE YOUR CONTAINER</h4>
                          <select class="js-source-states" style="width: 100%" id="upgradecontainer" name="upgradecontainer">
                                  <option value="1">20' FCL (32CBM)</option>
                                  <option value="2">40' FCL (66CBM)</option>
                                 <option value="3">40'HQ FCL (75CBM)</option>
                              </optgroup>
                          </select>
                        </br>
                        </br>
                          <button type="button" class="btn btn-primary" ng-click="Item.upgradeContainer()">Upgrade current ontainer</button>
                      </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<!-- popup close -->



</div>
<div id="right-sidebar" class="animated fadeInRight">

    <div class="p-m">
        <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md"><i class="pe pe-7s-close"></i>
        </button>
        <div>
            <span class="font-bold no-margins"> Analytics </span>
            <br>
            <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
        </div>
        <div class="row m-t-sm m-b-sm">
            <div class="col-lg-6">
                <h3 class="no-margins font-extra-bold text-success">300,102</h3>

                <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
            </div>
            <div class="col-lg-6">
                <h3 class="no-margins font-extra-bold text-success">280,200</h3>

                <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
            </div>
        </div>
        <div class="progress m-t-xs full progress-small">
            <div style="width: 25%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar"
                 class=" progress-bar progress-bar-success">
                <span class="sr-only">35% Complete (success)</span>
            </div>
        </div>
    </div>
    <div class="p-m bg-light border-bottom border-top">
        <span class="font-bold no-margins"> Social talks </span>
        <br>
        <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
        <div class="m-t-md">
            <div class="social-talk">
                <div class="media social-profile clearfix">
                    <a class="pull-left">
                        <img src="images/a1.jpg" alt="profile-picture">
                    </a>

                    <div class="media-body">
                        <span class="font-bold">John Novak</span>
                        <small class="text-muted">21.03.2015</small>
                        <div class="social-content small">
                            Injected humour, or randomised words which don't look even slightly believable.
                        </div>
                    </div>
                </div>
            </div>
            <div class="social-talk">
                <div class="media social-profile clearfix">
                    <a class="pull-left">
                        <img src="images/a3.jpg" alt="profile-picture">
                    </a>

                    <div class="media-body">
                        <span class="font-bold">Mark Smith</span>
                        <small class="text-muted">14.04.2015</small>
                        <div class="social-content">
                            Many desktop publishing packages and web page editors.
                        </div>
                    </div>
                </div>
            </div>
            <div class="social-talk">
                <div class="media social-profile clearfix">
                    <a class="pull-left">
                        <img src="images/a4.jpg" alt="profile-picture">
                    </a>

                    <div class="media-body">
                        <span class="font-bold">Marica Morgan</span>
                        <small class="text-muted">21.03.2015</small>

                        <div class="social-content">
                            There are many variations of passages of Lorem Ipsum available, but the majority have
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-m">
        <span class="font-bold no-margins"> Sales in last week </span>
        <div class="m-t-xs">
            <div class="row">
                <div class="col-xs-6">
                    <small>Today</small>
                    <h4 class="m-t-xs">$170,20 <i class="fa fa-level-up text-success"></i></h4>
                </div>
                <div class="col-xs-6">
                    <small>Last week</small>
                    <h4 class="m-t-xs">$580,90 <i class="fa fa-level-up text-success"></i></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <small>Today</small>
                    <h4 class="m-t-xs">$620,20 <i class="fa fa-level-up text-success"></i></h4>
                </div>
                <div class="col-xs-6">
                    <small>Last week</small>
                    <h4 class="m-t-xs">$140,70 <i class="fa fa-level-up text-success"></i></h4>
                </div>
            </div>
        </div>
        <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.
            Many desktop publishing packages and web page editors.
        </small>
    </div>

</div>
</div>
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="../assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="../assets/vendor/select2-3.5.2/select2.min.js"></script>
<script src="../assets/vendor/iCheck/icheck.min.js"></script>
<script src="../assets/js/intlTelInput.js"></script>

<script>
$( document ).ready(function() {

  var image = $('#imagefeild').val();

if(image == '')
  {
  //  image.src = "../assets/images/item-defult.jpg";
  //  $('#myImage').attr('src','../assets/images/item-defult.jpg');
  $('#imagefeild').attr('src', '../assets/images/item-defult.jpg');
  }
});

$(".js-source-states").select2();

</script>

<?php $this->load->view('layouts/footer'); ?>

<div class="modal fade" id="MyModel" tabindex="-1" role="dialog" ng-controller="OrderController" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Image Upload</h4>

            </div>
            <div class="modal-body">
              <div id="image-holder"><img id="My_Img" ng-src="{{myImage}}" onerror="this.src='../../../MMC/assets/images/upload.png'" class="img-circle m-b" height="100" width="100"/></div>
              <input type="file" accept="image/*" id="fileInput" class="file" ng-model="Item.Image" name="files[]" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"  ng-click="Item.ImageUpload()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="MyModel1" tabindex="-1" role="dialog" ng-controller="OrderController" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">New Booth</h4>

            </div>
            <div class="modal-body">
              <div class = "row">
                <div class="form-group col-lg-12">
                      <div class="form-group col-lg-6">
                          <label>Booth company name</label>
                          <input type="text" value="" id="newboothname" class="form-control" name="newboothname" placeholder="Company Name">
                      </div>
                      <div class="form-group col-lg-6">
                          <label>Booth No</label>
                          <input type="text" value="" id="newboothno" class="form-control" name="newboothno" placeholder="Booth number">
                      </div>
                </div>
              </div>
                <div class = "row">
                    <div class="form-group col-lg-12">
                        <div class="form-group col-lg-6">
                            <label>Telephone</label>
                            <input type="text" value="" id="newtelephone" class="form-control" name="newtelephone" placeholder="Telephone">
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Business scope</label>
                            <input type="text" value="" id="newbusinessscope" class="form-control" name="newbusinessscope" placeholder="Busincess scope">
                        </div>
                  </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="Item.Addbooth()">Save Booth</button>
            </div>
        </div>
    </div>
</div>
