<!--########### spread sheet ####### -->

<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
//echo $users;

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=User_list.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
 <table border='1'>
  <tr style="font-weight: 900;">
    <td>Booth Number</td>
    <!-- <td>User Phone Number</td> -->
    <td>Item Number</td>
    <td>Description</td>
    <td>Container</td>
    <td>Unit Quantity</td>
    <td>Carton Quantity</td>
    <td>USD</td>
    <td>RMB</td>
    <td>Total USD</td>
    <td>Total RMB</td>   
  </tr>
  
  <?php
foreach ($email as $key => $value) {
	?>
    <tr>
    <td><?php echo $value['BoothNumber'];
?></td>
    <td><?php echo $value['ItemNo'];
?></td>
    <td><?php echo $value['DescriptionFrom'];
?></td>
    <td><?php echo $value['ContainerType'];
?></td>
    <td><?php echo $value['UnitQuantity'];
?></td>
    <td><?php echo $value['CartonQuantity'];
?></td>
    <td><?php echo $value['TotalPriceInUS'];
?></td>
    <td><?php echo $value['TotalPriceInRMB'];
?></td>
    <td><?php echo $value['PriceInUs'];
?></td>
    <td><?php echo $value['PriceInRmb'];
?></td> 
    </tr>
  <?php
}
?>

 
</table> 
