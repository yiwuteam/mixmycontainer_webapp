<?php

   class Pdf
   {
	public function __construct()
	{
		$this->ci =&get_instance();
		$this->ci->load->helper('pdf_helper');
		tcpdf();
		
	}
	public function createpdf($message,$OrderNumber)
    {

	// create new PDF document
	$pdf = new TCPDF("A4", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('');
	$pdf->SetTitle('');
	$pdf->SetSubject('');
	$pdf->SetKeywords('');
	
	// set default header data
	$pdf->SetHeaderData("", "75px", "PURCHASE ORDER", "MixMyContainer");
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	// set margins
	$pdf->SetMargins("10px", "10px", "10px");
	$pdf->SetHeaderMargin(5);
	$pdf->SetFooterMargin(5);
	
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->SetFont('kozminproregular', '', 8);
	// add a page
	$pdf->AddPage();
	
			
   	$pdf->SetXY(10, 10);
    try
    {	 
    	$date = date("dmY");
    	$ordernumber = $OrderNumber;
    	$file_name=$date.','.$ordernumber;

    	//$file_name="report.pdf";
         $fullpath=__DIR__."/S3/$file_name.pdf";
         $pdf->writeHTML($message, 0, false, true, false, '');
         $pdf->Output($fullpath,"F"); 
         //echo $fullpath;
         
       //$fileatt= $pdf->Output($file_name,"E"); 

       return $fullpath;
    }
    catch(HTML2PDF_exception $e)
	{
        //echo $e;
        exit;
    }
 }

}