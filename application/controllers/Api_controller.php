<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

require APPPATH . '/libraries/REST_Controller.php';

class Api_controller extends REST_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('Api_model');
		$this->load->model('Order_model');
		$this->load->model('ResetPasswordEmail_model');
		$this->load->model('Email_model');
		$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['users_delete']['limit'] = 50; // 50 requests per hour per usekey
		//$this->load->library('image_lib');
	}

	/*
		  * Get common details like company deatils, Container Details
		  * Trade deatils
		  * @val_orderemail string -user email id
		  * @isCompanyset  int   - Check if user compnay is set or not
	*/
	function dashboard_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		$isCompanyset = 0;
		$goldagentdet = array();
		if (!$this->get('token')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Token missing'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid token'], REST_Controller::HTTP_NOT_FOUND);
			}

			$result = $this->Api_model->companydetails($val_orderemail);
			$containerDetails = $this->Api_model->getallcontainer();
			if ($result) {
				if ($result->GrossweightCarton != 0 && $result->Width != 0 && $result->Height != 0 && $result->Length != 0) {
					$isCompanyset = 1;
				} else {
					$deafultCarton = $this->Api_model->deafultcartondeatils();
					$result->GrossweightCarton = $deafultCarton->GrossWeight;
					$result->Width = $deafultCarton->Width;
					$result->Height = $deafultCarton->Height;
					$result->Length = $deafultCarton->Length;
					if ($result->ExchangeRate == 0) {
						$result->ExchangeRate = $deafultCarton->ExchangeRate;
					}
				}

				if ($result->ExchangeRate == 0) {
					$deafultCarton = $this->Api_model->deafultcartondeatils();
					$result->ExchangeRate = $deafultCarton->ExchangeRate;
				}

				if ($result->TransilateFrom == "" || $result->TransilateTo == "") {
					$result->TransilateFrom = "en";
					$result->TransilateTo = "zh";
				}

				$val_compnayid = $result->CompanyID;
			}

			$tradeDetails = $this->Api_model->getalltradedeatils($val_compnayid);

			$goldagent = $this->Api_model->getallgoldagent();
			foreach ($goldagent as $key) {
				$goldagentrep = $this->Api_model->getgoldagentrepresentativebyagentid($key->Id);
				$key->contact = $goldagentrep;
			}

			$allBooth = $this->Api_model->getAllBooth();
			$this->set_response(['message' => 'success', 'resultCode' => 1, 'isCompanyset' => $isCompanyset, 'companyDetails' => $result, 'containerDetails' => $containerDetails, 'tradeDeatils' => $tradeDetails, 'goldAgentDeatils' => $goldagent, 'boothDeatils' => $allBooth], REST_Controller::HTTP_OK);
		}
	}

	/* ################## LOGIN FUNCTIONS ########################*/
	/*user login check*/
	function login_get() {
		$email = "";
		$tokenString = 'ak8QW83oPg1nOd1E4c2Xa4g'; /*random charactors for create token*/
		$token = str_shuffle($tokenString); /*generate token*/
		if (!$this->get('username') || !$this->get('password')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'log in Failed'], REST_Controller::HTTP_OK);
		} else {
			$username = $this->get('username');
			$password = md5($this->get('password'));
			$usercheck = $this->Api_model->checkuserexists($username);
			if($usercheck){
			$result = $this->Api_model->login($username, $password);
			if ($result) {
				foreach ($result as $key) {
					$email = $key->Email;
				}

				$usertoken = array(
					'Token' => $token,
					'OrderEmail' => $email,
				);
				$this->Api_model->saveusertoken($usertoken);
				$accounts = $this->Api_model->getaccountdetails($email);
				foreach ($accounts as $key) {
					$key->Image = "https://mixmycontainer.com/QA/assets/images/profilepic/" . $key->Image;
				}

				$message = ['resultCode' => 1, 'token' => $token, 'accountDeatils' => $accounts, 'message' => 'login success'];
				return $this->set_response($message, REST_Controller::HTTP_OK);
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Login Failed'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
		else{
			return $this->set_response(['resultCode' => 0, 'message' => 'Please register first, to login'], REST_Controller::HTTP_NOT_FOUND);
		}
		}
	}

	/*end*/
	/*
		  *change password with token
	*/
	function changepassword_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$email = "";
		$currentpasswordget = "";
		if ((!$this->post('token')) || (!$this->post('currentPassword')) || (!$this->post('newPassword'))) {
			/*error responce*/
			return $this->set_response(['resultCode' => 0, 'messsage' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'messsage' => 'Invalid User'], REST_Controller::HTTP_OK);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$formcurrentpassword = $this->post('currentPassword');
				$formcurrentpassword = md5($formcurrentpassword);
				$emailid = $this->Api_model->getcompanyemailid($val_compnayid); // fetch the email id from the company table
				foreach ($emailid as $value) {
					$email = $value->OrderEmail;
				}

				/*check current password is valid*/
				$checkpassword = $this->Api_model->getcurrentpassword($email);
				foreach ($checkpassword as $key) {
					$currentpasswordget = $key->Password;
				}

				if ($currentpasswordget == $formcurrentpassword) {
					$newpassword = md5($this->post('newPassword'));
					$val_changepassword = array(
						'Password' => $newpassword,
					);
					$result = $this->Api_model->changepassword($val_changepassword, $email);
					if ($result) {
						/*success responce*/
						$message = ['resultCode' => 1, 'message' => 'Password changed successfully'];
						return $this->set_response($message, REST_Controller::HTTP_OK);
					} else {
						/*error reponce*/
						return $this->set_response(['resultCode' => 0, 'message' => 'Password changed failed'], REST_Controller::HTTP_OK);
					}
				} else {
					return $this->set_response(['resultCode' => 0, 'messege' => 'Incorrect currentpassword'], REST_Controller::HTTP_OK);
				}
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Not valid token'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*
		  * Chnage password without token
	*/
	function changepasswordwithmail_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$email = "";
		if ((!$this->post('email')) || (!$this->post('newPassword'))) {
			/*error responce*/
			return $this->set_response(['resultCode' => 0, 'messsage' => 'Fileds Required'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_orderemail = $this->post('email');
			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			if ($companyid) {
				$val_compnayid = $companyid->CompanyID;
				$newpassword = md5($this->post('newPassword'));
				$val_changepassword = array(
					'Password' => $newpassword,
				);
				$result = $this->Api_model->changepassword($val_changepassword, $val_orderemail);
				if ($result) {
					return $this->set_response(['resultCode' => 1, 'message' => 'Password changed successfully'], REST_Controller::HTTP_OK);
				} else {
					return $this->set_response(['resultCode' => 0, 'message' => 'Password changed failed'], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid user'], REST_Controller::HTTP_OK);
			}
		}
	}

	/* ################## ORDER FUNCTONS ########################*/
	/*Api get order details*/
	function userorderdetails_get() {
		$val_orderemail = "";
		$val_companyid = "";
		if ((!$this->get('token'))) {
			/*error responce*/
			return $this->set_response(['resultCode' => 0, 'message' => 'Value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid User'], REST_Controller::HTTP_NOT_FOUND);
			}

			$companyid = $this->Api_model->companydetails($val_orderemail);
			if ($companyid) {
				$val_companyid = $companyid->CompanyID;
				$result = $this->Api_model->orderdetails($val_companyid);
				if ($result) {
					$message = ['message' => 'success', 'resultCode' => 1, 'data' => $result];
					return $this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					return $this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*end*/
	/*Continue Order */
	function continueorder_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		if (!$this->get('token')) {
			/*error responce*/
			return $this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->get('token');
			$val_order = $this->get('orderid');
			$orderemail = $this->Api_model->selectordermail($val_token);

			// echo $orderemail;exit;

			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid user'], REST_Controller::HTTP_NOT_FOUND);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$lastbooth = $this->Api_model->selectlastboothdetails($val_order);
				$orderdteails = $this->Api_model->selectorderdetails($val_order);
				$orderno = $orderdteails->OrderNumber;
				$orderid = $orderdteails->OrderId;
				$totalorderCBM = $orderdteails->TotalOrderCBM;
				$containerdetails = $this->Api_model->selectlastcontainer($val_order);
				$containerid = $containerdetails->ContainerId;
				$containertype = $containerdetails->ContainerType;
				if ($orderdteails) {
					return $this->set_response(['message' => 'success', 'resultCode' => 1, 'LastBooth' => $lastbooth, 'OrderId' => $orderid, 'OrderNo' => $orderno, 'TotalOrderCBM' => $totalorderCBM, 'ContainerId' => $containerid, 'ContainerType' => $containertype], REST_Controller::HTTP_OK);
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data', 'data' => '[]'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*end order */
	/* ################## COMPANY DETAILS FUNCTIONS ########################*/
	/*
		  *Api get company details
		  * @val_orderemail - user mail id
		  * @isCompanyset   - Check if compnay is set or not
	*/
	function companydetails_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		$isCompanyset = 0;
		if (!$this->get('token')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Token missing'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if (!$orderemail) {
				return $this->set_response(['resultCode' => 0, 'message' => 'No user found'], REST_Controller::HTTP_NOT_FOUND);
			}

			$result = $this->Api_model->companydetails($orderemail->OrderEmail);
			if ($result) {
				if ($result->ExchangeRate != 0 && $result->OrderEmail != "" && $result->GrossweightCarton != 0 && $result->Width != 0 && $result->Height != 0 && $result->Length != 0) {
					$isCompanyset = 1;
				}
			}

			$this->set_response(['message' => 'success', 'resultCode' => 1, 'isCompanyset' => $isCompanyset, 'companyDetails' => $result], REST_Controller::HTTP_OK);
		}
	}

	/*end*/
	/*
		  * Insert Trade and Hall
		  *
	*/
	function insertnewtradeandhall_post() {
		$val_orderemail = "";
		$val_companyid = "";
		$val_name = "";
		$val_parent = 0;
		if (!$this->post('token')) {
			$this->set_response(['resultCode' => 0, 'message' => 'Token missing'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if (!$orderemail) {
				return $this->set_response(['resultCode' => 0, 'message' => 'user not found'], REST_Controller::HTTP_NOT_FOUND);
			}

			$result = $this->Api_model->companydetails($orderemail->OrderEmail);
			if ($result) {
				$val_companyid = $result->CompanyID;
				$val_name = $this->post('name');
				$val_parent = $this->post('parent');
				$data = array(
					'Name' => $val_name,
					'Parent' => $val_parent,
					'CompanyId' => $val_companyid,
				);
				$lastInsertId = $this->Api_model->inserttradeorhall($data);
				return $this->set_response(['resultCode' => 1, 'Name' => $val_name, 'Id' => $lastInsertId, 'CompanyId' => $val_companyid, 'Parent' => $val_parent, 'message' => "Data sucessfully inserted"], REST_Controller::HTTP_OK);
			}
		}
	}

	/* Api to validate company details */
	function checkvalidcompany_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		if (!$this->get('token')) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			$result = $this->Api_model->companydetails($orderemail->OrderEmail);
			if ($result) {
				if ($result->ExchangeRate != 0 && $result->OrderEmail != "" && $result->GrossweightCarton != 0 && $result->Width != 0 && $result->Height != 0 && $result->Length != 0) {
					$this->set_response(['message' => 'success', 'resultCode' => 1], REST_Controller::HTTP_OK);
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'Please complete company setup'], REST_Controller::HTTP_OK);
				}
			} else {

				// error reponce

				$this->set_response(['resultCode' => 0, 'message' => 'Please complete company setup'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*
		  * get profile deatils
		  * @val_orderemail
	*/
	function getaccount_get() {
		if (!$this->get('token')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Token required'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$accounts = $this->Api_model->getaccountdetails($orderemail->OrderEmail);
				return $this->set_response(['resultCode' => 1, 'accountDeatils' => $accounts], REST_Controller::HTTP_OK);
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid User'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*
		  * profile add ,update
		  * type 1-add ,
	*/
	function accountoperation_post() {
		$val_plaintext = 0;
		$val_language = "";
		$val_localset = "";
		$val_orderemail = "";
		if (!$this->post('token')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Token required'], REST_Controller::HTTP_NOT_FOUND);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			$val_plaintext = $this->post('plaintext');
			$val_language = $this->post('language');
			$val_localset = $this->post('localset');
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
				if ((!$val_language) || (!$val_localset)) {
					return $this->set_response(['resultCode' => 0, 'message' => 'Fields required'], REST_Controller::HTTP_NOT_FOUND);
				} else {
					$accounts = $this->Api_model->getaccountdetails($val_orderemail);
					$data_accouts = array(
						'EmailSettings' => $val_plaintext,
						'Language' => $val_language,
						'Locale' => $val_localset,
						'EmailId' => $val_orderemail,
					);
					if ($accounts) {
						$this->Api_model->storeorupdateaccount($data_accouts, 2, $val_orderemail);
					} else {
						$this->Api_model->storeorupdateaccount($data_accouts, 1, $val_orderemail);
					}

					return $this->set_response(['resultCode' => 1, 'message' => 'Account updated successfully'], REST_Controller::HTTP_OK);
				}
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid User'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*Api update company address */
	function updatecompanyaddress_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		if ((!$this->post('token')) || (!$this->post('companyname'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$val_companyaddressdata = array(
					'CompanyName' => $this->post('companyname'),
					'Country' => $this->post('country'),
					'AddressType' => $this->post('addressType'),
					'Address' => $this->post('address'),
					'ZipCode' => $this->post('zipCode'),
					'Phone' => $this->post('phone'),
					'Town/City' => $this->post('towncity'),
					'Mobile' => $this->post('mobile'),
				);
				/*calling model sql query*/
				$result = $this->Api_model->updatecompanyaddress($val_companyaddressdata, $val_compnayid);
				if ($result) {
					/*success responce*/
					$message = ['resultCode' => 1, 'message' => 'successfully updated'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {

					// error reponce

					$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'Invalid token'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*update company contact*/
	function updatecompanycontact_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$val_orderemail = "";
		if (($this->post('token') == "") || ($this->post('orderemail') == "")) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Token not valid'], REST_Controller::HTTP_OK);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				if ($this->post('agentemail') == "0") {
					$agentemail = "";
				} else {
					$agentemail = $this->post('agentemail');
				}

				if ($this->post('agentname') == "0") {
					$agentname = "";
				} else {
					$agentname = $this->post('agentname');
				}

				$val_companycontactdata = array(
					'Fax' => $this->post('fax'),
					'Skype' => $this->post('skype'),
					'MsnID' => $this->post('msnid'),
					'QqID' => $this->post('qqid'),
					'MailOrder' => $this->post('orderemail'),
					'AgentEmail' => $agentemail,
					'AgentId' => $this->post('agentid'),
					'AgentName' => $agentname,
				);
				$result_companycontact = $this->Api_model->updatecompanycontact($val_companycontactdata, $val_compnayid);
				if ($result_companycontact) {
					$message = ['resultCode' => 1, 'message' => 'Successfully Updated'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'Update Failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*update company exchange rate set up*/
	function updatecompanyexchangerate_post() {
		$val_orderemail = "";
		$val_compnayid = "";

		// || (!$this->post('thirdCurrency')) || (!$this->post('exchangeRate3d'))

		/*check request values is null*/
		if ((!$this->post('token'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$val_companyexchangerate = array(
					'ExchangeRate' => $this->post('exchangeRate'),
					'Active3dCurrency' => $this->post('activate3dCurrency'),
					'ThirdCurrency' => $this->post('thirdCurrency'),
					'ExchangeRate3dCurrency' => $this->post('exchangeRate3d'),
				);
				/*calling model sql query*/
				$result = $this->Api_model->updatecompanyexchangerate($val_companyexchangerate, $val_compnayid);
				if ($result) {
					/*success responce*/
					$message = ['resultCode' => 1, 'message' => 'successfully updated'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'INo data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*update company average carton setup*/
	function updatecompanyaveragecarton_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		/*check request values is null*/
		if ((!$this->post('grossWeight')) || (!$this->post('width')) || (!$this->post('height')) || (!$this->post('length')) || (!$this->post('token'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$val_companyaveragecartons = array(
					'GrossweightCarton' => $this->post('grossWeight'),
					'Width' => $this->post('width'),
					'Height' => $this->post('height'),
					'Length' => $this->post('length'),
				);
				/*calling model sql query*/
				$result = $this->Api_model->updatecompanyaveragecarton($val_companyaveragecartons, $val_compnayid);
				if ($result) {
					/*success responce*/
					$message = ['resultCode' => 1, 'message' => 'successfully updated'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*update company transilation setup*/
	function updatecompanytransilationsetup_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		/*check request values is null*/
		if ((!$this->post('token'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$val_companytransilationsetup = array(
					'TransilateFrom' => $this->post('translateFrom'),
					'TransilateTo' => $this->post('translateTo'),
				);
				/*calling model sql query*/
				$result = $this->Api_model->updatecompanytrasilationsetup($val_companytransilationsetup, $val_compnayid);
				if ($result) {
					/*success responce*/
					$message = ['resultCode' => 1, 'message' => 'successfully updated'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/* ################## WORLD COUNTRY NAMES FUNCTIONS ########################*/
	/*world country names*/
	function worldcountrynames_get() {
		/*calling model sql query*/
		$result = $this->Api_model->getcountrycodes();
		if ($result) {
			/*success responce*/
			$message = ['resultCode' => 1, 'data' => $result];
			$this->set_response($message, REST_Controller::HTTP_OK);
		} else {
			/*error reponce*/
			$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
		}
	}

	/*end*/
	/* ################## WORLD CURRENCY FUNCTIONS ########################*/
	/*world currency names*/
	function worldcurrency_get() {
		/*calling model sql query*/
		$result = $this->Api_model->getcurrencynames();
		if ($result) {
			/*success responce*/
			$message = ['resultCode' => 1, 'data' => $result];
			$this->set_response($message, REST_Controller::HTTP_OK);
		} else {
			/*error reponce*/
			$this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
		}
	}

	/*end*/
	/*
		  *Get Booth deatils by id
	*/
	function getbooth_get() {
		$val_boothid = "";
		if (!$this->get('boothno')) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Booth no required'], REST_Controller::HTTP_OK);
		} else {
			$val_boothid = $this->get('boothno');
			$boothdetails = $this->Api_model->getBoothById($val_boothid);
			if ($boothdetails) {
				return $this->set_response(['resultCode' => 1, 'boothDeatils' => $boothdetails], REST_Controller::HTTP_OK);
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Booth does not exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/* ################## ORDER ITEMS FUNCTIONS ########################*/
	/*new order*/
	function saveneworder_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$val_orderNo = "";
		if ((!$this->post('token')) || (!$this->post('ordernumber')) || (!$this->post('containertype'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			$val_orderNo = $this->post('ordernumber');
			$checkIfOrderExist = $this->Api_model->checkorderno($val_orderNo);
			if ($checkIfOrderExist) {
				$isOrderNo = false;
				$neworderno = "OD" . mt_rand(100000, 9999999);
				while ($isOrderNo) {
					$result = $this->Api_model->checkorderno($neworderno);
					if (!$result) {
						$isOrderNo = true;
					} else {
						$neworderno = "OD" . mt_rand(100000, 9999999);
					}
				}

				return $this->set_response(['resultCode' => 0, 'message' => 'Order no already exist', 'newOrderNo' => $neworderno], REST_Controller::HTTP_OK);
			}

			if ($val_compnayid) {
				$date = date("Y-m-d");
				$orderdetails = array(
					'CompanyId' => $val_compnayid,
					'TotalContainer' => 1,
					'LastUpdate' => $date,
					'OrderNumber' => $this->post('ordernumber'),
					'Trade' => 0,
					'ProductType' => 0,
				);
				/*save order*/
				$result_saveorder = $this->Api_model->saveorder($orderdetails);
				/* Fetch Container details  */
				$val_containertype = $this->post('containertype');
				$getContainerDetails = $this->Api_model->selectcontainercapacity($val_containertype);
				/*check order id */
				if ($result_saveorder != 'null') {
					/*save cotainer type and item data to container table*/
					$ordercontainerdetails = array(
						'OrderId' => $result_saveorder, // the value of $result_oredersave is last entering oder id
						'ContainerType' => $this->post('containertype'),
						'CompanyId' => $val_compnayid,
					);
					$result_saveacontainerdata = $this->Api_model->savecontainertype($ordercontainerdetails);
					if ($result_saveacontainerdata) {
						$this->set_response(['resultCode' => 1, 'message' => 'save order', 'orderId' => $result_saveorder, // the value of $result_oredersave is last entering oder id ,
							'containerId' => $result_saveacontainerdata, 'contianerDetails' => $getContainerDetails], REST_Controller::HTTP_OK);
					} else {
						$this->set_response(['resultCode' => 0, 'message' => 'save container data failed'], REST_Controller::HTTP_OK);
					}
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'save order data failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => ''], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	
	/*get new order number*/
	function getnewordernumber_post() {
		if ((!$this->post('token'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			 $token = $this->session->userdata('userToken');
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $companydetails = $this->Order_model->companydetails($val_orderemail);
                if ($companydetails)
                    {
                    /*check all details enterd */
                    // if ($companydetails->ExchangeRate != 0 && $companydetails->OrderEmail != "" && $companydetails->GrossweightCarton != 0 && $companydetails->Width != 0 && $companydetails->Height != 0 && $companydetails->Length != 0)
                    //     {
                        $isOrderNo = false;
                        $neworderno = "OD" . mt_rand(100000, 9999999);

                        // starts loop

                        while ($isOrderNo)
                            {
                            $result = $this->Order_model->checkorderno($neworderno);
                            if (!$result)
                                {
                                $isOrderNo = true;
                                }
                              else
                                {
                                $neworderno = "OD" . mt_rand(100000, 9999999);
                                }
                            }

                        $this->set_response(['resultCode' => 1, 'message' => 'new order number', 'OrderId' => $neworderno], REST_Controller::HTTP_OK);
                    }
                  else
                    {
                   $this->set_response(['resultCode' => 0, 'message' => 'invalid company'], REST_Controller::HTTP_OK);
                    }
		}

	}
	/*end*/

	/*save booth details*/
	function savebooth_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$boothcount = "";
		if ((!$this->post('token')) || (!$this->post('orderId')) || (!$this->post('boothNumber'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			if ($companyid) {
				$val_compnayid = $companyid->CompanyID;
				$date = date("Y-m-d");
				if (($this->post('filename') != "0") && ($this->post('filename'))) {
					$filename = $this->post('filename');
				} else {
					$filename = "default.jpg";
				}

				$boothdetails = array(
					'OrderId' => $this->post('orderId'),
					'CompanyId' => $val_compnayid,
					'BoothNumber' => $this->post('boothNumber'),
					'BoothName' => $this->post('boothName'),
					'Phone' => $this->post('telephone'),
					'BusinessScope' => $this->post('businesScope'),
					'HallId' => $this->post('hallId'),
					'TradeId' => $this->post('tradeId'),
					'BusinessCard' => $filename,
					'ValuePurchase' => 0,
					'ValuePurchaseRMB' => 0,
					'BoothStatus' => 0,
				);

				// echo json_encode($boothdetails);
				// exit;

				$val_orderid = $this->post('orderId');
				$val_boothno = $this->post('boothNumber');
				$savedbooth = $this->Api_model->checkboothsaved($val_orderid, $val_boothno);
				$lastbooth = $this->Api_model->selectlastboothdetails($val_orderid);
				$lastboothid = 0;
				if ($lastbooth) {
					$lastboothid = $lastbooth->BoothId;
				}

				$status = 1;
				$this->Api_model->changeboothstatus($lastboothid, $status);
				if ($savedbooth) {
					$val_boothid = $savedbooth->BoothId;
					$status = 0;
					$updateboothdetails = array(
						'BoothName' => $this->post('boothName'),
						'Phone' => $this->post('telephone'),
						'BusinessScope' => $this->post('businesScope'),
						'BusinessCard' => $filename,
					);
					$this->Api_model->updateboothvalue($val_boothid, $updateboothdetails);
					$this->Api_model->changeboothstatus($val_boothid, $status);
					$this->set_response(['resultCode' => 1, 'message' => 'save booth details', 'boothId' => $val_boothid], REST_Controller::HTTP_OK);
				} else {
					$result_savebooth = $this->Api_model->savebooth($boothdetails);
					if ($result_savebooth != 'null') {
						$boothdetails = $this->Api_model->getboothinfo($val_boothno);
						if (!$boothdetails) {
							$newboothinfo = array(
								'BoothNumber' => $this->post('boothNumber'),
								'BoothName' => $this->post('boothName'),
								'Phone' => $this->post('telephone'),
								'BusinessScope' => $this->post('businesScope'),
								'BusinessCard' => $filename,
							);
							$this->Api_model->saveboothinfo($newboothinfo);
						}

						$orderid = $this->post('orderId');
						$fetchBoothCount = $this->Api_model->fetchboothcount($orderid);
						foreach ($fetchBoothCount as $key) {
							$boothcount = $key->TotalBooth;
						}

						$val_newboothcount = $boothcount + 1;
						$val_boothcount = array(
							'TotalBooth' => $val_newboothcount,
						);
						$result_updatetotalbooth = $this->Api_model->updateboothno($val_boothcount, $orderid);
						if ($result_updatetotalbooth) {
							$this->set_response(['resultCode' => 1, 'message' => 'save booth details', 'boothId' => $result_savebooth], REST_Controller::HTTP_OK);
						} else {
							$this->set_response(['resultCode' => 0, 'message' => 'save booth failed'], REST_Controller::HTTP_OK);
						}
					} else {
						$this->set_response(['resultCode' => 0, 'message' => 'save order data failed'], REST_Controller::HTTP_OK);
					}
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*save image*/
	function savenewitem_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		/*check request values for its null or not*/
		if ((!$this->post('token')) || (!$this->post('orderId')) || (!$this->post('boothId')) || (!$this->post('ContainerId')) || (!$this->post('itemNumber')) || (!$this->post('fileName')) || (!$this->post('DescriptionFrom'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$imagefilepath = $this->post('fileName');
				if (($this->post('fileName') != "0") && ($this->post('fileName'))) {
		          //$imagefilepath = $this->post('fileName');
      		    $imagefilepath = $this->Api_model->saveimage($imagefilepath);
       			 }
       			 else {
				$imagefilepath = "default.jpg";
       			}
				$val_itemdetails = array(
					'ItemNo' => $this->post('itemNumber'),
					'CompanyId' => $val_compnayid,
					'OrderId' => $this->post('orderId'),
					'BoothId' => $this->post('boothId'),
					'ContainerId' => $this->post('ContainerId'),
					'DescriptionFrom ' => $this->post('DescriptionFrom'),
					'DescriptionTo' => $this->post('DescriptionTo'),
					'Picture' => $imagefilepath,
				);
				$result_saveitem = $this->Api_model->saveitemdetails($val_itemdetails);
				if ($result_saveitem) {
					$message = ['resultCode' => 1, 'itemId' => $result_saveitem, 'message' => 'item saved successfully'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'item saved saved failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*save carton details*/
	function savecarton_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$containertype = "";
		$currentVolume = "";
		$currentWeight = "";
		$volumecapacity = "";
		$weightcapacity = "";
		$val_totalOrderWeight = "";
		$val_totalOrderCBM = "";
		/*check request values for its null or not*/
		if ((!$this->post('token')) || (!$this->post('orderid')) || (!$this->post('itemid')) || (!$this->post('unitpercarton')) || (!$this->post('grossweight')) || (!$this->post('length')) || (!$this->post('width')) || (!$this->post('height')) || (!$this->post('unitquantity')) || (!$this->post('cbm')) || (!$this->post('cartonquanity'))) {
			return $this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$orderid = $this->post('orderid');
				$fill = $this->Api_model->getfillprocess($orderid);
				$this->Api_model->removetempconatiner($orderid);
				$this->Api_model->removetempitems($orderid);
				if ($fill) {
					$this->Api_model->removetempprocess($fill->ItemId);
				}

				$length = $this->post('length');
				$width = $this->post('width');
				$Height = $this->post('height');
				$unitquatity = $this->post('unitquantity');
				$unitpercarton = $this->post('unitpercarton');
				$cbm = $this->post('cbm');
				$cartonqty = $this->post('cartonquanity');
				$grossweight = $this->post('grossweight');
				$val_itemid = $this->post('itemid');
				$orderid = $this->post('orderid');
				/*Saving to Temprary Tbl */
				$val_tmpcartondetails = array(
					'ItemId' => $val_itemid,
					'Length' => $length,
					'Width' => $width,
					'Height' => $Height,
					'UnitQTY' => $unitquatity,
					'UnitPerCarton' => $unitpercarton,
					'CBM' => $cbm,
					'CartonQTY' => $cartonqty,
					'GrossWeight' => $grossweight,
					'OrderId' => $orderid,
					'SaveStatus' => 0,
				);
				$tmpid = $this->Api_model->savetmpcarton($val_tmpcartondetails);
				/*calculate volume of the carton*/
				$currentitemvolume = $cartonqty * $cbm;
				$currentitemweight = $cartonqty * $grossweight;
				/*find current volume and weight the current container*/
				$CurrentContainerstatus = $this->Api_model->fetchcontainerspance($orderid);
				foreach ($CurrentContainerstatus as $key) {
					$containertype = $key->ContainerType;
					$currentVolume = $key->FilledVolume;
					$currentWeight = $key->FilledWeight;
				}

				$val_currentVolume = $currentVolume + $currentitemvolume;
				$val_currentWeight = $currentWeight + $currentitemweight;
				/*find the maximum capacity of the selected container*/
				$ContainerCapacity = $this->Api_model->selectcontainercapacity($containertype);
				$volumecapacity = $ContainerCapacity->MaximumVolume;
				$weightcapacity = $ContainerCapacity->MaximumWeight;
				$availablevolume = $volumecapacity - $currentVolume;
				$availableweight = $weightcapacity - $currentWeight;
				/*check the container is filled or not both volume and weight*/
				if ($currentitemvolume > $availablevolume || $currentitemweight > $availableweight) {
					if ($currentitemvolume > $availablevolume && $currentitemweight < $availableweight) {
						$balancevolume = $currentitemvolume - $availablevolume;
						$noofcartonmoved = ($balancevolume / $cbm);
						$noofcartonmoved = ceil($noofcartonmoved);
						$newcontainervolume = $noofcartonmoved * $cbm;
						$newcontainerweight = $noofcartonmoved * $grossweight;
						$usedvolume = $currentVolume + (($cbm * $cartonqty) - $newcontainervolume);
						$usedweight = $currentWeight + (($grossweight * $cartonqty) - $newcontainerweight);
						$val_tmpfillprocess = array(
							'ItemId' => $val_itemid,
							'BalVolume' => $newcontainervolume,
							'BalWeight' => $newcontainerweight,
							'UsedVolume' => $usedvolume,
							'UsedWeight' => $usedweight,
							'BalCartons' => 0,
							'ProcessStatus' => 0,
						);
						$tmpid = $this->Api_model->insertfillprocess($val_tmpfillprocess);
						$lastcontainer = $this->Api_model->selectlastcontainer($orderid);
						if ($lastcontainer) {
							$tempcontainer = array(
								'ContainerType' => $lastcontainer->ContainerType,
								'FilledWeight' => $lastcontainer->FilledWeight,
								'FilledVolume' => $lastcontainer->FilledVolume,
								'Volumepercentage' => $lastcontainer->Volumepercentage,
								'Weightpercentage' => $lastcontainer->Weightpercentage,
								'OrderId' => $lastcontainer->OrderId,
								'CompanyId' => $lastcontainer->CompanyId,
								'OrginalContainerId' => $lastcontainer->ContainerId,
								'Status' => 0,
							);
							$id = $this->Api_model->addtempnewcontainer($tempcontainer);
						}

						return $this->set_response(['resultCode' => 1, 'balancevolume' => "$newcontainervolume", 'balanceweight' => "$newcontainerweight", 'usedvolume' => "$usedvolume", 'usedweight' => "$usedweight", 'totalcontainervolume' => "$currentitemvolume", 'totalcontainerweight' => "$currentitemweight", 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
					} else
					if ($currentitemweight > $availableweight && $currentitemvolume < $availablevolume) {
						$balanceweight = $currentitemweight - $availableweight;
						$noofcartonmoved = ($balanceweight / $grossweight);
						$noofcartonmoved = ceil($noofcartonmoved);
						$newcontainervolume = $noofcartonmoved * $cbm;
						$newcontainerweight = $noofcartonmoved * $grossweight;
						$usedvolume = $currentVolume + (($cbm * $cartonqty) - $newcontainervolume);
						$usedweight = $currentWeight + (($grossweight * $cartonqty) - $newcontainerweight);
						$val_tmpfillprocess = array(
							'ItemId' => $val_itemid,
							'BalVolume' => $newcontainervolume,
							'BalWeight' => $newcontainerweight,
							'UsedVolume' => $usedvolume,
							'UsedWeight' => $usedweight,
							'BalCartons' => 0,
							'ProcessStatus' => 0,
						);
						$tmpid = $this->Api_model->insertfillprocess($val_tmpfillprocess);
						$lastcontainer = $this->Api_model->selectlastcontainer($orderid);
						if ($lastcontainer) {
							$tempcontainer = array(
								'ContainerType' => $lastcontainer->ContainerType,
								'FilledWeight' => $lastcontainer->FilledWeight,
								'FilledVolume' => $lastcontainer->FilledVolume,
								'Volumepercentage' => $lastcontainer->Volumepercentage,
								'Weightpercentage' => $lastcontainer->Weightpercentage,
								'OrderId' => $lastcontainer->OrderId,
								'CompanyId' => $lastcontainer->CompanyId,
								'OrginalContainerId' => $lastcontainer->ContainerId,
								'Status' => 0,
							);
							$id = $this->Api_model->addtempnewcontainer($tempcontainer);
						}

						return $this->set_response(['resultCode' => 1, 'balancevolume' => "$newcontainervolume", 'balanceweight' => "$newcontainerweight", 'usedvolume' => "$usedvolume", 'usedweight' => "$usedweight", 'totalcontainervolume' => "$currentitemvolume", 'totalcontainerweight' => "$currentitemweight", 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
					} else
					if ($currentitemvolume > $availablevolume && $currentitemweight > $availableweight) {
						/**process when total item weight and volume is greater than container weight capacity**/
						/*calculate carton values based on volume*/
						$balancevolume = $currentitemvolume - $availablevolume;
						$noofcartonmovedforvolume = ($balancevolume / $cbm);
						$noofcartonmovedforvolume = ceil($noofcartonmovedforvolume);
						/*calculate carton values based on weight*/
						$balanceweight = $currentitemweight - $availableweight;
						$noofcartonmovedforweight = ($balanceweight / $grossweight);
						$noofcartonmovedforweight = ceil($noofcartonmovedforweight);
						if ($noofcartonmovedforvolume > $noofcartonmovedforweight) {
							$newcontainervolumeforvolume = $noofcartonmovedforvolume * $cbm;
							$newcontainerweightforweight = $noofcartonmovedforvolume * $grossweight;
							$usedvolumeforvolume = $currentVolume + (($cbm * $cartonqty) - $newcontainervolumeforvolume);
							$usedweightforvolume = $currentWeight + (($grossweight * $cartonqty) - $newcontainerweightforweight);
							$newcontinerweightforvolume = $noofcartonmovedforvolume * $grossweight;
							$val_tmpfillprocess = array(
								'ItemId' => $val_itemid,
								'BalVolume' => $newcontainervolumeforvolume,
								'BalWeight' => $newcontainerweightforweight,
								'UsedVolume' => $usedvolumeforvolume,
								'UsedWeight' => $usedweightforvolume,
								'BalCartons' => 0,
								'ProcessStatus' => 0,
							);
							$tmpid = $this->Api_model->insertfillprocess($val_tmpfillprocess);
							$lastcontainer = $this->Api_model->selectlastcontainer($orderid);
							if ($lastcontainer) {
								$tempcontainer = array(
									'ContainerType' => $lastcontainer->ContainerType,
									'FilledWeight' => $lastcontainer->FilledWeight,
									'FilledVolume' => $lastcontainer->FilledVolume,
									'Volumepercentage' => $lastcontainer->Volumepercentage,
									'Weightpercentage' => $lastcontainer->Weightpercentage,
									'OrderId' => $lastcontainer->OrderId,
									'CompanyId' => $lastcontainer->CompanyId,
									'OrginalContainerId' => $lastcontainer->ContainerId,
									'Status' => 0,
								);
								$id = $this->Api_model->addtempnewcontainer($tempcontainer);
							}

							return $this->set_response(['resultCode' => 1, 'balancevolume' => "$newcontainervolumeforvolume", 'balanceweight' => "$newcontinerweightforvolume", 'usedvolume' => "$usedvolumeforvolume", 'usedweight' => "$usedweightforvolume", 'totalcontainervolume' => "$currentitemvolume", 'totalcontainerweight' => "$currentitemweight", 'balancecartons' => "$noofcartonmovedforvolume", 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
						} else
						if ($noofcartonmovedforvolume < $noofcartonmovedforweight) {
							$newcontainervolumeforvolume = $noofcartonmovedforweight * $cbm;
							$newcontainerweightforweight = $noofcartonmovedforweight * $grossweight;
							$usedweightforweight = $currentWeight + (($grossweight * $cartonqty) - $newcontainerweightforweight);
							$usedvolumeforweight = $currentVolume + (($cbm * $cartonqty) - $newcontainervolumeforvolume);
							$val_tmpfillprocess = array(
								'ItemId' => $val_itemid,
								'BalVolume' => $newcontainervolumeforvolume,
								'BalWeight' => $newcontainerweightforweight,
								'UsedVolume' => $usedvolumeforweight,
								'UsedWeight' => $usedvolumeforweight,
								'BalCartons' => 0,
								'ProcessStatus' => 0,
							);
							$tmpid = $this->Api_model->insertfillprocess($val_tmpfillprocess);
							$lastcontainer = $this->Api_model->selectlastcontainer($orderid);
							if ($lastcontainer) {
								$tempcontainer = array(
									'ContainerType' => $lastcontainer->ContainerType,
									'FilledWeight' => $lastcontainer->FilledWeight,
									'FilledVolume' => $lastcontainer->FilledVolume,
									'Volumepercentage' => $lastcontainer->Volumepercentage,
									'Weightpercentage' => $lastcontainer->Weightpercentage,
									'OrderId' => $lastcontainer->OrderId,
									'CompanyId' => $lastcontainer->CompanyId,
									'OrginalContainerId' => $lastcontainer->ContainerId,
									'Status' => 0,
								);
								$id = $this->Api_model->addtempnewcontainer($tempcontainer);
							}

							return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolumeforvolume, 'balanceweight' => $newcontainerweightforweight, 'usedvolume' => $usedvolumeforweight, 'usedweight' => $usedweightforweight, 'totalcontainervolume' => $currentitemvolume, 'totalcontainerweight' => $currentitemweight, 'balancecartons' => $noofcartonmovedforweight, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
						} else {
							$newcontainervolumeforvolume = $noofcartonmovedforvolume * $cbm;
							$newcontainerweightforweight = $noofcartonmovedforvolume * $grossweight;
							$usedvolumeforvolume = $currentVolume + (($cbm * $cartonqty) - $newcontainervolumeforvolume);
							$usedweightforvolume = $currentWeight + (($grossweight * $cartonqty) - $newcontainerweightforweight);
							$newcontinerweightforvolume = $noofcartonmovedforvolume * $grossweight;
							$val_tmpfillprocess = array(
								'ItemId' => $val_itemid,
								'BalVolume' => $newcontainervolumeforvolume,
								'BalWeight' => $newcontainerweightforweight,
								'UsedVolume' => $usedvolumeforvolume,
								'UsedWeight' => $usedweightforvolume,
								'BalCartons' => 0,
								'ProcessStatus' => 0,
							);
							$tmpid = $this->Api_model->insertfillprocess($val_tmpfillprocess);
							$lastcontainer = $this->Api_model->selectlastcontainer($orderid);
							if ($lastcontainer) {
								$tempcontainer = array(
									'ContainerType' => $lastcontainer->ContainerType,
									'FilledWeight' => $lastcontainer->FilledWeight,
									'FilledVolume' => $lastcontainer->FilledVolume,
									'Volumepercentage' => $lastcontainer->Volumepercentage,
									'Weightpercentage' => $lastcontainer->Weightpercentage,
									'OrderId' => $lastcontainer->OrderId,
									'CompanyId' => $lastcontainer->CompanyId,
									'OrginalContainerId' => $lastcontainer->ContainerId,
									'Status' => 0,
								);
								$id = $this->Api_model->addtempnewcontainer($tempcontainer);
							}

							return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolumeforvolume, 'balanceweight' => $newcontinerweightforvolume, 'usedvolume' => $usedvolumeforvolume, 'usedweight' => $usedweightforvolume, 'totalcontainervolume' => $currentitemvolume, 'totalcontainerweight' => $currentitemweight, 'balancecartons' => $noofcartonmovedforvolume, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
						}
					}
				} else {
					$val_itemid = $this->post('itemid');
					$val_cartondetails = array(
						'CompanyId' => $val_compnayid,
						'UnitperCarton' => $this->post('unitpercarton'),
						'WeightofCarton' => $this->post('grossweight'),
						'Length' => $this->post('length'),
						'Width' => $this->post('width'),
						'Height' => $this->post('height'),
						'CBM' => $this->post('cbm'),
						'UnitQuantity' => $this->post('unitquantity'),
						'CartonQuantity' => $this->post('cartonquanity'),
					);
					/*calling model sql query*/
					$result_cartondetails = $this->Api_model->savecartondetails($val_cartondetails, $val_itemid);
					$updatedspance = array(
						'FilledWeight' => $val_currentWeight,
						'FilledVolume' => $val_currentVolume,
					);
					$result_updatecontainerstatus = $this->Api_model->updatecontainerstatus($updatedspance, $orderid);
					/*update totoal volume,weight,total order value*/
					$result_currenttotalordervalues = $this->Api_model->fetchtotalordervalues($orderid);
					foreach ($result_currenttotalordervalues as $key) {
						$val_totalOrderWeight = $key->TotalOrderWeight;
						$val_totalOrderCBM = $key->TotalOrderCBM;
						$val_totalCartons = $key->TotalCartons;
					}

					$currentcortons = $this->post('cartonquanity');
					$val_totalOrderWeight = $val_totalOrderWeight + $currentitemweight;
					$val_totalOrderCBM = $val_totalOrderCBM + $currentitemvolume;
					$val_totalCartons = $val_totalCartons + $currentcortons;
					$val_totalordervalues = array(
						'TotalOrderWeight' => $val_totalOrderWeight,
						'TotalOrderCBM' => $val_totalOrderCBM,
						'TotalCartons' => $val_totalCartons,
					);
					/*update total ordervalues*/
					$result_updateorderdetails = $this->Api_model->updateordervalues($orderid, $val_totalordervalues);
					/*send percentage of the container current filled space*/
					$volumepercentage = ($val_currentVolume / $volumecapacity) * 100;
					$weightpercentage = ($val_currentWeight / $weightcapacity) * 100;
					/*update container percentage*/
					$contaienerstatus = array(
						'Volumepercentage' => $volumepercentage,
						'Weightpercentage' => $weightpercentage,
					);
					$updatecontainerpercentage = $this->Api_model->updatecontainerpercentage($orderid, $contaienerstatus);
					if ($result_cartondetails) {
						/*success responce*/
						return $this->set_response(['resultCode' => 1, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'totalcontainervolume' => "", 'totalcontainerweight' => "", 'balancecartons' => "", 'itemId' => $val_itemid, 'totalVolumePercentage' => "$volumepercentage", 'totalWeightPercentage' => "$weightpercentage", 'message' => 'carton details of item id : ' . $val_itemid . ' saved successfully'], REST_Controller::HTTP_OK);
					} else {
						/*error reponce*/
						return $this->set_response(['resultCode' => 0, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'totalcontainervolume' => "", 'totalcontainerweight' => "", 'balancecartons' => "", 'itemId' => "", 'totalVolumePercentage' => "$volumepercentage", 'totalWeightPercentage' => "$weightpercentage", 'message' => 'item saved saved failed'], REST_Controller::HTTP_OK);
						return $this->set_response(['resultCode' => 0, 'message' => 'item saved saved failed'], REST_Controller::HTTP_OK);
					}
				}
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*subsavecarton_post*/
	function subsavecarton_post() {
		/*fetch id using token value*/
		$token = $this->post('token');
		$orderid = $this->post('orderid');
		$orderemail = $this->Api_model->selectordermail($token);
		if ($orderemail) {
			$val_orderemail = $orderemail->OrderEmail;
		}

		$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
		$val_compnayid = $companyid->CompanyID;
		if ($val_compnayid) {
			$fillprocess = $this->Api_model->getfillprocess($orderid);
			$tempitem = $this->Api_model->gettempitem($orderid);
			$tempcontainer = $this->Api_model->gettempcontainer($orderid);
			$val_itemid = $tempitem->ItemId;
			$balancevolume1 = $fillprocess->BalVolume;
			$balanceweight1 = $fillprocess->BalWeight;
			$usedvolume = $fillprocess->UsedVolume;
			$usedweight = $fillprocess->UsedWeight;
			$cartonqty = $tempitem->CartonQTY;
			$cartonvolume = $tempitem->CBM;
			$cartonweight = $tempitem->GrossWeight;
			$containertype = $tempcontainer->ContainerType;
			$ContainerCapacity = $this->Api_model->selectcontainercapacity($containertype);
			$contaierMaxVolume = $ContainerCapacity->MaximumVolume;
			$containerMaxWeight = $ContainerCapacity->MaximumWeight;
			if ($balancevolume1 > $contaierMaxVolume || $balanceweight1 > $containerMaxWeight) {
				if ($balancevolume1 > $contaierMaxVolume && $balanceweight1 < $containerMaxWeight) {
					$balancevolume = $balancevolume1 - $contaierMaxVolume; // calculation for balance volume after fill the item in the container
					$noofcartonmoved = ($balancevolume / $cartonvolume);
					$noofcartonmoved = ceil($noofcartonmoved);
					$newcontainervolume = $noofcartonmoved * $cartonvolume;
					$newcontainerweight = $noofcartonmoved * $cartonweight;
					$usedvolume = $balancevolume1 - $newcontainervolume;
					$usedweight = $balanceweight1 - $newcontainerweight;
					/**return balance volume, balance weight and used volume and used weight**/
					$val_tmpfillprocess = array(
						'ItemId' => $val_itemid,
						'BalVolume' => $newcontainervolume,
						'BalWeight' => $newcontainerweight,
						'UsedVolume' => $usedvolume,
						'UsedWeight' => $usedweight,
						'BalCartons' => $noofcartonmoved,
						'ProcessStatus' => 0,
					);
					$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
					return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolume, 'balanceweight' => $newcontainerweight, 'usedvolume' => $usedvolume, 'usedweight' => $usedweight, 'balancecartons' => $noofcartonmoved, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
				} else
				if ($balanceweight1 > $containerMaxWeight && $balancevolume1 < $contaierMaxVolume) {
					/**process when balance weight is greater than container capacity**/
					$balanceweight = $balanceweight1 - $containerMaxWeight;
					$noofcartonmoved = ($balanceweight / $cartonweight);
					$noofcartonmoved = ceil($noofcartonmoved);
					$newcontainerweight = $noofcartonmoved * $cartonweight;
					$newcontainervolume = $noofcartonmoved * $cartonvolume;
					$usedweight = $balanceweight1 - $newcontainerweight;
					$usedvolume = $balancevolume1 - $newcontainervolume;
					/**return balance weight, balance volume used volume, used weight**/
					$val_tmpfillprocess = array(
						'ItemId' => $val_itemid,
						'BalVolume' => $newcontainervolume,
						'BalWeight' => $newcontainerweight,
						'UsedVolume' => $usedvolume,
						'UsedWeight' => $usedweight,
						'BalCartons' => $noofcartonmoved,
						'ProcessStatus' => 0,
					);
					$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
					return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolume, 'balanceweight' => $newcontainerweight, 'usedvolume' => $usedvolume, 'usedweight' => $usedweight, 'balancecartons' => $noofcartonmoved, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
				} else
				if ($balancevolume1 > $contaierMaxVolume && $balanceweight1 > $containerMaxWeight) {
					$balancevolume = $balancevolume1 - $contaierMaxVolume; //find over flow volume
					$noofcartonmovedforvolume = ($balancevolume / $cartonvolume); //cartons for balance volume
					$noofcartonmovedforvolume = ceil($noofcartonmovedforvolume); //round off no of carton volume
					$balanceweight = $balanceweight1 - $containerMaxWeight; //find over flow weight
					$noofcartonmovedforweight = ($balanceweight / $cartonweight); //find cartons for balace volume
					$noofcartonmovedforweight = ceil($noofcartonmovedforweight); //round off no of carton volume
					if ($noofcartonmovedforvolume > $noofcartonmovedforweight) {
						$newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
						$newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
						$usedvolume = $balancevolume1 - $newcontainervolumeforvolume;
						$usedweight = $balanceweight1 - $newcontainerweightforvolume;
						$newcontinerweightforvolume = $noofcartonmovedforweight * $cartonweight;
						/**return balance volume, balance weight, used volume, used weight**/
						$val_tmpfillprocess = array(
							'ItemId' => $val_itemid,
							'BalVolume' => $newcontainervolumeforvolume,
							'BalWeight' => $newcontainerweightforvolume,
							'UsedVolume' => $usedvolume,
							'UsedWeight' => $usedweight,
							'BalCartons' => $noofcartonmovedforvolume,
							'ProcessStatus' => 0,
						);
						$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
						return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolumeforvolume, 'balanceweight' => $newcontinerweightforvolume, 'usedvolume' => $usedvolume, 'usedweight' => $usedweight, 'balancecartons' => $noofcartonmovedforvolume, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
					} else
					if ($noofcartonmovedforvolume < $noofcartonmovedforweight) {
						$newcontainervolumeforweight = $noofcartonmovedforweight * $cartonvolume;
						$newcontainerweightforweight = $noofcartonmovedforweight * $cartonweight;
						$usedvolume = $balancevolume1 - $newcontainervolumeforweight;
						$usedweight = $balanceweight1 - $newcontainerweightforweight;
						$newcontinerweightforweight = $noofcartonmovedforweight * $cartonweight;
						$val_tmpfillprocess = array(
							'ItemId' => $val_itemid,
							'BalVolume' => $newcontainervolumeforweight,
							'BalWeight' => $newcontainerweightforweight,
							'UsedVolume' => $usedvolume,
							'UsedWeight' => $usedweight,
							'BalCartons' => $noofcartonmovedforweight,
							'ProcessStatus' => 0,
						);
						$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
						return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolumeforweight, 'balanceweight' => $newcontinerweightforweight, 'usedvolume' => $usedvolume, 'usedweight' => $usedweight, 'balancecartons' => $noofcartonmovedforweight, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
					} else {
						$newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
						$newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
						$usedvolume = $balancevolume1 - $newcontainervolumeforvolume;
						$usedweight = $balanceweight1 - $newcontainerweightforvolume;
						$newcontinerweightforvolume = $noofcartonmovedforweight * $cartonweight;
						$val_tmpfillprocess = array(
							'ItemId' => $val_itemid,
							'BalVolume' => $newcontainervolumeforvolume,
							'BalWeight' => $newcontainerweightforvolume,
							'UsedVolume' => $usedvolume,
							'UsedWeight' => $usedweight,
							'BalCartons' => $noofcartonmovedforvolume,
							'ProcessStatus' => 0,
						);
						$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
						return $this->set_response(['resultCode' => 1, 'balancevolume' => $newcontainervolumeforvolume, 'balanceweight' => $newcontinerweightforvolume, 'usedvolume' => $usedvolume, 'usedweight' => $usedweight, 'balancecartons' => $noofcartonmovedforvolume, 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'Container full'], REST_Controller::HTTP_OK);
					}
				} else {
					return $this->set_response(['resultCode' => 0, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'balancecartons' => "", 'itemId' => "", 'totalVolumePercentage' => "", 'totalWeightPercentage' => "", 'message' => 'item saved saved failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$val_cartondetails = array(
					'CompanyId' => $val_compnayid,
					'UnitperCarton' => $tempitem->UnitPerCarton,
					'WeightofCarton' => $tempitem->GrossWeight,
					'Length' => $tempitem->Length,
					'Width' => $tempitem->Width,
					'Height' => $tempitem->Height,
					'CBM' => $tempitem->CBM,
					'UnitQuantity' => $tempitem->UnitQTY,
					'CartonQuantity' => $tempitem->CartonQTY,
				);
				/*calling model sql query*/
				$result_cartondetails = $this->Api_model->savecartondetails($val_cartondetails, $val_itemid);
				$updatedspance = array(
					'FilledWeight' => $balanceweight1,
					'FilledVolume' => $balancevolume1,
				);
				$result_updatecontainerstatus = $this->Api_model->updatetempcontainerstatus($updatedspance, $orderid);
				/*update totoal volume,weight,total order value*/
				$result_currenttotalordervalues = $this->Api_model->fetchtotalordervalues($orderid);
				foreach ($result_currenttotalordervalues as $key) {
					$val_totalOrderWeight = $key->TotalOrderWeight;
					$val_totalOrderCBM = $key->TotalOrderCBM;
					$val_totalCartons = $key->TotalCartons;
				}

				$currentCartons = $tempitem->CartonQTY;
				$cweight = $tempitem->GrossWeight;
				$ccbm = $tempitem->CBM;
				$totalccbm = $currentCartons * $ccbm;
				$totalcweight = $currentCartons * $cweight;
				$val_totalOrderWeight = $val_totalOrderWeight + $totalcweight;
				$val_totalOrderCBM = $val_totalOrderCBM + $totalccbm;
				$val_totalCartons = $val_totalCartons + $currentCartons;
				$val_totalordervalues = array(
					'TotalOrderWeight' => $val_totalOrderWeight,
					'TotalOrderCBM' => $val_totalOrderCBM,
					'TotalCartons' => $val_totalCartons,
				);
				/*update total ordervalues*/
				$result_updateorderdetails = $this->Api_model->updateordervalues($orderid, $val_totalordervalues);
				/*send percentage of the container current filled space*/
				$volumepercentage = ($balancevolume1 / $contaierMaxVolume) * 100;
				$weightpercentage = ($balanceweight1 / $containerMaxWeight) * 100;
				/*update container percentage*/
				$contaienerstatus = array(
					'Volumepercentage' => $volumepercentage,
					'Weightpercentage' => $weightpercentage,
				);
				$updatecontainerpercentage = $this->Api_model->updatetempcontainerpercentage($orderid, $contaienerstatus);
				if ($result_cartondetails) {
					/*success responce*/
					/*remove all temp */
					$alltempconatiners = $this->Api_model->getalltempcontainers($orderid);
					$itemcontainerid = "";
					foreach ($alltempconatiners as $key) {
						if ($key->OrginalContainerId != 0) {
							$oldconatainer = $key->OrginalContainerId;
							$containerSpance = array(
								'ContainerType' => $key->ContainerType,
								'FilledWeight' => $key->FilledWeight,
								'FilledVolume' => $key->FilledVolume,
								'Volumepercentage' => $key->Volumepercentage,
								'Weightpercentage' => $key->Weightpercentage,
								'Status' => $key->Status,
								'ItemSaveStatus' => 1,
							);
							$updateCotainerSpace = $this->Api_model->updateoldcontainer($containerSpance, $oldconatainer);
							if ($itemcontainerid == "") {
								$itemcontainerid = $oldconatainer;
							} else {
								$itemcontainerid = $itemcontainerid . "," . $oldconatainer;
							}
						} else {
							$val_newcontainer = array(
								'ContainerType' => $key->ContainerType,
								'OrderId' => $key->OrderId,
								'CompanyId' => $key->CompanyId,
								'FilledWeight' => $key->FilledWeight,
								'FilledVolume' => $key->FilledVolume,
								'Volumepercentage' => $key->Volumepercentage,
								'Weightpercentage' => $key->Weightpercentage,
								'Status' => $key->Status,
								'ItemSaveStatus' => 1,
							);
							/*calling model sql query*/
							$result_newcontainer = $this->Api_model->addnewcontainer($val_newcontainer);
							if ($itemcontainerid == "") {
								$itemcontainerid = $result_newcontainer;
							} else {
								$itemcontainerid = $itemcontainerid . "," . $result_newcontainer;
							}
						}
					}

					$itemcontainersid = array(
						'ContainerId' => $itemcontainerid,
					);
					/**update item container type**/
					$updateitemcontainertype = $this->Api_model->updateitemcontainertype($itemcontainersid, $val_itemid);
					$this->Api_model->removetempconatiner($orderid);
					$this->Api_model->removetempitems($orderid);
					$this->Api_model->removetempprocess($val_itemid);
					return $this->set_response(['resultCode' => 1, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'balancecartons' => "", 'itemId' => $val_itemid, 'totalVolumePercentage' => $volumepercentage, 'totalWeightPercentage' => $weightpercentage, 'message' => 'carton details of item id : ' . $val_itemid . ' saved successfully'], REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					return $this->set_response(['resultCode' => 0, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'balancecartons' => "", 'itemId' => $val_itemid, 'totalVolumePercentage' => $volumepercentage, 'totalWeightPercentage' => $weightpercentage, 'message' => 'item saved saved failed'], REST_Controller::HTTP_OK);
				}
			}
		} else {
			return $this->set_response(['resultCode' => 0, 'balancevolume' => "", 'balanceweight' => "", 'usedvolume' => "", 'usedweight' => "", 'balancecartons' => "", 'itemId' => $val_itemid, 'totalVolumePercentage' => $volumepercentage, 'totalWeightPercentage' => $weightpercentage, 'message' => 'Company dones not exist in this token'], REST_Controller::HTTP_OK);
		}
	}

	/*end*/
	/*price details*/
	function saveitemprice_post() {
		$val_orderemail = "";
		$val_compnayid = "";
		$totalusprice = "";
		$totalrbmprice = "";
		$totalzarprice = "";
		/*check request values for its null or not*/
		if ((!$this->post('token')) || (!$this->post('orderId')) || (!$this->post('itemId')) || (!$this->post('priceInUs')) || (!$this->post('priceInRmb')) || (!$this->post('boothid'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->post('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$orderid = $this->post('orderId');
				$itemid = $this->post('itemId');
				$itemdetails = $this->Api_model->fetitemdetails($orderid, $itemid);
				if ($itemdetails) {
					foreach ($itemdetails as $key => $value) {
						$noofunits = $value->UnitQuantity;
					}
				} else {
					$noofunits = 1;
				}

				$currentUsprice = $this->post('priceInUs');
				$currentRmbprice = $this->post('priceInRmb');
				if ($this->post('PriceInZar') != "") {
					$currentZarprice = $this->post('PriceInZar');
					$totalZarPriceItem = $currentZarprice * $noofunits;
				} else {
					$totalZarPriceItem = 0;
				}

				$totalPriceItemUs = $noofunits * $currentUsprice;
				$totalPriceItemRmb = $noofunits * $currentRmbprice;
				$val_itemdetails = array(
					'PriceInUs' => $this->post('priceInUs'),
					'PriceInRmb' => $this->post('priceInRmb'),
					'PriceInThirdCurrency' => $this->post('PriceInZar'),
					'TotalPriceInUs' => $totalPriceItemUs,
					'TotalPriceInRmb' => $totalPriceItemRmb,
					'TotalPriceIn3rdCurrency' => $totalZarPriceItem,
				);
				/*calling model sql query*/
				$result_itemprice = $this->Api_model->saveitemprice($orderid, $itemid, $val_itemdetails);
				/*update order total price*/
				if ($result_itemprice) {
					/*success responce*/
					$currentotalprice = $this->Api_model->fetchordertotal($orderid);
					$totalusprice = $currentotalprice->TotalOrderUsValue;
					$totalrbmprice = $currentotalprice->TotalOrderRmbValue;
					$totalzarprice = $currentotalprice->TotalOrderZarValue;
					$totalusprice = $totalPriceItemUs + $totalusprice;
					$totalrbmprice = $totalPriceItemRmb + $totalrbmprice;
					$totalzarprice = $totalZarPriceItem + $totalzarprice;
					$val_totalpricevalue = array(
						'TotalOrderUsValue' => $totalusprice,
						'TotalOrderRmbValue' => $totalrbmprice,
						'TotalOrderZarValue' => $totalzarprice,
					);
					$result = $this->Api_model->updatetotalprice($orderid, $val_totalpricevalue);
					/*update current booth price*/
					$currentboothid = $this->post('boothid');
					$currentboothvalue = $this->Api_model->selectcurrentboothvalue($orderid, $currentboothid);
					$boothvalueus = $currentboothvalue->ValuePurchase;
					$boothvaluermb = $currentboothvalue->ValuePurchaseRMB;
					$boothvalueus = $boothvalueus + $totalPriceItemUs;
					$boothvaluermb = $boothvaluermb + $totalPriceItemRmb;
					$boothvalue = array(
						'ValuePurchase' => $boothvalueus,
						'ValuePurchaseRMB' => $boothvaluermb,
					);
					$this->Api_model->updateboothvalue($currentboothid, $boothvalue);
					$selectordersummary = $this->Api_model->selectordersummary($orderid);
					$message = ['resultCode' => 1, 'itemId' => $itemid, 'summary' => $selectordersummary, 'message' => 'item price saved successfully'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'item price saved saved failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*end order*/
	function endorder_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		/*check request values is null*/
		if ((!$this->get('orderid')) || (!$this->get('token'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$orderid = $this->get('orderid');
				$val_endorderstatus = array(
					'OrderStatus' => 1,
				);
				/*calling model sql query*/
				$result = $this->Api_model->endorderStatuschange($val_endorderstatus, $orderid, $val_compnayid);
				if ($result) {
					/*success responce*/
					$message = ['resultCode' => 1, 'message' => 'successfully end order'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'message' => 'end order failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*add new container*/
	/*add new container*/
	function addnewcontainer_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		/*check request values is null*/
		if ((!$this->get('orderid')) || (!$this->get('token')) || (!$this->get('containertype')) || (!$this->get('currentcontainertype'))) {
			/*error responce*/
			$this->set_response(['resultCode' => 0, 'data' => '[]', 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				$currentcontainertype = $this->get('currentcontainertype'); //type current container
				$totalContainerVolume = $this->get('usedvolume'); // volume used for current container
				$totalContainerWeight = $this->get('usedweight'); // weight used for current container
				$selectcontainercapacity = $this->Api_model->selectcontainercapacity($currentcontainertype);
				$containervolume_capacity = $selectcontainercapacity->MaximumVolume;
				$containerweight_capacity = $selectcontainercapacity->MaximumWeight;
				$volumepercentage = ($totalContainerVolume / $containervolume_capacity) * 100;
				$weightpercentage = ($totalContainerWeight / $containerweight_capacity) * 100;
				$volumepercentage = number_format($volumepercentage, 2);
				$weightpercentage = number_format($weightpercentage, 2);
				$orderid = $this->get('orderid');
				$containerSpance = array(
					'FilledWeight' => $totalContainerWeight,
					'FilledVolume' => $totalContainerVolume,
					'Volumepercentage' => $volumepercentage,
					'Weightpercentage' => $weightpercentage,
					'Status' => 1,
				);
				$updateCotainerSpace = $this->Api_model->updatetempContainerSpace($containerSpance, $orderid);
				$val_newcontainer = array(
					'ContainerType' => $this->get('containertype'),
					'OrderId' => $orderid,
					'CompanyId' => $val_compnayid,
					'OrginalContainerId' => 0,
				);
				/*calling model sql query*/
				$result_newcontainer = $this->Api_model->addtempnewcontainer($val_newcontainer);
				if ($result_newcontainer) {
					/*success responce*/
					$newcontainertype = $this->get('containertype');
					$message = ['resultCode' => 1, 'data' => $result_newcontainer, 'message' => 'new container added'];
					$this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					/*error reponce*/
					$this->set_response(['resultCode' => 0, 'data' => "", 'message' => 'add container failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'data' => "", 'mssage' => 'No data exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*upgrade container */
	function upgradecontainer_get() {
		if ((!$this->get('upgradecontainertype')) || (!$this->get('orderid')) || (!$this->get('contanerid'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$orderid = $this->get('orderid');
			$tempfill = $this->Api_model->getfillprocess($orderid);
			$balancevolume = $tempfill->BalVolume + $tempfill->UsedVolume;
			$balaceweight = $tempfill->BalWeight + $tempfill->UsedWeight;
			$val_itemid = $tempfill->ItemId;
			$orderid = $this->get('orderid');
			$containerid = $this->get('containerid');
			$upgradecontainertype = $this->get('upgradecontainertype');
			$totalContainerVolume = $balancevolume;
			$totalContainerWeight = $balaceweight;
			/**calculate volume and weight percentage for current container**/
			$selectcontainercapacity = $this->Api_model->selectcontainercapacity($upgradecontainertype);
			$containervolume_capacity = $selectcontainercapacity->MaximumVolume;
			$containerweight_capacity = $selectcontainercapacity->MaximumWeight;
			$volumepercentage = ($totalContainerVolume / $containervolume_capacity) * 100;
			$weightpercentage = ($totalContainerWeight / $containerweight_capacity) * 100;
			$volumepercentage = number_format($volumepercentage, 2);
			$weightpercentage = number_format($weightpercentage, 2);
			$containerdata = array(
				'ContainerType' => $upgradecontainertype,
				'Volumepercentage' => $volumepercentage,
				'Weightpercentage' => $weightpercentage,
			);
			/**upgrade container**/
			$upgradecontainer = $this->Api_model->upgradetempcontainer($containerdata, $orderid);
			$val_tmpfillprocess = array(
				'BalVolume' => $balancevolume,
				'BalWeight' => $balaceweight,
				'UsedVolume' => 0,
				'UsedWeight' => 0,
				'ProcessStatus' => 0,
			);
			$tmpid = $this->Api_model->updatefillprocess($val_itemid, $val_tmpfillprocess);
			if ($upgradecontainer == "1") {
				$this->set_response(['resultCode' => 1, 'message' => 'contianer upgraded successfully'], REST_Controller::HTTP_OK);
			} else {
				$this->set_response(['resultCode' => $upgradecontainer, 'message' => 'contianer upgraded failed'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*end*/
	/*view order items*/
	function vieworderitems_get() {
		$val_orderemail = "";
		$val_compnayid = "";
		if ((!$this->get('token')) || (!$this->get('orderid'))) {
			return $this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			$val_orderid = $this->get('orderid');
			/*fetch id using token value*/
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'Invalid user'], REST_Controller::HTTP_OK);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			if ($companyid) {
				$val_compnayid = $companyid->CompanyID;
				$orderitemdetails = $this->Api_model->selectorderitems($val_orderid);
				$orderDeatils = $this->Api_model->getoredrbyid($val_orderid);
				$containerList = $this->Api_model->selectcontainerdetails($val_orderid);
				$totalconatiner = "";
				$index = 0;
				foreach ($containerList as $key) {
					$containername = $this->Api_model->selectlistcontainername($key->ContainerType);
					$name = $containername->Container;
					if ($index == 0) {
						$totalconatiner = $name . ":" . $key->Count;
					} else {
						$totalconatiner = $totalconatiner . "," . $name . ":" . $key->Count;
					}

					$index++;
				}

				foreach ($orderitemdetails as $key) {
					$containeid = $key->ContainerId;
					$myArray = explode(',', $containeid);
					$list = "";
					$index = 0;
					foreach ($myArray as $container) {
						$containername = $this->Api_model->selectcontainername($container);
						if ($containername) {
							$name = $containername->Container;
						} else {
							$name = "";
						}

						if ($index == 0) {
							$list = $name;
						} else {
							$list = $list . "," . $name;
						}

						$index++;
					}

					$key->ContainerId = $list;
				}

				if ($orderDeatils) {
					foreach ($orderDeatils as $key) {
						$key->Containers = $totalconatiner;
					}

					$message = ['resultCode' => 1, 'data' => $orderitemdetails, 'orderDeatils' => $orderDeatils, 'message' => 'success'];
					return $this->set_response($message, REST_Controller::HTTP_OK);
				} else {

					// error reponce

					$this->set_response(['resultCode' => 0, 'data' => $orderitemdetails, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'invalid token'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*
		  * Image Upload
		  * @val_type -is 1 -Item Images ,2 -Profile,3-Booth
		  *
	*/
	function uploaditemimage_post() {
		$val_type = "";
		$val_orderemail = "";
		$val_compnayid = "";
		$val_imagelocation = "assets/images/";
		if ((!$this->post('token'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'Token Required'], REST_Controller::HTTP_OK);
		} else {
			$val_type = $this->post('type');
			$val_token = $this->post('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'message' => 'No user found'], REST_Controller::HTTP_OK);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;
			if ($val_compnayid) {
				if ($val_type == 2) {
					$val_imagelocation = "assets/images/profilepic";
				}

				$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
				$imagename = time() . '.' . $ext;
				$config['upload_path'] = $val_imagelocation;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|ico|icon|image|image/ico';
				$config['file_name'] = $imagename;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					if ($val_type == 2) {
						$accounts = $this->Api_model->getaccountdetails($val_orderemail);
						if ($accounts) {
							$data_accouts = array(
								'Image  ' => $imagename,
							);
							$this->Api_model->storeorupdateaccount($data_accouts, 2, $val_orderemail);
						} else {
							$data_accouts = array(
								'EmailSettings' => "",
								'Language' => "",
								'Locale' => "",
								'EmailId' => $val_orderemail,
								'Image  ' => $imagename,
							);
							$this->Api_model->storeorupdateaccount($data_accouts, 1, $val_orderemail);
						}

						$filename = "https://mixmycontainer.com/QA/assets/images/profilepic/" . $imagename;
					} else {
						$filename = $imagename;
					}

					$this->set_response(['resultCode' => 1, 'message' => 'success', 'filename' => $filename], REST_Controller::HTTP_OK);
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'Image upload failed'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'Invalid token'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*signup */
	function signup_get() {
		$val_loginemail = "";
		$val_compnayid = "";
		if ((!$this->get('emailid')) || (!$this->get('password')) || (!$this->get('companyname')) || (!$this->get('country')) || (!$this->get('address')) || (!$this->get('zipcode')) || (!$this->get('mobile')) || (!$this->get('phone')) || (!$this->get('towncity'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_email = $this->get('emailid');
			$val_password = $this->get('password');
			$val_username = "";
			/*fetch id using token value*/
			$loginemail = $this->Api_model->selectloginemail($val_email);
			If ($loginemail) {
				$this->set_response(['resultCode' => 0, 'message' => 'Email Id already exist'], REST_Controller::HTTP_OK);
			} else {
				$encryptedpassword = md5($val_password);
				$userpassword = $encryptedpassword;
				$useremail = $val_email;
				$username = '';
				$userregisterdetails = array(
					'Password' => $userpassword,
					'Email' => $useremail,
					'Username' => $username,
					'UserType' => 'normal',
				);
				$result = $this->Api_model->saveuseregister($userregisterdetails);
				$companysetup = array(
					'CompanyName' => $this->get('companyname'),
					//'Address' => preg_replace("/[^a-zA-Z]/", "", $this->get('address')),
					'Address' => $this->get('address'),
					'ZipCode' => $this->get('zipcode'),
					'Phone' => $this->get('phone'),
					'Country' => $this->get('country'),
					'Mobile' => $this->get('mobile'),
					'Town/City' => $this->get('towncity') ,
					'OrderEmail' => $useremail,
					'MailOrder' => $useremail,
					'TransilateFrom' =>'en',
					'TransilateTo' =>'zh',
				);
				$result = $this->Api_model->savecompanysetup($companysetup);
				if ($result) {
					$this->set_response(['resultCode' => 1, 'message' => 'User successfully registerd','companyid' => $result], REST_Controller::HTTP_OK);
				} else {
					$this->set_response(['resultCode' => 0, 'message' => 'User registration failed'], REST_Controller::HTTP_OK);
				}
			}
		}
	}

	/*forgot password*/
	function forgotpassword_get() {
		$username = "";
		if (!$this->get('emailid')) {
			$this->set_response(['resultCode' => 0, 'message' => 'Email id required'], REST_Controller::HTTP_OK);
		}

		$otp = mt_rand(1000, 9999);
		$useremail = $this->get('emailid');
		$usercheck = $this->Api_model->checkuserexists($useremail);
		if ($usercheck) {
			$setotp = array(
				'Otp' => $otp,
			);
			$saveotp = $this->Api_model->saveotp($setotp, $useremail);
			$content = "Your Request For Reset Password In Mixmycontainer Has Been Granted, Please enter the OTP : $otp  ";
			$url = "https://mixmycontainer.com/QA/Login/reset";
			$emailmessage = $this->ResetPasswordEmail_model->EmailMessage($useremail, $content, $url);
			$from_email = "webmaster@mixmycontainer.com";
			$this->load->library('email');
			$config['protocol'] = "sendmail";
			$config['smtp_host'] = "smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = "webmaster@mixmycontainer.com";
			$config['smtp_pass'] = "kbta59cz";
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
			$config['priority'] = 5;
			$config['newline'] = "\r\n";
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from($from_email, 'MixMyContainer');
			$this->email->to($useremail);
			$this->email->subject('Mixmycontainer Account Reset');
			$this->email->message($emailmessage);
			$this->email->send();
			return $this->set_response(['resultCode' => 1, 'otpCode' => $otp], REST_Controller::HTTP_OK);
		} else {
			return $this->set_response(['resultCode' => 0, 'message' => "user not found"], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	/*end*/
	function endcurrentbooth_get() {
		$email = "";
		if ((!$this->get('boothid')) || (!$this->get('orderid'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_boothid = $this->get('boothid');
			$val_orderid = $this->get('orderid');
			/*check booth is exist or not*/
			$checkboothid = $this->Api_model->checkboothid($val_boothid, $val_orderid);
			if ($checkboothid) {
				$boothstatus = array(
					'BoothStatus' => 1,
				);
				$loginemail = $this->Api_model->updateboothstatus($val_boothid, $val_orderid, $boothstatus);
				$this->set_response(['resultCode' => 1, 'message' => 'Current Booth stoped'], REST_Controller::HTTP_OK);
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'Booth  does not exist'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*ordersummary*/
	function ordersummary_get() {
		if ((!$this->get('orderid'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$val_orderid = $this->get('orderid');
			/*fetch id using token value*/
			$ordersummary = $this->Api_model->selectordersummary($val_orderid);
			If ($ordersummary) {
				$ordersummarybooth = $this->Api_model->selectordersummarybooth($val_orderid);
				if ($ordersummarybooth) {
					$ordersummary->ValuePurchase = $ordersummarybooth->ValuePurchase;
					$ordersummary->ValuePurchaseRMB = $ordersummarybooth->ValuePurchaseRMB;
				} else {
					$ordersummary->ValuePurchase = 0;
					$ordersummary->ValuePurchaseRMB = 0;
				}

				$containerlist = $this->Api_model->selectusedcontainers($val_orderid);
				$this->set_response(['resultCode' => 1, 'data' => $ordersummary, 'conatiners' => $containerlist, 'message' => 'success'], REST_Controller::HTTP_OK);
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'Invalid order Id'], REST_Controller::HTTP_OK);
			}
		}
	}

	/*end*/
	/*Abhijith A Nair's code*/
	function ignoreupgrade_get() {
		if ((!$this->get('orderid')) || (!$this->get('token'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$orderid = $this->get('orderid');
			$fill = $this->Api_model->getfillprocess($orderid);
			$this->Api_model->removetempconatiner($orderid);
			$this->Api_model->removetempitems($orderid);
			if ($fill) {
				$this->Api_model->removetempprocess($fill->ItemId);
			}

			return $this->set_response(['resultCode' => 1, 'message' => 'Process Cancelled'], REST_Controller::HTTP_OK);
		}
	}

	function getcountry_get() {
		try {
			$countrylist = $this->Api_model->getcountrylist();
			if (!$countrylist) {
				return $this->set_response(['resultCode' => 0, 'message' => 'No country found'], REST_Controller::HTTP_OK);
			}

			return $this->set_response(['resultCode' => 1, 'data' => $countrylist], REST_Controller::HTTP_OK);
		} catch (Exception $e) {
			return $this->set_response(['resultCode' => 0, 'message' => 'Something bad happend'], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function sendorderemail_get() {
		if ((!$this->get('orderid')) || (!$this->get('token'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			try {
				$val_orderid = $this->get('orderid');
				$val_token = $this->get('token');
				$orderemail = $this->Api_model->selectordermail($val_token);
				if ($orderemail) {
					$orderemail = $orderemail->OrderEmail;
					$company = $this->Api_model->fetchcompanyid($orderemail);
					$companyid = $company->CompanyID;
					$orderemail = $this->Api_model->selectmailorders($companyid);
					$ordermails = $orderemail->MailOrder;
					if ($ordermails != "" && $ordermails) {
						$agentid = $orderemail->AgentId;
						$agentemail = "";
						if ($agentid != "0") {
							$agentdetails = $this->Api_model->selectagentemail($agentid);
							foreach ($agentdetails as $value) {
								$agentemail = $agentemail . $value->Email . ",";
							}
						} else {
							$agentemail = $orderemail->AgentEmail;
						}

						$items = $this->Api_model->getordersinorderid($val_orderid);
						if ($items) {
							$fetchemailboothdetails = $this->Api_model->getemailorderboothdetails($val_orderid);
							$getemailcontainerdetails = $this->Order_model->gettotalcontainers($val_orderid);
							$fetchemailcompanydetails = $this->Order_model->getemailcompanydetails($companyid);
							foreach ($fetchemailboothdetails as $key) {
								$getemailitemdetails = $this->Api_model->getemailitemdetails($key->OrderId, $key->BoothId);
								$key->item = $getemailitemdetails;
							}

							if ($fetchemailboothdetails) {
								foreach ($fetchemailboothdetails as $value) {
									$OrderNumber = $value->OrderNumber;
									$Date = $value->LastUpdate;
									$TotalOrderUsValue = $value->TotalOrderUsValue;
									$TotalBooth = $value->TotalBooth;
									$TotalOrderRmbValue = $value->TotalOrderRmbValue;
									$TotalOrderCBM = $value->TotalOrderCBM;
									$TotalContainer = $value->TotalContainer;
									$TotalOrderThirdCurrencyValue = $value->TotalOrderZarValue;
								}

								foreach ($fetchemailcompanydetails as $value) {
									$CompanyName = $value->CompanyName;
									$CompanyAgent = $value->AgentName;
									$CompanyPhone = $value->Phone;
									$CompanyCountry = $value->Country;
									$CompanyMobile = $value->Mobile;
									$CompanyAddress = $value->Address;
									$CompanyEmailAddress = $value->OrderEmail;
									$Companythirdcurrency = $value->ThirdCurrency;
									$CompanyZipcode = $value->ZipCode;
									if ($CompanyZipcode == 0) {
										$CompanyZipcode = "";
									}
								}

								if ($Companythirdcurrency != "") {
									preg_match('#\((.*?)\)#', $Companythirdcurrency, $out);
									$Companythirdcurrencysymbol = $out[1];
								} else {
									$Companythirdcurrencysymbol = "";
								}

								// $pdfdoc = $this->Email_model->EmailMessage($fetchemailboothdetails, $OrderNumber, $Date, $TotalOrderUsValue, $TotalBooth, $TotalOrderRmbValue, $TotalOrderCBM, $TotalContainer);

								$pdfdoc = $this->Email_model->EmailMessage($fetchemailboothdetails, $OrderNumber, $Date, $TotalOrderUsValue, $TotalBooth, $TotalOrderRmbValue, $TotalOrderCBM, $TotalContainer, $CompanyName, $CompanyPhone, $CompanyCountry, $CompanyMobile, $CompanyAddress, $CompanyEmailAddress, $CompanyZipcode, $TotalOrderThirdCurrencyValue, $CompanyAgent, $getemailcontainerdetails, $Companythirdcurrencysymbol);
								$attachment = $pdfdoc;
								$from_email = "webmaster@mixmycontainer.com";
								$this->load->library('email');
								$config['protocol'] = "sendmail";
								$config['smtp_host'] = "smtp.gmail.com";
								$config['smtp_port'] = "465";
								$config['smtp_user'] = "webmaster@mixmycontainer.com";
								$config['smtp_pass'] = "kbta59cz";
								$config['charset'] = "utf-8";
								$config['mailtype'] = "html";
								$config['priority'] = 5;
								$config['newline'] = "\r\n";
								$this->email->initialize($config);
								$this->email->from($from_email, 'MixMyContainer');
								$this->email->to($ordermails);
								$this->email->cc($agentemail);
								$this->email->subject('Order Details');
								$this->email->message("Hi,
                                          Please find the attachement you requested. For any query please don't hesistate to contact us .
                                          \nOur mail id is: webmaster@mixmycontainer.com");
								$this->email->attach($attachment);
								if ($this->email->send()) {
									return $this->set_response(['resultCode' => 1, 'message' => 'Email send successfully'], REST_Controller::HTTP_NOT_FOUND);
								} else {
									return $this->set_response(['resultCode' => 0, 'message' => 'Email send failed'], REST_Controller::HTTP_NOT_FOUND);
								}
							} else {
								return $this->set_response(['resultCode' => 0, 'message' => 'No items in the current order'], REST_Controller::HTTP_NOT_FOUND);
							}
						} else {
							return $this->set_response(['resultCode' => 0, 'message' => 'No items in the current order'], REST_Controller::HTTP_NOT_FOUND);
						}
					} else {
						return $this->set_response(['resultCode' => 0, 'message' => 'No order mailid specified in company setup'], REST_Controller::HTTP_NOT_FOUND);
					}
				} else {
					return $this->set_response(['resultCode' => 0, 'message' => 'Invalid token'], REST_Controller::HTTP_NOT_FOUND);
				}
			} catch (Exception $e) {
				return $this->set_response(['resultCode' => 0, 'message' => $e->getmessage()], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*Offline get order details*/
	function offlineuserorderdetails_get() {
		if ((!$this->get('token'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'Token missing'], REST_Controller::HTTP_OK);
		} else {
			$val_token = $this->get('token');
			$orderemail = $this->Api_model->selectordermail($val_token);
			if ($orderemail) {
				$val_orderemail = $orderemail->OrderEmail;
			} else {
				return $this->set_response(['resultCode' => 0, 'messsage' => 'Invalid User'], REST_Controller::HTTP_OK);
			}

			$companyid = $this->Api_model->fetchcompanyid($val_orderemail);
			$val_compnayid = $companyid->CompanyID;

			// $companyid = $this->get('companyid');
			// $valcompanyid = $this->Api_model->checkifcompanyexists($companyid);

			if ($val_compnayid) {
				$result = $this->Api_model->companyorderdetails($val_compnayid);
				foreach ($result as $key) {
					
					$boothdetailsbyorderid = $this->Api_model->boothdetailsbyorderid($key->OrderId);
					$key->boothdetailsbyorderid = $boothdetailsbyorderid;

					$boothitems = $this->Api_model->GetItemDeatilsByOrderId($key->OrderId);
					$key->itemdetailsbyboothorderid = $boothitems;

					foreach ($boothitems as $key ) {
			              $filename = str_replace('_thumb.jpg', '.jpg', $key->Picture);
			              $key->Picture = $filename;
			            }
					
				}

				foreach ($result as $key) {
					$containerdetailsbyorderid = $this->Api_model->containerdetailsbyorderid($key->OrderId);
					$key->containerdetailsbyorderid = $containerdetailsbyorderid;
				}

				if ($result) {
					$message = ['message' => 'success', 'resultCode' => 1, 'data' => $result];
					return $this->set_response($message, REST_Controller::HTTP_OK);
				} else {
					return $this->set_response(['resultCode' => 0, 'message' => 'empty result set'], REST_Controller::HTTP_OK);
				}
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_NOT_FOUND);
			}
		}
	}

	/*end*/
	/*
		  *Offline Insert Trade and Hall
		  *
	*/
	function offlineinsertnewtradeandhall_post() {
		if ((!$this->post('companyid')) || (!$this->post('name')) || (!$this->post('parent'))) {
			$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
		} else {
			$companyid = $this->post('companyid');
			$valcompanyid = $this->Api_model->checkifcompanyexists($companyid);
			if ($valcompanyid) {
				$val_companyid = $valcompanyid->CompanyID;
				$val_name = $this->post('name');
				$val_parent = $this->post('parent');
				$data = array(
					'Name' => $val_name,
					'Parent' => $val_parent,
					'CompanyId' => $val_companyid,
				);
				$lastInsertId = $this->Api_model->inserttradeorhall($data);
				return $this->set_response(['resultCode' => 1, 'Name' => $val_name, 'Id' => $lastInsertId, 'CompanyId' => $val_companyid, 'Parent' => $val_parent, 'message' => "Data sucessfully inserted"], REST_Controller::HTTP_OK);
			} else {
				$this->set_response(['resultCode' => 0, 'message' => 'No data exist'], REST_Controller::HTTP_NOT_FOUND);
			}
		}

		// }

	}

	
	function googletranslate_get(){
		echo "string";

    $apiKey = 'AIzaSyAemHXikEKrzxKdKON_oPv88lk0DpfxI8s';
    $text = 'Hello world!';
    $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&source=en&target=zh';

    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($handle);                 
    $responseDecoded = json_decode($response, true);
    curl_close($handle);
    // print_r(json_decode($response, true));
    echo 'Source: ' . $text . '<br>';
    echo 'Translation: ' . $responseDecoded['data']['translations'][0]['translatedText'];
  }

  	function changeimagetobase64_post() {
  		try{
  			if((!$this->post('data'))) {
  				$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
  			}
  			else
  			{
  				$data = $this->post('data');
  				foreach ($data as $booth) {
  				$boothimage = $booth['boothdetailsbyorderid'];
  				foreach ($boothimage as $key) {
  				 		$boothimagename = $key['BusinessCard'];
  				 		$filename = $this->Api_model->testsaveimage($boothimagename);
  				 	}
  				 	
  				 	// }
  				 	//print_r($boothimage);
  				 	

  				 }

  			}
  		}
  		catch (Exception $e) {
            return $this->set_response(['resultCode' => 0, 'message' => $e->getMessage()], REST_Controller::HTTP_OK);
        }
  	}
  	public function do_upload_post()
	{
		try{
			if((!$this->post('data'))) {
  				$this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
  			}
  			else
  			{
  				$data = $this->post('data');
  				foreach ($data as $booth) {
  				$boothimage = $booth['boothdetailsbyorderid'];
  				foreach ($boothimage as $key) {

  				$data = array();

  				//$imgData = base64_decode($key['BusinessCard']);
  				// $decoded=base64_decode($base64img);
 				$file_element_name = $key['BusinessCard'];
 				$decoded=base64_decode($file_element_name);
 				//echo $decoded;
 				$file_name = $this->Api_model->testsaveimage($file_element_name);
 				// /echo $file_name;
 				// echo $filename;
 				// exit();
			 	//$file_element_name = $key['BusinessCard'];
			 	//$file_element_name = 'BusinessCard';
			 	// echo $file_element_name;
			 	// exit();
				//$file_element_name = 'userfile';
		 		//echo $file_element_name;
				//$user_upload_path = 'images';
				//echo $user_upload_path;
		 
				//$config['upload_path'] = './' . $user_upload_path;
				// $config['upload_path'] = './images';
				// $config['allowed_types'] = 'gif|jpg|png';
				// $config['max_size']  = 1024 * 4;
				// $config['encrypt_name'] = TRUE;
		 
				// $this->load->library('upload', $config);
				// $this->upload->initialize($config);
		 	// 	echo $file_element_name;
				// if (!$this->upload->do_upload($file_element_name))
				// {
				// 	$data['upload_error'] = $this->upload->display_errors();
				// 	print_r($data['upload_error']);
				// }
				// else
				// {
					// echo "uploading";

					// $data_upload = $this->upload->data();
		 
					// $file_name = $data_upload["file_name"];
					// //$file_name_thumb = $data_upload['raw_name'].'_thumb' . $data_upload['file_ext'];
					// $file_name_t = $data_upload['raw_name'] . $data_upload['file_ext'];
		 
					$this->load->library('image_lib');
					$config_resize['image_library'] = 'gd2';	
					$config_resize['create_thumb'] = TRUE;
					//$config_resize['create_thumb'] = False;
					$config_resize['overwrite'] = TRUE;
					$config_resize['maintain_ratio'] = TRUE;
					$config_resize['master_dim'] = 'height';
					$config_resize['quality'] = "100%";
					//$config_resize['source_image'] = './' . $user_upload_path . $file_name;
					$config_resize['source_image'] = 'assets/testimages/' . $file_name;
				 	$config_resize['width']         = 210;
					$config_resize['height']       = 140;
					// $config_resize['height'] = 140;
					// $config_resize['width'] = 210;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize();
					if (!$this->image_lib->resize())
					{

					//echo "not able to resize";
					$data['upload_error'] = $this->image_lib->display_errors();
					//print_r($data['upload_error']);
					}
				else{
					echo "resized";
				}
		 
					// $data["file_name_url"] = base_url() . $user_upload_path . $file_name;
					// $data["file_name_thumb_url"] = base_url() . $user_upload_path . $file_name_thumb;
				// }
	}
}
  			}
		}
		catch (Exception $e) {
            return $this->set_response(['resultCode' => 0, 'message' => $e->getMessage()], REST_Controller::HTTP_OK);
        }
		
 
		//$this->load->view('upload_example_result',$data);
	}

  	function imageResize_post(){
		$data = array();
 
		$file_element_name = 'userfile';
 		//echo $file_element_name;
		$user_upload_path = 'images';
		//echo $user_upload_path;
 
		//$config['upload_path'] = './' . $user_upload_path;
		$config['upload_path'] = './images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = 1024 * 4;
		$config['encrypt_name'] = TRUE;
 
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
 
		if (!$this->upload->do_upload($file_element_name))
		{
			//echo "error";
			$data['upload_error'] = $this->upload->display_errors();
			print_r($data['upload_error']);
		}
		else
		{
			$data_upload = $this->upload->data();
		 
					$file_name = $data_upload["file_name"];
					//$file_name_thumb = $data_upload['raw_name'].'_thumb' . $data_upload['file_ext'];
					$file_name_t = $data_upload['raw_name'] . $data_upload['file_ext'];
		 
					$this->load->library('image_lib');
					$config_resize['image_library'] = 'gd2';	
					$config_resize['create_thumb'] = TRUE;
					$config_resize['maintain_ratio'] = TRUE;
					$config_resize['master_dim'] = 'height';
					$config_resize['quality'] = "100%";
					//$config_resize['source_image'] = './' . $user_upload_path . $file_name;
					$config_resize['source_image'] = './images/' . $file_name;
				 	$config_resize['width']         = 210;
					$config_resize['height']       = 140;
					// $config_resize['height'] = 140;
					// $config_resize['width'] = 210;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize();
					if (!$this->image_lib->resize())
					{

					//echo "not able to resize";
					$data['upload_error'] = $this->image_lib->display_errors();
					//print_r($data['upload_error']);
					}
				else{
					echo "resized";
				}
 
			//$data["file_name_url"] = base_url() . $user_upload_path . $file_name;
			// $data["file_name_thumb_url"] = base_url() . $user_upload_path . $file_name_thumb;
		}
	}	
	
    /*end*/
    function offlinesyncorder_post() {
        try {
            if ((!$this->post('data'))) {
                $this->set_response(['resultCode' => 0, 'message' => 'value missing'], REST_Controller::HTTP_OK);
            } else {
            	//echo "string";
                $data = $this->post('data');
                foreach ($data as $order) {
                    $itemcontainer = array();
                    
                    $ordertradecompany = $order['CompanyId'];
                    if(preg_match('/[^a-zA-Z\d]/', $order['Trade'])) {
                    	$ordertradename = $order['TradeName'];
                    	$orderhallname  = $order['HallName'];
                    	$result = $this->Api_model->checkifTradeexist($ordertradename,$ordertradecompany);

                    	if($result ==0)
                    	{
                    		$tradedetails = array(
                    			'Name' 		=> $ordertradename,
                    			'Parent'    => 0,
                    			'CompanyId' => $order['CompanyId']
                    			);

                    		$inserttrade = $this->Api_model->inserttrade($tradedetails);
                    		$order['Trade'] = $inserttrade;
                    		$halldetails = array(
                    			'Name'   	=> $orderhallname,
                    			'Parent' 	=> $inserttrade,
                    			'CompanyId' => $order['CompanyId']
                    			);
                    		$inserthall = $this->Api_model->inserthall($halldetails);
                    		$order['ProductType'] = $inserthall;

                    	}
                    	else 
                    	{
                    		//echo "exist";
                    		$gettradeidifexists = $this->Api_model->gettradeidifTradeexist($ordertradename,$ordertradecompany);
                    		$order['Trade'] = $gettradeidifexists;
                    		$gethallidifexists = $this->Api_model->gethallidifhallexist($orderhallname,$gettradeidifexists,$ordertradecompany);
                    		$order['ProductType'] = $gethallidifexists;

                    	}

                    }
                    if(preg_match('/[^a-zA-Z\d]/', $order['ProductType'])){
                    	$ordertradename = $order['TradeName'];
                    	$orderhallname  = $order['HallName'];
                    	$parentid = $order['Trade'];
                    	$result = $this->Api_model->checkifHallexist($orderhallname,$parentid,$ordertradecompany);
                    	if($result ==0)
                    	{
                    	$halldetails = array(
                    			'Name'   	=> $orderhallname,
                    			'Parent' 	=> $parentid,
                    			'CompanyId' => $order['CompanyId']
                    			);
                    	$inserthall = $this->Api_model->inserthall($halldetails);
                    	$order['ProductType'] = $inserthall;
                    	//echo "match";
                    	}
                    	else
                    	{
                    		$gethallidifexists = $this->Api_model->gethallidifhallexist($orderhallname,$parentid,$ordertradecompany);
                    		$order['ProductType'] = $gethallidifexists;
                    	}

                    }
                    // $tradename = $order['Trade'];
                    // $hallname = $order['ProductType'];

                    $orderdetails = array(
                        //'OrderId' => $order['OrderId'],
                        'OrderNumber' => $order['OrderNumber'],
                        'CompanyId' => $order['CompanyId'],
                        'Trade' => $order['Trade'],
                        'ProductType' => $order['ProductType'],
                        'TotalOrderUsValue' => $order['TotalOrderUsValue'],
                        'TotalOrderRmbValue' => $order['TotalOrderRmbValue'],
                        'TotalOrderZarValue' => $order['TotalOrderZarValue'],
                        'Total3rdCurrencyValue' => $order['Total3rdCurrencyValue'],
                        'TotalCartons' => $order['TotalCartons'],
                        'TotalBooth' => $order['TotalBooth'],
                        'TotalContainer' => $order['TotalContainer'],
                        'TotalOrderWeight' => $order['TotalOrderWeight'],
                        'TotalOrderCBM' => $order['TotalOrderCBM'],
                        'LastUpdate' => $order['LastUpdate'],
                        'OrderStatus' => $order['OrderStatus'],
                    );
                    //$orderid = $order['OrderId'];
                    $orderid = $order['OrderNumber'];
                    //echo json_encode($this->Api_model->checkoldorder($order['CompanyId'], $order['OrderNumber']));
                    // exit;
                    if (!$this->Api_model->checkoldorder($order['CompanyId'], $order['OrderNumber'])) {
                        /*save order*/
                        //echo $order['OrderNumber'];
                        if ($this->Api_model->checkorderno($order['OrderNumber'])) {
                            $isOrderNo = false;
                            $neworderno = "OD" . mt_rand(100000, 9999999);
                            // starts loop
                            while ($isOrderNo) {
                                $result = $this->Api_model->checkorderno($neworderno);
                                if (!$result) {
                                    $isOrderNo = true;
                                } else {
                                    $neworderno = "OD" . mt_rand(100000, 9999999);
                                }
                            }

                            $order['OrderNumber'] = $neworderno;
                        }
                        // echo $neworderno;
                        //echo "inserting";
                        $orderid = $this->Api_model->saveorder($orderdetails);
                    } else {
                        //echo "updating";
                        /*update order if exists */
                        //echo json_encode($orderid);
                        //print_r($orderdetails);
                        $orderid = $this->Api_model->updateordervalues($orderid, $orderdetails);
                        //echo json_encode($orderid);
                    }

                    $boothlist = $order['boothdetailsbyorderid'];
                    //$i=0;

                    foreach ($boothlist as $booth) {
                    	// $boothimageexist = $this->Api_model->checkifboothimagealreadyexist($booth['BoothId'],$orderid,$booth['CompanyId'],$booth['BusinessCard']);
                    	$boothimageexist = $this->Api_model->checkifboothimagealreadyexist($booth['BoothNumber'],$booth['BusinessCard']);
                    	if($boothimageexist == 1)
                    	{
                    		//echo "alreayexist";
							$filename = $booth['BusinessCard'];
                    	}
                    	else{

                    		//echo "notalreadyexist";
                    	if (($booth['BusinessCard'] != "0") && ($booth['BusinessCard']) && ($booth['BusinessCard'] != "default.jpg")) {

		                   	$file_element_name = $booth['BusinessCard'];
		 					$file_name = $this->Api_model->testsaveimage($file_element_name);
							$this->load->library('image_lib');
							$config_resize['image_library'] = 'gd2';	
							$config_resize['create_thumb'] = TRUE;
							$config_resize['maintain_ratio'] = TRUE;
							$config_resize['master_dim'] = 'height';
							$config_resize['quality'] = "100%";
							//$config_resize['source_image'] = './' . $user_upload_path . $file_name;
							$config_resize['source_image'] = 'assets/testimages/' . $file_name;
						 	$config_resize['width']         = 210;
							$config_resize['height']       = 140;
							// $config_resize['height'] = 140;
							// $config_resize['width'] = 210;
							$this->image_lib->initialize($config_resize);
							$this->image_lib->resize();
							$filename = str_replace('.jpg', '_thumb.jpg', $file_name);

                            // $base64 = $booth['BusinessCard'];
                            // $filename = $this->Api_model->saveimage($base64);
                        } else {
                            $filename = "default.jpg";
                        }
                    	}
                        


                    if(preg_match('/[^a-zA-Z\d]/', $booth['TradeId'])) {
                    	//echo "trade";
	 					$ordertradename = $booth['TradeName'];
	                    $orderhallname  = $booth['HallName'];
	                    $ordertradecompany = $booth['CompanyId'];
                    	$result = $this->Api_model->checkifTradeexist($ordertradename,$ordertradecompany);

                    	if($result ==0)
                    	{
                    		$tradedetails = array(
                    			'Name' 		=> $ordertradename,
                    			'Parent'    => 0,
                    			'CompanyId' => $booth['CompanyId']
                    			);

                    		$inserttrade = $this->Api_model->inserttrade($tradedetails);
                    		$booth['TradeId'] = $inserttrade;
                    		$halldetails = array(
                    			'Name'   	=> $orderhallname,
                    			'Parent' 	=> $inserttrade,
                    			'CompanyId' => $order['CompanyId']
                    			);
                    		$inserthall = $this->Api_model->inserthall($halldetails);
                    		$booth['HallId'] = $inserthall;

                    	}
                    	else 
                    	{
                    		//echo "exist";
                    		$gettradeidifexists = $this->Api_model->gettradeidifTradeexist($ordertradename,$ordertradecompany);
                    		$booth['TradeId'] = $gettradeidifexists;
                    		$gethallidifexists = $this->Api_model->gethallidifhallexist($orderhallname,$gettradeidifexists,$ordertradecompany);
                    		$booth['HallId'] = $gethallidifexists;

                    	}

                    }
                    if(preg_match('/[^a-zA-Z\d]/', $booth['HallId'])){
                    	// /echo "hall";
                    	$ordertradename = $booth['TradeName'];
                    	$orderhallname  = $booth['HallName'];
                    	$ordertradecompany = $booth['CompanyId'];
                    	$parentid = $booth['TradeId'];
                    	$result = $this->Api_model->checkifHallexist($orderhallname,$parentid,$ordertradecompany);
                    	if($result ==0)
                    	{
                    	$halldetails = array(
                    			'Name'   	=> $orderhallname,
                    			'Parent' 	=> $parentid,
                    			'CompanyId' => $order['CompanyId']
                    			);
                    	$inserthall = $this->Api_model->inserthall($halldetails);
                    	$booth['HallId'] = $inserthall;
                    	//echo "match";
                    	}
                    	else
                    	{
                    		$gethallidifexists = $this->Api_model->gethallidifhallexist($orderhallname,$parentid,$ordertradecompany);
                    		$booth['HallId'] = $gethallidifexists;
                    	}

                    }
	                    // $boothtradeid  = 	$booth['TradeId'];
	                    // $boothhallid   =     $booth['HallId'];
	                    // echo $boothtradeid.'-'.$boothhallid;
                        $boothdetails = array(
                            'OrderId' => $orderid,
                            'CompanyId' => $booth['CompanyId'],
                            'BoothNumber' => $booth['BoothNumber'],
                            'BoothName' => $booth['BoothName'],
                            'Phone' => $booth['Phone'],
                            'BusinessScope' => $booth['BusinessScope'],
                            'HallId' => $booth['HallId'],
                            'TradeId' => $booth['TradeId'],
                            'BusinessCard' => $filename,
                            'ValuePurchase' => $booth['ValuePurchase'],
                            'ValuePurchaseRMB' => $booth['ValuePurchaseRMB'],
                            'BoothStatus' => $booth['BoothStatus'],
                        );
                        if (!$this->Api_model->getBoothById($booth['BoothNumber'])) {

                            // main booth not exist then save

                            $newboothinfo = array(
                                'BoothNumber' => $booth['BoothNumber'],
                                'BoothName' => $booth['BoothName'],
                                'Phone' => $booth['Phone'],
                                'BusinessScope' => $booth['BusinessScope'],
                                'BusinessCard' => $filename,
                            );
                            $this->Api_model->saveboothinfo($newboothinfo);
                        }

                        //$boothid = $booth['BoothId'];
                        $boothid = $booth['BoothNumber'];
                        $companyid = $order['CompanyId'];

                        // echo json_encode($companyid);
                        // echo json_encode($boothid);
                        //echo json_encode($this->Api_model->checkboothid($boothid,$orderid,$companyid));
                        
                       
                        if ($this->Api_model->checkboothid($boothid,$orderid,$companyid)) {

                            // if booth exist
                             
                            //echo "strings"
                            //echo $boothid;
                            $boothid = $this->Api_model->updateboothdetails($boothid, $orderid,$companyid, $boothdetails);
                            //echo json_encode($boothid);
                            //print_r($boothid) ;
                            //$boothid = $this->Api_model->getlastid
                            // echo $boothid;
                        } else {

                            // not exist
                            // echo "string";
                            // echo $boothid;
                            $boothid = $this->Api_model->savebooth($boothdetails);
                            
                        }
                        $bthDetauils[] = $boothid;

                        
                    }

                    $itemlist = $order['itemdetailsbyboothorderid'];
                    foreach ($itemlist as $item) {

                        	$itemimageexist = $this->Api_model->checkifitemimagealreadyexist($companyid,$orderid,$boothid,$item['ItemId'],$item['Picture']);
                        	if($itemimageexist == 1) {
                        		$filename = $item['Picture'];
                        	}
                        	else
                        	{
                        		if (($item['Picture'] != "0") && ($item['Picture']) && ($item['Picture'] != "default.jpg")) 
                        		{

                        		$file_element_name = $item['Picture'];
			 					$file_name = $this->Api_model->testsaveimage($file_element_name);
								$this->load->library('image_lib');
								$config_resize['image_library'] = 'gd2';	
								$config_resize['create_thumb'] = TRUE;
								$config_resize['maintain_ratio'] = TRUE;
								$config_resize['master_dim'] = 'height';
								$config_resize['quality'] = "100%";
								//$config_resize['source_image'] = './' . $user_upload_path . $file_name;
								$config_resize['source_image'] = 'assets/testimages/' . $file_name;
							 	$config_resize['width']         = 210;
								$config_resize['height']       = 140;
								// $config_resize['height'] = 140;
								// $config_resize['width'] = 210;
								$this->image_lib->initialize($config_resize);
								$this->image_lib->resize();
								$filename = str_replace('.jpg', '_thumb.jpg', $file_name);	

                                // $base64 = $item['Picture'];
                                // //echo $base64;
                                // $filename = $this->Api_model->saveimage($base64);
                                // $base64 = $item['Picture'];
                                // $filename = $this->Api_model->saveimage($base64);
                            	} 	else 
                            	{
                                $filename = "default.jpg";
                            	}
                        	}
                            

                            $itemid = $item['ItemId'];

                            $gettranslationdetails = $this->Api_model->gettranslationdetails($companyid);
                            foreach ($gettranslationdetails as $key) {
                            	$translationfrom = $key->TransilateFrom;
                            	$translationto = $key->TransilateTo;
                            	if($translationfrom == $translationto){

                            	}
                            	else
                            	{
                            		$apiKey = 'AIzaSyAemHXikEKrzxKdKON_oPv88lk0DpfxI8s';
								    $text = $item['DescriptionFrom'];
								    $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&source='.$translationfrom.'&target='.$translationto.'';

								    $handle = curl_init($url);
								    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
								    $response = curl_exec($handle);                 
								    $responseDecoded = json_decode($response, true);
								    curl_close($handle);
								    // print_r(json_decode($response, true));
								    //echo 'Source: ' . $text . '<br>';
								    //echo 'Translation: ' . $responseDecoded['data']['translations'][0]['translatedText'];
								    $item['DescriptionTo'] = $responseDecoded['data']['translations'][0]['translatedText'];


                            	}
                            	//echo $translationfrom.'-'.$translationto;
                            }
                            //print_r($gettranslationdetails);

                            $itemdetails = array(
                              
                                'CompanyId' => $item['CompanyId'],
                                'OrderId' => $orderid,
                                'BoothId' => $boothid,
                                'ContainerId' => $item['ContainerId'],
                                'ItemNo' => $item['ItemNo'],
                                'DescriptionFrom' => $item['DescriptionFrom'],
                                'DescriptionTo' => $item['DescriptionTo'],
                                'UnitperCarton' => $item['UnitperCarton'],
                                'WeightofCarton' => $item['WeightofCarton'],
                                'Length' => $item['Length'],
                                'Width' => $item['Width'],
                                'Height' => $item['Height'],
                                'CBM' => $item['CBM'],
                                'Picture' => $filename,
                                'PriceInUs' => $item['PriceInUs'],
                                'PriceInRmb' => $item['PriceInRmb'],
                                'PriceInThirdCurrency' => $item['PriceInThirdCurrency'],
                                'UnitQuantity' => $item['UnitQuantity'],
                                'CartonQuantity' => $item['CartonQuantity'],
                                'TotalPriceInUS' => $item['TotalPriceInUS'],
                                'TotalPriceInRMB' => $item['TotalPriceInRMB'],
                                'TotalPriceIn3rdCurrency' => $item['TotalPriceIn3rdCurrency'],
                                'Date' => $item['Date'],
                            );
                            //$itemid = $item['ItemId'];
                            $companyid = $order['CompanyId'];
                            //$boothid = $booth['BoothId'];
                            //echo json_encode($companyid);
                            
                            //echo json_encode($this->Api_model->checkolditem($companyid,$orderid,$boothid,$itemid));
                            // if ($this->Api_model->checkolditem($itemid, $orderid)) {
                            // echo json_encode($companyid);
                            // echo json_encode($orderid);
                            // echo json_encode($boothid);
                            //echo json_encode($itemid);
                             if ($this->Api_model->checkolditem($companyid,$orderid,$boothid,$itemid)) {

                                // update item details
                                // echo "updating";
                                // print_r($itemdetails);
                                // /$this->Api_model->updateitemorder($itemid, $orderid, $itemdetails);
                                $this->Api_model->updateitemorder($companyid,$orderid,$itemdetails,$itemid,$boothid);
                            } else {

                                // new item
                                //echo "inserting";
                                $itemid = $this->Api_model->saveitemdetails($itemdetails);
                            }
                            //echo $itemid;
                            $containers = explode(',', $item['ContainerId']);
                            $obj = new stdClass;
                            $obj->Itemid = $itemid;
                            $obj->Boothid = $boothid;
                            $obj->Conteiners = $containers;
                            $itemcontainer[] = $obj;
                    }
                   // print_r($itemcontainer);
                    $containerlist = $order['containerdetailsbyorderid'];
                   
					foreach ($containerlist as $container) {
						$oldcontainerid = $container['ContainerId'];
						$containertype = $container['ContainerType'];
						$containerstatus = $container['Status'];
						$ordercontainer = array(
							'OrderId' => $orderid, // the value of $result_oredersave is last entering oder id
							'ContainerType' => $container['ContainerType'],
							'CompanyId' => $container['CompanyId'],
							'FilledWeight' => $container['FilledWeight'],
							'FilledVolume' => $container['FilledVolume'],
							'Volumepercentage' => $container['Volumepercentage'],
							'Weightpercentage' => $container['Weightpercentage'],
							'Status' => $container['Status'],
							'ItemSaveStatus' => $container['ItemSaveStatus'],
						);
						if ($this->Api_model->checkoldcontainer($oldcontainerid,$containertype, $orderid,$companyid)) {

							// update item details
// echo "update container";
							
							$newcontinerid = $this->Api_model->updatecontainer($ordercontainer, $orderid, $companyid,$containertype,$oldcontainerid);

						} else {

							// new item

							$newcontinerid = $this->Api_model->savecontainertype($ordercontainer);
							
							
							// exit();

							
							//echo json_encode($itemid);
							
						}
						foreach ($itemcontainer as $container) {
								$itemid = $container->Itemid;
								$boothid = $container->Boothid;
                        		//$list = "";
								$i = 0;
								foreach ($container->Conteiners as $itemconatinerid) {
									//echo $itemconatinerid;
									if ($oldcontainerid == $itemconatinerid) {
										
										$itemconatinersid = $newcontinerid;
										//echo json_encode($itemconatinerid) ;
										//echo $itemconatinerid;
										if ($i == 0) {
                                			$list = $itemconatinersid;
                            		} 	else {
                                			$list = $list . "," . $itemconatinersid;
                            			}

                            			$itemcontainers = array(
		                            'ContainerId' => $list,
		                        );
		                        //echo json_encode($itemid);
		                        //print_r($itemcontainers);
								$this->Api_model->updateitemorder($companyid,$orderid, $itemcontainers,$itemid,$boothid);
									}
								$i++;
								
								}

								//exit();
							}
										
					}
					// print_r($itemcontainers);
					// exit();
					
                    // foreach ($itemcontainer as $container) {
                    // 	//print_r($container);
                    // 	$itemid = $container->Itemid;
                    // 	//echo json_encode($itemid);
                    //     $list = "";
                    //     $i = 0;
                    //     foreach ($container->Conteiners as $itemconatinerid) {
                    //         if ($i == 0) {
                    //             $list = $itemconatinerid;
                    //         } else {
                    //             $list = $list . "," . $itemconatinerid;
                    //         }

                    //         $i++;
                    //     }

                    //     //$container->Itemid as $itemsid){
                    //     	 //$list = $container->Itemid
                    //     //}


                    //     // update item

                    //     $itemcontainer = array(
                    //         'ContainerId' => $list,
                    //     );
                    //     	 //print_r($list);
                    //     //echo json_encode($itemcontainer);
                    //     // echo json_encode($itemid);
                    //     //exit();
                    //    //$this->Api_model->updateitemorder($companyid,$orderid, $itemcontainer,$itemid);
                    // }
                }

                return $this->set_response(['resultCode' => 1, 'message' => "Success"], REST_Controller::HTTP_OK);
            }
        } catch (Exception $e) {
            return $this->set_response(['resultCode' => 0, 'message' => $e->getMessage()], REST_Controller::HTTP_OK);
        }
    }
}?>