<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Api_model');
        // $this->load->model('Token_model');
    }
   

      function userorderdetails()
    {
        
         /*check request values is null*/
        if(!$_GET['token'])
        { /*error responce*/

    
             
                    return $this->output->set_content_type('application/json')->set_output(json_encode(array(
                        'resultCode' => 0,
                                'message'    => 'Value missing'
                    )));
        }
        else
        {
            $val_token = $_GET['token'];
            /*fetch id using token value*/
             $orderemail = $this->Api_model->selectordermail($val_token);
             foreach ($orderemail as $key) {
                $val_orderemail = $key->OrderEmail;
             }
            
            $companyid = $this->Api_model->fetchcompanyid($val_orderemail);
            foreach ($companyid as $key)
                        {
                $val_companyid = $key->CompanyID;
            }
           
            /*calling model sql query*/
            if($val_companyid)
            {
            $result = $this->Api_model->orderdetails($val_companyid);
                 if($result)
                { //success responce
                    


                   return $this->output->set_content_type('application/json')->set_output(json_encode(array(
                         'message'    => 'success',
                    'resultCode' => 1,
                    'data'       => $result,
                    )));
                }
                 else
                {   //error reponce
              

                      return $this->output->set_content_type('application/json')->set_output(json_encode(array(
                       'resultCode' => 0,
                                                'message'    => 'empty result set'
                    )));


               }
           }
           else
           {
              
  return $this->output->set_content_type('application/json')->set_output(json_encode(array(
                       'resultCode' => 0,
                        'message'    => 'No data exist'.$val_companyid,
                    )));


           }
     }
    }
}
?>