<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Login_model');
		$this->load->model('Api_model');
		$this->load->model('Register_model');
		$this->load->model('Profile_model');
		$token=$this->session->userdata('userToken');
		$account     = $this->Api_model->selectordermail($token);
		if(!$account){
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');
		}

	}
		function index()
	{
		// $data['page_title'] = 'ISW : Sign In';
		// $this->load->helper(array('form'));
		// $this->load->view('header');
		if($this->session->userdata('userToken'))
		{
			$log = $this->session->userdata('log');
			if($log = 'login'){
				redirect('Login/dashboard','refresh');	
			}
			else{
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');
				$this->load->view('index');
			}
		}
		else{
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');
			$this->load->view('index');
		}
		
		// $this->load->view('footer');
	}
	function dashboard()
	{
        if($this->session->userdata('userToken'))
		{
			$log = $this->session->userdata('log');
			if($log = 'login'){
				$this->load->view('dashboard');	
			}
			else{
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');	
			}
		}
		else
		{
 			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');	
		}
		//$this->load->view('layouts/footer');
	}
  	function profile()
	{	 if($this->session->userdata('userToken'))
		{
			$log = $this->session->userdata('log');
			if($log = 'login'){
				$this->load->view('profile');
			}
			else{
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');	
			}
		}
		else
		{
 			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');	
		}
		
	}
	function setup()
	{
        if($this->session->userdata('userToken')){
			$log = $this->session->userdata('log');
			if($log = 'login'){
				$this->load->view('companysetup');
			}
			else{
			       	$this->session->unset_userdata('username');
					$this->session->unset_userdata('userToken');
					$this->session->unset_userdata('useremail');
					$this->session->unset_userdata('usertype');	
					redirect('Login','refresh');	
				}
		}
		else{
 			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');	
		}
		
	}
	function orders()
	{
		if($this->session->userdata('userToken'))
		{
			$log = $this->session->userdata('log');
			if($log = 'login'){
				$this->load->view('orders');	
			}
			else{
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');	
			}
		}
		else
		{
 			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');	
		}
		
	}
	function booth()
	{
		$log=$this->session->userdata('log');
		if($log = 'login'){
			$this->load->view('booth');
		}
		else{
			
	       	$this->session->unset_userdata('username');
			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');
		}
	}
 	function newOrder()
  	{
  	
	if($this->session->userdata('userToken'))
		{
			$log = $this->session->userdata('log');
			if($log = 'login'){
				$this->load->view('neworder');
			}
			else{
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');	
			}
		}
		else
		{
 			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');	
		}
    }
	function errorPage()
	{
		$log=$this->session->userdata('log');
		if($log = 'login'){
		$this->load->view('error_one');
	}
	else{
	
       	$this->session->unset_userdata('username');
		$this->session->unset_userdata('userToken');
		$this->session->unset_userdata('useremail');
		$this->session->unset_userdata('usertype');	
		redirect('Login','refresh');
	}
	}
	function vieworder()
	{
		if($this->session->userdata("userToken")){
		  	$log=$this->session->userdata('log');
			if($log = 'login'){
				// if($_GET['id']&&$_GET['status'])
				// {
			 		$data['orderid'] = $_GET['id'];
					$data['status'] = $_GET['status'];
					$this->load->view('vieworder',$data);
			    // }
			    // else
			    // {
			    // 	redirect('Login/orders','refresh');
			    // }
				
			}
			else{
				
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');
			}
		}
		else{
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');
		}
		
	}

	function forgot()
	{
		$this->load->view('forgotpassword_view');
	}
	function reset()
	{
		$this->load->view('reset');
	}
	function item()
	{
	     if($this->session->userdata("userToken")){
		  	$log=$this->session->userdata('log');
			if($log = 'login'){
		 		$this->load->view('item');
			}
			else{
				
		       	$this->session->unset_userdata('username');
				$this->session->unset_userdata('userToken');
				$this->session->unset_userdata('useremail');
				$this->session->unset_userdata('usertype');	
				redirect('Login','refresh');
			}
		}
		else{
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('useremail');
			$this->session->unset_userdata('usertype');	
			redirect('Login','refresh');
		}
	
	}
	function continoueorder()
	{
		$log=$this->session->userdata('log');
		if($log = 'login'){
		$this->load->view('item');
	}
	else{
		
       	$this->session->unset_userdata('username');
		$this->session->unset_userdata('userToken');
		$this->session->unset_userdata('useremail');
		$this->session->unset_userdata('usertype');	
		redirect('Login','refresh');
	}
	}
	function register()
  	{
	  $this->load->view('register');
	}
	function printorder()
	{
		$this->load->view('printorder');
	}
	function logout()
  	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('userToken');
		$this->session->unset_userdata('useremail');
		$this->session->unset_userdata('usertype');
		$this->session->set_userdata('log','logout');
		redirect('Login','refresh');
	}
	function checkuserexist()
	{
		$email = $_GET['id'];
		$result = $this->Login_model->checkuseremailexists($email);

		echo json_encode($result);
		//    echo json_encode("Success");
	}
	function chengepassword()
	{
		$_GET             = json_decode(file_get_contents('php://input'),true);
		$useremail         = get_cookie('email');
		$otp               = $_GET['otp'];
		$password          = $_GET['password'];
		$encryptedpassword = md5($password);
		$result            = 2;
		$shuffled          = str_shuffle($password);
		//$otpcookie=get_cookie('otp');
		$otpcheck    = $this->Register_model->checkotpexists($useremail);
		foreach ($otpcheck as $key) {
		$userotp = $key->Otp;
		}
		 //echo json_encode($userotp);
		 if($userotp==$otp){	
		 	
			//echo "equal";
			$usercheck    = $this->Register_model->checkuserexists($useremail);
			if($usercheck)
			{
					 $userregisterdetails = array('Password' => $encryptedpassword);
					 $result              = $this->Profile_model-> changepassword($userregisterdetails,$useremail);
					 $result = array('message' => "true");
					 echo json_encode($result);
			}
			else {
				$usernotexist="User Not Exist";
				 echo json_encode($usernotexist);
			}
		}
		else{
			//echo"not equal";
        $result = array('message' => "false");
				echo json_encode($result);
		}
	}

	function loginverification()
	{
			$username         = $_GET['uname'];
			$password         = $_GET['password'];
			$encryptpassword  = md5($password);
			$sucess           = '1';
			$error            = '2';
			$tokenString = 'ak8QW83oPg1nOd1E4c2Xa4g'; /*random charactors for create token*/
			$token       = str_shuffle($tokenString); /*generate token*/
			$logindata        = $this->Login_model->login($username,$encryptpassword);
			if($logindata    != null){
			foreach ($logindata as $value){
			$usertype    = $value->UserType;
			$useremail   = $value->Email;
			}
			$authotoken = array('userToken' => $token);
		//	$this->Api_model->updateUserToken($authotoken,$useremail);
			/** save user token**/
			$usertoken = array('Token'=>$token,
												 'OrderEmail'=>$useremail);
			$this->Login_model->saveusertoken($usertoken);
			/*update token value in the company table*/
			$this->session->set_userdata('username',$username);
			$this->session->set_userdata('userToken',$token);
			$this->session->set_userdata('useremail',$useremail);
			$this->session->set_userdata('usertype',$usertype);
			$this->session->set_userdata('log','login');
			echo json_encode($sucess);
			}
			else
			{
			echo json_encode($error);
			}
	}
}
