<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Homepage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

	}
		function index()
	{
		
		$this->load->view('homepage');
	}
	
}
