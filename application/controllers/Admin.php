<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Admin extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Admin_model');

	}
	/*
		     * LOGIN VIEW *
	*/
	public function index() {
		if (($this->session->userdata('adminToken'))) {
			$log = $this->session->userdata('adminlog');
			if ($log = 'login') {
				redirect('Admin/manage', 'refresh');
			} else {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->load->view('adminindex.php');
			}
		} else {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('userToken');
			$this->session->unset_userdata('usertype');
			$this->load->view('adminindex.php');
		}
	}
	function logout() {

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('adminToken');
		$this->session->unset_userdata('usertype');
		$this->session->set_userdata('adminlog', 'logout');
		// $this->index();
		$this->load->helper('url');
		redirect('Admin/index', 'refresh');
	}
	public function login() {
		try {
			if ($_GET['uname'] && $_GET['password']) {
				$username = $_GET['uname'];
				$password = $_GET['password'];
				$encryptpassword = md5($password);
				$tokenString = 'ak8QW83oPg1nOd1E4c2Xa4g'; /*random charactors for create token*/
				$token = str_shuffle($tokenString); /*generate token*/
				$logindata = $this->Admin_model->login($username, $encryptpassword);
				if ($logindata) {
					$usertype = $logindata->UserType;
					$username = $logindata->UserName;
					$authotoken = array('userToken' => $token);
					$usertoken = array('Token' => $token,
						'OrderEmail' => $username);
					$this->Admin_model->saveusertoken($usertoken);
					$this->session->set_userdata('username', $username);
					$this->session->set_userdata('adminToken', $token);
					$this->session->set_userdata('usertype', $usertype);
					$this->session->set_userdata('adminlog', 'login');
					$result = array(
						'message' => 'true',
						'comment' => 'Admin Login Successfull',
					);

				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Invalid Credentials',
					);
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Missing Values',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	public function manage() {
		$log = $this->session->userdata('adminlog');
		if ($log == 'login') {
			$this->load->view('adminmanage.php');
		} else {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('adminToken');
			$this->session->unset_userdata('usertype');
			redirect('Admin/index', 'refresh');
		}

	}
	public function viewdata() {
		$log = $this->session->userdata('adminlog');
		if ($log == 'login') {
			$this->load->view('adminview.php');
		} else {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('adminToken');
			$this->session->unset_userdata('usertype');
			redirect('Admin/index', 'refresh');
		}

	}
	function SaveAgent() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['agentname'] && $_POST['agentoffice'] && $_POST['companyphone'] && $_POST['contact']) {

				$agentname = $_POST['agentname'];
				$agentoffice = $_POST['agentoffice'];
				$companyphone = $_POST['companyphone'];
				$contact = $_POST['contact'];
				$faxnum = $_POST['faxnum'];
				$businessyear = $_POST['businessyear'];
				$goldyear = $_POST['goldyear'];

				$contactdetails = array('CompanyName' => $agentname,
					'OfficeAddress' => $agentoffice,
					'PhoneNumber' => $companyphone,
					'BusinessYear' => $businessyear,
					'FaxNumber' => $faxnum,
					'GoldAgentYears' => $goldyear,
				);

				$agentid = $this->Admin_model->saveagentcompany($contactdetails);
				if ($agentid) {
					for ($i = 0; $i < count($contact); $i++) {
						if ($contact[$i]['Image'] != "") {
							$ImagePath = $this->Admin_model->saveimage($contact[$i]['Image']);

						} else {
							$ImagePath = "goldagent.jpg";
						}
						$contactdetails = array('GoldAgentId' => $agentid,
							'Name' => $contact[$i]['Name'],
							'Email' => $contact[$i]['Email'],
							'Mobile' => $contact[$i]['Mobile'],
							'Skype' => $contact[$i]['Skype'],
							'ImageName' => $ImagePath,
						);
						$this->Admin_model->saveagentcontacts($contactdetails);
					}
					//saved
					$result = array(
						'message' => 'true',
						'comment' => 'Agent Saved',
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Error Occured',
					);
				}

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	function GetTradeList() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			// if ($this->session->userdata('adminToken')) {
			$parent = 0;
			$tradelist = $this->Admin_model->gettradorhalllist($parent);
			if ($tradelist) {
				$result = array(
					'message' => 'true',
					'tradelist' => $tradelist,
				);
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No records found',
				);
			}

			// } else {
			//     $result = array(
			//         'message' => 'false',
			//         'comment' => 'No Access'
			//     );
			// }
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}

		echo json_encode($result);
	}
	function SaveTradeOrHall() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}

			// if ($tradename!="" && $parent!="") {

			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			$name = $_POST['name'];
			$parent = $_POST['parent'];
			$result_tradeorhall = $this->Admin_model->checktradeorhall($name, $parent);
			/** match trade name is exists or not **/
			if (!$result_tradeorhall) {
				$tradedetails = array(
					'Name' => $name,
					'CompanyId' => '0',
					'Parent' => $parent,
				);
				/** save trade or hall details**/
				$id = $this->Admin_model->savetradeorhall($tradedetails);
				if ($id) {
					/** success response of save trade or hall **/
					$result = array(
						'message' => 'true',
						'Id' => $id,
					);
				} else {
					/** false response **/
					$result = array(
						'message' => 'false',
						'comment' => 'Database error occured',
					);

				}
			} else {
				/** false response for same order no**/
				$result = array(
					'message' => 'false',
					'comment' => 'Exists',
				);
			}
			// } else {
			//     /** false response for missing input data**/
			//     $result = array(
			//         'message' => 'false',
			//         'comment' => 'Missing values'
			//     );
			// }
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function SaveContainer() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['container'] && $_POST['maxvolume'] && $_POST['maxweight']) {

				$container = $_POST['container'];
				$maxvolume = $_POST['maxvolume'];
				$maxweight = $_POST['maxweight'];
				$isoldconatiner = $this->Admin_model->checkconatinerduplicate($container, $maxvolume, $maxweight);
				if ($isoldconatiner) {
					$result = array(
						'message' => 'false',
						'comment' => 'Exists',
					);
				} else {
					$container = array('Container' => $container,
						'MaximumWeight' => $maxweight,
						'MaximumVolume' => $maxvolume,
					);
					$containerid = $this->Admin_model->savecontainer($container);
					if ($containerid) {
						//saved
						$result = array(
							'message' => 'true',
							'comment' => 'Container Saved',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Error Occured',
						);
					}
				}

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	function GetGoldAgent() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$page = $_GET['page'];
			$goldagents = $this->Admin_model->getallgoldagents($page);
			if ($goldagents) {
				foreach ($goldagents as $key) {
					$goldagentcontact = $this->Admin_model->getgoldagentcontats($key->Id);
					$key->Contact = $goldagentcontact;
				}
				$result = array(
					'message' => 'true',
					'comment' => 'Success',
					'goldagents' => $goldagents,
					'page' => $page,
				);

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No data',

				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function ChangePassword() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('log', 'logout');
				redirect('Admin/index', 'refresh');
			}

			$_POST = json_decode(file_get_contents('php://input'), true);
			$username = $this->session->userdata('username');
			$password = $_POST['password'];
			$oldpassword = md5($_POST['oldpassword']);
			$encryptedpassword = md5($password);
			$user = $this->Admin_model->login($username, $oldpassword);
			if ($user) {
				$newpassword = array(
					'Password' => $encryptedpassword,
				);
				$result = $this->Admin_model->changepassword($newpassword, $username);
				if ($result) {
					$result = array(
						'message' => 'true',
						'comment' => 'Password Changed',
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Error Occured',
					);
				}

				echo json_encode($result);
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Old Password Wrong',
				);
				echo json_encode($result);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
	}
	function GetHallList() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$halllist = $this->Admin_model->gethalllist();
			if ($halllist) {

				$result = array(
					'message' => 'true',
					'comment' => 'Success',
					'halllist' => $halllist,
				);

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No data',

				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function GetContainerList() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$containerlist = $this->Admin_model->getconatinerist();
			if ($containerlist) {

				$result = array(
					'message' => 'true',
					'comment' => 'Success',
					'containerlist' => $containerlist,
				);

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No data',

				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function DeleteContainer() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['containerid']) {
				$containerid = $_POST['containerid'];
				$containerlist = $this->Admin_model->checksavedcontainer($containerid);
				if ($containerlist) {
					$result = array(
						'message' => 'false',
						'comment' => 'Not Allowed',
					);
				} else {
					$result = $this->Admin_model->deletecontainer($containerid);
					if ($result) {
						$result = array(
							'message' => 'true',
							'comment' => 'Delete success',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Delete failed',
						);
					}
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}

		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function UpdateContainer() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['container'] && $_POST['maxvolume'] && $_POST['maxweight'] && $_POST['containerid']) {

				$container = $_POST['container'];
				$maxvolume = $_POST['maxvolume'];
				$maxweight = $_POST['maxweight'];
				$containerid = $_POST['containerid'];
				$isoldconatiner = $this->Admin_model->updateconatinerduplicate($containerid, $container, $maxvolume, $maxweight);
				if ($isoldconatiner) {
					$result = array(
						'message' => 'false',
						'comment' => 'Exists',
					);
				} else {
					$container = array('Container' => $container,
						'MaximumWeight' => $maxweight,
						'MaximumVolume' => $maxvolume,
					);
					$result = $this->Admin_model->updatecontainer($containerid, $container);
					if ($result) {
						//saved
						$result = array(
							'message' => 'true',
							'comment' => 'Container Updated',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Error Occured',
						);
					}
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	function DeleteTradeOrHall() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['id']) {
				$id = $_POST['id'];
				$orders = $this->Admin_model->checksavedtradeorhall($id);
				if ($orders) {
					$result = array(
						'message' => 'false',
						'comment' => 'Not Allowed',
					);
				} else {
					$result = $this->Admin_model->deletetradeorhall($id);
					if ($result) {
						$result = array(
							'message' => 'true',
							'comment' => 'Delete success',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Delete failed',
						);
					}
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}

		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function UpdateTradeOrHall() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['name'] && $_POST['id']) {
				$name = $_POST['name'];
				$parent = $_POST['parent'];
				$id = $_POST['id'];
				$result_tradeorhall = $this->Admin_model->checktradeorhall($name, $parent);
				if (!$result_tradeorhall) {
					$tradedetails = array(
						'Name' => $name,
						'Parent' => $parent,
					);
					$result = $this->Admin_model->updatetradeorhall($id, $tradedetails);
					if ($result) {
						//saved
						$result = array(
							'message' => 'true',
							'comment' => 'Container Updated',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Error Occured',
						);
					}
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Exists',
					);
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	public function user() {
		$log = $this->session->userdata('adminlog');
		if ($log == 'login') {
			$this->load->view('userview.php');
		} else {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('adminToken');
			$this->session->unset_userdata('usertype');
			redirect('Admin/index', 'refresh');
		}
	}
	function GetUsedetails() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$page = $_GET['page'];
			// if(!$page){
			//     $page=1;
			// }
			$userlist = $this->Admin_model->getaccountdetails($page);
			if ($userlist) {
				$result = array(
					'message' => 'true',
					'comment' => 'Success',
					'userlist' => $userlist,
					'page' => $page,
				);
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No data',

				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	public function agents() {
		$log = $this->session->userdata('adminlog');
		if ($log == 'login') {
			$this->load->view('agentview.php');
		} else {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('adminToken');
			$this->session->unset_userdata('usertype');
			redirect('Admin/index', 'refresh');
		}
	}
	function GetDefault() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$default = $this->Admin_model->getdefaultdetails();
			if ($default) {
				$result = array(
					'message' => 'true',
					'comment' => 'Success',
					'default' => $default,
				);
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'No data',

				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function UpdateDefault() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['DefaultValues']) {
				$default = $_POST['DefaultValues'];
				if ($this->Admin_model->getdefaultdetails()) {
					$result = $this->Admin_model->updatedefault($default);
				} else {
					$result = $this->Admin_model->insertdefault($default);
				}
				if ($result) {
					//saved
					$result = array(
						'message' => 'true',
						'comment' => 'Default values updated',
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Error Occured',
					);
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	function DeleteAgent() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['GoldAgentId']) {
				$id = $_POST['GoldAgentId'];
				$orders = $this->Admin_model->checkusedgoldagent($id);
				if ($orders) {
					$result = array(
						'message' => 'false',
						'comment' => 'Not Allowed',
					);
				} else {
					$result = $this->Admin_model->deletegoldagent($id);
					$resultcontacts = $this->Admin_model->deletegoldagentcontact($id);
					if ($result) {
						$result = array(
							'message' => 'true',
							'comment' => 'Delete success',
						);
					} else {
						$result = array(
							'message' => 'false',
							'comment' => 'Delete failed',
						);
					}
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}

		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function DeleteUser() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['UserEmail']) {
				$email = $_POST['UserEmail'];
				$loginresult = $this->Admin_model->deletelogin($email);
				$accountresult = $this->Admin_model->deleteaccount($email);
				$companyresult = $this->Admin_model->deletecompany($email);
				$tokenresult = $this->Admin_model->deletetoken($email);
				if ($loginresult) {
					$result = array(
						'message' => 'true',
						'comment' => 'Delete success',
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Delete failed',
					);
				}

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}

		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
	function UpdateGoldAgent() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$token = $this->session->userdata('adminToken');
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['agentname'] && $_POST['agentoffice'] && $_POST['companyphone'] && $_POST['contact']) {

				$agentname = $_POST['agentname'];
				$agentoffice = $_POST['agentoffice'];
				$companyphone = $_POST['companyphone'];
				$contact = $_POST['contact'];
				$faxnum = $_POST['faxnum'];
				$businessyear = $_POST['businessyear'];
				$goldyear = $_POST['goldyear'];
				$goldagentid = $_POST['agentid'];
				$contactdetails = array('CompanyName' => $agentname,
					'OfficeAddress' => $agentoffice,
					'PhoneNumber' => $companyphone,
					'BusinessYear' => $businessyear,
					'FaxNumber' => $faxnum,
					'GoldAgentYears' => $goldyear,
				);

				$agentid = $this->Admin_model->updateagentcompany($goldagentid, $contactdetails);
				$resultcontacts = $this->Admin_model->deletegoldagentcontact($goldagentid);
				if ($agentid) {
					for ($i = 0; $i < count($contact); $i++) {
						if ($contact[$i]['Image'] != "") {
							$ImagePath = $this->Admin_model->saveimage($contact[$i]['Image']);

						} else if ($contact[$i]['ImageName']) {
							$ImagePath = $contact[$i]['ImageName'];
						} else {
							$ImagePath = "goldagent.jpg";
						}
						$contactdetails = array('GoldAgentId' => $goldagentid,
							'Name' => $contact[$i]['Name'],
							'Email' => $contact[$i]['Email'],
							'Mobile' => $contact[$i]['Mobile'],
							'Skype' => $contact[$i]['Skype'],
							'ImageName' => $ImagePath,
						);
						// $contactid=$contact[$i]['Id'];
						$resultcontact = $this->Admin_model->saveagentcontacts($contactdetails);
						if ($resultcontact) {
							$contact[$i] = $contactdetails;
						}
					}
					//saved
					$result = array(
						'message' => 'true',
						'comment' => 'Agent Saved',
						'Contact' => $contact,
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Error Occured',
					);
				}

			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);

		}
		echo json_encode($result);
	}
	function DeactivateAgent() {
		try {
			if (!$this->session->userdata('adminToken')) {
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('adminToken');
				$this->session->unset_userdata('usertype');
				$this->session->set_userdata('adminlog', 'logout');
				redirect('Admin/index', 'refresh');
			}
			$_POST = json_decode(file_get_contents('php://input'), true);
			if ($_POST['GoldAgentId']) {
				$id = $_POST['GoldAgentId'];
				$result = $this->Admin_model->deactivategoldagent($id);
				if ($result) {
					$result = array(
						'message' => 'true',
						'comment' => 'Delete success',
					);
				} else {
					$result = array(
						'message' => 'false',
						'comment' => 'Deactivate failed',
					);
				}
			} else {
				$result = array(
					'message' => 'false',
					'comment' => 'Value Missing',
				);
			}
		} catch (Exception $e) {
			$result = array(
				'message' => 'false',
				'comment' => $e->getMessage(),
			);
		}
		echo json_encode($result);
	}
}
