<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('Order_model');
    $this->load->model('Email_model');
  }

  function email()
  {
    $orderid       = $_GET['orderid'];

    $company = $this->Order_model->selectordercompanyid($orderid);
    $companyid = $company->CompanyId;
    $orderemail    = $this->Order_model->selectmailorders($companyid);
    $ordermails = $orderemail->MailOrder;
    $agentid = $orderemail->AgentId;
    $agentemail="";
    if($agentid !="0"){
      $agentdetails = $this->Order_model->selectagentemail($agentid);
      foreach ($agentdetails as $value) {
      $agentemail =  $agentemail.$value->Email.",";

      }

    }
    else{
      $agentemail = $orderemail->AgentEmail;
    }

    $fetchemailboothdetails= $this->Order_model->getemailorderboothdetails($orderid);
    $getemailcontainerdetails=$this->Order_model->gettotalcontainers($orderid);
    $fetchemailcompanydetails= $this->Order_model->getemailcompanydetails($companyid);
    foreach ($fetchemailboothdetails as $key) {
     $getemailitemdetails=$this->Order_model->getemailitemdetails($key->OrderId,$key->BoothId);
     $key->item=$getemailitemdetails;
   }
    
    
       foreach ($fetchemailboothdetails as $value) {
          $OrderNumber                  = $value->OrderNumber;
          $Date                         = $value->LastUpdate;
          $TotalOrderUsValue            = $value->TotalOrderUsValue;
          $TotalBooth                   = $value->TotalBooth;
          $TotalOrderRmbValue           = $value->TotalOrderRmbValue;
          $TotalOrderCBM                = $value->TotalOrderCBM;
          $TotalOrderWeight             = $value->TotalOrderWeight;
          $TotalContainer               = $value->TotalContainer;
          $TotalBooth                   = $value->TotalBooth;
          $TotalOrderThirdCurrencyValue = $value->TotalOrderZarValue;
      }
      foreach ($fetchemailcompanydetails as $value){
          $CompanyName = $value->CompanyName;
          $CompanyAgent = $value->AgentName;
          $CompanyPhone = $value->Phone;
          $CompanyCountry = $value->Country;
          $CompanyMobile = $value->Mobile;
          $CompanyAddress = $value->Address;
          $CompanyEmailAddress = $value->OrderEmail;
          $Companythirdcurrency = $value->ThirdCurrency;
          $CompanyZipcode = $value->ZipCode;
          if($CompanyZipcode ==0){
            $CompanyZipcode ="";
          }
      }

        if($Companythirdcurrency != "")
        {
         preg_match('#\((.*?)\)#', $Companythirdcurrency, $out);
          $Companythirdcurrencysymbol=$out[1];
        }
        else{
          $Companythirdcurrencysymbol ="";
        }

    $pdfdoc = $this->Email_model->EmailMessage($fetchemailboothdetails,$OrderNumber,$Date,$TotalOrderUsValue,$TotalBooth,$TotalOrderRmbValue,$TotalOrderCBM,$TotalOrderWeight,$TotalContainer,$CompanyName,$CompanyPhone,$CompanyCountry,$CompanyMobile,$CompanyAddress,$CompanyEmailAddress,$CompanyZipcode,$TotalOrderThirdCurrencyValue,$CompanyAgent,$getemailcontainerdetails,$Companythirdcurrencysymbol);

      $attachment = $pdfdoc;

      $from_email = "webmaster@mixmycontainer.com"; 

        $this->load->library('email');
        $config['protocol'] = "sendmail";
        $config['smtp_host'] = "smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "webmaster@mixmycontainer.com"; 
        $config['smtp_pass'] = "kbta59cz";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['priority'] = 5;
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
         $this->email->from($from_email,'MixMyContainer'); 
         $this->email->to($ordermails);
         $this->email->cc($agentemail);
         $this->email->subject('Order Details'); 
         $this->email->message("Hi,
        Please find the attachement you requested. For any query please don't hesistate to contact us . 
        Our mail id is: webmaster@mixmycontainer.com"); 
         $this->email->attach($attachment);
   
         //Send mail 
         //$this->email->priority(3);
         if($this->email->send())
         {
            $result = array(
                            'message' => 'success'
                           
                        );
           echo json_encode($result);
           //echo json_encode("success");
         } 
         else
         {
           $result = array(
                            'message' => 'failed'
                            
                        );
           echo json_encode($result);
           //echo json_encode("failed");
         }
  }
  /*check if items are there for senting email*/
  function checkemailitems()
  {
    $orderid       = $_GET['orderid'];
    $getemailitemdetails=$this->Order_model->checkemailitems($orderid);
    if(empty($getemailitemdetails))
    {
      $result = array(
                     'message' => 'noitem' 
                     );
      echo json_encode($result);
    } 
    else
     {
       $result = array(
                        'message' => 'itemsfound'  
                      );
       echo json_encode($result);
     }
  }
  /*end*/


}


/* End of file Mail.php */
/* Location: ./application/controllers/mail.php */