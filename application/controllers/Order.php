<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller

    {
    function __construct()
        {
        parent::__construct();
        $this->load->model('Order_model');
        //if (!$this->session->userdata("userToken")) redirect('Login', 'refresh');
        }

    /** Abhijith A Nair's Code */
    /** function for save order details(booth details, trade) **/
    function GetBoothInfo()
        {
        try
            {
            if (($_GET['boothno']) && ($this->session->userdata('userToken')))
                {
                $token = $this->session->userdata('userToken');
                $boothno = $_GET['boothno'];
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $company = $this->Order_model->companydetails($val_orderemail);
                if ($company)
                    {
                    $boothinfo = $this->Order_model->getboothinfo($boothno);
                    if ($boothinfo)
                        {
                        $result = array(
                            'message' => 'true',
                            'boothinfo' => $boothinfo
                        );
                        }
                      else
                        {
                        $result = array(
                            'message' => 'false',
                            'comment' => 'No record found'
                        );
                        }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No Access'
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'Missing Arguments'
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /*Check OrderNo
    *@isOrderNo - Loop Variable *
    */
    function CheckOrderNo()
        {
        try
            {
            if (($_GET['orderno']) && ($this->session->userdata('userToken')))
                // if (($_GET['orderno']) && ($_GET['userToken']))
                {
                $token = $this->session->userdata('userToken');
                $token = $this->session->userdata('userToken');
                //$token = $_GET['userToken'];
                $orderno = $_GET['orderno'];
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $company = $this->Order_model->companydetails($val_orderemail);
                if ($company)
                    {
                    $companyid = $company->CompanyID;
                    //echo $companyid;
                    //exit();
                    //$result = $this->Order_model->checkorderno($orderno);
                    $result = $this->Order_model->checkordernobycompany($orderno,$companyid);
                    if ($result)
                        {
                        $isOrderNo = false;
                        $neworderno = "OD" . mt_rand(100000, 9999999);
                        while ($isOrderNo)
                            {
                            $result = $this->Order_model->checkorderno($neworderno);
                            if (!$result)
                                {
                                $isOrderNo = true;
                                }
                              else
                                {
                                $neworderno = "OD" . mt_rand(100000, 9999999);
                                }
                            }

                        // end loop

                        $result = array(
                            'message' => 'false',
                            'comment' => 'Order No Exists',
                            'OrderNo' => $neworderno
                        );
                        }
                      else
                        {
                        $result = array(
                            'message' => 'true',
                            'comment' => 'Non existing orderno',
                            'OrderNo' => $orderno
                        );
                        }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No Access',
                        'OrderNo' => $orderno
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'Missing Arguments',
                    'OrderNo' => $orderno
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage() ,
                'OrderNo' => $orderno
            );
            }

        echo json_encode($result);
        }

    /* Get Trade List */
    function GetTradeList()
        {
        try
            {
            if ($this->session->userdata('userToken'))
                {
                $token = $this->session->userdata('userToken');
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $company = $this->Order_model->companydetails($val_orderemail);
                if ($company)
                    {
                    $val_companyid = $company->CompanyID;
                    $parent = 0;
                    $tradelist = $this->Order_model->gettradorhalllist($parent, $val_companyid);
                    if ($tradelist)
                        {
                        $result = array(
                            'message' => 'true',
                            'tradelist' => $tradelist
                        );
                        }
                      else
                        {
                        $result = array(
                            'message' => 'false',
                            'comment' => 'No records found'
                        );
                        }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No Access'
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'No Access'
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /* Get Hall List on Trade */
    function GetHallist()
        {
        try
            {
            if ($_GET['parent'] && $token = $this->session->userdata('userToken'))
                {
                $token = $this->session->userdata('userToken');
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $company = $this->Order_model->companydetails($val_orderemail);
                $val_companyid = $company->CompanyID;
                $parent = $_GET['parent'];
                if ($val_companyid)
                    {
                    $halllist = $this->Order_model->gettradorhalllist($parent, $val_companyid);
                    if ($halllist)
                        {
                        $result = array(
                            'message' => 'true',
                            'halllist' => $halllist
                        );
                        }
                      else
                        {
                        $result = array(
                            'message' => 'false',
                            'comment' => 'No record found'
                        );
                        }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No Access'
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'Missing Arguments'
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /* Get Container List */
    function GetContainerList()
        {
        try
            {
            if ($this->session->userdata('userToken'))
                {
                $token = $this->session->userdata('userToken');
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $company = $this->Order_model->companydetails($val_orderemail);
                if ($company)
                    {
                    $conatinerlist = $this->Order_model->getcontainertype();
                    if ($conatinerlist)
                        {
                        $result = array(
                            'message' => 'true',
                            'containerlist' => $conatinerlist
                        );
                        }
                      else
                        {
                        $result = array(
                            'message' => 'false',
                            'comment' => 'No record found'
                        );
                        }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No Access'
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'No Access'
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /*Save Custom Trade */
    function SaveCustomTradeOrHall()
        {
        try
            {

            // if ($tradename!="" && $parent!="") {

            $token = $this->session->userdata('userToken');
            $orderemail = $this->Order_model->getordermail($token);
            $val_orderemail = $orderemail->OrderEmail;
            $company = $this->Order_model->companydetails($val_orderemail);
            $val_companyid = $company->CompanyID;
            $_POST = json_decode(file_get_contents('php://input') , true);
            $name = $_POST['name'];
            $parent = $_POST['parent'];

            // $result_tradename = $this->Order_model->checktrade($tradename,$val_companyid);

            /** match trade name is exists or not **/

            //   if ($result_tradename == null) {

            $tradedetails = array(
                'Name' => $name,
                'CompanyId' => $val_companyid,
                'Parent' => $parent
            );
            /** save trade or hall details**/
            $id = $this->Order_model->savecustomtradeorhall($tradedetails);
            if ($id)
                {
                /** success response of save trade or hall **/
                $result = array(
                    'message' => 'true',
                    'Id' => $id
                );
                }
              else
                {
                /** false response **/
                $result = array(
                    'message' => 'false',
                    'comment' => 'Database error occured'
                );
                }

            // } else {
            //     /** false response for same order no**/
            //     $result = array(
            //         'message' => 'false',
            //         'comment' =>'Trade all ready saved'
            //     );
            // }
            // } else {
            //     /** false response for missing input data**/
            //     $result = array(
            //         'message' => 'false',
            //         'comment' => 'Missing values'
            //     );
            // }

            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /** function for save order details(booth details, trade) **/
    function SaveOrder()
        {
        try
            {
            $token = $this->session->userdata('userToken');
            $_POST = json_decode(file_get_contents('php://input') , true);
            $orderno = $_POST['orderno'];
            $hall = $_POST['hall'];
            $containertype = $_POST['ordertype'];
            $trade = $_POST['trade'];
            $boothname = $_POST['boothname'];
            $boothno = $_POST['boothno'];
            $telephone = $_POST['telephone'];
            $businesscope = $_POST['businessscope'];
            $Businesscard = $_POST['Businesscard'];
            if ($orderno != "" && $containertype != "" && $boothno != "")
                {
                $result_orderno = $this->Order_model->checkorderno($orderno);
                /** match order number is exists or not **/
                if ($result_orderno == null)
                    {
                    $token = $this->session->userdata('userToken');
                    $orderemail = $this->Order_model->getordermail($token);
                    $val_orderemail = $orderemail->OrderEmail;
                    $company = $this->Order_model->companydetails($val_orderemail);
                    $val_companyid = $company->CompanyID;
                    if ($val_companyid)
                        {
                        if ($Businesscard != "")
                            {
                            $ImagePath = $this->Order_model->saveimage($Businesscard);
                            }
                          else
                            {
                            $ImagePath = "default.jpg";
                            }

                        $date = date("Y-m-d");
                        $orderdetails = array(
                            'OrderNumber' => $orderno,
                            'CompanyId' => $val_companyid,
                            'ProductType' => $hall,
                            'Trade' => $trade,
                            'LastUpdate' => $date,
                            'TotalBooth' => 1,
                            'TotalContainer' => 1
                        );
                        /** save order details**/
                        $orderid = $this->Order_model->SaveOrder($orderdetails);
                        $boothdetails = array(
                            'OrderId' => $orderid,
                            'CompanyId' => $val_companyid,
                            'BoothNumber' => $boothno,
                            'BoothName' => $boothname,
                            'Phone' => $telephone,
                            'BusinessScope' => $businesscope,
                            'BusinessCard' => $ImagePath,
                            'ValuePurchase' => 0,
                            'ValuePurchaseRMB' => 0,
                            'BoothStatus' =>0,
                            'TradeId' =>$trade,
                            'HallId'=>$hall
                        );
                        $result_savebooth = $this->Order_model->SaveBooth($boothdetails);
                        $newboothinfo = array(
                            'BoothNumber' => $boothno,
                            'BoothName' => $boothname,
                            'Phone' => $telephone,
                            'BusinessScope' => $businesscope,
                            'BusinessCard' => $ImagePath
                        );
                        $boothinfo = $this->Order_model->getboothinfo($boothno);
                        if (!$boothinfo)
                            {
                            $boothno = $this->Order_model->saveboothinfo($newboothinfo);
                            }

                        /** save booth details **/
                        $containerdetails = array(
                            'ContainerType' => $containertype,
                            'OrderId' => $orderid,
                            'CompanyId' => $val_companyid,
                            'ItemSaveStatus' => 1
                        );
                        /** save container details**/
                        $result_savecontainer = $this->Order_model->saveContainer($containerdetails);
                        if ($orderid && $result_savebooth && $result_savecontainer)
                            {
                            /** success response of save order booth and container **/
                            $result = array(
                                'message' => 'true',
                                'orderid' => $orderid,
                                'boothid' => $result_savebooth,
                                'containerid' => $result_savecontainer
                            );
                            }
                          else
                            {
                            /** false response **/
                            $result = array(
                                'message' => 'false',
                                'comment' => 'Error Occured'
                            );
                            }
                        }
                      else
                        {
                        $result = array(
                            'message' => 'false',
                            'comment' => 'No Access'
                        );
                        }
                    }
                  else
                    {
                    /** false response for same order no**/
                    $result = array(
                        'message' => 'false',
                        'comment' => 'OrderId Exists',
                        'result' => $result_orderno
                    );
                    }
                }
              else
                {
                /** false response for missing input data**/
                $result = array(
                    'message' => 'false',
                    'comment' => 'Value Missing'
                );
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /**Check Data entered in company details */
    function CheckCompanyDetails()
        {
        try
            {
            if ($this->session->userdata('userToken'))
                {
                $token = $this->session->userdata('userToken');
                $orderemail = $this->Order_model->getordermail($token);
                $val_orderemail = $orderemail->OrderEmail;
                $companydetails = $this->Order_model->companydetails($val_orderemail);
                if ($companydetails)
                    {
                    /*check all details enterd */
                    // if ($companydetails->ExchangeRate != 0 && $companydetails->OrderEmail != "" && $companydetails->GrossweightCarton != 0 && $companydetails->Width != 0 && $companydetails->Height != 0 && $companydetails->Length != 0)
                    //     {
                        $isOrderNo = false;
                        $neworderno = "OD" . mt_rand(100000, 9999999);

                        // starts loop

                        while ($isOrderNo)
                            {
                            $result = $this->Order_model->checkorderno($neworderno);
                            if (!$result)
                                {
                                $isOrderNo = true;
                                }
                              else
                                {
                                $neworderno = "OD" . mt_rand(100000, 9999999);
                                }
                            }

                        $result = array(
                            'message' => 'true',
                            'comment' => 'Data entered',
                            'OrderNo' => $neworderno
                        );
                      //   }
                      // else
                      //   {
                      //   $result = array(
                      //       'message' => 'false',
                      //       'comment' => 'Company Setup no entered'
                      //   );
                      //   }
                    }
                  else
                    {
                    $result = array(
                        'message' => 'false',
                        'comment' => 'No record found'
                    );
                    }
                }
              else
                {
                $result = array(
                    'message' => 'false',
                    'comment' => 'No Access'
                );
                redirect('/Login/logout');
                }
            }

        catch(Exception $e)
            {
            $result = array(
                'message' => 'false',
                'comment' => $e->getMessage()
            );
            }

        echo json_encode($result);
        }

    /**function for view all order**/
    function Viewallorder()
        {
        $token = $this->session->userdata('userToken');
        $orderemail = $this->Order_model->getordermail($token);
        $val_orderemail = $orderemail->OrderEmail;
        $company = $this->Order_model->companydetails($val_orderemail);
        $val_companyid = $company->CompanyID;
        $allorder = $this->Order_model->getallorders($val_companyid);
        echo json_encode($allorder);
        }

    /**end view all order function**/
    /**function filter by date **/
    function FilterByDate()
        {
        try
            {
            if ($this->session->userdata('userToken'))
                {
                if ($_GET['fromdate'] && $_GET['todate'])
                    {
                    $val_orderemail = "";
                    $token = $this->session->userdata('userToken');
                    $orderemail = $this->Order_model->getordermail($token);
                    $val_orderemail = $orderemail->OrderEmail;
                    $company = $this->Order_model->companydetails($val_orderemail);
                    $fromdate = $_GET['fromdate'];
                    $todate = $_GET['todate'];
                    if ($company)
                        {
                        $val_companyid = $company->CompanyID;
                        $orderresult = $this->Order_model->getordersbydate($val_companyid, $fromdate, $todate);
                        $result = $orderresult;

                        // $result = array(
                        //     'message' => 'true',
                        //     'Orders'  =>$orderresult,
                        //     'fromdate'=>$fromdate,
                        //     'todate'  =>$todate
                        // );

                        }
                      else
                        {
                        $val_companyid = 0;
                        $orderresult = $this->Order_model->getordersbydate($val_companyid, $fromdate, $todate);
                        $result = $orderresult;

                        // $result = array(
                        //    'message' => 'false',
                        //    'comment' => 'No Access'
                        // );

                        }
                    }
                  else
                    {
                    $val_companyid = 0;
                    $orderresult = $this->Order_model->getordersbydate($val_companyid, $fromdate, $todate);
                    $result = $orderresult;

                    // $result = array(
                    //         'message' => 'false',
                    //         'comment' => 'Value Missing'
                    // );
                    // if value is missing

                    }
                }
              else
                {

                //  $result = array(
                //             'message' => 'false',
                //             'comment' => 'Invalid session'
                // );

                $val_companyid = 0;
                $orderresult = $this->Order_model->getordersbydate($val_companyid, $fromdate, $todate);
                $result = $orderresult;
                }
            }

        catch(Exception $e)
            {

            // $result = array(
            //     'message' => 'false',
            //     'comment' => $e->getMessage()
            // );

            $val_companyid = 0;
            $orderresult = $this->Order_model->getordersbydate($val_companyid, $fromdate, $todate);
            $result = $orderresult;
            }

        echo json_encode($result);
        }

    /** Abhijith A Nair's Code */
    /*=========================================================================================*/
    /* function for get carton default details to the item add page*/
    function Getcartonunites()
        {
        $token = $this->session->userdata('userToken');
        $orderemail = $this->Order_model->selectordermail($token);
        foreach($orderemail as $key)
            {
            $val_orderemail = $key->OrderEmail;
            }

        /**fetch carton details from database using token value**/
        $result_cartonunits = $this->Order_model->getcartonunits($val_orderemail);
        if ($result_cartonunits)
            {
            echo json_encode($result_cartonunits);
            }
          else
            {
            $result = 0;
            echo json_encode($result);
            }
        }

    /**end get carton details**/
    /**function for get new container when current container full**/
     function Savecontainer()
    {
        $token = $this->session->userdata('userToken');
        $_POST = json_decode(file_get_contents('php://input'), true);
        if ($_POST['newcontianertype'] != "" && $_POST['orderid'] != "" && $_POST['containerid'] != "" && $_POST['usedweight'] != "" && $_POST['usedvolume'] != "" && $_POST['currentcontainertype'] != "") {
            $orderemail = $this->Order_model->selectordermail($token);
            foreach ($orderemail as $key) {
                $val_orderemail = $key->OrderEmail;
            }
            
            $compayid = $this->Order_model->selectcompanyid($val_orderemail);
            $orederid = $_POST['orderid'];
            foreach ($compayid as $key) {
                $val_companyid = $key->CompanyID;
            }
            
            $currentcontainertype    = $_POST['currentcontainertype']; //type current container
            $totalContainerVolume    = $_POST['usedvolume']; // volume used for current container
            $totalContainerWeight    = $_POST['usedweight']; // weight used for current container
            /**calculate volume and weight percentage for current container**/
            $selectcontainercapacity = $this->Order_model->selectcontainercapacity($currentcontainertype);
            foreach ($selectcontainercapacity as $key) {
                $containervolume_capacity = $key->MaximumVolume;
                $containerweight_capacity = $key->MaximumWeight;
            }
            
            // if ($currentcontainertype == 1) {
            
            $volumepercentage = ($totalContainerVolume / $containervolume_capacity) * 100;
            $weightpercentage = ($totalContainerWeight / $containerweight_capacity) * 100;
            $volumepercentage = number_format($volumepercentage, 2);
            $weightpercentage = number_format($weightpercentage, 2);
            
            // } else if ($currentcontainertype == 2) {
            //     $volumepercentage = ($totalContainerVolume / 66) * 100;
            //     $weightpercentage = ($totalContainerWeight / 28800) * 100;
            //     $volumepercentage = number_format($volumepercentage, 2);
            //     $weightpercentage = number_format($weightpercentage, 2);
            // } else {
            //     $volumepercentage = ($totalContainerVolume / 75) * 100;
            //     $weightpercentage = ($totalContainerWeight / 29600) * 100;
            //     $volumepercentage = number_format($volumepercentage, 2);
            //     $weightpercentage = number_format($weightpercentage, 2);
            // }
            
            $containerSpance     = array(
                'FilledWeight' => $totalContainerWeight,
                'FilledVolume' => $totalContainerVolume,
                'Volumepercentage' => $volumepercentage,
                'Weightpercentage' => $weightpercentage,
                'Status' => 1
            );
            /**update current contaer spance**/
            $updateCotainerSpace = $this->Order_model->updatetempContainerSpace($containerSpance, $orederid);
            /**add and save new container**/
            $containerdata       = array(
                'ContainerType' => $_POST['newcontianertype'],
                'OrderId' => $_POST['orderid'],
                'CompanyId' => $val_companyid,
                'OrginalContainerId' => 0
            );
            $savenewcontainer    = $this->Order_model->addtempnewcontainer($containerdata);
            // $savenewcontainer    = $this->Order_model->savenewcontainer($containerdata);
            /** combine current container id and new container id separated by comma for save in the item feild **/
            
            // $combinecontainerid  = $_POST['containerid'];
            // $combinecontainerid  = $combinecontainerid . ',' . $savenewcontainer;
            
            /**response of new container id and combined containerid**/
            $result = array(
                'message' => 'true',
                'containerid' => $savenewcontainer
            );
            echo json_encode($result);
        } else {
            $result = array(
                'message' => 'true'
            );
            echo json_encode($result);
        }
    }

    /**function for upgrade the size of current container**/
 function Upgradecontainer()
    {
        $token = $this->session->userdata('userToken');
        $_POST = json_decode(file_get_contents('php://input'), true);
        if ($_POST['upgradecontainertype'] != "" && $_POST['orderid'] != "" && $_POST['containerid'] != "" && $_POST['totalcontainervolume'] != "" && $_POST['totalcontainerweight'] != "") {
            $orderid                 = $_POST['orderid'];
            $containerid             = $_POST['containerid'];
            $upgradecontainertype    = $_POST['upgradecontainertype'];
            $totalContainerVolume    = $_POST['totalcontainervolume'];
            $totalContainerWeight    = $_POST['totalcontainerweight'];
            /**calculate volume and weight percentage for current container**/
            $selectcontainercapacity = $this->Order_model->selectcontainercapacity($upgradecontainertype);
            foreach ($selectcontainercapacity as $key) {
                $containervolume_capacity = $key->MaximumVolume;
                $containerweight_capacity = $key->MaximumWeight;
            }
            
            // if ($upgradecontainertype == 1) {
            
            $volumepercentage = ($totalContainerVolume / $containervolume_capacity) * 100;
            $weightpercentage = ($totalContainerWeight / $containerweight_capacity) * 100;
            $volumepercentage = number_format($volumepercentage, 2);
            $weightpercentage = number_format($weightpercentage, 2);
            
            // } else if ($upgradecontainertype == 2) {
            //     $volumepercentage = ($totalContainerVolume / 66) * 100;
            //     $weightpercentage = ($totalContainerWeight / 28800) * 100;
            //     $volumepercentage = number_format($volumepercentage, 2);
            //     $weightpercentage = number_format($weightpercentage, 2);
            // } else {
            //     $volumepercentage = ($totalContainerVolume / 75) * 100;
            //     $weightpercentage = ($totalContainerWeight / 29600) * 100;
            //     $volumepercentage = number_format($volumepercentage, 2);
            //     $weightpercentage = number_format($weightpercentage, 2);
            // }
            
            $containerdata    = array(
                'ContainerType' => $upgradecontainertype,
                'Volumepercentage' => $volumepercentage,
                'Weightpercentage' => $weightpercentage
            );
            $upgradecontainer = $this->Order_model->upgradetempcontainer($containerdata, $orderid);
            /**upgrade container**/
            $upgradecontainer = $this->Order_model->upgradecontainer($containerdata, $orderid, $containerid);
            $result           = array(
                'message' => 'true'
            );
            echo json_encode($result);
        }
    }

    /**end upgrade container**/
    /**function for save item**/
  function SaveItem()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = $this->session->userdata('userToken');
        if ($_POST['orderid'] != "" && $_POST['boothid'] != "" && $_POST['containerid'] != "" && $_POST['itemno'] != "" && $_POST['description'] != "" && $_POST['cartonGrossWeight'] != "" && $_POST['Length'] != "" && $_POST['Width'] != "" && $_POST['Height'] != "" && $_POST['cbm'] != "" && $_POST['usprice'] != "" && $_POST['unitpercarton'] != "" && $_POST['unitqty'] != "" && $_POST['priceRMB'] != "" && $_POST['cartonqty'] != "" && $_POST['containertype'] != "") {
            $orderemail = $this->Order_model->selectordermail($token);
            foreach ($orderemail as $key) {
                $val_orderemail = $key->OrderEmail;
            }
            
            $compayid = $this->Order_model->selectcompanyid($val_orderemail);
            foreach ($compayid as $key) {
                $val_companyid = $key->CompanyID;
            }
            
            $orederid = $_POST['orderid'];
            if ($val_companyid) {
                /**calculate current weight and volume of item **/
                $currentitemweight = ($_POST['cartonqty']) * ($_POST['cartonGrossWeight']);
                $currentitemvolume = ($_POST['cartonqty']) * ($_POST['cbm']);
                /**check current container space status**/
                $containerstatus   = $this->Order_model->selectcontainerstatus($orederid);
                foreach ($containerstatus as $key) {
                    $filledvolume = $key->FilledVolume;
                    $filledweight = $key->FilledWeight;
                }
                
                $totalContainerVolume    = $currentitemvolume + $filledvolume;
                $totalContainerWeight    = $currentitemweight + $filledweight;
                /**calculate weight and volume after item add for check item added to the current container**/
                $containertype           = $_POST['containertype'];
                $selectContainerCapacity = $this->Order_model->selectconatinercapacity($containertype, $orederid);
                foreach ($selectContainerCapacity as $key) {
                    $contaierMaxVolume  = $key->MaximumVolume;
                    $containerMaxWeight = $key->MaximumWeight;
                }
                
                $availablevolume = $contaierMaxVolume - $filledvolume;
                $availableweight = $containerMaxWeight - $filledweight;
                /**check volume or weight of item is greater than container capacity**/
                if ($currentitemvolume > $availablevolume || $currentitemweight > $availableweight) {
                    if ($currentitemvolume > $availablevolume && $currentitemweight < $availableweight) {
                        /**process when totalitem volume is greater than container volume capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonvolume       = $_POST['cbm'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $balancevolume      = $currentitemvolume - $availablevolume; //find balance volume after item added to the current container
                        $noofcartonmoved    = ($balancevolume / $cartonvolume); //find no of cartons moved to the next container
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontainervolume = $noofcartonmoved * $cartonvolume;
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $usedvolume         = $filledvolume + (($cartonvolume * $cartonqty) - $newcontainervolume);
                        $usedweight         = $filledweight + (($cartonweight * $cartonqty) - $newcontainerweight);
                        /**return response of balance item volume, weight and volume and weight used in previous container**/
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontainervolume,
                            'balanceweight' => $newcontainerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'totalcontainervolume' => $currentitemvolume,
                            'totalcontainerweight' => $currentitemweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    }
                    
                    /**check weight of item is greater than container capacity**/
                    else if ($currentitemweight > $availableweight && $currentitemvolume < $availablevolume) {
                        /**process when totalitem weight is greater than container weight capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonvolume       = $_POST['cbm'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $balanceweight      = $currentitemweight - $availableweight; //find balance weight after item added to the current container
                        $noofcartonmoved    = ($balanceweight / $cartonweight); //find no of cartons moved to the next container
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontinervolume  = $noofcartonmoved * $cartonvolume;
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $usedvolume         = $filledvolume + (($cartonvolume * $cartonqty) - $newcontinervolume);
                        $usedweight         = $filledweight + (($cartonweight * $cartonqty) - $newcontainerweight);
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontinervolume,
                            'balanceweight' => $newcontainerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'totalcontainervolume' => $currentitemvolume,
                            'totalcontainerweight' => $currentitemweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    }
                    
                    /**check volume and weight of item is greater than container capacity**/
                    else if ($currentitemvolume > $availablevolume && $currentitemweight > $availableweight) {
                        /**process when total item weight and volume is greater than container weight capacity**/
                        $cartonqty                = $_POST['cartonqty'];
                        $cartonvolume             = $_POST['cbm'];
                        $cartonweight             = $_POST['cartonGrossWeight'];
                        /*calculate carton values based on volume*/
                        $balancevolume            = $currentitemvolume - $availablevolume;
                        $noofcartonmovedforvolume = ($balancevolume / $cartonvolume);
                        $noofcartonmovedforvolume = ceil($noofcartonmovedforvolume);
                        /*calculate carton values based on weight*/
                        $balanceweight            = $currentitemweight - $availableweight;
                        $noofcartonmovedforweight = ($balanceweight / $cartonweight);
                        $noofcartonmovedforweight = ceil($noofcartonmovedforweight);
                        if ($noofcartonmovedforvolume > $noofcartonmovedforweight) {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforweight = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolumeforvolume         = $filledvolume + (($cartonvolume * $cartonqty) - $newcontainervolumeforvolume);
                            $usedweightforvolume         = $filledweight + (($cartonweight * $cartonqty) - $newcontainerweightforweight);
                            $newcontinerweightforvolume  = $noofcartonmovedforvolume * $cartonweight;
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolumeforvolume,
                                'usedweight' => $usedweightforvolume,
                                'totalcontainervolume' => $currentitemvolume,
                                'totalcontainerweight' => $currentitemweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        } else if ($noofcartonmovedforvolume < $noofcartonmovedforweight) {
                            $newcontainervolumeforvolume = $noofcartonmovedforweight * $cartonvolume;
                            $newcontainerweightforweight = $noofcartonmovedforweight * $cartonweight;
                            $usedweightforweight         = $filledweight + (($cartonweight * $cartonqty) - $newcontainerweightforweight);
                            $usedvolumeforweight         = $filledvolume + (($cartonvolume * $cartonqty) - $newcontainervolumeforvolume);
                            $newcontinervolumeforweight  = $noofcartonmovedforweight * $cartonvolume;
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontinervolumeforweight,
                                'balanceweight' => $newcontainerweightforweight,
                                'usedvolume' => $usedvolumeforweight,
                                'usedweight' => $usedweightforweight,
                                'totalcontainervolume' => $currentitemvolume,
                                'totalcontainerweight' => $currentitemweight,
                                'balancecartons' => $noofcartonmovedforweight
                            );
                            echo json_encode($result);
                        } else {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforweight = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolumeforvolume         = $filledvolume + (($cartonvolume * $cartonqty) - $newcontainervolumeforvolume);
                            $usedweightforvolume         = $filledweight + (($cartonweight * $cartonqty) - $newcontainerweightforweight);
                            $newcontinerweightforvolume  = $noofcartonmovedforvolume * $cartonweight;
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolumeforvolume,
                                'usedweight' => $usedweightforvolume,
                                'totalcontainervolume' => $currentitemvolume,
                                'totalcontainerweight' => $currentitemweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        }
                    }
                    $this->Order_model->removetempconatiner($orederid);
                    //Save Last Conatiner to temp 
                    $lastcontainer = $this->Order_model->selectlastcontainer($orederid);
                    if ($lastcontainer) {
                        $tempcontainer = array(
                            'ContainerType' => $lastcontainer->ContainerType,
                            'FilledWeight' => $lastcontainer->FilledWeight,
                            'FilledVolume' => $lastcontainer->FilledVolume,
                            'Volumepercentage' => $lastcontainer->Volumepercentage,
                            'Weightpercentage' => $lastcontainer->Weightpercentage,
                            'OrderId' => $lastcontainer->OrderId,
                            'CompanyId' => $lastcontainer->CompanyId,
                            'OrginalContainerId' => $lastcontainer->ContainerId,
                            'Status' => 0
                        );
                        $id = $this->Order_model->addtempnewcontainer($tempcontainer);
                    }
                } else {
                    /**process when current continer volume is not full**/
                    $selectcontainercapacity = $this->Order_model->selectcontainercapacity($containertype);
                    foreach ($selectcontainercapacity as $key) {
                        $containervolume_capacity = $key->MaximumVolume;
                        $containerweight_capacity = $key->MaximumWeight;
                    }
                    
                    // if ($containertype == 1) {
                    
                    $volumepercentage = ($totalContainerVolume / $containervolume_capacity) * 100;
                    $weightpercentage = ($totalContainerWeight / $containerweight_capacity) * 100;
                    $volumepercentage = number_format($volumepercentage, 2);
                    $weightpercentage = number_format($weightpercentage, 2);
                    
                    // }
                    // if ($containertype == 2) {
                    //     $volumepercentage = ($totalContainerVolume / 66) * 100;
                    //     $weightpercentage = ($totalContainerWeight / 28800) * 100;
                    //     $volumepercentage = number_format($volumepercentage, 2);
                    //     $weightpercentage = number_format($weightpercentage, 2);
                    // }
                    // if ($containertype == 3) {
                    //     $volumepercentage = ($totalContainerVolume / 75) * 100;
                    //     $weightpercentage = ($totalContainerWeight / 29600) * 100;
                    //     $volumepercentage = number_format($volumepercentage, 2);
                    //     $weightpercentage = number_format($weightpercentage, 2);
                    // }
                    
                    $containerSpance         = array(
                        'FilledWeight' => $totalContainerWeight,
                        'FilledVolume' => $totalContainerVolume,
                        'Volumepercentage' => $volumepercentage,
                        'Weightpercentage' => $weightpercentage
                    );
                    /**update container spacne after new item is added**/
                    $updateCotainerSpace     = $this->Order_model->updateContainerSpace($containerSpance, $orederid);
                    $TotalPriceInUS          = ($_POST['unitqty']) * ($_POST['usprice']);
                    $TotalPriceInRMB         = ($_POST['unitqty']) * ($_POST['priceRMB']);
                    $TotalPriceIn3rdCurrency = ($_POST['unitqty']) * ($_POST['ExchangeRate3dCurrency']);
                    /**select current total values of the order**/
                    $selectCurrentordertotal = $this->Order_model->selectordertotal($orederid);
                    foreach ($selectCurrentordertotal as $key) {
                        $TotalOrderUsValue  = $key->TotalOrderUsValue;
                        $TotalOrderRmbValue = $key->TotalOrderRmbValue;
                        $TotalOrderZarValue = $key->TotalOrderZarValue;
                        $TotalCartons       = $key->TotalCartons;
                        $TotalOrderWeight   = $key->TotalOrderWeight;
                        $TotalOrderCBM      = $key->TotalOrderCBM;
                    }
                    
                    $ordertotalvalues = array(
                        'TotalOrderUsValue' => $TotalOrderUsValue + $TotalPriceInUS,
                        'TotalOrderRmbValue' => $TotalOrderRmbValue + $TotalPriceInRMB,
                        'TotalOrderZarValue' => $TotalOrderZarValue + $TotalPriceIn3rdCurrency,
                        'TotalCartons' => $TotalCartons + $_POST['cartonqty'],
                        'TotalOrderWeight' => $TotalOrderWeight + ($_POST['cartonqty'] * $_POST['cartonGrossWeight']),
                        'TotalOrderCBM' => $TotalOrderCBM + ($_POST['cartonqty'] * $_POST['cbm'])
                    );
                    /**update order totoal values**/
                    $updateordervales = $this->Order_model->updateordervalues($ordertotalvalues, $orederid);
                    $date             = date("Y/m/d");
                    $imagename        = $_POST['itemimage'];
                    if ($imagename != 'default.jpg') {
                        $filename_path = md5(time() . uniqid()) . ".jpg";
                        $decoded       = base64_decode($imagename);
                        file_put_contents("assets/images/" . $filename_path, $decoded);
                    } else {
                        $filename_path = $_POST['itemimage'];
                    }
                    
                    $itemdetails             = array(
                        'CompanyId' => $val_companyid,
                        'OrderId' => $_POST['orderid'],
                        'BoothId' => $_POST['boothid'],
                        'ContainerId' => $_POST['containerid'],
                        'ItemNo' => $_POST['itemno'],
                        'DescriptionFrom' => $_POST['description'],
                        'DescriptionTo' => $_POST['altdescription'],
                        'UnitperCarton' => $_POST['unitpercarton'],
                        'WeightofCarton' => $_POST['cartonGrossWeight'],
                        'Length' => $_POST['Length'],
                        'Width' => $_POST['Width'],
                        'Height' => $_POST['Height'],
                        'CBM' => $_POST['cbm'],
                        'Picture' => $filename_path,
                        'PriceInUs' => $_POST['usprice'],
                        'PriceInRmb' => $_POST['priceRMB'],
                        'PriceInThirdCurrency' => $_POST['ExchangeRate3dCurrency'],
                        'UnitQuantity' => $_POST['unitqty'],
                        'CartonQuantity' => $_POST['cartonqty'],
                        'TotalPriceInUS' => $TotalPriceInUS,
                        'TotalPriceInRMB' => $TotalPriceInRMB,
                        'TotalPriceIn3rdCurrency' => $TotalPriceIn3rdCurrency,
                        'Date' => $date
                    );
                    /**save item**/
                    $itemid                  = $this->Order_model->saveitem($itemdetails);
                    $boothid                 = $_POST['boothid'];
                    $selectcurrentboothvalue = $this->Order_model->selectcurrentboothvalue($orederid, $boothid);
                    foreach ($selectcurrentboothvalue as $key) {
                        $currentboothvalue    = $key->ValuePurchase;
                        $currentboothrmbvalue = $key->ValuePurchaseRMB;
                    }
                    
                    $newtotalboothvalue            = $currentboothvalue + $TotalPriceInUS;
                    $newtotalboothrmbvalue         = $currentboothrmbvalue + $TotalPriceInRMB;
                    $totalboothvalue               = array(
                        'ValuePurchase' => $newtotalboothvalue,
                        'ValuePurchaseRMB' => $newtotalboothrmbvalue
                    );
                    /**update current booth values**/
                    $updatecurrentboothvalue       = $this->Order_model->updatecurrentboothvalue($orederid, $boothid, $totalboothvalue);
                    $updatecontaineritemsavestatus = $this->Order_model->updatecontaineritemsavestatus($orederid);
                    $result                        = array(
                        'message' => 'saved',
                        'result' => $newtotalboothvalue
                    );
                    echo json_encode($result);
                }
            } else {
                /**return false response when wrong user**/
                $result = array(
                    'message' => 'false'
                );
                echo json_encode($result);
            }
        } else {
            /**return false response when value missing**/
            $result = array(
                'message' => 'mfalse'
            );
            echo json_encode($result);
        }
    }

    /**end save item**/
    /**function for save item after add new conctainer**/
     function SubSaveItem()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = $this->session->userdata('userToken');
        if ($_POST['orderid'] != "" && $_POST['boothid'] != "" && $_POST['containerid'] != "") {
            $orderemail = $this->Order_model->selectordermail($token);
            foreach ($orderemail as $key) {
                $val_orderemail = $key->OrderEmail;
            }
            
            $compayid = $this->Order_model->selectcompanyid($val_orderemail);
            foreach ($compayid as $key) {
                $val_companyid = $key->CompanyID;
            }
            
            $orederid        = $_POST['orderid'];
            $containerstatus = $this->Order_model->selectcontainerstatus($orederid);
            foreach ($containerstatus as $key) {
                $filledvolume = $key->FilledVolume;
                $filledweight = $key->FilledWeight;
            }
            
            $balancevolume = $_POST['balancevolume'];
            $balanceweight1 = $_POST['balanceweight'];
            
            // $totalContainerVolume    = $balancevolume + $filledvolume;
            // $totalContainerWeight    = $balanceweight1 + $filledweight;
            
            if ($val_companyid) {
                $containertype           = $_POST['containertype'];
                $selectContainerCapacity = $this->Order_model->selectconatinercapacity($containertype, $orederid);
                foreach ($selectContainerCapacity as $key) {
                    $contaierMaxVolume  = $key->MaximumVolume;
                    $containerMaxWeight = $key->MaximumWeight;
                }
                
                /**check selected container is full or not **/
                if ($balancevolume > $contaierMaxVolume || $balanceweight1 > $containerMaxWeight) {
                    if ($balancevolume > $contaierMaxVolume && $balanceweight1 < $containerMaxWeight) {
                        /**process balance volume is gratere than container volme capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonvolume       = $_POST['cbm'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $balancevolume      = $balancevolume - $contaierMaxVolume; // calculation for balance volume after fill the item in the container
                        $noofcartonmoved    = ($balancevolume / $cartonvolume);
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontainervolume = $noofcartonmoved * $cartonvolume;
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $usedvolume         = $balancevolume - $newcontainervolume;
                        $usedweight         = $balanceweight1 - $newcontainerweight;
                        $newcontinerweight  = $noofcartonmoved * $cartonweight;
                        /**return balance volume, balance weight and used volume and used weight**/
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontainervolume,
                            'balanceweight' => $newcontinerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    } else if ($balanceweight1 > $containerMaxWeight && $balancevolume < $contaierMaxVolume) {
                        /**process when balance weight is greater than container capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $cartonvolume       = $_POST['cbm'];
                        $balanceweight      = $balanceweight1 - $containerMaxWeight;
                        $noofcartonmoved    = ($balanceweight / $cartonweight);
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $newcontainervolume = $noofcartonmoved * $cartonvolume;
                        $usedweight         = $balanceweight1 - $newcontainerweight;
                        $usedvolume         = $balancevolume - $newcontainervolume;
                        $newcontinervolume  = $noofcartonmoved * $cartonvolume;
                        /**return balance weight, balance volume used volume, used weight**/
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontinervolume,
                            'balanceweight' => $newcontainerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    } else if ($balancevolume > $contaierMaxVolume && $balanceweight1 > $containerMaxWeight) {
                        $cartonqty                = $_POST['cartonqty'];
                        $cartonweight             = $_POST['cartonGrossWeight'];
                        $cartonvolume             = $_POST['cbm'];
                        $balancevolume            = $balancevolume - $contaierMaxVolume; //find over flow volume
                        $noofcartonmovedforvolume = ($balancevolume / $cartonvolume); //cartons for balance volume
                        $noofcartonmovedforvolume = ceil($noofcartonmovedforvolume); //round off no of carton volume
                        $balanceweight            = $balanceweight1 - $containerMaxWeight; //find over flow weight
                        $noofcartonmovedforweight = ($balanceweight / $cartonweight); //find cartons for balace volume
                        $noofcartonmovedforweight = ceil($noofcartonmovedforweight); //round off no of carton volume
                        if ($noofcartonmovedforvolume > $noofcartonmovedforweight) {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforvolume;
                            $usedweight                  = $balanceweight1 - $newcontainerweightforvolume;
                            $newcontinerweightforvolume  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        } else if ($noofcartonmovedforvolume < $noofcartonmovedforweight) {
                            $newcontainervolumeforweight = $noofcartonmovedforweight * $cartonvolume;
                            $newcontainerweightforweight = $noofcartonmovedforweight * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforweight;
                            $usedweight                  = $balanceweight1 - $newcontainerweightforweight;
                            $newcontinerweightforweight  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforweight,
                                'balanceweight' => $newcontinerweightforweight,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforweight
                            );
                            echo json_encode($result);
                        } else {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforvolume;
                            $usedweight                  = $balanceweight1 - $newcontainerweightforvolume;
                            $newcontinerweightforvolume  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        }
                    } else {
                        $result = array(
                            'message' => 'false'
                        );
                        echo json_encode($result);
                    }
                } else {
                    /**process when container is not full**/
                    $selectcontainercapacity = $this->Order_model->selectcontainercapacity($containertype);
                    foreach ($selectcontainercapacity as $key) {
                        $containervolume_capacity = $key->MaximumVolume;
                        $containerweight_capacity = $key->MaximumWeight;
                    }
                    
                    /**calculate the volume percentage and weight percentage for differenct containers**/
                    
                    // if ($containertype == 1) {
                    
                    $volumepercentage = ($balancevolume / $containervolume_capacity) * 100;
                    $weightpercentage = ($balanceweight1 / $containerweight_capacity) * 100;
                    $volumepercentage = number_format($volumepercentage, 2);
                    $weightpercentage = number_format($weightpercentage, 2);
                    
                    // }
                    // if ($containertype == 2) {
                    //     $volumepercentage = ($balancevolume / 66) * 100;
                    //     $weightpercentage = ($balanceweight1 / 28800) * 100;
                    //     $volumepercentage = number_format($volumepercentage, 2);
                    //     $weightpercentage = number_format($weightpercentage, 2);
                    // }
                    // if ($containertype == 3) {
                    //     $volumepercentage = ($balancevolume / 75) * 100;
                    //     $weightpercentage = ($balanceweight1 / 29600) * 100;
                    //     $volumepercentage = number_format($volumepercentage, 2);
                    //     $weightpercentage = number_format($weightpercentage, 2);
                    // }
                    $orderid=$_POST['orderid'];
                    $containerSpance         = array(
                        'FilledWeight' => $balanceweight1,
                        'FilledVolume' => $balancevolume,
                        'Volumepercentage' => $volumepercentage,
                        'Weightpercentage' => $weightpercentage
                    );
                    /**update contaiener space**/
                    // $updateCotainerSpace     = $this->Order_model->updateContainerSpace($containerSpance, $orederid);
                    $updateCotainerSpace     = $this->Order_model->updatetempcontainerpercentage($orderid, $containerSpance);
                    $TotalPriceInUS          = ($_POST['unitqty']) * ($_POST['usprice']);
                    $TotalPriceInRMB         = ($_POST['unitqty']) * ($_POST['priceRMB']);
                    $TotalPriceIn3rdCurrency = ($_POST['unitqty']) * ($_POST['ExchangeRate3dCurrency']);
                    $selectCurrentordertotal = $this->Order_model->selectordertotal($orederid);
                    foreach ($selectCurrentordertotal as $key) {
                        $TotalOrderUsValue  = $key->TotalOrderUsValue;
                        $TotalOrderRmbValue = $key->TotalOrderRmbValue;
                        $TotalOrderZarValue = $key->TotalOrderZarValue;
                        $TotalCartons       = $key->TotalCartons;
                        $TotalOrderWeight   = $key->TotalOrderWeight;
                        $TotalOrderCBM      = $key->TotalOrderCBM;
                    }
                    
                    $ordertotalvalues = array(
                        'TotalOrderUsValue' => $TotalOrderUsValue + $TotalPriceInUS,
                        'TotalOrderRmbValue' => $TotalOrderRmbValue + $TotalPriceInRMB,
                        'TotalOrderZarValue' => $TotalOrderZarValue + $TotalPriceIn3rdCurrency,
                        'TotalCartons' => $TotalCartons + $_POST['cartonqty'],
                        'TotalOrderWeight' => $TotalOrderWeight + ($_POST['cartonqty'] * $_POST['cartonGrossWeight']),
                        'TotalOrderCBM' => $TotalOrderCBM + ($_POST['cartonqty'] * $_POST['cbm'])
                    );
                    /**update order values**/
                    $updateordervales = $this->Order_model->updateordervalues($ordertotalvalues, $orederid);
                    $date             = date("Y/m/d");
                    $imagename        = $_POST['itemimage'];
                    if ($imagename != 'default.jpg') {
                        $filename_path = md5(time() . uniqid()) . ".jpg";
                        $decoded       = base64_decode($imagename);
                        file_put_contents("assets/images/" . $filename_path, $decoded);
                    } else {
                        $filename_path = $_POST['itemimage'];
                    }
                  
                    $itemdetails = array(
                        'CompanyId' => $val_companyid,
                        'OrderId' => $_POST['orderid'],
                        'BoothId' => $_POST['boothid'],
                        'ContainerId' => $_POST['containerid'],
                        'ItemNo' => $_POST['itemno'],
                        'DescriptionFrom' => $_POST['description'],
                        'DescriptionTo' => $_POST['altdescription'],
                        'UnitperCarton' => $_POST['unitpercarton'],
                        'WeightofCarton' => $_POST['cartonGrossWeight'],
                        'Length' => $_POST['Length'],
                        'Width' => $_POST['Width'],
                        'Height' => $_POST['Height'],
                        'CBM' => $_POST['cbm'],
                        'Picture' => $filename_path,
                        'PriceInUs' => $_POST['usprice'],
                        'PriceInRmb' => $_POST['priceRMB'],
                        'PriceInThirdCurrency' => $_POST['ExchangeRate3dCurrency'],
                        'UnitQuantity' => $_POST['unitqty'],
                        'CartonQuantity' => $_POST['cartonqty'],
                        'TotalPriceInUS' => $TotalPriceInUS,
                        'TotalPriceInRMB' => $TotalPriceInRMB,
                        'TotalPriceIn3rdCurrency' => $TotalPriceIn3rdCurrency,
                        'Date' => $date
                    );
                    /**save item details**/
                    $itemid      = $this->Order_model->saveitem($itemdetails);
                    $flag        = $_POST['flag'];

                        $alltempconatiners = $this->Order_model->getalltempcontainers($orderid);
                        $itemcontainerid="";
                        foreach ($alltempconatiners as $key) {

                            if ($key->OrginalContainerId != 0) {
                                $oldconatainer       = $key->OrginalContainerId;
                                $containerSpance     = array(
                                    'FilledWeight' => $key->FilledWeight,
                                    'FilledVolume' => $key->FilledVolume,
                                    'Volumepercentage' => $key->Volumepercentage,
                                    'Weightpercentage' => $key->Weightpercentage,
                                    'Status' => $key->Status,
                                    'ItemSaveStatus' => 1
                                );
                                $updateCotainerSpace = $this->Order_model->updateoldcontainer($containerSpance, $oldconatainer);
                                 if($itemcontainerid==""){
                                    $itemcontainerid= $oldconatainer;
                                }
                                else{
                                    $itemcontainerid= $itemcontainerid.",".$oldconatainer;
                                }
                                //$itemcontainerid= $itemcontainerid.",".$oldconatainer;
                            }
                            
                            else {
                                $val_newcontainer    = array(
                                    'ContainerType' => $key->ContainerType,
                                    'OrderId' => $key->OrderId,
                                    'CompanyId' => $key->CompanyId,
                                    'FilledWeight' => $key->FilledWeight,
                                    'FilledVolume' => $key->FilledVolume,
                                    'Volumepercentage' => $key->Volumepercentage,
                                    'Weightpercentage' => $key->Weightpercentage,
                                    'Status' => $key->Status,
                                    'ItemSaveStatus' => 1
                                );
                                /*calling model sql query*/
                                $result_newcontainer = $this->Order_model->savenewcontainer($val_newcontainer);
                                 if($itemcontainerid==""){
                                    $itemcontainerid=$result_newcontainer;
                                }
                                else{
                                    $itemcontainerid= $itemcontainerid.",".$result_newcontainer;
                                }
                                //$itemcontainerid= $itemcontainerid.",".$result_newcontainer;
                                
                            }
                        }
                        $this->Order_model->removetempconatiner($orderid);
                         if($flag == 0)
                         {
                            $lastcontainer = $this->Order_model->selectlastcontainer($orderid);
                            if ($lastcontainer) {
                                 $containerid        =$lastcontainer->ContainerId;
                               }
                               $itemcontainersid         = array(
                        'ContainerId' => $containerid
                    );
                         }
                         else
                         {
                             $itemcontainersid         = array(
                        'ContainerId' => $itemcontainerid
                    );
                         }
                    // $lastcontainer = $this->Order_model->selectlastcontainer($orderid);
                    // if ($lastcontainer) {
                    //      $containerid        =$lastcontainer->ContainerId;
                    //    }

                    // $containerid             = $_POST['containerid'];

                    // $itemcontainersid         = array(
                    //     'ContainerId' => $itemcontainerid
                    // );
                    /**update item container type**/
                    $updateitemcontainertype = $this->Order_model->updateitemcontainertype($itemcontainersid, $itemid);
                    $boothid                 = $_POST['boothid'];
                    /**select current booth values**/
                    $selectcurrentboothvalue = $this->Order_model->selectcurrentboothvalue($orederid, $boothid);
                    foreach ($selectcurrentboothvalue as $key) {
                        $currentboothvalue    = $key->ValuePurchase;
                        $currentboothrmbvalue = $key->ValuePurchaseRMB;
                    }
                    
                    $newtotalboothvalue            = $currentboothvalue + $TotalPriceInUS;
                    $newtotalboothrmbvalue         = $currentboothrmbvalue + $TotalPriceInRMB;
                    $totalboothvalue               = array(
                        'ValuePurchase' => $newtotalboothvalue,
                        'ValuePurchaseRMB' => $newtotalboothrmbvalue
                    );
                    /**update current booth values**/
                    $updatecurrentboothvalue       = $this->Order_model->updatecurrentboothvalue($orederid, $boothid, $totalboothvalue);
                    $updatecontaineritemsavestatus = $this->Order_model->updatecontaineritemsavestatus($orederid);
                    $result                        = array(
                        'message' => 'saved',
                        'result' => $newtotalboothvalue,
                        'itemcontainersid' =>$itemcontainerid
                    );
                    echo json_encode($result);
                }
            } else {
                /**return false when invalid user**/
                $result = array(
                    'message' => 'false'
                );
                echo json_encode($result);
            }
        } else {
            /**return false when value missing**/
            $result = array(
                'message' => 'mfalse'
            );
            echo json_encode($result);
        }
    }
    

    /**end save item sub function**/
    /**function for image upload**/
    function imageUpload()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        if (!empty($_FILES['image']))
            {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $image = time() . '.' . $ext;
            move_uploaded_file($_FILES["image"]["tmp_name"], 'assets/images/' . $image);
            echo json_encode($image);
            }
          else
            {
            echo "Image Is Empty";
            }
        }

    /**end image upload**/
    /*function save new booth*/
    function Savebooth()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $token = $this->session->userdata('userToken');
        $val_businesscard = "";
        $orderid = $_POST['orderid'];
        if ($_POST['orderid'] != "" && $_POST['newBoothNo'] != "")
            {
            $orderemail = $this->Order_model->selectordermail($token);
            foreach($orderemail as $key)
                {
                $val_orderemail = $key->OrderEmail;
                }

            $compayid = $this->Order_model->selectcompanyid($val_orderemail);
            foreach($compayid as $key)
                {
                $val_companyid = $key->CompanyID;
                }

            if ($val_companyid)
                {
                $boothnum = $_POST['newBoothNo'];
                $oldboothdetail = $this->Order_model->getoldbooth($boothnum, $orderid);
                $val_businesscard = $_POST['businesscard'];
                if ($val_businesscard != 'default.jpg')
                    {
                    $filename_path = md5(time() . uniqid()) . ".jpg";
                    $decoded = base64_decode($val_businesscard);
                    file_put_contents("assets/images/" . $filename_path, $decoded);
                    }
                  else
                    {
                    $filename_path = $_POST['businesscard'];
                    }

                if ($oldboothdetail)
                    {
                    $oldboothid = $oldboothdetail->BoothId;
                    $selectlastbooth = $this->Order_model->selectlastboothid($orderid);
                    $lastboothid = 0;
                    foreach($selectlastbooth as $key)
                        {
                        $lastboothid = $key->BoothId;
                        }

                    /**change status of the previous booth**/
                    $closelastbooth = $this->Order_model->closelastbooth($lastboothid);
                    $boothno = $_POST['newBoothNo'];
                    $newboothinfo = array(
                       'BoothNumber' => $_POST['newBoothNo'],
                       'BoothName' => $_POST['newBoothName'],
                       'Phone' => $_POST['newTelephone'],
                       'BusinessScope' => $_POST['newBusinessScope'],
                       'BusinessCard' => $filename_path
                       );
                    $updatebooth = $this->Order_model->updateboothinfo($newboothinfo,$orderid,$boothno);
                    $openoldbooth = $this->Order_model->openboothold($oldboothid);
                    if ($openoldbooth != 0)
                        {
                        echo json_encode($oldboothid);
                        }
                      else
                        {
                        $result = 0;
                        echo json_encode($result);
                        }
                    }
                  else
                    {
                    $boothdetails = array(
                        'OrderId' => $_POST['orderid'],
                        'CompanyId' => $val_companyid,
                        'BoothNumber' => $_POST['newBoothNo'],
                        'BoothName' => $_POST['newBoothName'],
                        'Phone' => $_POST['newTelephone'],
                        'BusinessScope' => $_POST['newBusinessScope'],
                        'BusinessCard' => $filename_path,
                        'BoothStatus'  => '0'
                    );
                    /**save new booth**/
                    $selectlastbooth = $this->Order_model->selectlastboothid($orderid);
                    $lastboothid = 0;
                    foreach($selectlastbooth as $key)
                        {
                        $lastboothid = $key->BoothId;
                        }

                    $savebooth = $this->Order_model->savenewbooth($boothdetails);
                    $boothno = $_POST['newBoothNo'];
                    $newboothinfo = array(
                        'BoothNumber' => $_POST['newBoothNo'],
                        'BoothName' => $_POST['newBoothName'],
                        'Phone' => $_POST['newTelephone'],
                        'BusinessScope' => $_POST['newBusinessScope'],
                        'BusinessCard' => $filename_path
                    );
                    $boothinfo = $this->Order_model->getboothinfo($boothno);
                    if (!$boothinfo)
                        {
                        $boothno = $this->Order_model->saveboothinfo($newboothinfo);
                        }

                    /*save new booth*/
                    if ($savebooth)
                        {
                        echo json_encode($savebooth);
                        $closelastbooth = $this->Order_model->closelastbooth($lastboothid);
                        $fetchboothcount = $this->Order_model->fetchboothcount($orderid);
                        foreach($fetchboothcount as $key)
                            {
                            $boothcount = $key->TotalBooth;
                            }

                        $newboothcount = $boothcount + 1;
                        $boothcountarray = array(
                            'TotalBooth' => $newboothcount
                        );
                        $updateboothcount = $this->Order_model->updatecountboothcount($boothcountarray, $orderid);
                        }
                      else
                        {
                        $result = 0;
                        echo json_encode($result);
                        }

                    /**update booth count**/
                    }
                }
              else
                {
                /**return 0 when invalid user**/
                $result = 0;
                echo json_encode($result);
                }
            }
          else
            {
            /**return 0 when value missing**/
            $result = 0;
            echo json_encode($result);
            }
        }

    /**end function new booth**/
    /*function for end order*/
    function Endorder()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $orderenddate = date("Y-m-d");
        // $orderenddate = array(
        //     'OrderEndDate' =>$time
        // );
        $endorder = $this->Order_model->endorder($orderid,$orderenddate);
        }

    /**end endorder function**/
    /**fetch order values for display in the item page**/
    function Ordervalues()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $fetchordervalue = $this->Order_model->selectordervalues($orderid);
        echo json_encode($fetchordervalue);
        }

    /**end order value function**/
    /**function for get booth values**/
    function Getboothvalues()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $boothid = $_POST['boothid'];
        $fetchboothvalue = $this->Order_model->selectboothvalues($orderid, $boothid);
        echo json_encode($fetchboothvalue);
        }

    /**end get booth values**/
    /**get contianer values**/
    function GetContainerValues()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $fetchcontainerpercentage = $this->Order_model->selectcontainerpercentage($orderid);
        echo json_encode($fetchcontainerpercentage);
        }

    /**end contaienr value function**/
    function Viewallorderitems()
        {
        $i = 0;
        $containername = "";
        $containerarray = array();
        $containeid = "";
        $myArray = array();
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $order = $this->Order_model->selectorderdetails($orderid);
        $orderitems = $this->Order_model->selectorderitems($orderid);
        foreach($orderitems as $key)
            {
            $containeid = $key->ContainerId;
            $myArray = explode(',', $containeid);
            $list = "";
            $i = 0;
            foreach($myArray as $key)
                {
                $containername = $this->Order_model->selectcontainername($key);
                $name="";
                if($containername)
                {
                $name = $containername->Container;
                }
                //  echo json_encode($name);

                if ($i == 0)
                    {
                    $list = $name;
                    }
                  else
                    {
                    $list = $list . "," . $name;
                    }

                //  $containerarray[] = $containername;

                $i++;
                }

            $containerarray[] = $list;
            }

        $result = array(
            'message' => true,
            'order' => $order,
            'orderitems' => $orderitems,
            'containerid' => $containerarray
        );
        echo json_encode($result);
        }

    /**end view all order function**/
    /**function view order deatils**/
    function Vieworderdetails()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $orderdetails = $this->Order_model->selectorderdetails($orderid);
        echo json_encode($orderdetails);
        }

    /**end view order details function**/
    /**function get contaienr details function**/
    function Getcontainerdetails()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $orderdetails = $this->Order_model->selectcontainerdetails($orderid);
        foreach($orderdetails as $key)
            {
            $containername = $this->Order_model->selectlistcontainername($key->ContainerType);
            $name = $containername->Container;
            $key->ContainerName = $name;
            }

        echo json_encode($orderdetails);
        }

    /*end get container details function*/
    /**get all details of the order for continoue the order**/
    function Getalltheorderdetails()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $getalldetails = $this->Order_model->selectalldetailsorder($orderid);
        $result = array(
            'message' => true,
            'data' => $getalldetails
        );
        echo json_encode($result);
        }

    /**end getall the order details**/
  /**invoice order**/
    function Invoceorder()
    {
        //echo "HAI";
        $_POST             = json_decode(file_get_contents('php://input'), true);
         //$orderid           = $_GET['orderid'];

        
        $orderid           = $_POST['orderid'];
        $getinvoicedetails = $this->Order_model->getallorderitems($orderid);
        
        $companyid= $getinvoicedetails->CompanyId;
        $userdetails = $this->Order_model->getuserdetails($companyid);
        // $companythirdcurrency = $userdetails->ThirdCurrency;

        $boothdetails = $this->Order_model->getboothdetails($orderid);
        $totalcontainers = $this->Order_model->gettotalcontainers($orderid);

        foreach ($boothdetails as $key) {
        $getboothdetailsbyorder=$this->Order_model-> getboothdetailsbyorder($key->BoothId,$orderid);
        $key->orderitems=$getboothdetailsbyorder;
         }

        foreach ($userdetails as $key) {
             $companythirdcurrency = $key->ThirdCurrency;
        }
        //echo json_encode($companythirdcurrency);
         $getinvoicedetails->booth=$boothdetails;
         $getinvoicedetails->container=$totalcontainers;
         $getinvoicedetails->user=$userdetails;

         if($companythirdcurrency != ""){
          preg_match_all('/\(([A-Za-z0-9 ]+?)\)/', $companythirdcurrency, $out);
         $extractedthirdcurrency=$out[1][0];
        }
        else{
            $extractedthirdcurrency ="";
        }
         //echo json_encode($extract);

         $result            = array(
             'message' => true,
            'data' => $getinvoicedetails,
            'extractedthirdcurrencyvalue' =>$extractedthirdcurrency
         );
         
         echo json_encode($result);
    }
    /*end of invoice order */
    /*get transilation*/
    function GetTransilation()
        {
        $grantType = "client_credentials";
        $scopeUrl = "http://api.microsofttranslator.com";
        $clientID = "MixMyContainer_Sid-2016";
        $clientSecret = "y8Synf5Yc80DuqDdCP1DZXQkHGGn+dBJR2Ku3xGYUuY=";
        $authUrl = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
        try
            {

            // Initialize the Curl Session.

            $ch = curl_init();

            // Create the request Array.

            $paramArr = array(
                'grant_type' => $grantType,
                'scope' => $scopeUrl,
                'client_id' => $clientID,
                'client_secret' => $clientSecret
            );

            // Create an Http Query.//

            $paramArr = http_build_query($paramArr);

            // Set the Curl URL.

            curl_setopt($ch, CURLOPT_URL, $authUrl);

            // Set HTTP POST Request.

            curl_setopt($ch, CURLOPT_POST, TRUE);

            // Set data to POST in HTTP "POST" Operation.

            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramArr);

            // CURLOPT_RETURNTRANSFER- TRUE to return the transfer as a string of the return value of curl_exec().

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            // CURLOPT_SSL_VERIFYPEER- Set FALSE to stop cURL from verifying the peer's certificate.

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Execute the  cURL session.

            $strResponse = curl_exec($ch);

            // Get the Error Code returned by Curl.

            $curlErrno = curl_errno($ch);
            if ($curlErrno)
                {
                $curlError = curl_error($ch);
                throw new Exception($curlError);
                }

            // Close the Curl Session.

            curl_close($ch);

            // Decode the returned JSON string.

            $objResponse = json_decode($strResponse);

            // if ($objResponse->error){
            //     throw new Exception($objResponse->error_description);
            // }

            $token = $objResponse;
            $arr = array(
                'token' => $token
            );
            echo json_encode($arr);
            }

        catch(Exception $e)
            {
            echo "Exception-" . $e->getMessage();
            }
        }

    /*end*/
    function listcontainer()
        {
        $containerlist = $this->Order_model->selectcontainerlist();
        $result = array(
            'message' => true,
            'data' => $containerlist
        );
        echo json_encode($result);
        }

    function Usedcontainers()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
         //$orderid = $_GET['orderid'];
        // echo 'hello'.$orderid;

        $usedcontainers = $this->Order_model->selectusedcontainers($orderid);
        $result = array(
            'message' => true,
            'data' => $usedcontainers
        );
        echo json_encode($result);
        }

    function showactivecontainer()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $activecontainerid = $_POST['activecontianerid'];
        $activecontianerstatus = $this->Order_model->selectactiveccontainerstatus($orderid, $activecontainerid);
        $result = array(
            'message' => true,
            'data' => $activecontianerstatus
        );
        echo json_encode($result);
        }

    function saveimage()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $imagename = $_POST['imagename'];
        $filename_path = md5(time() . uniqid()) . ".jpg";
        $decoded = base64_decode($imagename);
        file_put_contents("assets/images/" . $filename_path, $decoded);
        echo json_encode($filename_path);
        }

    function selectusercompanydetails()
        {
        $token = $this->session->userdata('userToken');
        $orderemail = $this->Order_model->selectordermail($token);
        foreach($orderemail as $key)
            {
            $val_orderemail = $key->OrderEmail;
            }

        $usercompanydetails = $this->Order_model->selectcompanyid($val_orderemail);
        $result = array(
            'message' => true,
            'data' => $usercompanydetails
        );
        echo json_encode($result);
        }

    function GetContainertypes()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $volume = $_POST['volume'];
        $weight = $_POST['weight'];
        $containertype = $_POST['contianertype'];

        //  $containertypes = $this->Order_model->selectcontainertypes($volume);

        $currentcontainername = $this->Order_model->selectcurrentcontainername($containertype);
        $containername = $currentcontainername->Container;
        if ($currentcontainername)
            {
            $containertypes = $this->Order_model->selectcontainertypes($volume, $weight);

            // $containername = $currentcontainername->Container;
            // $maxvolume = $currentcontainername->MaximumVolume;
            // $maxweight = $currentcontainername->MaximumWeight;
            // $volume_per = ($volume/$maxvolume)*100;
            // $weight_per = ($weight/$maxweight)*100;
            // if($volume_per > $weight_per)
            // {
            // }
            // else
            // {
            //   $containertypes = $this->Order_model->selectcontainertypes_weight($weight);
            // }

            if ($containertypes)
                {
                $result = array(
                    'message' => true,
                    'data' => $containertypes,
                    'currentcontainer' => $containername
                );
                echo json_encode($result);
                }
              else
                {
                $result = array(
                    'message' => false,
                    'volume' => $volume,
                    'weight' => $weight,
                    'containertype' => $containertypes,
                    'pervolume' => $volume_per,
                    'perweight' => $weight_per
                );
                echo json_encode($result);
                }
            }
          else
            {
            $result = array(
                'message' => false,
                'volume' => $volume,
                'weight' => $weight,
                'containertype' => $containertypes,
                'pervolume' => $volume_per,
                'perweight' => $weight_per
            );
            echo json_encode($result);
            }
        }

    function Checkboothisset()
        {
        $_POST = json_decode(file_get_contents('php://input') , true);
        $orderid = $_POST['orderid'];
        $boothid = $this->Order_model->Checkboothisselected($orderid);
        if (!$boothid)
            {
            $result = array(
                'message' => false
            );
            echo json_encode($result);
            }
          else
            {
            $result = array(
                'message' => true,
                'data' => $boothid
            );
            echo json_encode($result);
            }
        }

 function Ignoreorderitemsave()
    {
        $_POST                = json_decode(file_get_contents('php://input'), true);
        $orderid              = $_POST['ignoreorderid'];
        $this->Order_model->removetempconatiner($orderid);
     }
     function getadminexhangerate()
     {
        $result     = $this->Order_model->getadminexhangerate();
        echo json_encode($result);
     }
     function Checkfornotfilledcontainers()
    {
        $_POST              = json_decode(file_get_contents('php://input'), true);

        $orderid            = $_POST['orderid'];

        // $currentcontainerid = $_POST['containerid'];
        $balancevolume      = $_POST['balancevolume'];
        $balanceweight      = $_POST['balanceweight'];
        $balancecarton     = $_POST['balancecarton'];
        // $usedweight         = $_POST['usedweight'];
        // $usedvolume         = $_POST['usedvolume'];
        // $currentcontainertype = $_POST['containertype'];

        //echo json_encode($balancevolume.'-'.$balancecarton);
        $volume_percarton   = $balancevolume/$balancecarton;
        $weight_percarton   = $balanceweight/$balancecarton;


            $list = "";
            $i = 0;
            $getallunfilledcontainers = $this->Order_model->getunfilledcontainer($orderid);

            $countofunfilledcontainers = count($getallunfilledcontainers);
            //exit();
            //echo json_encode($countofunfilledcontainers);
            // exit();
            if($countofunfilledcontainers > 1)
            {
                  foreach ($getallunfilledcontainers as $key) {
                $filledvolume = $key->FilledVolume;
                $filledweight = $key->FilledWeight;
                $containertype = $key->ContainerType;

                $selectcontainercapacity = $this->Order_model->selectcontainercapacity($containertype);
            foreach ($selectcontainercapacity as $keys) {
                $containervolume_capacity = $keys->MaximumVolume;
                $containerweight_capacity = $keys->MaximumWeight;
            }

            $availablevolume = $containervolume_capacity - $filledvolume;
            $availableweight = $containerweight_capacity - $filledweight;

            // $noofcartonpervolume = ceil($availablevolume/$volume_percarton);
            // $noofcartonperweight = ceil($availableweight/$weight_percarton);
            

            // echo json_encode($volume_percarton.'-'.$noofcartonperweight);
            // echo json_encode($availablevolume.'-'.$balanceweight);
                    if($filledvolume && $filledweight >0)
                    {
                       if($volume_percarton <= $availablevolume && $weight_percarton <= $availableweight)
                    {


                        $name = $key->ContainerId;
                        if ($i == 0)
                            {

                            $container=array($name);

                            }
                          else
                            {

                            array_push($container,$name);

                            }
                            $i++;
                    }
                    else
                    {
                        $result = array(
                            'message' => false
                         );
                    } 
                    }
                    else
                    {
                        $result = array(
                            'message' => false
                         );
                    }
                    
               }
               // echo json_encode($conta);
               if (empty($container))
                {
                    $result = array(
                    'message' => "containerempty"
                    );
                    echo json_encode($result);
                }
                else
                {
                    $result = array(
                    'message' => true,
                    'data'    =>$container
                    );
                    echo json_encode($result);
                } 
            }
            else {
                 $result = array(
                    'message' => "containerempty"
                    );
                    echo json_encode($result);
            }

            


     }
     function UpdateContainer()
     {
        
         //$orderid            = $_POST['orderid'];
        
        //$token               = $_POST['ordertoken'];
        //echo $token;
        $_POST = json_decode(file_get_contents('php://input') , true);
        $token = $this->session->userdata('userToken');
        $orderemail = $this->Order_model->selectordermail($token);
            foreach ($orderemail as $key) {
                $val_orderemail = $key->OrderEmail;
            }
            
            $compayid = $this->Order_model->selectcompanyid($val_orderemail);
            foreach ($compayid as $key) {
                $val_companyid = $key->CompanyID;
            }

        if ($val_companyid) {
            
        $orderid                    = $_POST['orderid'];
        $boothid                    = $_POST['boothid'];
        $containerid                = $_POST['containerid'];
        $unfilledcontainerid        = $_POST['unfilledcontainerid'];
        // echo $unfilledcontainerid;
        // exit();
        $usedvolume                 = $_POST['usedvolume'];
        $usedweight                 = $_POST['usedweight'];
        $balancevolume              = $_POST['balancevolume'];
        $balanceweight              = $_POST['balanceweight'];
        $balancecartons             = $_POST['balancecartons'];
        $currentcontainertype       = $_POST['currentcontainertype'];


        $volume_percarton   = $balancevolume/$balancecartons;
        $weight_percarton   = $balanceweight/$balancecartons;

        // $noofcartonthatcanbemovedforvolume = $balancevolume/$volume_percarton;
        // $noofcartonthatcanbemovedforweight = $balanceweight/$weight_percarton;


        $selectcontainercapacity = $this->Order_model->selectcontainercapacity($currentcontainertype);
            foreach ($selectcontainercapacity as $key) {
                $containervolume_capacity = $key->MaximumVolume;
                $containerweight_capacity = $key->MaximumWeight;
            }
            
            
            $volumepercentage = ($usedvolume / $containervolume_capacity) * 100;
            $weightpercentage = ($usedweight / $containerweight_capacity) * 100;
            $volumepercentage = number_format($volumepercentage, 2);
            $weightpercentage = number_format($weightpercentage, 2);
            //echo $usedvolume.'-'.$volumepercentage;
            
            $currentfillingcontainer     = array(
                'FilledWeight' => $usedweight,
                'FilledVolume' => $usedvolume,
                'Volumepercentage' => $volumepercentage,
                'Weightpercentage' => $weightpercentage
            );

            $updateCotainerSpace = $this->Order_model->updatetempContainerSpace($currentfillingcontainer, $orderid);


            

           $getunfilledcontainertype = $this->Order_model->getunfilledcontainerdetails($unfilledcontainerid); 
           foreach ($getunfilledcontainertype as $key) {
            $filledvolume = $key->FilledVolume;
            $filledweight = $key->FilledWeight;
            $containertype = $key->ContainerType;
           }

            $selectcontainercapacity = $this->Order_model->selectcontainercapacity($containertype);
            foreach ($selectcontainercapacity as $key) {
                $containervolume_capacity = $key->MaximumVolume;
                $containerweight_capacity = $key->MaximumWeight;
            }

            $availablevolume = $containervolume_capacity - $filledvolume;
            $availableweight = $containerweight_capacity - $filledweight;

            $noofcartontobemoved = $availablevolume/$volume_percarton;


            if ($balancevolume > $availablevolume || $balanceweight > $availableweight) {
                    if ($balancevolume > $availablevolume && $balanceweight < $availableweight) {
                        /**process balance volume is gratere than container volme capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonvolume       = $_POST['cbm'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $balancevolume      = $balancevolume - $availablevolume; // calculation for balance volume after fill the item in the container
                        $noofcartonmoved    = ($balancevolume / $cartonvolume);
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontainervolume = $noofcartonmoved * $cartonvolume;
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $usedvolume         = $balancevolume - $newcontainervolume;
                        $usedweight         = $balanceweight - $newcontainerweight;
                        $newcontinerweight  = $noofcartonmoved * $cartonweight;
                        /**return balance volume, balance weight and used volume and used weight**/
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontainervolume,
                            'balanceweight' => $newcontinerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    } else if ($balanceweight > $availableweight && $balancevolume < $availablevolume) {
                        /**process when balance weight is greater than container capacity**/
                        $cartonqty          = $_POST['cartonqty'];
                        $cartonweight       = $_POST['cartonGrossWeight'];
                        $cartonvolume       = $_POST['cbm'];
                        $balanceweight      = $balanceweight - $availableweight;
                        $noofcartonmoved    = ($balanceweight / $cartonweight);
                        $noofcartonmoved    = ceil($noofcartonmoved);
                        $newcontainerweight = $noofcartonmoved * $cartonweight;
                        $newcontainervolume = $noofcartonmoved * $cartonvolume;
                        $usedweight         = $balanceweight - $newcontainerweight;
                        $usedvolume         = $balancevolume - $newcontainervolume;
                        $newcontinervolume  = $noofcartonmoved * $cartonvolume;
                        /**return balance weight, balance volume used volume, used weight**/
                        $result             = array(
                            'message' => 'true',
                            'balancevolume' => $newcontinervolume,
                            'balanceweight' => $newcontainerweight,
                            'usedvolume' => $usedvolume,
                            'usedweight' => $usedweight,
                            'balancecartons' => $noofcartonmoved
                        );
                        echo json_encode($result);
                    } else if ($balancevolume > $availablevolume && $balanceweight > $availableweight) {
                        $cartonqty                = $_POST['cartonqty'];
                        $cartonweight             = $_POST['cartonGrossWeight'];
                        $cartonvolume             = $_POST['cbm'];
                        $balancevolume            = $balancevolume - $availablevolume; //find over flow volume
                        $noofcartonmovedforvolume = ($balancevolume / $cartonvolume); //cartons for balance volume
                        $noofcartonmovedforvolume = ceil($noofcartonmovedforvolume); //round off no of carton volume
                        $balanceweight            = $balanceweight - $availableweight; //find over flow weight
                        $noofcartonmovedforweight = ($balanceweight / $cartonweight); //find cartons for balace volume
                        $noofcartonmovedforweight = ceil($noofcartonmovedforweight); //round off no of carton volume
                        if ($noofcartonmovedforvolume > $noofcartonmovedforweight) {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforvolume;
                            $usedweight                  = $balanceweight - $newcontainerweightforvolume;
                            $newcontinerweightforvolume  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        } else if ($noofcartonmovedforvolume < $noofcartonmovedforweight) {
                            $newcontainervolumeforweight = $noofcartonmovedforweight * $cartonvolume;
                            $newcontainerweightforweight = $noofcartonmovedforweight * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforweight;
                            $usedweight                  = $balanceweight - $newcontainerweightforweight;
                            $newcontinerweightforweight  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforweight,
                                'balanceweight' => $newcontinerweightforweight,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforweight
                            );
                            echo json_encode($result);
                        } else {
                            $newcontainervolumeforvolume = $noofcartonmovedforvolume * $cartonvolume;
                            $newcontainerweightforvolume = $noofcartonmovedforvolume * $cartonweight;
                            $usedvolume                  = $balancevolume - $newcontainervolumeforvolume;
                            $usedweight                  = $balanceweight - $newcontainerweightforvolume;
                            $newcontinerweightforvolume  = $noofcartonmovedforweight * $cartonweight;
                            /**return balance volume, balance weight, used volume, used weight**/
                            $result                      = array(
                                'message' => 'true',
                                'balancevolume' => $newcontainervolumeforvolume,
                                'balanceweight' => $newcontinerweightforvolume,
                                'usedvolume' => $usedvolume,
                                'usedweight' => $usedweight,
                                'balancecartons' => $noofcartonmovedforvolume
                            );
                            echo json_encode($result);
                        }
                    } else {
                        $result = array(
                            'message' => 'false'
                        );
                        echo json_encode($result);
                    }

                $volumeforfirstunfilledcontainer = $noofcartontobemoved * $volume_percarton;
                $weightforfirstunfilledcontainer = $noofcartontobemoved * $weight_percarton;

                $totalvolumeforunfilledcontainer = $volumeforfirstunfilledcontainer + $filledvolume;
                $totalweightforunfilledcontainer = $weightforfirstunfilledcontainer + $filledweight;

                $volumepercentage = ($totalvolumeforunfilledcontainer / $containervolume_capacity) * 100;
                $weightpercentage = ($totalweightforunfilledcontainer / $containerweight_capacity) * 100;
                $volumepercentage = number_format($volumepercentage, 2);
                $weightpercentage = number_format($weightpercentage, 2);

                $unfilledfillingcontainer     = array(
                'FilledWeight' => $totalweightforunfilledcontainer,
                'FilledVolume' => $totalvolumeforunfilledcontainer,
                'Volumepercentage' => $volumepercentage,
                'Weightpercentage' => $weightpercentage
            );

            $updateCotainerSpace = $this->Order_model->updateunfilledContainerSpace($unfilledfillingcontainer, $orderid,$unfilledcontainerid);

                }
                else
                {
                    $date             = date("Y/m/d");
                    $imagename        = $_POST['itemimage'];
                    if ($imagename != 'default.jpg') {
                        $filename_path = md5(time() . uniqid()) . ".jpg";
                        $decoded       = base64_decode($imagename);
                        file_put_contents("assets/images/" . $filename_path, $decoded);
                    } else {
                        $filename_path = $_POST['itemimage'];
                    }

                    $TotalPriceInUS          = ($_POST['unitqty']) * ($_POST['usprice']);
                    $TotalPriceInRMB         = ($_POST['unitqty']) * ($_POST['priceRMB']);
                    $TotalPriceIn3rdCurrency = ($_POST['unitqty']) * ($_POST['ExchangeRate3dCurrency']);
                  
                    $itemdetails = array(
                        'CompanyId' => $val_companyid,
                        'OrderId' => $_POST['orderid'],
                        'BoothId' => $_POST['boothid'],
                        'ContainerId' => $unfilledcontainerid,
                        'ItemNo' => $_POST['itemno'],
                        'DescriptionFrom' => $_POST['description'],
                        'DescriptionTo' => $_POST['altdescription'],
                        'UnitperCarton' => $_POST['unitpercarton'],
                        'WeightofCarton' => $_POST['cartonGrossWeight'],
                        'Length' => $_POST['Length'],
                        'Width' => $_POST['Width'],
                        'Height' => $_POST['Height'],
                        'CBM' => $_POST['cbm'],
                        'Picture' => $filename_path,
                        'PriceInUs' => $_POST['usprice'],
                        'PriceInRmb' => $_POST['priceRMB'],
                        'PriceInThirdCurrency' => $_POST['ExchangeRate3dCurrency'],
                        'UnitQuantity' => $_POST['unitqty'],
                        'CartonQuantity' => $_POST['cartonqty'],
                        'TotalPriceInUS' => $TotalPriceInUS,
                        'TotalPriceInRMB' => $TotalPriceInRMB,
                        'TotalPriceIn3rdCurrency' => $TotalPriceIn3rdCurrency,
                        'Date' => $date
                    );
                    // echo json_encode($itemdetails);
                    // exit();
                    /**save item details**/
                    $itemid      = $this->Order_model->saveitem($itemdetails);

                    
                    $selectCurrentordertotal = $this->Order_model->selectordertotal($orderid);
                    foreach ($selectCurrentordertotal as $key) {
                        $TotalOrderUsValue  = $key->TotalOrderUsValue;
                        $TotalOrderRmbValue = $key->TotalOrderRmbValue;
                        $TotalOrderZarValue = $key->TotalOrderZarValue;
                        $TotalCartons       = $key->TotalCartons;
                        $TotalOrderWeight   = $key->TotalOrderWeight;
                        $TotalOrderCBM      = $key->TotalOrderCBM;
                    }
                    
                    $ordertotalvalues = array(
                        'TotalOrderUsValue' => $TotalOrderUsValue + $TotalPriceInUS,
                        'TotalOrderRmbValue' => $TotalOrderRmbValue + $TotalPriceInRMB,
                        'TotalOrderZarValue' => $TotalOrderZarValue + $TotalPriceIn3rdCurrency,
                        'TotalCartons' => $TotalCartons + $_POST['cartonqty'],
                        'TotalOrderWeight' => $TotalOrderWeight + ($_POST['cartonqty'] * $_POST['cartonGrossWeight']),
                        'TotalOrderCBM' => $TotalOrderCBM + ($_POST['cartonqty'] * $_POST['cbm'])
                    );
                    /**update order values**/
                    $updateordervales = $this->Order_model->updateordervalues($ordertotalvalues, $orderid);

                    
                    $boothid                 = $_POST['boothid'];
                    /**select current booth values**/
                    $selectcurrentboothvalue = $this->Order_model->selectcurrentboothvalue($orderid, $boothid);
                    foreach ($selectcurrentboothvalue as $key) {
                        $currentboothvalue    = $key->ValuePurchase;
                        $currentboothrmbvalue = $key->ValuePurchaseRMB;
                    }
                    
                    $newtotalboothvalue            = $currentboothvalue + $TotalPriceInUS;
                    $newtotalboothrmbvalue         = $currentboothrmbvalue + $TotalPriceInRMB;
                    $totalboothvalue               = array(
                        'ValuePurchase' => $newtotalboothvalue,
                        'ValuePurchaseRMB' => $newtotalboothrmbvalue
                    );
                    /**update current booth values**/
                    $updatecurrentboothvalue       = $this->Order_model->updatecurrentboothvalue($orderid, $boothid, $totalboothvalue);
                    $updatecontaineritemsavestatus = $this->Order_model->updatecontaineritemsavestatus($orderid);

                    $getunfilledcontainertype = $this->Order_model->getunfilledcontainerdetails($unfilledcontainerid);
                    foreach ($getunfilledcontainertype as $key) {
                        $unfilledcontainetrtype   = $key->ContainerType;
                        $filledvolume             = $key->FilledVolume;
                        $filledweight             = $key->FilledWeight;

                    }

                    $unfilledcontainertotalvolume = $filledvolume + $balancevolume;
                    $unfilledconatinerttalweight  = $filledweight + $balanceweight;

                    $selectcontainercapacity = $this->Order_model->selectcontainercapacity($unfilledcontainetrtype);
                        foreach ($selectcontainercapacity as $key) {
                            $containervolume_capacity = $key->MaximumVolume;
                            $containerweight_capacity = $key->MaximumWeight;
                        }

                    $volumepercentage = ($unfilledcontainertotalvolume / $containervolume_capacity) * 100;
                    $weightpercentage = ($unfilledconatinerttalweight / $containerweight_capacity) * 100;
                    $volumepercentage = number_format($volumepercentage, 2);
                    $weightpercentage = number_format($weightpercentage, 2);
                    //echo $usedvolume.'-'.$volumepercentage;
                    
                    $unfilledfillingcontainer     = array(
                        'FilledWeight' => $unfilledconatinerttalweight,
                        'FilledVolume' => $unfilledcontainertotalvolume,
                        'Volumepercentage' => $volumepercentage,
                        'Weightpercentage' => $weightpercentage
                    );

                    $updateCotainerSpace = $this->Order_model->updateunfilledContainerSpace($unfilledfillingcontainer, $orderid,$unfilledcontainerid);

                    $alltempconatiners = $this->Order_model->getalltempcontainers($orderid);
                        $itemcontainerid="";
                        foreach ($alltempconatiners as $key) {

                            if ($key->OrginalContainerId != 0) {
                                $oldconatainer       = $key->OrginalContainerId;
                                $containerSpance     = array(
                                    'FilledWeight' => $key->FilledWeight,
                                    'FilledVolume' => $key->FilledVolume,
                                    'Volumepercentage' => $key->Volumepercentage,
                                    'Weightpercentage' => $key->Weightpercentage,
                                    'Status' => $key->Status,
                                    'ItemSaveStatus' => 1
                                );
                                $updateCotainerSpace = $this->Order_model->updateoldcontainer($containerSpance, $oldconatainer);
                                //$itemcontainerid= $itemcontainerid.",".$oldconatainer;
                            }
                            
                            else {
                                $val_newcontainer    = array(
                                    'ContainerType' => $key->ContainerType,
                                    'OrderId' => $key->OrderId,
                                    'CompanyId' => $key->CompanyId,
                                    'FilledWeight' => $key->FilledWeight,
                                    'FilledVolume' => $key->FilledVolume,
                                    'Volumepercentage' => $key->Volumepercentage,
                                    'Weightpercentage' => $key->Weightpercentage,
                                    'Status' => $key->Status,
                                    'ItemSaveStatus' => 1
                                );
                                /*calling model sql query*/
                                $result_newcontainer = $this->Order_model->savenewcontainer($val_newcontainer);

                                
                            }
                        }
                        $this->Order_model->removetempconatiner($orderid);

                    $result     = array(
                        'message' => 'containerupdated'
                        );

                    echo json_encode($result);

                }


                
        } else {

            $result = array(
                'message' => 'not valid company'
                );
            echo json_encode($result);
                
                }

     }
    }
// }
?>