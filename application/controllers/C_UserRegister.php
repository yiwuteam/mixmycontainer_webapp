<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_UserRegister extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

         $this->load->model('Register_model');
         $this->load->model('HtmlLoginEmail_model');
         $this->load->model('ResetPasswordEmail_model');
    }

    function index()
    {
      // $this->load->helper(array('form'));



    }
    // view registratoin page
    function view_registation_page()
    {

        $this->load->view('register');

    }
    //end

    //view forgotpassword page
     function forgotpassword_view()
    {
         $this->load->view('header');
        $this->load->view('forgotpassword_view');
        $this->load->view('footer');
    }
    //end
    //user registration

     public function userregister(){


        $result                = 2;
        $_GET = json_decode(file_get_contents('php://input'),true);
        $useremail              = $_GET['email'];
        $userenteredpassword   = $_GET['password'];
        //$shuffled              = str_shuffle($userenteredpassword);
        $encryptedpassword     = md5($userenteredpassword);
        $userpassword          = $encryptedpassword;
        $usercompanyname     = $_GET['companyname'];
        $usercountryname     = $_GET['country'];
        $userzipcode         = $_GET['zipcode'];
        $userphone           = $_GET['phonenumber'];
        $usermobile          = $_GET['mobilenumber'];
        $useraddress         = $_GET['address'];
        $usertowncity        = $_GET['towncity'];
        // $success          = 1;

        $usercheck              = $this->Register_model->checkuserexists($useremail);
        //$usernamecheck        = $this->Register_model->checkusernameexists($username);
        if($usercheck == null)
        {
            $userregisterdetails = array('Password'=> $userpassword,
                                'Email'    => $useremail,
                                'UserType' => 'normal' );
         $result     = $this->Register_model-> saveuseregister($userregisterdetails);
         
         $companysetup = array(
                'OrderEmail'    => $useremail,
                'MailOrder'     => $useremail,
                'CompanyName'   => $usercompanyname,
                'Country'       => $usercountryname,
                'Address'       => $useraddress,
                'Town/City'     => $usertowncity,
                'ZipCode'       => $userzipcode,
                'Phone'         => $userphone,
                'Mobile'        => $usermobile
            );
         $result =$this->Register_model->savecompanysetup($companysetup);

           $result           = 1;
          echo json_encode($result);
       }
       else
       {
            
            echo json_encode($result);
       }
       

     }

     //end
     //check userexist when registration

     function checkusernameexist()
     {
            $username = $_GET['id'];
            $result = $this->Register_model->checkusernameexists($username);
             echo json_encode($result);

     }
     //end
     //forgot password
     function forgotpassword()
     {
        $str = 'Aa8DcWb42z6T';
        $forotp = '852145';
        $shuffled = str_shuffle($str);
        $otp =  str_shuffle($forotp);
        $this->session->set_userdata('otp',$otp);
        set_cookie('otp',$otp,'3600');

        $encryptedpassword = md5($shuffled);
        $userpassword = $encryptedpassword;
        $_POST = json_decode(file_get_contents('php://input'),true);
        $useremail         = $_POST['email'];
        set_cookie('email',$useremail,'3600');
        //$useremail = $this->input->post('forgotuseremail');
        $usercheck = $this->Register_model->checkuserexists($useremail);
        if($usercheck != null)
        {
            $this->session->set_userdata('mail',$useremail);
            $setnewpassword=array('Password'=>$userpassword);
            $setotp=array('Otp'=>$otp);
            $saveotp = $this->Register_model->saveotp($setotp,$useremail);
            $content="Your Request For Reset Password In Mixmycontainer Has Been Granted, Please Follow The Link To Reset Your Password & Provide The OTP As $otp  ";
            $url="https://mixmycontainer.com/QA/Login/reset";
            $emailmessage=$this->ResetPasswordEmail_model->EmailMessage($useremail,$content,$url);
            $from_email = "webmaster@mixmycontainer.com";
       //  Load email library
            $this->load->library('email');
             $config['charset'] = 'utf-8';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = TRUE;
                    $this->email->initialize($config);
            $this->email->from($from_email,'Mixmycontainer');
            $this->email->to($useremail);
            $this->email->subject('Mixmycontainer Account Reset');
            $this->email->message($emailmessage);
        // Send mail
            $this->email->send();
            $result="1";
            echo json_encode($result);
        }
        else
        {
            $result = 2;
             echo json_encode($result);
        }
     }
     //end

     //get all countries list for registration
    function getcountries()
    {
        $result = $this->Register_model->getcountries();
        echo json_encode($result);
    }
    //end

}
