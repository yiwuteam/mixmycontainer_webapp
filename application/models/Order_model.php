<?php
Class Order_model extends CI_Model
{

        /** Abhijith A Nair's Code */
    /*Get Booth Info */
    function getboothinfo($boothno)
    {
        $this->db->select('*');
        $this->db->from('tble_boothinfo');
        $this->db->where('BoothNumber', $boothno);
        $query = $this->db->get();
        return $query->row();
    }
    /* Get All Trades from Database */
    function gettradorhalllist($parent,$company)
    {
        $this->db->select('*');
        $this->db->from('tble_tradeandhall');
        $this->db->where('Parent='.$parent.' AND (CompanyId= '.$company.' OR CompanyId=0)' );
        $query = $this->db->get();
        return $query->result();
    }
    /* get container types */
    function getcontainertype()
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $query = $this->db->get();
        return $query->result();
    }
    /* add custom trade */
     function savecustomtradeorhall($tradedetails)
    {
        $this->db->insert('tble_tradeandhall', $tradedetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    /*save booth main*/
    function saveboothinfo($boothinfo)
    {
        $this->db->insert('tble_boothinfo', $boothinfo);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function updateboothinfo($newboothinfo,$orderid,$boothno)
    {
        $this->db->where('OrderId', $orderid);
        $this->db->where('BoothNumber', $boothno);
        $this->db->update('tble_booth', $newboothinfo);
    }
    /*check trade already saved or not */
    function checktradeorhall($name,$company)
    {
        $this->db->select('*');
        $this->db->from('tble_tradeandhall');
        $this->db->where('(Name='.$name.') AND (CompanyId='.$company.' OR CompanyId=0)');
        $query = $this->db->get();
        return $query->result();
    }
     /*Save base64 image */
     function saveimage($base64img)
     {
        if (strpos($base64img, '.jpg') ) {
            $filename_path=$base64img;
        }
        else{
        $filename_path = md5(time().uniqid()).".jpg";
        $decoded=base64_decode($base64img);
        file_put_contents("assets/images/".$filename_path,$decoded);
        }
        return $filename_path;
     }
     function companydetails($val_orderemail)
    {
        $this->db->select('*');
        $this->db->from('tble_companysetup');
        $this->db->where('OrderEmail',$val_orderemail);
        $query = $this->db->get();
        return $query->row();
    }
       function getordermail($token)
    {
        $this->db->select('*');
        $this->db->from('tble_token');
        $this->db->where('Token', $token);
        $query = $this->db->get();
        return $query->row();

    }

     function getordersbydate($val_companyid,$fromdate,$todate)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('CompanyId', $val_companyid);
        $this->db->where('LastUpdate >=', $fromdate);
        $this->db->where('LastUpdate <=', $todate);
        $query = $this->db->get();
        return $query->result();
    }

    /** Abhijith A Nair's Code */


    function checkorderno($orderno)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('OrderNumber', $orderno);
        $query = $this->db->get();
        return $query->result();
    }
    function SaveOrder($orderdetails)
    {
        $this->db->insert('tble_orders', $orderdetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function SaveBooth($boothdetails)
    {
        $this->db->insert('tble_booth', $boothdetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function getcartonunits($val_orderemail)
    {
        $this->db->select('GrossweightCarton,Width,Height,Length,ExchangeRate,ThirdCurrency,ExchangeRate3dCurrency,Active3dCurrency');
        $this->db->from('tble_companysetup');
        $this->db->where('OrderEmail', $val_orderemail);
        $query = $this->db->get();
        return $query->result();

    }

    function selectcompanyid($val_orderemail)
    {
        $this->db->select('*');
        $this->db->from('tble_companysetup');
        $this->db->where('OrderEmail', $val_orderemail);
        $query = $this->db->get();
        return $query->result();
    }
    function saveContainer($containerdetails)
    {
        $this->db->insert('tble_container', $containerdetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function saveitem($itemdetails)
    {
        $this->db->insert('tble_orderitems', $itemdetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function selectordertotal($orederid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('OrderId', $orederid);
        $this->db->where('OrderStatus', $status);
        $query = $this->db->get();
        return $query->result();
    }
    function updateordervalues($ordervalues, $orederid)
    {
        $status = 0;
        $this->db->where('OrderId', $orederid);
        $this->db->update('tble_orders', $ordervalues);
    }
    function savenewbooth($boothdetails)
    {
        $this->db->insert('tble_booth', $boothdetails);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function selectlastboothid($orderid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tble_booth');
        $this->db->where('BoothStatus', $status);
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function closelastbooth($lastboothid)
    {
        $value=1;
        $sql = "UPDATE tble_booth SET BoothStatus = ? WHERE BoothId = ?";
        $this->db->query($sql, array($value, $lastboothid));
       // $rows=$this->db->last_query();
         $rows = $this->db->affected_rows();

        return $rows;
    }
    function selectcontainerstatus($orederid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tble_container');
        $this->db->where('OrderId', $orederid);
        $this->db->where('Status', $status);
        $query = $this->db->get();
        return $query->result();
    }
    function selectconatinercapacity($containertype)
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $this->db->where('ID', $containertype);
        $query = $this->db->get();
        return $query->result();
    }
    function updateContainerSpace($containerSpance, $orederid)
    {
        $status = 0;
        $this->db->where('OrderId', $orederid);
        $this->db->where('Status', $status);
        $this->db->update('tble_container', $containerSpance);
    }
    // function closecontainer($containerid)
    // {
    //   $status = array('Status' => '1');
    //   $this->db->where('ContainerId',$containerid);
    //   $this->db->update('tble_container',$status);
    // }
    function savenewcontainer($containerdata)
    {
        $this->db->insert('tble_container', $containerdata);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function upgradecontainer($containerdata, $orderid, $containerid)
    {
        $status = 0;
        $this->db->where('ContainerId', $containerid);
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', $status);
        $this->db->update('tble_container', $containerdata);
    }
    function endorder($orderid)
    {
        $status1 = array(
            'OrderStatus' => 1
        );
        $status2 = array(
            'BoothStatus' => 1
        );
        $status3 = array(
            'Status' => 1
        );

        $this->db->where('tble_orders.OrderId', $orderid);
        $this->db->update('tble_orders', $status1);
        $this->db->where('tble_booth.OrderId', $orderid);
        $this->db->update('tble_booth', $status2);
        $this->db->where('tble_container.OrderId', $orderid);
        $this->db->update('tble_container', $status3);

    }
    function selectcurrentboothvalue($orederid, $boothid)
    {
        $this->db->select('*');
        $this->db->from('tble_booth');
        $this->db->where('BoothId', $boothid);
        $this->db->where('OrderId', $orederid);
        $query = $this->db->get();
        return $query->result();
    }

    function updatecurrentboothvalue($orederid, $boothid, $totalboothvalue)
    {
        $this->db->where('OrderId', $orederid);
        $this->db->where('BoothId', $boothid);
        $this->db->update('tble_booth', $totalboothvalue);
    }
    function selectordervalues($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function selectboothvalues($orderid, $boothid)
    {
        $this->db->select('*');
        $this->db->from('tble_booth');
        $this->db->where('OrderId', $orderid);
        $this->db->where('BoothId', $boothid);
        $query = $this->db->get();
        return $query->result();
    }
    function selectcontainerpercentage($orderid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tble_container');
        $this->db->join('tble_containertypes', 'tble_containertypes.ID = tble_container.ContainerType');
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', $status);
        $query = $this->db->get();
        return $query->result();
    }
    function getallorders($val_companyid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('CompanyId', $val_companyid);
        //$this->db->order_by("tble_orders.OrderId", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function selectorderitems($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orderitems');
        //$this->db->join('tble_container','tble_orderitems.OrderId = tble_container.OrderId');
        $this->db->join('tble_booth', 'tble_orderitems.BoothId = tble_booth.BoothId');
        $this->db->join('tble_orders', 'tble_orderitems.OrderId = tble_orders.OrderId');
        $this->db->where('tble_orderitems.OrderId', $orderid);

        $query = $this->db->get();
        return $query->result();
    }
    function selectorderdetails($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function selectcontainerdetails($orderid)
    {
        $this->db->select('ContainerType, COUNT(ContainerType) as Count');
        $this->db->from('tble_container');
        $this->db->group_by('ContainerType');
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function selectlistcontainername($key)
    {
        $this->db->select('tble_containertypes.Container');
        $this->db->from('tble_containertypes');
        $this->db->where('tble_containertypes.ID', $key);

        $query = $this->db->get();
        return $query->row();
    }
    function updateitemcontainertype($itemcontainertype, $itemid)
    {
        $this->db->where('ItemId', $itemid);
        $this->db->update('tble_orderitems', $itemcontainertype);
    }
    function selectalldetailsorder($orderid)
    {

        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->join('tble_container', 'tble_orders.OrderId = tble_container.OrderId AND tble_container.Status = 0');
        //$this->db->join('tble_booth', 'tble_orders.OrderId = tble_booth.OrderId AND tble_booth.BoothStatus = 0');
        $this->db->where('tble_orders.OrderId', $orderid);

        $query = $this->db->get();
        return $query->result();
    }
    function getallorderitems($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        //$this->db->join('tble_orderitems', 'tble_orders.OrderId = tble_orderitems.OrderId');
        $this->db->where('tble_orders.OrderId', $orderid);
        $query = $this->db->get();
        return $query->row();
    }
    function selectordermail($token)
    {
        $this->db->select('*');
        $this->db->from('tble_token');
        $this->db->where('Token', $token);
        $query = $this->db->get();
        return $query->result();

    }
    function getemailorderboothdetails($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->join('tble_booth', 'tble_orders.OrderId = tble_booth.OrderId');
        $this->db->join('tble_tradeandhall', 'tble_tradeandhall.Id = tble_booth.TradeId');
        $this->db->where('tble_orders.OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function getemailitemdetails($orderid,$boothid)
    {
        $this->db->select('*');
        $this->db->from('tble_orderitems');
        $this->db->join('tble_orders', 'tble_orderitems.OrderId = tble_orders.OrderId');
        // $this->db->join('tble_booth', 'tble_orderitems.OrderId = tble_booth.OrderId');
        // $this->db->join('tble_orders', 'tble_orderitems.OrderId = tble_orders.OrderId');
        $this->db->where('tble_orderitems.OrderId', $orderid);
        $this->db->where('tble_orderitems.BoothId', $boothid);
        //$this->db->where('tble_orderitems.BoothId', $orderboothid);
        $query = $this->db->get();
        return $query->result();
    }
    function fetchboothcount($orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_orders');
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function updatecountboothcount($newboothcount, $orderid)
    {
        $this->db->where('OrderId', $orderid);
        $this->db->update('tble_orders', $newboothcount);
    }
    function selectcontainername($key)
    {
        $this->db->select('tble_containertypes.Container');
        $this->db->from('tble_containertypes');
        $this->db->join('tble_container', 'tble_container.ContainerType = tble_containertypes.ID');
        $this->db->where('tble_container.ContainerId', $key);

        $query = $this->db->get();
        return $query->row();
    }
    function selectcontainerlist()
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $query = $this->db->get();
        return $query->result();
    }
    function selectusedcontainers($orderid)
    {
        $this->db->select('*,COUNT(ContainerType) as Count');
        $this->db->from('tble_container');
        $this->db->group_by('ContainerType');
        $this->db->order_by("tble_container.ContainerId", "desc");
        $this->db->join('tble_containertypes', 'tble_containertypes.ID = tble_container.ContainerType');
        $this->db->where('tble_container.OrderId', $orderid);

        $query = $this->db->get();
        return $query->result();
    }
    function selectactiveccontainerstatus($orderid, $activecontainerid)
    {
        $this->db->select('*');
        $this->db->from('tble_container');
        $this->db->where('OrderId', $orderid);
        $this->db->where('ContainerId', $activecontainerid);
        $query = $this->db->get();
        return $query->row();
    }
   function selectcontainertypes($volume,$weight)
    {
      $this->db->select('*');
      $this->db->from('tble_containertypes');
      $this->db->where('MaximumVolume >',$volume);//.'AND MaximumWeight >'.$weight);
      $this->db->where('MaximumWeight >',$weight);
      $query = $this->db->get();
      return  $query->result();
    }
     
    function selectcurrentcontainername($containertype)
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $this->db->where('ID',$containertype);
        $query = $this->db->get();
        return $query->row();
    }
    function selectordercompanyid($orderid)
    {
      $this->db->select('CompanyId');
      $this->db->from('tble_orders');
      $this->db->where('OrderId',$orderid);
      $query = $this->db->get();
      return  $query->row();
    }
    function selectmailorders($companyid)
    {
      $this->db->select('*');
      $this->db->from('tble_companysetup');
      $this->db->where('CompanyID',$companyid);
      $query = $this->db->get();
      return  $query->row();
    }
    function selectagentemail($agentid)
    {
      $this->db->select('*');
      $this->db->from('tble_goldagentrepresentative');
      $this->db->where('GoldAgentId',$agentid);
      $query = $this->db->get();
      return  $query->result();
    }
    function selectcontainercapacity($containertype)
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $this->db->where('ID',$containertype);
        $query = $this->db->get();
        return $query->result();
    }
      function Checkboothisselected($orderid)
    {
        $this->db->select('BoothId');
        $this->db->from('tble_booth');
        $this->db->where('OrderId',$orderid);
        $this->db->where('BoothStatus=',0);

        $query = $this->db->get();
        return $query->result();
    }
     function getoldbooth($boothno,$orderid)
    {
        $this->db->select('*');
        $this->db->from('tble_booth');
        $this->db->where('OrderId',$orderid);
        $this->db->where('BoothNumber',$boothno);
        $query = $this->db->get();
        return $query->row();
    }
    function openboothold($oldboothid)
    {
        
         $value=0;
         $sql = "UPDATE tble_booth SET BoothStatus = ? WHERE BoothId = ?";
         $this->db->query($sql, array($value, $oldboothid));
         $rows = $this->db->affected_rows();
         return $rows;
    }
    function getboothdetails($orderid)
    {
      $this->db->select('*');
      $this->db->from('tble_booth');
      $this->db->join('tble_tradeandhall', 'tble_tradeandhall.Id = tble_booth.TradeId');
      $this->db->where('OrderId',$orderid);
      $query = $this->db->get();
      return $query->result();
    }
    function getboothdetailsbyorder($boothid,$orderid)
    {
      $this->db->select('*');
      $this->db->from('tble_orderitems');
      $this->db->where('BoothId',$boothid);
      $this->db->where('OrderId',$orderid);
      $query = $this->db->get();  
      return $query->result();
    }
     function updatecontaineritemsavestatus($orederid)
    {
        $containersavesatus = array('ItemSaveStatus' => 1);
        $this->db->where('OrderId', $orederid);
        $this->db->update('tble_container', $containersavesatus); 
    }
    function deleteinvalidcontainer($orderid)
    {
        $status = 0;
        $this->db->where('OrderId', $orderid);
        $this->db->where('ItemSaveStatus',$status);
        $this->db->delete('tble_container');

    }
    function resetcontainer($firstcontainerid,$resetcontainervalues)
    {

        $this->db->where('ContainerId',$firstcontainerid);
        $this->db->update('tble_container',$resetcontainervalues); 
        return "hello";
    }
   /*Santhosh Code */
   function gettotalcontainers($orderid){
       $this->db->select('*,Count(ContainerType) as Count');
       $this->db->from('tble_container');
       $this->db->join('tble_containertypes', 'tble_containertypes.ID = tble_container.ContainerType');
       $this->db->group_by('ContainerType');
       $this->db->where('OrderId', $orderid);
       $query = $this->db->get();
       return $query->result();
    }
    /*Temp Container Function --Abhijith Code --*/
    function removetempconatiner($orderid)
    {
         $status = 0;
         $this->db->where('OrderId',$orderid); 
         $this->db->delete('tmp_tble_container'); 
    }
    function updatetempcontainerstatus($updatedspance, $orderid)
    {
        $val_status = 0;
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status=', $val_status);
        return $this->db->update('tmp_tble_container', $updatedspance);
    }
    function updatetempContainerSpace($containerSpance, $orederid)
    {
        $status = 0;
        $this->db->where('OrderId', $orederid);
        $this->db->where('Status', $status);
        $this->db->update('tmp_tble_container', $containerSpance);
    }

    function updatetempcontainerpercentage($orderid, $contaienerstatus)
    {
        $status = 0;
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', $status);
        return $this->db->update('tmp_tble_container', $contaienerstatus);
    }
    function gettempcontainer($orderid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tmp_tble_container');
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', 0);
        $query = $this->db->get();
        return $query->row();
    }
    function addtempnewcontainer($val_newcontainer)
    {
        $this->db->insert('tmp_tble_container', $val_newcontainer);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function upgradetempcontainer($containerdata, $orderid)
    {
        $status = 0;
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', $status);
        $this->db->update('tmp_tble_container', $containerdata);
        $rows = $this->db->affected_rows();
        return $rows;
    }
    function getalltempcontainers($orderid)
    {
        $this->db->select('*');
        $this->db->from('tmp_tble_container');
        $this->db->where('OrderId', $orderid);
        $query = $this->db->get();
        return $query->result();
    }
    function updateoldcontainer($updatedspace, $containerid)
    {
        $this->db->where('ContainerId', $containerid);
        return $this->db->update('tble_container', $updatedspace);
    }
    function selectlastcontainer($orederid)
    {
        $status = 0;
        $this->db->select('*');
        $this->db->from('tble_container');
        $this->db->where('OrderId', $orederid);
        $this->db->where('Status', $status);
        $query = $this->db->get();
        return $query->row();
    }
    function getadminexhangerate()
    {
       $this->db->select('ExchangeRate');
       $this->db->from('tble_averagecartonsetup');
       $query = $this->db->get();
       return $query->row();
    }
    function getuserdetails($companyid)
    {
      $this->db->select('*');
      $this->db->from('tble_companysetup');
      $this->db->where('CompanyID',$companyid);
      $query = $this->db->get();
      return $query->result();
    }
    function checkemailitems($orderid)
    {
      $this->db->select('*');
      $this->db->from('tble_orderitems');
      $this->db->where('OrderId',$orderid);
      $query = $this->db->get();
      return $query->result();
    }
    function getemailcompanydetails($companyid)
    {
      $this->db->select('*');
      $this->db->from('tble_companysetup');
      $this->db->where('CompanyID',$companyid);
      $query = $this->db->get();
      return  $query->result();
    }
}
?>
