<?php
Class Admin_model extends CI_Model {
/* ################## ADMIN MOLDEL FUNCTIONS ########################*/
	/*login model api*/
	function login($username, $password) {
		$this->db->select('*');
		$this->db->from('tble_login');
		$this->db->where('UserName', $username);
		$this->db->where('Password', $password);
		$this->db->where('UserType', 'admin');
		$query = $this->db->get();
		return $query->row();
	}
	/*end*/
	function saveusertoken($usertoken) {
		$this->db->insert('tble_token', $usertoken);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function saveagentcontacts($contacts) {
		$this->db->insert('tble_goldagentrepresentative', $contacts);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function saveagentcompany($agent) {
		$this->db->insert('tble_goldagent', $agent);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function saveimage($base64img) {

		$filename_path = md5(time() . uniqid()) . ".jpg";
		$decoded = base64_decode($base64img);
		file_put_contents("assets/images/" . $filename_path, $decoded);
		return $filename_path;
	}
	function gettradorhalllist($parent) {
		$this->db->select('*');
		$this->db->from('tble_tradeandhall');
		$this->db->where('Parent=' . $parent . ' AND CompanyId=0');
		$query = $this->db->get();
		return $query->result();
	}
	function savetradeorhall($tradedetails) {
		$this->db->insert('tble_tradeandhall', $tradedetails);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function savecontainer($container) {
		$this->db->insert('tble_containertypes', $container);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function getallgoldagents($page) {
		$this->db->select('*');
		$this->db->from('tble_goldagent');
		$this->db->where('IsActive', 1);
		$this->db->limit(12, $page);
		$this->db->order_by("Id", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	function getgoldagentcontats($agentid) {
		$this->db->select('*');
		$this->db->from('tble_goldagentrepresentative');
		$this->db->where('GoldAgentId', $agentid);
		$query = $this->db->get();
		return $query->result();
	}
	function checktradeorhall($name, $parent) {
		$this->db->select('*');
		$this->db->from('tble_tradeandhall');
		$this->db->where('Name', $name);
		$this->db->where('Parent', $parent);
		$this->db->where('CompanyId', '0');
		$query = $this->db->get();
		return $query->result();
	}
	function changepassword($password, $username) {
		$this->db->where('UserName', $username);
		$result = $this->db->update('tble_login', $password);
		return $result;
	}

	function gethalllist() {
		$parent = 0;
		$this->db->select('*');
		$this->db->from('tble_tradeandhall');
		$this->db->where('Parent!=' . $parent . ' AND CompanyId=0');
		$query = $this->db->get();
		return $query->result();
	}
	function getconatinerist() {
		$parent = 0;
		$this->db->select('*');
		$this->db->from('tble_containertypes');
		$query = $this->db->get();
		return $query->result();
	}
	function checksavedcontainer($containerid) {
		$this->db->select('*');
		$this->db->from('tble_container');
		$this->db->where('ContainerType', $containerid);
		$query = $this->db->get();
		return $query->result();
	}
	function deletecontainer($containerid) {
		$this->db->where('ID', $containerid);
		$result = $this->db->delete('tble_containertypes');
		return $result;
	}
	function updatecontainer($containerid, $container) {
		$this->db->where('ID', $containerid);
		return $this->db->update('tble_containertypes', $container);

	}
	function checksavedtradeorhall($id) {
		$this->db->select('*');
		$this->db->from('tble_booth');
		$this->db->where('TradeId=' . $id . ' OR HallId=' . $id);
		$query = $this->db->get();
		return $query->result();
	}
	function deletetradeorhall($id) {
		$this->db->where('Id', $id);
		$result = $this->db->delete('tble_tradeandhall');
		return $result;
	}
	function updatetradeorhall($id, $tradeorhall) {
		$this->db->where('ID', $id);
		return $this->db->update('tble_tradeandhall', $tradeorhall);

	}
	function getaccountdetails($page) {
		$this->db->select('*');
		$this->db->from('tble_login');
		$this->db->join('tble_account', 'tble_login.Email = tble_account.EmailId', 'left');
		$this->db->where('tble_login.UserType', 'normal');
		$this->db->limit(12, $page);
		$query = $this->db->get();
		return $query->result();
	}
	function getdefaultdetails() {
		$this->db->select('*');
		$this->db->from('tble_averagecartonsetup');
		$query = $this->db->get();
		return $query->row();
	}
	function updatedefault($default) {
		return $this->db->update('tble_averagecartonsetup', $default);
	}
	function insertdefault($default) {
		return $this->db->insert('tble_averagecartonsetup', $default);
	}
	function deletegoldagent($id) {
		$this->db->where('Id', $id);
		$result = $this->db->delete('tble_goldagent');
		return $result;
	}
	function deletegoldagentcontact($id) {
		$this->db->where('GoldAgentId', $id);
		$result = $this->db->delete('tble_goldagentrepresentative');
		return $result;
	}
	function checkusedgoldagent($id) {
		$this->db->select('*');
		$this->db->from('tble_companysetup');
		$this->db->where('AgentId', $id);
		$query = $this->db->get();
		return $query->result();
	}
	function deleteaccount($email) {
		$this->db->where('EmailId', $email);
		$result = $this->db->delete('tble_account');
		return $result;
	}
	function deletelogin($email) {
		$this->db->where('Email', $email);
		$result = $this->db->delete('tble_login');
		return $result;
	}
	function deletecompany($email) {
		$this->db->where('OrderEmail', $email);
		$result = $this->db->delete('tble_companysetup');
		return $result;
	}
	function updateagentcompany($id, $goldagent) {
		$this->db->where('Id', $id);
		return $this->db->update('tble_goldagent', $goldagent);

	}
	function updateagentcontacts($id, $contact) {
		$this->db->where('Id', $id);
		return $this->db->update('tble_goldagentrepresentative', $contact);

	}
	function deletetoken($email) {
		$this->db->where('OrderEmail', $email);
		$result = $this->db->delete('tble_token');
		return $result;
	}
	function checkconatinerduplicate($containertype, $cbm, $weight) {
		$this->db->select('*');
		$this->db->from('tble_containertypes');
		$this->db->where('Container', $containertype);
		$this->db->where('MaximumVolume', $cbm);
		$this->db->where('MaximumWeight', $weight);
		$query = $this->db->get();
		return $query->result();
	}
	function updateconatinerduplicate($id, $containertype, $cbm, $weight) {
		$this->db->select('*');
		$this->db->from('tble_containertypes');
		$this->db->where('Container', $containertype);
		$this->db->where('MaximumVolume', $cbm);
		$this->db->where('MaximumWeight', $weight);
		$this->db->where('ID !=', $weight);
		$query = $this->db->get();
		return $query->result();
	}
	function deactivategoldagent($id) {
		$upatestatus = array('IsActive' => 0);
		$this->db->where('Id', $id);
		return $this->db->update('tble_goldagent', $upatestatus);
	}

}
