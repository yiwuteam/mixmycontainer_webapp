<?php

Class Api_model_test extends CI_Model

{



/* ################## LOGIN SETUP MOLDEL FUNCTIONS ########################*/

  /*login model api*/

	function login($username, $password)
	{
        $this->db->select('*');

        $this->db->from('tble_login');

        $this->db->where('Email',$username);

        $this->db->where('Password',$password);

        $query = $this->db->get();

		    return $query->result();

	}

  /*
  *Check if orderNo is exist or not
  */
  function checkorderno($orderno)
  {
      $this->db->select('*');
      $this->db->from('tble_orders');
      $this->db->where('OrderNumber', $orderno);
      $query = $this->db->get();
      return $query->result();
  }

	/*end*/



	/*updateuser token*/

	function updateUserToken($authotoken,$email)

	{

		  $this->db->where('OrderEmail',$email);

      return $this->db->update('tble_companysetup', $authotoken);

	}



	/*end*/


/* ################## account Operation -- Start ##################*/
/*
* fetch account details
*/
function getaccountdetails($val_orderemail)
{
  $this->db->select('*');
  $this->db->from('tble_account');
  $this->db->where('EmailId',$val_orderemail);
  $query = $this->db->get();
  return $query->result();
}

/*
* Update or Insert Account Details
* @type insert or Update (1 - insert ,2 -Update)
*/

function storeorupdateaccount($data,$type,$val_orderemail)
{
  if($type==1)
  {
   $this->db->insert('tble_account',$data);
  }
  else if($type==2)
  {
    $this->db->where('EmailId',$val_orderemail);
      $this->db->update('tble_account',$data);
  }
}
/* ################## Profile Operation -- End   ##################*/


/* ################## ORDER SETUP MOLDEL FUNCTIONS ########################*/



	/*order details*/

	function orderdetails($val_companyid)
	{
 // represents the order is complete or not, 0 for not complete 1 for complete
		$this->db->select('*');
		$this->db->from('tble_orders');
		$this->db->where('CompanyId',$val_companyid);
		$query = $this->db->get();
		return $query->result();
	}
	/*end*/

    /*
    *User is exist or not
    */
  function checkuserexists($useremail)
  {
  	$this->db->select('*');
  	$this->db->from('tble_login');
  	$this->db->where('Email',$useremail);
  	$query = $this->db->get();
  	return $query->result();
  }

  function saveotp($setotp,$useremail)
  {
  	  $this->db->where('Email',$useremail);
  	    $this->db->update('tble_login',$setotp);
         return '1';
  }
/* ################## COMPANY SETUP MOLDEL FUNCTIONS ########################*/

	/*fetch company id by passing token*/

	function fetchcompanyid($val_orderemail)

	{

		$this->db->select('*');

		$this->db->from('tble_companysetup');

		$this->db->where('OrderEmail',$val_orderemail);

		$query = $this->db->get();

		return $query->row();

	}

	/*end*/



	/*company details*/

	function companydetails($val_orderemail)

	{

		$this->db->select('*');

		$this->db->from('tble_companysetup');

		$this->db->where('OrderEmail',$val_orderemail);

		$query = $this->db->get();

		return $query->row();

	}

	/*end*/



	/*update company address*/

	function updatecompanyaddress($val_companyaddressdata,$val_companyid)

	{



    $this->db->where('CompanyID',$val_companyid);

    return $this->db->update('tble_companysetup', $val_companyaddressdata);



	}

	/*end*/



	/*update company contact*/

	function updatecompanycontact($val_companycontactdata,$val_compnayid)

	{

		  $this->db->where('CompanyID',$val_compnayid);

      return $this->db->update('tble_companysetup', $val_companycontactdata);

	}

	/*end*/

	/*update login email*/

	function updateloginemail($val_emaillogin,$val_orderemail)

	{

		  $this->db->where('Email',$val_orderemail);

      return $this->db->update('tble_login', $val_emaillogin);

	}

	/*end*/



	/*update company exchange rate*/

	function updatecompanyexchangerate($val_companyexchangerate,$val_compnayid)

	{

		  $this->db->where('CompanyID',$val_compnayid);

      return $this->db->update('tble_companysetup', $val_companyexchangerate);

	}

	/*end*/



	/*update company average carton setup*/

	function updatecompanyaveragecarton($val_companyaveragecartons,$val_compnayid)

	{

		  $this->db->where('CompanyID',$val_compnayid);

      return $this->db->update('tble_companysetup', $val_companyaveragecartons);

	}

	/*end*/



	/*update company transilation setup*/

	function updatecompanytrasilationsetup($val_companytransilationsetup,$val_compnayid)

	{

		  $this->db->where('CompanyID',$val_compnayid);

      return $this->db->update('tble_companysetup', $val_companytransilationsetup);

	}

	/*end*/



	/*get country codes*/

	function getcountrycodes()

	{

		$this->db->select('CountryName');

		$this->db->from('tble_countrynames');

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



	/*get currency names*/

	function getcurrencynames()

	{

		$this->db->select('CurrencyName	');

		$this->db->from('tble_currencyname');

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



	/*get company email id*/

	function getcompanyemailid($val_compnayid)

	{

		$this->db->select('OrderEmail');

		$this->db->from('tble_companysetup');

		$this->db->where('CompanyID',$val_compnayid);

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



	/*get current password from login table*/

		function getcurrentpassword($email)

		{

			$this->db->select('Password');

			$this->db->from('tble_login');

			$this->db->where('Email',$email);

			$query = $this->db->get();

			return $query->result();

		}

	/*end*/



	/*change password*/

	function changepassword($val_changepassword,$email)

	{

		  $this->db->where('Email',$email);

      return $this->db->update('tble_login',$val_changepassword);

	}

	/*end*/



	/* ################## ORDER ITEM MOLDEL FUNCTIONS ########################*/



	/*save container type into container table*/

	function savecontainertype($ordercontainerdetails)

	{

	   $this->db->insert('tble_container',$ordercontainerdetails);

	   $insert_id = $this->db->insert_id();

  	 return  $insert_id;

	}

	/*end*/



	/*save item image file path into item table*/

	function saveitemimagefile($val_itemimage,$val_compnayid)

	{

		//$this->db->insert('tble_orderitem','');

	}

	/*end*/

	/*save order new order*/

	function saveorder($orderdetails)

	{

		$this->db->insert('tble_orders',$orderdetails);

	  $insert_id = $this->db->insert_id();

  	return  $insert_id;

	}

	/*end*/



	/*save booth */

	function savebooth($boothdetails)

	{

		$this->db->insert('tble_booth',$boothdetails);

	  $insert_id = $this->db->insert_id();

  	return  $insert_id;

	}

	/*end*/

	/*fetch total booth count from order table*/

	function fetchboothcount($orderid)

	{

		$this->db->select('TotalBooth');

		$this->db->from('tble_orders');

		$this->db->where('OrderId',$orderid);

		$query = $this->db->get();

		return $query->result();

	}
    /*
    *Get Booth Details By Bootyh Id
    */
    function getboothByid($boothid)
    {
      $this->db->select('*');
      $this->db->from('tble_booth');
      $this->db->where('BoothNumber',$boothid);
      $query = $this->db->get();
      return $query->row();
    }
	/*end*/

	/*update total booth count*/

	function updateboothno($val_boothcount,$orderid)

	{

		  $this->db->where('OrderId',$orderid);

      return $this->db->update('tble_orders', $val_boothcount);

	}

	/*end*/

	/*save new item details*/

	function saveitemdetails($val_itemdetails)

	{

     $this->db->insert('tble_orderitems',$val_itemdetails);

	   $insert_id = $this->db->insert_id();

  	 return  $insert_id;

	}

	/*end*/

	/*select container current volume and weight filled*/

	function fetchcontainerspance($orderid)

	{

    $val_status = 0;

		$this->db->select('*');

		$this->db->from('tble_container');

		$this->db->where('OrderId',$orderid);

		$this->db->where('Status',$val_status);

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/

	/*fetch container capacity*/

	function selectcontainercapacity($containertype)
    {
        $this->db->select('*');
        $this->db->from('tble_containertypes');
        $this->db->where('ID',$containertype);
        $query = $this->db->get();
        return $query->row();
    }

	/*end*/

	/*update container available space*/

	function updatecontainerstatus($updatedspance,$orderid)

	{  $val_status = 0;

			$this->db->where('OrderId',$orderid);

			$this->db->where('Status=',$val_status);

	    return	$this->db->update('tble_container',$updatedspance);

	}

	/*end*/



	/*save carton details*/

	function savecartondetails($val_cartondetails,$val_itemid)

	{



			$this->db->where('ItemId',$val_itemid);

	    return	$this->db->update('tble_orderitems',$val_cartondetails);



	}

	/*end*/

	/*select order summery*/

	function fetchordersummery($orderid)

	{

		$this->db->select('OrderNumber,TotalOrderValue,TotalOrderCBM,TotalOrderWeight');

		$this->db->from('tble_orders');

		$this->db->where('OrderId',$orderid);

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



	/*select booth summery*/

	function fetchboothsummery($boothid,$orderid)

	{

		$this->db->select('ValuePurchase');

		$this->db->from('tble_booth');

		$this->db->where('BoothId',$boothid);

		$this->db->where('OrderId',$orderid);

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



	/*select container status*/

	function fetchcontainerstatus($orderid)

	{

		$this->db->select('*');

		$this->db->from('tble_container');

		$this->db->where('OrderId',$orderid);

		$query = $this->db->get();

		return $query->result();

	}

	/*end*/



  /*select container summery*/

function fetitemdetails($orderid,$itemid)

{

  $this->db->select('*');

  $this->db->from('tble_orderitems');

  $this->db->where('OrderId',$orderid);

  $this->db->where('ItemId',$itemid);

  $query = $this->db->get();

  return $query->result();

}

  /*end*/



	/*update end order status*/

	function endorderStatuschange($val_endorderstatus,$orderid,$val_compnayid)

	{

			$this->db->where('OrderId',$orderid);

			$this->db->where('CompanyId',$val_compnayid);

    	return	$this->db->update('tble_orders',$val_endorderstatus);

	}

	/*end*/



  /*

  * Get all container

  */

  function getallcontainer()

  {

    $this->db->select('*');

    $this->db->from('tble_containertypes');

    $query = $this->db->get();

    return $query->result();

  }

  /*add new container*/

  function addnewcontainer($val_newcontainer)

  {

    $this->db->insert('tble_container',$val_newcontainer);

    $insert_id = $this->db->insert_id();

    return  $insert_id;

  }

  /*end*/





  /*upgrade container*/

  function upgradecontainer($containerdata, $orderid, $containerid)
    {
        $status = 0;
        $this->db->where('ContainerId', $containerid);
        $this->db->where('OrderId', $orderid);
        $this->db->where('Status', $status);
        $this->db->update('tble_container', $containerdata);
    }

  /*end*/



  /*fetch order values*/

  function fetchtotalordervalues($orderid)

  {

    $this->db->select('*');

    $this->db->from('tble_orders');

    $this->db->where('OrderId',$orderid);

    $query = $this->db->get();

    return $query->result();

  }

  /*end*/



  /*

  * Get All Trades from Database

  */

  function gettradorhalllist($parent,$company)

  {

      $this->db->select('*');

      $this->db->from('tble_tradeandhall');

      $this->db->where('Parent='.$parent.' AND (CompanyId= '.$company.' OR CompanyId=0)' );

      $query = $this->db->get();

      return $query->result();

  }
  /*
  * Get All Trade details
  */
  function getalltradedeatils($company)

  {

      $this->db->select('*');

      $this->db->from('tble_tradeandhall');

      $this->db->where('(CompanyId= '.$company.' OR CompanyId=0)' );

      $query = $this->db->get();

      return $query->result();

  }
  /*
  * Insert trade and hall details
  */
  function inserttradeorhall($data){
    	$this->db->insert('tble_tradeandhall',$data);
      $insert_id = $this->db->insert_id();
      return  $insert_id;

  }


  /*

  * Get All Trades from Database

  */

  function getallgoldagent()

  {

      $this->db->select('*');

      $this->db->from('tble_goldagent');

      $query = $this->db->get();

      return $query->result();

  }

  function getgoldagentrepresentativebyagentid($agentId)

  {

      $this->db->select('*');

      $this->db->from('tble_goldagentrepresentative');

      $this->db->where('GoldAgentId',$agentId);

      $query = $this->db->get();

      return $query->result();

  }

  /*update oder values*/

  function updateordervalues($orderid,$val_totalordervalues)

  {

    $this->db->where('OrderId',$orderid);

    return $this->db->update('tble_orders', $val_totalordervalues);

  }

  /*

  *Get Default Carton Deatils

  */

  function deafultcartondeatils()

  {

    $this->db->select('*');

    $this->db->from('tble_averagecartonsetup');

    $query = $this->db->get();

    return $query->row();

  }

  /*end*/



  /*save item price details*/

   function saveitemprice($orderid,$itemid,$val_itemdetails)

   {

     $this->db->where('OrderId',$orderid);

     $this->db->where('ItemId',$itemid);

     return $this->db->update('tble_orderitems', $val_itemdetails);

   }

  /*end*/

  /*update total price value*/

  function updatetotalprice($orderid,$val_totalpricevalue)

  {

    $this->db->where('OrderId',$orderid);

    return $this->db->update('tble_orders', $val_totalpricevalue);

  }

  /*end*/

  /*add token*/

  function saveusertoken($usertoken)

  {

  	$this->db->insert('tble_token',$usertoken);

		$insert_id = $this->db->insert_id();

		return  $insert_id;



  }


  /*end*/

function selectordermail($token)

  {

  	 $this->db->select('*');

    $this->db->from('tble_token');

    $this->db->where('Token',$token);

    $query = $this->db->get();

    return $query->row();

  }

  /*
  * Get order by id
  */

  function getoredrbyid($val_orderid)
  {
    $this->db->select('*');
    $this->db->from('tble_orders');
    $this->db->where('OrderId',$val_orderid);
    $query = $this->db->get();

    return $query->result();
  }

  /*
  * gET ORDER SUMMERY
  */

  function selectordersummary($orderid)
{
     $status = 0;
     $this->db->select('tble_orders.OrderNumber,tble_orders.TotalOrderUsValue,tble_orders.TotalOrderRmbValue,tble_orders.TotalOrderCBM,tble_orders.TotalOrderWeight,tble_container.Volumepercentage,tble_container.Weightpercentage');
     $this->db->from('tble_orders');
     $this->db->join('tble_container', 'tble_orders.OrderId = tble_container.OrderId');
     $this->db->where('tble_orders.OrderId', $orderid);
     $this->db->where('tble_container.Status',$status);
     $query = $this->db->get();
     return $query->row();
}
function selectordersummarybooth($orderid)
{     $status = 0;
      $this->db->select('tble_booth.ValuePurchase,tble_booth.ValuePurchaseRMB');
     $this->db->from('tble_booth');
     $this->db->where('tble_booth.OrderId', $orderid);
     $this->db->where('tble_booth.BoothStatus',$status);
     $query = $this->db->get();
     return $query->row();
}

 /*order items*/

 function selectorderitems($val_orderid)



{

  $this->db->select('*');

  $this->db->from('tble_orderitems');

  $this->db->join('tble_container','tble_orderitems.OrderId = tble_container.OrderId');

  $this->db->join('tble_booth','tble_orderitems.OrderId = tble_booth.OrderId');

  $this->db->join('tble_orders','tble_orderitems.OrderId = tble_orders.OrderId');

  $this->db->where('tble_orderitems.OrderId',$val_orderid);



  $query = $this->db->get();

  return $query->result();

} /*end*/

/*login email*/

function selectloginemail($val_email)

{

	 $this->db->select('*');

    $this->db->from('tble_login');

    $this->db->where('Email',$val_email);

    $query = $this->db->get();

    return $query->result();

}

/*end*/

/*signup*/

function saveuseregister($userregisterdetails)

    {

        $this->db->insert('tble_login',$userregisterdetails);

         $insert_id = $this->db->insert_id();

  	 return  $insert_id;



    }

/*end*/

/*start company set up*/

function savecompanysetup($companysetup)

{

  $this->db->insert('tble_companysetup',$companysetup);

  $insert_id = $this->db->insert_id();

  	 return  $insert_id;

}

function updateloginpassword($updatepassword,$val_email)

{

	 $this->db->where('Email',$val_email);

      return $this->db->update('tble_login', $updatepassword);

}



/*end*/

function fetchordertotal($orderid)

{

	 $this->db->select('*');

    $this->db->from('tble_orders');

    $this->db->where('OrderId',$orderid);

    $query = $this->db->get();

    return $query->result();

}

function updateboothstatus($val_boothid,$val_orderid,$boothstatus)

{

	 $this->db->where('BoothId',$val_boothid);

	 $this->db->where('OrderId',$val_orderid);

      return $this->db->update('tble_companysetup', $authotoken);



}

function checkboothid($val_boothid,$val_orderid)

{

	$this->db->select('*');

	$this->db->from('tble_booth');

	$this->db->where('OrderId',$val_orderid);

	$this->db->where('BoothId',$val_boothid);

	$query = $this->db->get();

	return $query->result();

}

function fetchordersummay($val_orderid,$val_boothid)

{

  $status = 0;

  $this->db->select('OrderNumber,TotalOrderUsValue,TotalOrderRmbValue,ValuePurchase,ValuePurchaseRMB,TotalOrderCBM,TotalOrderWeight,Volumepercentage,Weightpercentage');

  $this->db->from('tble_orderitems');

  $this->db->join('tble_container','tble_orderitems.OrderId = tble_container.OrderId');

  $this->db->join('tble_booth','tble_orderitems.OrderId = tble_booth.OrderId');

  $this->db->join('tble_orders','tble_orderitems.OrderId = tble_orders.OrderId');

  $this->db->where('tble_orderitems.OrderId',$val_orderid);

  $this->db->where('tble_booth.BoothId',$val_boothid);

  $this->db->where('tble_container.Status',$status);

  $query = $this->db->get();

  return $query->row();

}

function selectlastboothdetails($orderid)

{

  $status = 0;

  $this->db->select('*');

  $this->db->from('tble_booth');

  $this->db->where('BoothStatus',$status);

  $this->db->where('OrderId',$orderid);

  $query = $this->db->get();

  return $query->row();

}

function selectorderdetails($orederid)

{

  $status = 0;

  $this->db->select('*');

  $this->db->from('tble_orders');

  $this->db->where('OrderId',$orederid);

  $this->db->where('OrderStatus',$status);

  $query = $this->db->get();

  return $query->row();

}

function selectlastcontainer($orederid)

{

  $status = 0;

  $this->db->select('*');

  $this->db->from('tble_container');

  $this->db->where('OrderId',$orederid);

  $this->db->where('Status',$status);

  $query = $this->db->get();

  return $query->row();

}
function updateContainerSpace($containerSpance, $orederid)
    {
        $status = 0;
        $this->db->where('OrderId', $orederid);
        $this->db->where('Status', $status);
        $this->db->update('tble_container', $containerSpance);
    }
function updatecontainerpercentage($orderid,$contaienerstatus)
{
    $status = 0;
    $this->db->where('OrderId',$orderid);
    $this->db->where('Status',$status);
      return $this->db->update('tble_container', $contaienerstatus);
}

}
