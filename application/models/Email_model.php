<?php 



class Email_model extends CI_Model{ 
  function __construct(){ 
   parent::__construct();
   $this->load->database();
   $this->load->library('pdf');
 } 



  function EmailMessage($fetchemailboothdetails,$OrderNumber,$Date,$TotalOrderUsValue,$TotalBooth,$TotalOrderRmbValue,$TotalOrderCBM,$TotalOrderWeight,$TotalContainer,$CompanyName,$CompanyPhone,$CompanyCountry,$CompanyMobile,$CompanyAddress,$CompanyEmailAddress,$CompanyZipcode,$TotalOrderThirdCurrencyValue,$CompanyAgent,$getemailcontainerdetails,$Companythirdcurrencysymbol)
 {
  

$message = '<html><body>';

  // Disaply Common Matters //
 $message =$message.'
<table width="970">
<tbody>
<tr>
<td colspan="2" width="219">
<p><img style="float: left;" src="https://mixmycontainer.com/QA/assets/images/logo.png" alt="" width="200" height="75" /></p>
</td>
<td width="75">
<p></p>
</td>
</tr>
<tr>
<td width="100">
<p><strong>Order No</strong> : '.$OrderNumber.'</p>
</td>
<td width="150">
<p><strong>No of Booths visited</strong> : '.$TotalBooth.'</p>
</td>
</tr>
<tr>
<td width="100">
<p><strong>Date</strong> : '.$Date.'</p>
</td>

<td width="400">
<p><strong>Total Value of order</strong> : USD : '.$TotalOrderUsValue.'/RMB: '.$TotalOrderRmbValue.'/'.$Companythirdcurrencysymbol .' '.$TotalOrderThirdCurrencyValue.'</p>
</td>
<td width="120" colspan="3">
<p></p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="100">
<p><strong>Agent</strong> : '.$CompanyAgent.'</p>
</td>
<td width="400">
<p><strong>Total CBM/ Weight of order</strong> : '.$TotalOrderCBM.' / '.$TotalOrderWeight.'</p>
</td>
<td width="74">
<p>&nbsp;</p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="2" width="200">
<p><strong>USER DETAILS</strong></p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tbody ng-repeat="User in Item.Order.user">
<tr>
<td width="135">
<p><strong>User</strong> : <span style="color:blue;"> <u>'.$CompanyEmailAddress.'</u></span></p>
</td>

<td width="100">
<p><strong>Tel</strong> : '.$CompanyPhone.'</p>
</td>
<td width="121">
<p></p>
</td>
<td width="74">
<p></p>
</td>
<td width="88">
<p>&nbsp;</p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="135">
<p><strong>Name</strong> : '.$CompanyName.'</p>
</td>
<td width="400" colspan="4">
<p><strong>Country</strong> : '.$CompanyCountry.'</p>
</td>
<td width="121">
<p></p>
</td>
<td width="71">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="100" colspan="2">
<p><strong>No of Containers Filled</strong>:</p>
</td>';
$index=1;
foreach ($getemailcontainerdetails as $containerdetails){  
$message=$message.'<td width="100">
<p>'.$containerdetails->Count.'X'.$containerdetails->Container.'</p>
</td>';$index++;
if($index%4==0){
  $message = $message.'</tr>
  <tr>';
}
// }
}
$message = $message.'</tr><tr>
<td  width="530" colspan="8"><hr/></td>
</tr>
</tbody>
</tbody>
</table>';
 // Ends Here
$i = 1;
  foreach ($fetchemailboothdetails as $booth){
    
   if(!empty($booth->item)){
 $message=$message.'<table  width="970">

<tbody>
<tr><td></td></tr>
<tr>
<td width="94">
<p><strong>Booth No</strong> : '.$booth->BoothNumber.'</p>
</td>
<td width="100">
<p><strong>Tel</strong> : '.$booth->Phone.'</p>
</td>

<td colspan="3" width="132">
<p><strong>No of items ordered (this Booth)</strong>:'.count($booth->item).'</p>
</td>

<td colspan="1" rowspan="4" width="200">
<p style="text-align:center;"><img src="https://mixmycontainer.com/QA/assets/images/'.$booth->BusinessCard.'" alt=""  /></p>
</td>
</tr>
<tr>
<td colspan="4" width="286">
<p><strong>Booth Name</strong> :'.$booth->BoothName.'</p>
</td>
</tr>
<tr>
<td colspan="2" width="200">
<p><strong>Trade District</strong> : '.$booth->Name.'</p>
</td>
<td colspan="6" width="348">
<p></p>
</td>
</tr>
<tr>
<td colspan="2" width="225">
<p><strong>Total Purchases for this Booth</strong> : USD: $'.$booth->ValuePurchase.'&nbsp; /RMB: '.$booth->ValuePurchaseRMB.'</p>
</td>
<td colspan="3" width="200">
<p></p>
</td>
<td colspan="3" width="132">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td colspan="2" width="139">
<p>&nbsp;</p>
</td>
<td colspan="3" width="132">
<p>&nbsp;</p>
</td>
<td colspan="3" rowspan="2" width="211">
<p>&nbsp;</p>
</td>
</tr>
<tr><td width="530" colspan="8"><hr style="border-color:#292929;"></td></tr>';
  }
  foreach ($booth->item as $item){
$message=$message.'<tr>
<td colspan="1" rowspan="4" width="200"><p style="text-align:center;"><img src="https://mixmycontainer.com/QA/assets/images/'. $item->Picture.'" alt="" /></p></td>
<td width="75"><strong>Item No</strong></td>
<td width="80">'. $item->ItemNo.'</td>
<td width="100"><strong>Total Carton Quantity</strong></td>
<td>'. $item->CartonQuantity.'</td>
</tr>
<tr>
<td><strong>Description</strong></td>
<td width="240" colspan="4">'.$item->DescriptionFrom.'/'.$item->DescriptionTo.'</td>
</tr>
<tr>
<td><strong>Quantity in case</strong></td>
<td width="80">'. $item->UnitperCarton.'</td>
<td><strong>Total  Quantity</strong></td>
<td>'.$item->UnitQuantity .'</td>
</tr>
<tr>
<td><strong>Total  Price</strong></td>
<td width="80">$'.$item->TotalPriceInUS.'/¥'.$item->TotalPriceInRMB.'/'.$Companythirdcurrencysymbol.' '.$item->TotalPriceIn3rdCurrency.'</td>
<td><strong>Unit  Price</strong></td>
<td width="80">$'. $item->PriceInUs.'/¥'.$item->PriceInRmb.'/'.$Companythirdcurrencysymbol.' '.$item->PriceInThirdCurrency.'</td>
</tr><tr><td></td></tr><tr><td width="530" colspan="8"><hr style="background-color:#FFFF00;"></td></tr>';
}
$message=$message.'<tr>
<td colspan="2" width="200">
</td>
<td colspan="2" width="140">
<p><strong>Total Price in Dollar</strong>:</p>
</td>
<td colspan="2" width="100">
<p><strong>Total Price in RMB</strong>:</p>
</td>
</tr>
<tr>
<td colspan="4" width="200">
<p><strong>Booth Totals</strong></p>
</td>
<td colspan="3" width="140">
<p>'.$booth->ValuePurchase.'</p>
</td>
<td colspan="4" width="66">
<p>'.$booth->ValuePurchaseRMB.'</p>
</td>
</tr>';$i++;
$message=$message.'</tbody></table>';
if ($i <= $TotalBooth){
 $message=$message.'<br pagebreak="true"/>';
}

}
//}
$message=$message.'</body></html>';

 $pdfdoc= $this->pdf->createpdf($message,$OrderNumber);
 return $pdfdoc;

//return $message;

 }



  }
  ?>
