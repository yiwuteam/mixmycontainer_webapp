<?php
Class Profile_model extends CI_Model
{

  function changepassword($userregisterdetails,$useremail)
  {
               $this->db->where('Email',$useremail);
    $result =  $this->db->update('tble_login', $userregisterdetails);
    return $result;
   }
   function updateprofile($userrprofiledetails,$usermail)
   {
                $this->db->where('EmailId',$usermail);
     $result =  $this->db->update('tble_account', $userrprofiledetails);
     return $result;
    }
    function insertprofile($userrprofiledetails,$usermail)
    {
                 $this->db->where('EmailId',$usermail);
      $result =  $this->db->insert('tble_account', $userrprofiledetails);
      return $result;
     }
    function getcontries()
    {
      $this->db->select('CountryName');
      $this->db->from('tble_countrynames');
      $query = $this->db->get();
      return $query->result();
     }
     function getcorrency(){
       $this->db->select('CurrencyName	');
       $this->db->from('tble_currencyname');
       $query = $this->db->get();
       return $query->result();
     }
     function checkcompanydetails($usermail){
       $this->db->select('*');
       $this->db->from('tble_companysetup');
       $this->db->where('OrderEmail',$usermail);
       $query = $this->db->get();
       return $query->result();
     }
     function addcompanydetails($companysetup){
      $result =  $this->db->insert('tble_companysetup', $companysetup);
      return $result;
     }
     function updatecompanydetails($companysetup,$usermail){
                 $this->db->where('OrderEmail',$usermail);
      $result =  $this->db->update('tble_companysetup', $companysetup);
      return $result;
     }

     function updateimage($image,$usermail){
                  $this->db->where('EmailId',$usermail);
       $result =  $this->db->update('tble_account', $image);
       return $result;
     }
     function checkAccountExist($usermail){
       $this->db->select('*');
       $this->db->from('tble_account');
       $this->db->where('EmailId',$usermail);
       $query = $this->db->get();
       return $query->result();
     }
     function insertimage($image,$usermail){
                  $this->db->where('EmailId',$usermail);
       $result =  $this->db->insert('tble_account', $image);
       return $result;
     }
     function getaccount($usermail){
       $this->db->select('*');
       $this->db->from('tble_account');
       $this->db->where('EmailId',$usermail);
       $query = $this->db->get();
       return $query->result();
     }
     function getCompanyDetails($usermail)
     {
       $this->db->select('*');
       $this->db->from('tble_companysetup');
       $this->db->where('OrderEmail',$usermail);
       $query = $this->db->get();
       return $query->result();
     }
     function checkoldpassword($useremail,$oldpassword)
     {
       $this->db->select('*');
       $this->db->from('tble_login');
       $this->db->where('Email',$useremail);
        $this->db->where('Password',$oldpassword);
       $query = $this->db->get();
       return $query->result();
     }

     function getCartonvalues(){
       $this->db->select('*');
       $this->db->from('tble_averagecartonsetup');
       $query = $this->db->get();
       return $query->row();
     }
     function getallgoldagents(){
       $status=1;
       $this->db->select('*');
       $this->db->from('tble_goldagent');
       $this->db->where('IsActive',$status);
       $query = $this->db->get();
       return $query->result();
     }
     function getgoldagentcontats($agentid){
       $this->db->select('*');
       $this->db->from('tble_goldagentrepresentative');
       $this->db->where('GoldAgentId',$agentid);
       $query = $this->db->get();
       return $query->result();
     }



}
